//
//  Communication.m
//  nur10
//
//  Created by Wony Shin on 10/19/12.
//  Copyright 2013 nur10. All rights reserved.
//

#import "Communication.h"
#import "const.h"
#import "../JSON/JSON.h"

const NSString *multipartBoundary = @"-------------111";

///
// Implementation for Communication class
///
@implementation Communication

@synthesize m_strCurrentEncodedPassword;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void) dealloc {
    [super dealloc];
    if (m_strCurrentEncodedPassword != nil) {
        [m_strCurrentEncodedPassword release];
    }
}

- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
}

/// parameter - respData is reatined in this function
- (BOOL)sendRequest:(NSString *)requestURL respData:(NSString **)respString
{
    NSLog(@"Request URL: %@", requestURL);
    
    NSString *encodedUrl = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    // message type
    [request setHTTPMethod:@"GET"];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!responseData) {
        // "Connection Error", "Failed to Connect to the Internet"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
    NSInteger nResponseCode = [(NSHTTPURLResponse*)response statusCode];
    
    if (nResponseCode != 200 ) {
        NSLog(@"response = %d", nResponseCode);
        return NO;
    }
    
    NSString *respStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"RECEIVED DATA : %@", respStr);
    
    *respString = respStr;
    return YES;
}


/// parameter - respData is reatined in this function
- (BOOL)sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params respData:(NSDictionary **)respDic
{
//    NSString *encodedUrl = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Request URL: %@", requestURL);
    NSLog(@"PARAMS: %@", params);

    NSURL *url = [NSURL URLWithString: requestURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    // message type
    if (methodType == MethodTypePost) {
        [request setHTTPMethod:@"POST"];
    } else if (methodType == MethodTypeGet) {
        [request setHTTPMethod:@"GET"];
    } else if (methodType == MethodTypePut) {
        [request setHTTPMethod:@"PUT"];
    } else {
        [request setHTTPMethod:@"DELETE"];
    }
    
    // message body
    if (params) {
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!responseData) {
        // "Connection Error", "Failed to Connect to the Internet"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
    NSInteger nResponseCode = [(NSHTTPURLResponse*)response statusCode];
    
    if (nResponseCode != 200 && nResponseCode != 201) {
        NSLog(@"response = %d", nResponseCode);
        
        NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        if (respString && ![respString isEqualToString: @""]) {
            SBJSON *parser = [[[SBJSON alloc] init] autorelease];
            NSDictionary *dic = (NSDictionary *)[parser objectWithString: respString error:nil];
            [dic retain];
            *respDic = dic;
//            if ([dic objectForKey: @"message"]) {
//                NSString* strMessage = [dic objectForKey: @"message"];
//                NSNotification* noti = [[NSNotification alloc] init];
//                [noti setValue: strMessage forKey: @"message"];
//                [[NSNotificationCenter defaultCenter] postNotification: noti];
//            }
        }
        
        return NO;
    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
//    NSString* strValue1 = [respString stringByReplacingOccurrencesOfString: @":true" withString: @":\"true\""];
//    NSString* strValue2 = [strValue1 stringByReplacingOccurrencesOfString: @":false" withString: @":\"false\""];
    NSLog(@"RECEIVED DATA : %@", respString);
    
//    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
//    NSDictionary *dic = (NSDictionary *)[parser objectWithString:strValue2 error:nil];
    NSDictionary* dic = [respString JSONValue];
    [dic retain];
    *respDic = dic;
    return YES;
}

- (BOOL)sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params header: (NSString*) strHeader respData:(NSDictionary **)respDic
{
    NSString *encodedUrl = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Request URL: %@", requestURL);
    NSLog(@"PARAMS: %@", params);
    
    NSURL *url = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    // message type
    if (methodType == MethodTypePost) {
        [request setHTTPMethod:@"POST"];
    } else if (methodType == MethodTypeGet) {
        [request setHTTPMethod:@"GET"];
    } else if (methodType == MethodTypePut) {
        [request setHTTPMethod:@"PUT"];
    } else {
        [request setHTTPMethod:@"DELETE"];
    }
    
    // message body
    if (params) {
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if (strHeader && ![strHeader isEqualToString: @""]) {
        [request addValue: strHeader forHTTPHeaderField: REQUEST_API_KEY];
    }
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!responseData) {
        // "Connection Error", "Failed to Connect to the Internet"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
    NSInteger nResponseCode = [(NSHTTPURLResponse*)response statusCode];
    
    if (nResponseCode != 200 && nResponseCode != 201) {
        NSLog(@"response = %d", nResponseCode);
        
        NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        if (respString && ![respString isEqualToString: @""]) {
            SBJSON *parser = [[[SBJSON alloc] init] autorelease];
            NSDictionary *dic = (NSDictionary *)[parser objectWithString: respString error:nil];
            [dic retain];
            *respDic = dic;
            //            if ([dic objectForKey: @"message"]) {
            //                NSString* strMessage = [dic objectForKey: @"message"];
            //                NSNotification* noti = [[NSNotification alloc] init];
            //                [noti setValue: strMessage forKey: @"message"];
            //                [[NSNotificationCenter defaultCenter] postNotification: noti];
            //            }
        }
        
        return NO;
    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    //    NSString* strValue1 = [respString stringByReplacingOccurrencesOfString: @":true" withString: @":\"true\""];
    //    NSString* strValue2 = [strValue1 stringByReplacingOccurrencesOfString: @":false" withString: @":\"false\""];
    NSLog(@"RECEIVED DATA : %@", respString);
    
    //    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
    //    NSDictionary *dic = (NSDictionary *)[parser objectWithString:strValue2 error:nil];
    NSDictionary* dic = [respString JSONValue];
    [dic retain];
    *respDic = dic;
    return YES;
}

- (BOOL)sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params respArray:(NSArray **)respArray
{
    NSString *encodedUrl = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Request URL: %@", requestURL);
    NSLog(@"PARAMS: %@", params);
    
    NSURL *url = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    // message type
    if (methodType == MethodTypePost) {
        [request setHTTPMethod:@"POST"];
    } else if (methodType == MethodTypeGet) {
        [request setHTTPMethod:@"GET"];
    } else if (methodType == MethodTypePut) {
        [request setHTTPMethod:@"PUT"];
    } else {
        [request setHTTPMethod:@"DELETE"];
    }
    
    // message body
    if (params) {
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!responseData) {
        // "Connection Error", "Failed to Connect to the Internet"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
//    NSInteger nResponseCode = [(NSHTTPURLResponse*)response statusCode];
//    
//    if (nResponseCode != 200 && nResponseCode != 201) {
//        NSLog(@"response = %d", nResponseCode);
//        
//        NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
//        if (respString && ![respString isEqualToString: @""]) {
//            SBJSON *parser = [[[SBJSON alloc] init] autorelease];
//            NSDictionary *dic = (NSDictionary *)[parser objectWithString: respString error:nil];
//            [dic retain];
//            *respDic = dic;
//            //            if ([dic objectForKey: @"message"]) {
//            //                NSString* strMessage = [dic objectForKey: @"message"];
//            //                NSNotification* noti = [[NSNotification alloc] init];
//            //                [noti setValue: strMessage forKey: @"message"];
//            //                [[NSNotificationCenter defaultCenter] postNotification: noti];
//            //            }
//        }
//        
//        return NO;
//    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"RECEIVED DATA : %@", respString);
    
    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
    NSArray *array = (NSArray *)[parser objectWithString: respString error:nil];
    [array retain];
    *respArray = array;
    return YES;
}

- (NSInteger) sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params X_WSSE: (NSString *)strXSSE respData:(NSDictionary **)respDic
{
    NSLog(@"Request URL: %@", requestURL);
    NSLog(@"PARAMS: %@", params);
    
    NSString *encodedUrl = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:encodedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    // message type
    if (methodType == MethodTypePost) {
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];    //

    } else if (methodType == MethodTypeGet) {
        [request setHTTPMethod:@"GET"];

    } else if (methodType == MethodTypePut) {
        [request setHTTPMethod:@"PUT"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];    //

    } else {
        [request setHTTPMethod:@"DELETE"];
    }

    [request setValue:strXSSE forHTTPHeaderField:@"x-wsse"];
    
    NSLog(@"headers = %@", [request allHTTPHeaderFields]);
    
    // message body
    if (params) {
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!responseData) {
        // "Connection Error", "Failed to Connect to the Internet"
        NSLog(@"%@", [error description]);
        return 0;
    }
    
    NSInteger nResponseCode = [(NSHTTPURLResponse*)response statusCode];

    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"RECEIVED DATA : %@", respString);

    if (nResponseCode != 200 && nResponseCode != 201) {
        NSLog(@"response = %d", nResponseCode);

        NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        if (respString && ![respString isEqualToString: @""]) {
            NSString* strTemp = [[respString stringByReplacingOccurrencesOfString: @":true" withString: @":\"true\""] stringByReplacingOccurrencesOfString:@":false" withString: @":\"false\""];
            SBJSON *parser = [[[SBJSON alloc] init] autorelease];
            NSDictionary *dic = (NSDictionary *)[parser objectWithString: strTemp error:nil];
            [dic retain];
            *respDic = dic;
            //            if ([dic objectForKey: @"message"]) {
            //                NSString* strMessage = [dic objectForKey: @"message"];
            //                NSNotification* noti = [[NSNotification alloc] init];
            //                [noti setValue: strMessage forKey: @"message"];
            //                [[NSNotificationCenter defaultCenter] postNotification: noti];
            //            }
        }

        return nResponseCode;
    }
    
    NSString* strTemp = [[respString stringByReplacingOccurrencesOfString: @":true" withString: @":\"true\""] stringByReplacingOccurrencesOfString:@":false" withString: @":\"false\""];
    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
    NSDictionary *dic = (NSDictionary *)[parser objectWithString:strTemp error:nil];
    [dic retain];
    *respDic = dic;
    return nResponseCode;
}


///////////////////////////////////////////////
///////// File Upload
///////////////////////////////////////////////


- (BOOL)sendMultipartRequest:(NSString *)strService data:(NSData *)data respData:(NSDictionary **)respDic
{
    NSMutableString *strURL = [NSMutableString stringWithString:strService];
    
    NSLog(@"URL: %@", strURL);
   // NSLog(@"PARAMS: %@", data);
    
    NSURL *url = [NSURL URLWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", multipartBoundary];
	[request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!response) {
        // "Connection Error", "Failed to Connect to the URL"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"RECEIVED DATA : %@", respString);
    
    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
    NSDictionary *dic = (NSDictionary *)[parser objectWithString:respString error:nil];
    if (dic == nil) 
        return NO;  // invalid JSON format
    
    [dic retain];
    *respDic = dic;
    return YES;
}

- (BOOL)sendMultipartRequest:(NSString *)strService data:(NSData *)data header: (NSString*) strHeader respData:(NSDictionary **)respDic
{
    NSMutableString *strURL = [NSMutableString stringWithString:strService];
    
    NSLog(@"URL: %@", strURL);
    // NSLog(@"PARAMS: %@", data);
    
    NSURL *url = [NSURL URLWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", multipartBoundary];
	[request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    
    if (strHeader && ![strHeader isEqualToString: @""]) {
        [request addValue: strHeader forHTTPHeaderField: REQUEST_API_KEY];
    }

    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!response) {
        // "Connection Error", "Failed to Connect to the URL"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
    NSInteger nResponseCode = [(NSHTTPURLResponse*)response statusCode];
    
    if (nResponseCode != 200 && nResponseCode != 201) {
        NSLog(@"response = %d", nResponseCode);
        
        NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        if (respString && ![respString isEqualToString: @""]) {
            SBJSON *parser = [[[SBJSON alloc] init] autorelease];
            NSDictionary *dic = (NSDictionary *)[parser objectWithString: respString error:nil];
            [dic retain];
            *respDic = dic;
            //            if ([dic objectForKey: @"message"]) {
            //                NSString* strMessage = [dic objectForKey: @"message"];
            //                NSNotification* noti = [[NSNotification alloc] init];
            //                [noti setValue: strMessage forKey: @"message"];
            //                [[NSNotificationCenter defaultCenter] postNotification: noti];
            //            }
        }
        
        return YES;
    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"RECEIVED DATA : %@", respString);
    
    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
    NSDictionary *dic = (NSDictionary *)[parser objectWithString:respString error:nil];
    if (dic == nil)
        return NO;  // invalid JSON format
    
    [dic retain];
    *respDic = dic;
    return YES;
}

+ (BOOL)uploadFile:(NSString *)strURL filename:(NSString *)filename fileData:(NSString *)fileData respData:(NSDictionary **)respDic {
    
    NSLog(@"URL: %@", strURL);
    NSLog(@"FileName: %@", filename);
    
    NSURL *url = [NSURL URLWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", multipartBoundary];
	[request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", multipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"orderfile\"; filename=\"%@\"\r\n", filename]] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[@"Content-type: application/octet-stream\r\n\r\n" dataUsingEncoding: NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"%@", fileData]] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", multipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[request setHTTPBody: body];
    
    NSError *error;
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!response) {
        // "Connection Error", "Failed to Connect to the URL"
        NSLog(@"%@", [error description]);
        return NO;
    }
    
    NSString *respString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"RECEIVED DATA : %@", respString);
    
    SBJSON *parser = [[[SBJSON alloc] init] autorelease];
    NSDictionary *dic = (NSDictionary *)[parser objectWithString:respString error:nil];
    if (dic == nil) 
        return NO;  // invalid JSON format
    
    [dic retain];
    *respDic = dic;
    return YES;
}


- (NSData *)makeMultipartBody:(NSDictionary*)dic {
	
	NSMutableData *data = [NSMutableData data];
	
    for (NSString *key in dic) {
        NSString *value = [dic objectForKey:key];
        // set boundary
        [data appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", multipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [data appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, value] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [data appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", multipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *logString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
   // NSLog(@"%@", logString);
    [logString release];
    
    return data;
}

- (void)appendFileToBody:(NSMutableData *)data filenamekey:(NSString*)filenamekey filenamevalue:(NSString*)filenamevalue filedata:(NSData*)filedata {
	[data appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", multipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[data appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", filenamekey, filenamevalue]] dataUsingEncoding:NSUTF8StringEncoding]];
	[data appendData:[@"Content-type: application/octet-stream\r\n\r\n" dataUsingEncoding: NSUTF8StringEncoding]];
	[data appendData:filedata];
	[data appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", multipartBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
}


@end

