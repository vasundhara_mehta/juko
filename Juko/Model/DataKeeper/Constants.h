//
//  Constants.h

#define CENTER_PAGE_MY_PROFILE          0
#define CENTER_PAGE_MY_NOTES            1
#define CENTER_PAGE_TAKE_NOTES          2
#define CENTER_PAGE_FRIENDS             3
#define CENTER_PAGE_FIND_FRIENDS        4
#define CENTER_PAGE_MESSAGES            5
#define CENTER_PAGE_GROUPS              6
#define CENTER_PAGE_ACCOUNT             7
#define CENTER_PAGE_TERM_PRIVACY        8
#define CENTER_PAGE_SUBJECT             9
#define CENTER_PAGE_FOLDER              10
#define CENTER_PAGE_TAKE_NOTE_TEXT      11
#define CENTER_PAGE_TAKE_NOTE_IMAGE     12
#define CENTER_PAGE_TAKE_NOTE_PDF       13
#define CENTER_PAGE_FRIEND_PROFILE      14
#define CENTER_PAGE_FAVORITE_NOTES      15
#define CENTER_PAGE_TAGGED_NOTES        16
#define CENTER_PAGE_ALL_NOTES           17
#define CENTER_PAGE_CHATTING            18

#define POST_SHARE_FRIEND_ONLY      @"friend"
#define POST_SHARE_PUBLIC           @"public"
#define POST_SHARE_PRIVATE          @"private"

/*#define WEBAPI_SIGNUP                       @"http://192.168.0.177/chalk/index.php?/mobileservice/users/signup"
#define WEBAPI_LOGIN                        @"http://192.168.0.177/chalk/index.php?/mobileservice/users/signin"
#define WEBAPI_LOGOUT                       @"http://192.168.0.177/chalk/index.php?/mobileservice/users/logout"
#define WEBAPI_EDIT_ACCOUNT                 @"http://192.168.0.177/chalk/index.php?/mobileservice/users/editprofile"
#define WEBAPI_CHANGE_PASSWORD              @"http://192.168.0.177/chalk/index.php?/mobileservice/users/chagne_password"

#define WEBAPI_WRITE_POST                   @"http://192.168.0.177/chalk/index.php?/mobileservice/posts/write_post"
#define WEBAPI_GET_POSTS                    @"http://192.168.0.177/chalk/index.php?/mobileservice/posts/get_posts"

#define WEBAPI_CREATE_SUBJECT               @"http://192.168.0.177/chalk/index.php?/mobileservice/subjects/create"
#define WEBAPI_GET_MY_SUBJECTS              @"http://192.168.0.177/chalk/index.php?/mobileservice/subjects/get_my_subjects"
#define WEBAPI_CREATE_FOLDER                @"http://192.168.0.177/chalk/index.php?/mobileservice/folders/create"
#define WEBAPI_GET_FOLDERS                  @"http://192.168.0.177/chalk/index.php?/mobileservice/folders/get_folders"*/

#define WEBAPI_SIGNUP                       @"http://chalkapp.co/index.php?/mobileservice/users/signup"
#define WEBAPI_LOGIN                        @"http://chalkapp.co/index.php?/mobileservice/users/signin"
#define WEBAPI_LOGOUT                       @"http://chalkapp.co/index.php?/mobileservice/users/logout"
#define WEBAPI_EDIT_ACCOUNT                 @"http://chalkapp.co/index.php?/mobileservice/users/editprofile"
#define WEBAPI_CHANGE_PASSWORD              @"http://chalkapp.co/index.php?/mobileservice/users/chagne_password"

#define WEBAPI_WRITE_POST                   @"http://chalkapp.co/index.php?/mobileservice/posts/write_post"
#define WEBAPI_GET_POSTS                    @"http://chalkapp.co/index.php?/mobileservice/posts/get_posts"
#define WEBAPI_EDIT_POST                    @"http://chalkapp.co/index.php?/mobileservice/posts/edit_post"
#define WEBAPI_GET_A_POST                   @"http://chalkapp.co/index.php?/mobileservice/posts/view_post"             

#define WEBAPI_WRITE_POST_COMMENT           @"http://chalkapp.co/index.php?/mobileservice/posts/write_comment"
#define WEBAPI_GET_POST_COMMENTS            @"http://chalkapp.co/index.php?/mobileservice/posts/get_comments"

#define WEBAPI_CREATE_SUBJECT               @"http://chalkapp.co/index.php?/mobileservice/subjects/create"
#define WEBAPI_GET_MY_SUBJECTS              @"http://chalkapp.co/index.php?/mobileservice/subjects/get_my_subjects"
#define WEBAPI_CREATE_FOLDER                @"http://chalkapp.co/index.php?/mobileservice/folders/create"
#define WEBAPI_GET_FOLDERS                  @"http://chalkapp.co/index.php?/mobileservice/folders/get_folders"
#define WEBAPI_TAKE_NOTE                    @"http://chalkapp.co/index.php?/mobileservice/notes/take_note"
#define WEBAPI_NOTE_UPLOAD_FILE             @"http://chalkapp.co/index.php?/mobileservice/notes/upload_file"
#define WEBAPI_GET_NOTES                    @"http://chalkapp.co/index.php?/mobileservice/notes/get_notes"
#define WEBAPI_GET_FAVORITE_NOTES           @"http://chalkapp.co/index.php?/mobileservice/notes/get_favourites"
#define WEBAPI_GET_NOTES_BY_TAG             @"http://chalkapp.co/index.php?/mobileservice/tags/get_notes"
#define WEBAPI_SEARCH_TAG                   @"http://chalkapp.co/index.php?/mobileservice/tags/search_tags"
#define WEBAPI_VIEW_NOTE                    @"http://chalkapp.co/index.php?/mobileservice/notes/view_note"
