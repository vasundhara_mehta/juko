//
//  DataKeeper.m
    //  OpsightLite
//
//  Created by MacBook on 10/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataKeeper.h"
#import "const.h"
#import "CUtils.h"

#import "IGUser.h"
#import "IGItem.h"
#import "IGComment.h"
#import "IGItemUser.h"
#import "IGProfileInfo.h"

#import "IGCategory.h"
#import "IGTemplate.h"
#import "IGProduct.h"

@implementation DataKeeper

- (id)init
{
    self = [super init];
    if (self) {
        
        ////////////////////////////////////////////
        //// Initialize
        ////////////////////////////////////////////
        communication = [[Communication alloc] init];
        
        _m_arrayNewsFeed = [[NSMutableArray alloc] init];
        _m_arrayFeatured = [[NSMutableArray alloc] init];
        
        m_Categories = [[NSMutableArray alloc] init];
        m_Templates = [[NSMutableArray alloc] init];
        
        m_strStripePublishKey = @"";
        
//#ifdef TEST_MODE
        [self loadCategoryData];
//#endif
        
        [self loadStates];
    }
    
    return self;
}

- (void) loadInit
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@app/init", SERVER_URL];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: @"" respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            m_strStripePublishKey = [CUtils getStringValueFromDictionary: [dictResult objectForKey: @"stripe"] KEY:@"publishable_key"];
        }
    }
}

- (NSString*) getStripeKey
{
    return m_strStripePublishKey;
}

- (void) loadStates
{
    m_dictStates = [[NSMutableArray alloc] init];
    
    NSString * fName = [[NSBundle mainBundle] pathForResource: @"States" ofType:@"txt"];
    NSString* strStates = [NSString stringWithContentsOfFile: fName encoding: NSASCIIStringEncoding error:nil];
    
    NSArray* arrayStates = [strStates componentsSeparatedByString: @",\n"];
    for (NSString* strState in arrayStates) {
        NSArray* array = [strState componentsSeparatedByString: @": "];
        NSString* strKey = [array[0] stringByReplacingOccurrencesOfString: @"'" withString: @""];
        NSString* strFullStateName = [array[1] stringByReplacingOccurrencesOfString: @"'" withString: @""];
        NSDictionary* dictState = [NSDictionary dictionaryWithObjectsAndKeys: strFullStateName, strKey, nil];
        
        [m_dictStates addObject: dictState];
    }
}

- (NSArray*) getStates
{
    return m_dictStates;
}

- (int) getStateIdByKey: (NSString*) strKey
{
    for (int nIdx = 0; nIdx < m_dictStates.count; nIdx ++) {
        NSDictionary* dict = [m_dictStates objectAtIndex: nIdx];
        NSString* strDictKey = dict.allKeys[0];
        if (strKey && strDictKey && [strDictKey isEqualToString: strKey]) {
            return nIdx;
        }
    }
    return 0;
}

- (void) dealloc
{
    [super dealloc];
    
    [m_dictStates removeAllObjects];
    [m_dictStates release];
    
    [_m_arrayNewsFeed removeAllObjects];
    [_m_arrayNewsFeed release];

    [_m_arrayFeatured removeAllObjects];
    [_m_arrayFeatured release];
    
    [m_Categories removeAllObjects];
    [m_Categories release];

    [m_Templates removeAllObjects];
    [m_Templates release];
}

- (void) saveDeviceToken: (NSString*) tokenString
{
    m_strDeviceToken = [tokenString copy];

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: m_strDeviceToken forKey: @"device_token"];
    [defaults synchronize];
}

#pragma mark - Get Product List of a Category

- (NSArray*) getProductList: (int) nIndex
{
    IGCategory* clsCategory = m_Categories[nIndex];
    
    return clsCategory.m_ProductList;
}


#pragma mark - load blank item data

- (void) sendInfoToServer
{
    [self registerTemplates];
    
    //register category & product info to server
    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, @"category"];
    
    for (IGCategory* clsCategory in m_Categories) {
        NSMutableData* data = [NSMutableData data];
        NSMutableDictionary* dictParam = [NSMutableDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsCategory.m_nIndex], @"id", clsCategory.m_strName, @"name", nil];
        
        UIImage* image = [UIImage imageNamed: clsCategory.m_strImageName];
        [communication appendFileToBody: data filenamekey: @"image" filenamevalue: @"photo.jpg" filedata: UIImageJPEGRepresentation(image, 0.7)];
        [data appendData: [communication makeMultipartBody: dictParam]];

        NSDictionary* dictResult;
        BOOL FSuccess = [communication sendMultipartRequest: strApiUrl data: data respData: &dictResult];
        
        //add product
        if (FSuccess) {
            NSString* strAPI = [NSString stringWithFormat: @"%@category/%d/product", SERVER_URL, clsCategory.m_nIndex];
            for (IGProduct* clsProduct in clsCategory.m_ProductList) {
                NSMutableData* dataProduct = [NSMutableData data];
                NSMutableDictionary* dictParam = [NSMutableDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsProduct.m_nIndex], @"id", clsProduct.m_strName, @"name", clsProduct.m_strEditImage, @"edit_name", [NSString stringWithFormat: @"%d", clsProduct.m_nWidth], @"width", [NSString stringWithFormat: @"%d", clsProduct.m_nHeight], @"height", [NSString stringWithFormat: @"%.2f", clsProduct.m_rPrice], @"price", [self getTemlateListString: clsProduct.m_templates], @"templates", nil];
                
                UIImage* image = [UIImage imageNamed: clsProduct.m_strExampleImage];
                [communication appendFileToBody: dataProduct filenamekey: @"image" filenamevalue: @"photo.png" filedata: UIImageJPEGRepresentation(image, 0.7)];
                [dataProduct appendData: [communication makeMultipartBody: dictParam]];

                BOOL FSuccess = [communication sendMultipartRequest: strAPI data: dataProduct respData: &dictResult];
                if (!FSuccess) {
                    NSLog(@"Fail to add category %d , product(%@)", clsCategory.m_nIndex, clsProduct.m_strName);
                }
            }
        } else {
            NSLog(@"Fail to add category %d", clsCategory.m_nIndex);
        }
    }
}

- (NSString*) getTemlateListString: (NSArray*) templates
{
    int nIndex = [templates[0] intValue];
    int nTemplateDBId = 0;
    if (nIndex == 0) {
        nTemplateDBId = 1;
    } else {
        nTemplateDBId = (nIndex / abs(nIndex)) * (abs(nIndex) + 1);
    }
    
    NSMutableString* str = [NSMutableString stringWithFormat: @"%d", nTemplateDBId];
    for (int i = 1; i < templates.count; i ++) {
        nIndex = [templates[i] intValue];
        if (nIndex == 0) {
            nTemplateDBId = 1;
        } else {
            nTemplateDBId = (nIndex / abs(nIndex)) * (abs(nIndex) + 1);
        }
        
        [str appendString: [NSString stringWithFormat: @",%d", nTemplateDBId]];
    }
    return str;
}

- (void) registerTemplates
{
    //register category & product info to server
    BOOL FSuccess = NO;
    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, @"template"];
    NSDictionary* dictResult;
    
    for (IGTemplate* clsTemplate in m_Templates) {
        NSString* strParam = [NSString stringWithFormat: @"id=%d&cols=%d&rows=%d", clsTemplate.m_nIndex, clsTemplate.m_nCols, clsTemplate.m_nRows];
        FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam respData: &dictResult];
        if (!FSuccess) {
            NSLog(@"Fail to add Template %d", clsTemplate.m_nIndex);
            continue;
        }
        
        NSString* strAPI = [NSString stringWithFormat: @"%@template/%d/area", SERVER_URL, clsTemplate.m_nIndex];
        for (NSString* strRect in clsTemplate.m_Rects) {
            NSArray* arrayNums = [strRect componentsSeparatedByString: @","];
            NSString* strAreaParam = [NSString stringWithFormat: @"top=%@&left=%@&width=%@&height=%@", arrayNums[1], arrayNums[0], arrayNums[2], arrayNums[3]];
            FSuccess = [communication sendRequest: MethodTypePost requestURL: strAPI params: strAreaParam respData: &dictResult];
            if (!FSuccess) {
                NSLog(@"Fail to add Template[%d] Area %@", clsTemplate.m_nIndex, strRect);
                continue;
            }
        }
    }
}

- (void) loadCategoryData
{
    [m_Templates removeAllObjects];
    
    NSString* strPath = [[NSBundle mainBundle] pathForResource: @"template_info" ofType: @"plist"];
    NSArray* array = [NSArray arrayWithContentsOfFile: strPath];
    
    for (NSDictionary* dictTemplate in array) {
        IGTemplate* clsTemplate = [[IGTemplate alloc] initWithDictionary: dictTemplate];
        [m_Templates addObject: clsTemplate];
    }
    
    [m_Categories removeAllObjects];
    
    strPath = [[NSBundle mainBundle] pathForResource: @"category_info" ofType: @"plist"];
    array = [NSArray arrayWithContentsOfFile: strPath];
    
    for (NSDictionary* dictItem in array) {
        IGCategory* clsCategory = [[IGCategory alloc] initWithDictionary: dictItem];
        [m_Categories addObject: clsCategory];
    }

#ifdef TEST_MODE_ADD_DATA
    [self sendInfoToServer];
#endif
}

- (NSArray*) getCategoryList
{
    return m_Categories;
}

- (IGCategory*) getCategoryByIndex: (int) nCategoryIndex
{
    for (IGCategory* clsCategory in m_Categories) {
        if (clsCategory.m_nIndex == nCategoryIndex) {
            return clsCategory;
        }
    }
    return nil;
}

- (NSArray*) getTemplateList
{
    return m_Templates;
}

#pragma mark - login & sign up and get user information

- (BOOL) loginByFB: (NSString*) accessToken USERID: (NSString*) strUserId
{
    m_strDeviceToken = @"undefined";
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey: @"device_token"] && ![[defaults objectForKey: @"device_token"] isEqualToString: @""]) {
        m_strDeviceToken = [defaults objectForKey: @"device_token"];
    }

    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_FBLOGIN];
    NSString* strParam = [NSString stringWithFormat: @"%@=%@&%@=%@&%@=%@", REQUEST_FB_TOKEN, accessToken, REQUEST_FB_USER_ID, strUserId, REQUEST_DEVICE_TOKEN, m_strDeviceToken];
    
    NSDictionary* dictResult = nil;
    BOOL FRes = [communication sendRequest: MethodTypePost requestURL:strApiUrl params: strParam respData: &dictResult];
    
    int nCode = [[dictResult objectForKey: @"code"] intValue];
    if (nCode == 200) {
        m_strApiKey = [dictResult objectForKey: RESPONSE_API_KEY];
        NSDictionary* dictUser = [dictResult objectForKey: RESPONSE_USER];
        m_clsUser = [[IGUser alloc] initWithDictionary: dictUser];
        
        return YES;
    } else {
        m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
    }
    
    return FRes;
}

- (NSString*) getDownloadLink
{
    return m_clsUser.m_strDownloadLink;
}

- (NSString*) getErrMessage
{
    return m_strErrMessage;
}

- (BOOL) forgotPassword: (NSString*) strMail
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@user/forgotPassword", SERVER_URL];
    NSString* strParam = [NSString stringWithFormat: @"email=%@", strMail];
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        }
    }
    return NO;
}

- (BOOL) defaultLogin: (NSString*) strUser PASSWORD: (NSString*) strPassword
{
    m_strDeviceToken = @"undefined";
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey: @"device_token"] && ![[defaults objectForKey: @"device_token"] isEqualToString: @""]) {
        m_strDeviceToken = [defaults objectForKey: @"device_token"];
    }
    
    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_LOGIN];
    NSString* strParam = [NSString stringWithFormat: @"%@=%@&%@=%@&%@=%@", REQUEST_USER, strUser, REQUEST_PASSWORD, strPassword, REQUEST_DEVICE_TOKEN, m_strDeviceToken, nil];
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL:strApiUrl params: strParam respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            m_strApiKey = [dictResult objectForKey: RESPONSE_API_KEY];
            NSDictionary* dictUser = [dictResult objectForKey: RESPONSE_USER];
            m_clsUser = [[IGUser alloc] initWithDictionary: dictUser];
            
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (BOOL) signupProcess: (NSDictionary*) dict Image: (UIImage*) image
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_SIGNUP];
    NSDictionary* dictResult;
    NSMutableData* data = [NSMutableData data];
    
    [communication appendFileToBody: data filenamekey: @"profile_image" filenamevalue: @"photo.jpg" filedata: UIImageJPEGRepresentation(image, 0.7)];
    [data appendData: [communication makeMultipartBody: dict]];
    
    BOOL FSuccess = [communication sendMultipartRequest: strApiUrl data: data respData: &dictResult];

    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            m_strApiKey = [dictResult objectForKey: RESPONSE_API_KEY];
            NSDictionary* dictUser = [dictResult objectForKey: RESPONSE_USER];
            m_clsUser = [[IGUser alloc] initWithDictionary: dictUser];
            
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

- (BOOL) signupProcess: (NSString*) strUserInfo
{
    
    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_SIGNUP];
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strUserInfo respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            m_strApiKey = [dictResult objectForKey: RESPONSE_API_KEY];
            NSDictionary* dictUser = [dictResult objectForKey: RESPONSE_USER];
            m_clsUser = [[IGUser alloc] initWithDictionary: dictUser];
            
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

- (IGUser*) getUserInfo
{
    return m_clsUser;
}

- (BOOL) isMe: (NSString*) strUsername
{
    if ([m_clsUser.m_strUsername isEqualToString: strUsername]) {
        return YES;
    }
    return NO;
}

//get category info

- (NSArray*) getCategoryInfo
{
    return nil;
}

- (int) getProductIndex: (NSString*) strName Category: (IGCategory*) clsCategory
{
    int nIndex = 0;
    for (IGProduct* clsProduct in clsCategory.m_ProductList) {
        if ([clsProduct.m_strRealName.lowercaseString isEqualToString: strName.lowercaseString]) {
            return nIndex;
        }
        nIndex ++;
    }
    
    return 0;
}

- (NSDictionary*) getCategoryDescription: (int) nCategoryId
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@category/%d", SERVER_URL, nCategoryId];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return [dictResult objectForKey: @"category"]; //username, followAccount
        }
    }
    return nil;
}

- (NSDictionary*) getProductInfo: (int) nProductId
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@product/%d", SERVER_URL, nProductId];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return [dictResult objectForKey: @"product"]; //username, followAccount
        }
    }
    return nil;
}

#pragma mark - news feed
- (BOOL) loadNewsfeed: (int) nPage
{
    NSString* strApiUrl;
    if (nPage == 0) {
        [self clearNewsfeedData];
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_NEWS_FEED_FIRST];
    } else {
        strApiUrl = [NSString stringWithFormat: @"%@%@%d", SERVER_URL, API_NEWS_FEED, nPage];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayItems = [dictResult objectForKey: RESPONSE_ITEMS];
            if (arrayItems == nil || arrayItems.count == 0) {
                return NO;
            }
            for (NSDictionary* dictItem in arrayItems) {
                IGItem* clsItem = [[IGItem alloc] initWithDictionay: dictItem];
                [_m_arrayNewsFeed addObject: clsItem];
            }
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

- (void) clearNewsfeedData
{
    [_m_arrayNewsFeed removeAllObjects];
}

- (NSArray*) getNewsfeedData
{
    return _m_arrayNewsFeed;
}

#pragma mark - featured data

- (BOOL) loadFeaturedData: (int) nPage
{
    NSString* strApiUrl;
    if (nPage == 0) {
        [self clearFeaturedData];
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_FEATURED_FIRST];
    } else {
        strApiUrl = [NSString stringWithFormat: @"%@%@%d", SERVER_URL, API_FEATURED, nPage];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayItems = [dictResult objectForKey: RESPONSE_ITEMS];
            for (NSDictionary* dictItem in arrayItems) {
                IGItem* clsItem = [[IGItem alloc] initWithDictionay: dictItem];
                [_m_arrayFeatured addObject: clsItem];
            }
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

- (void) clearFeaturedData
{
    [_m_arrayFeatured removeAllObjects];
}

- (NSArray*) getFeaturedData
{
    return _m_arrayFeatured;
}

- (NSArray*) searchHashtags: (NSString*) strKey
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_SEARCH_HASHTAG, strKey]];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return [dictResult objectForKey: @"hashtags"]; //name, itemCount
        }
    }
    return nil;
}

- (NSArray*) searchUsernames: (NSString*) strKey
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_SEARCH_USERNAME, strKey]];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return [dictResult objectForKey: @"usernames"]; //username, followAccount
        }
    }
    return nil;
}

- (NSString*) getChoseCategories
{
    NSMutableString* strResult = [[NSMutableString alloc] init];
    for (IGCategory* clsCategory in m_Categories) {
        if (clsCategory.m_FChoose)
        {
            [strResult appendString: [NSString stringWithFormat: @"%d,", clsCategory.m_nIndex]];
        }
    }
    
    NSString* strValue = @"";
    if (![strResult isEqualToString: @""]) {
        strValue = [strResult substringToIndex: strResult.length - 1];
    }
    
    return strValue;
}

- (void) clearChoseStatus
{
    for (IGCategory* clsCategory in m_Categories) {
        clsCategory.m_FChoose = NO;
    }
}

- (NSArray*) loadTaggedItems: (NSString*) strTagName PAGE: (int) nPage
{
    //filter param : category_filter=",,,"

    NSString* strCategory = [self getChoseCategories];
    
    NSString* strApiUrl;
    if (nPage == 0) {
        if ([strCategory isEqualToString: @""])
            strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_TAGGED_ITEMS_FIRST, strTagName]];
        else
            strApiUrl = [NSString stringWithFormat: @"%@%@?category_filter=%@", SERVER_URL, [NSString stringWithFormat: API_TAGGED_ITEMS_FIRST, strTagName], strCategory];
    } else {
        if ([strCategory isEqualToString: @""])
            strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_TAGGED_ITEMS, strTagName, nPage]];
        else
            strApiUrl = [NSString stringWithFormat: @"%@%@?category_filter=%@", SERVER_URL, [NSString stringWithFormat: API_TAGGED_ITEMS, strTagName, nPage], strCategory];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayItems = [dictResult objectForKey: RESPONSE_ITEMS];
            return arrayItems;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}


#pragma mark - comment

- (NSMutableArray*) loadCommentList: (NSInteger) nItemId
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_COMMENT_LIST, (int)nItemId]];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayComments = [dictResult objectForKey: RESPONSE_COMMENTS];
            NSMutableArray* result = [NSMutableArray array];
            for (NSDictionary* dictComment in arrayComments) {
                IGComment* clsItem = [[IGComment alloc] initWithDictionay: dictComment];
                [result addObject: clsItem];
            }
            return result;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

- (IGComment*) postComment: (NSInteger) nItemId Text: (NSString*) strComment
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_POST_COMMENT, (int)nItemId]];
    
    NSDictionary* dictResult;
    NSString* strParam = [NSString stringWithFormat: @"comment=%@", strComment];
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSDictionary* dictComment = [dictResult objectForKey: @"comment"];
            IGComment* clsComment = [[IGComment alloc] initWithDictionay: dictComment];
            return clsComment;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

- (BOOL) deleteComment: (NSInteger) nCommentId
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@comment/%d", SERVER_URL, (int)nCommentId];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeDelete requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

- (BOOL) postLike: (NSInteger) nItemId Mode: (BOOL) FLike
{
    NSString* strApiUrl;
    if (FLike) {
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_POST_LIKE, (int)nItemId]];
    } else {
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_POST_UNLIKE, (int)nItemId]];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypePut requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

#pragma mark - profile

- (IGProfileInfo*) getProfileInfo: (NSString*) strUsername
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_USER_PROFILE, strUsername]];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            IGProfileInfo* clsProfile = [[IGProfileInfo alloc] initWithDictionary: [dictResult objectForKey: @"user"]];
            return clsProfile;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

- (NSDictionary*) getExtraInfo
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_USER_EXTRAINFO];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return dictResult;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

- (BOOL) removePhoto
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@user/changeProfileImage/", SERVER_URL];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: @"profile_image=0" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        }
    }
    return NO;
}

- (BOOL) changeProfilePicture: (UIImage*) image
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@user/changeProfileImage", SERVER_URL];
    
    NSMutableData* data = [NSMutableData data];
    [communication appendFileToBody: data filenamekey: @"profile_image" filenamevalue: @"profile_image.png" filedata: UIImageJPEGRepresentation(image, 0.8)];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendMultipartRequest: strApiUrl data: data header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        }
    }
    return NO;
}

- (NSArray*) getCreationList: (int) nUserIndex Page: (int) nPage
{
    NSString* strApiUrl;
    if (nPage == 0) {
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_CREATION_LIST_FIRST, nUserIndex]];
    } else {
        strApiUrl = [NSString stringWithFormat: @"%@%@%d", SERVER_URL, [NSString stringWithFormat: API_CREATION_LIST, nUserIndex], nPage];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayItems = [dictResult objectForKey: RESPONSE_ITEMS];
            NSMutableArray* arrayCreations = [NSMutableArray array];
            for (NSDictionary* dictItem in arrayItems) {
                IGItem* clsItem = [[IGItem alloc] initWithDictionay: dictItem];
                [arrayCreations addObject: clsItem];
            }
            return arrayCreations;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

- (NSArray*) getLikedList: (int) nUserIndex Page: (int) nPage
{
    NSString* strApiUrl;
    if (nPage == 0) {
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: API_LIKED_LIST_FIRST, nUserIndex]];
    } else {
        strApiUrl = [NSString stringWithFormat: @"%@%@%d", SERVER_URL, [NSString stringWithFormat: API_LIKED_LIST, nUserIndex], nPage];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayItems = [dictResult objectForKey: RESPONSE_ITEMS];
            NSMutableArray* arrayCreations = [NSMutableArray array];
            for (NSDictionary* dictItem in arrayItems) {
                IGItem* clsItem = [[IGItem alloc] initWithDictionay: [dictItem objectForKey: @"item"]];
                [arrayCreations addObject: clsItem];
            }
            return arrayCreations;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

- (NSArray*) getLikers: (int) nItemIndex PAGE: (int) nPage
{
    NSString* strApiUrl;
    strApiUrl = [NSString stringWithFormat: @"%@item/%d/like/%d", SERVER_URL, nItemIndex, nPage];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayItems = [dictResult objectForKey: @"likes"];
            return arrayItems;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

//   user/%d/follower, user/%d/following : get
// followers: [follow: true/false, user: {id, name, username, profile_image_url, }], totalCount

- (NSArray*) getFollowUsers: (int) nMode USER: (int) nUserIndex PAGE: (int) nPage
{
    NSString* strApiUrl;
    NSString* strAction;
    if (nMode == 0) { //followers
        strAction = API_FOLLOWERS;
    } else {
        strAction = API_FOLLOWINGS;
    }
    
    if (nPage == 0) {
        strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: strAction, nUserIndex]];
    } else {
        strApiUrl = [NSString stringWithFormat: @"%@%@/%d", SERVER_URL, [NSString stringWithFormat: strAction, nUserIndex], nPage];
    }
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* arrayUsers;
            if (nMode == 0) {
                arrayUsers = [dictResult objectForKey: @"followers"];
            } else {
                arrayUsers = [dictResult objectForKey: @"followings"];
            }
            return arrayUsers;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

//   user/%d/follow, user/%d/unfollow : put
//code

- (BOOL) follow: (NSDictionary*) dictParam
{
    NSString* strApiUrl;
    NSString* strAction;
    
    int nUserIndex = [[dictParam objectForKey: @"userIndex"] intValue];
    BOOL FFollow = [[dictParam objectForKey: @"follow"] boolValue];
    
    if (FFollow) { //follow
        strAction = API_FOLLOW;
    } else { //unfollow
        strAction = API_UNFOLLOW;
    }
    
    strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, [NSString stringWithFormat: strAction, nUserIndex]];
    
    NSDictionary* dictResult;
    BOOL FSuccess = [communication sendRequest: MethodTypePut requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}


#pragma mark - upload product info

- (IGItem*) uploadProduct: (NSDictionary*) dictProduct
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@%@", SERVER_URL, API_CREATE];

    NSMutableData* data = [NSMutableData data];
    NSMutableDictionary* dictParam = [NSMutableDictionary dictionary];
    
    for (NSString* strKey in [dictProduct allKeys]) {
        id field = [dictProduct objectForKey: strKey];
        if ([strKey rangeOfString: @"_image"].length > 0) {
            if (![field isKindOfClass: [NSString class]]) {
                UIImage* image = (UIImage*)field;
                if ([strKey isEqualToString: @"feed_image"]) {
                    [communication appendFileToBody: data filenamekey: strKey filenamevalue: [NSString stringWithFormat: @"%@.png", strKey] filedata: UIImagePNGRepresentation(image)];
                } else {
//                    [communication appendFileToBody: data filenamekey: strKey filenamevalue: [NSString stringWithFormat: @"%@.jpg", strKey] filedata: UIImageJPEGRepresentation(image, 0.8)];
                    [communication appendFileToBody: data filenamekey: strKey filenamevalue: [NSString stringWithFormat: @"%@.jpg", strKey] filedata: field];
                }
            }
        } else {
            [dictParam setObject: field forKey: strKey];
        }
    }
    
    [data appendData: [communication makeMultipartBody: dictParam]];
    
    NSDictionary* dict = nil;
    BOOL FSuccess = [communication sendMultipartRequest: strApiUrl data: data header: m_strApiKey respData: &dict];
    if (FSuccess) {
        int nCode = [[dict objectForKey: @"code"] intValue];
        if (nCode == 200) {
            IGItem* clsItem = [[[IGItem alloc] initWithDictionay: [dict objectForKey: @"item"]] autorelease];
            return clsItem;
        } else {
            m_strErrMessage = [dict objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return nil;
}

#pragma mark - methods for settings

- (BOOL) changePassword: (NSString*) strPassword
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@user/changePassword", SERVER_URL];
    
    NSDictionary* dictResult;
    NSString* strParam = [NSString stringWithFormat: @"new_password=%@", strPassword];
    BOOL FSuccess = [communication sendRequest: MethodTypePut requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
//            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    return NO;
}

- (BOOL) saveProfile: (NSDictionary*) dict
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@user/editProfile", SERVER_URL];
    
    NSDictionary* dictResult;
    NSMutableString* strParam = [[NSMutableString alloc] init];
    for (NSString* strKey in dict.allKeys) {
        NSString* strValue = [[dict objectForKey: strKey] stringByReplacingOccurrencesOfString: @"&" withString: STR_EXCHANGE];
        [strParam appendString: [NSString stringWithFormat: @"%@=%@&", strKey, strValue]];
    }
    
    NSString* strRealParam = [strParam substringToIndex: strParam.length - 1];
    
    BOOL FSuccess = [communication sendRequest: MethodTypePut requestURL: strApiUrl params: strRealParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            //            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (NSDictionary*) updateBankInfo: (NSString*) strParam
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@user/editProfile", SERVER_URL];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypePut requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            //            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            return [[dictResult objectForKey: @"profile"] objectForKey: @"bank_account"];
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return nil;
}

#pragma mark - Purchase & Cart methods

- (BOOL) addItemToCart: (int) nItemId Quantity: (int) nQuantity Additional: (NSString*) strInfo
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@cart/", SERVER_URL];
    
    NSDictionary* dictResult;
    NSString* strParam = @"";
    if (strInfo == nil || [strInfo isEqualToString: @""]) {
        strParam = [NSString stringWithFormat: @"item_id=%d&quantity=%d", nItemId, nQuantity];
    } else {
        strParam = [NSString stringWithFormat: @"item_id=%d&quantity=%d&additional_info=%@", nItemId, nQuantity, strInfo];
    }
    
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            //            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (BOOL) deleteItem: (int) nItemId
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@item/%d", SERVER_URL, nItemId];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypeDelete requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (BOOL) deleteCartItem: (int) nCartId
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@cart/%d", SERVER_URL, nCartId];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypeDelete requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (BOOL) updateCartItem: (int) nCartId Param: (NSString*) strParam
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@cart/%d", SERVER_URL, nCartId];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypePut requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            //            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (BOOL) loadCart
{
    m_cartItems = nil;
    
    NSString* strApiUrl = [NSString stringWithFormat: @"%@cart/", SERVER_URL];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            m_cartItems = [[dictResult objectForKey: @"cart_items"] mutableCopy];
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (NSMutableArray*) getCart
{
    return m_cartItems;
}

- (int) getCartItemCount
{
    return m_cartItems ? (int)m_cartItems.count : 0;
}

- (BOOL) cartCheckout: (NSString*) strTokenId ParamInfo: (NSString*) strInfo
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@cart/checkout", SERVER_URL];
    
    NSDictionary* dictResult;
    NSString* strParam = [NSString stringWithFormat: @"card=%@&%@", strTokenId, strInfo];
    
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (BOOL) itemCheckout: (int) nItemId CardToken: (NSString*) strTokenId ParamInfo: (NSString*) strInfo
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@item/%d/checkout", SERVER_URL, nItemId];
    
    NSDictionary* dictResult;
    NSString* strParam = [NSString stringWithFormat: @"card=%@&%@", strTokenId, strInfo];
    
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (int) createHybrid: (int) nItemId ProductId: (int) nProductId
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@item/%d/createHybrid/%d", SERVER_URL, nItemId, nProductId];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            //            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
            int nNewId = [CUtils getIntValueFromDictionary: [dictResult objectForKey: @"item"] KEY: @"id"];
            return nNewId;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return -1;
}

#pragma mark - Order Methods

- (NSArray*) getOrders: (int) nPage
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@order/index/%d", SERVER_URL, nPage];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* retArray = [dictResult objectForKey: @"orders"];
            return retArray;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return nil;
}

- (NSArray*) getOrderItems: (int) nOrderId
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@order/%d", SERVER_URL, nOrderId];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* retArray = [dictResult objectForKey: @"order_items"];
            return retArray;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return nil;
}

#pragma mark - Withdraw methods

- (BOOL) withdraw: (float) rAmount
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@withdrawal/", SERVER_URL];
    
    NSDictionary* dictResult;
    NSString* strParam = [NSString stringWithFormat: @"amount=%f", rAmount];
    
    BOOL FSuccess = [communication sendRequest: MethodTypePost requestURL: strApiUrl params: strParam header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            return YES;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return NO;
}

- (NSArray*) getWithdrawals: (int) nPage
{
    NSString* strApiUrl = [NSString stringWithFormat: @"%@withdrawal/index/%d", SERVER_URL, nPage];
    
    NSDictionary* dictResult;
    
    BOOL FSuccess = [communication sendRequest: MethodTypeGet requestURL: strApiUrl params: @"" header: m_strApiKey respData: &dictResult];
    if (FSuccess) {
#ifdef _DEBUG
        NSLog(@"Response Data : %@", dictResult);
#endif
        int nCode = [[dictResult objectForKey: @"code"] intValue];
        if (nCode == 200) {
            NSArray* retArray = [dictResult objectForKey: @"withdrawals"];
            return retArray;
        } else {
            m_strErrMessage = [dictResult objectForKey: RESPONSE_ERROR_MESSAGE];
        }
    }
    
    return nil;
}

@end