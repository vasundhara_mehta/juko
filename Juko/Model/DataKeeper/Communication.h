//
//  Communication.h
//  nur10
//
//  Created by Wony Shin on 10/19/12.
//  Copyright 2013 nur10. All rights reserved.
//

#import <Foundation/Foundation.h>


extern const NSString *multipartBoundary;

typedef enum {
    MethodTypePost = 0,
    MethodTypeGet = 1,
    MethodTypePut = 2,
    MethodTypeDelete = 3,
} MethodType;

// communication interface
@interface Communication : NSObject

@property (nonatomic, retain) NSString *m_strCurrentEncodedPassword;


// internal function
- (BOOL)sendRequest:(NSString *)requestURL respData:(NSString **)respString;

- (BOOL)sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params respData:(NSDictionary **)respDic;
- (BOOL)sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params respArray:(NSArray **)respArray;
- (BOOL)sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params header: (NSString*) strHeader respData:(NSDictionary **)respDic;

- (NSInteger) sendRequest:(MethodType)methodType requestURL:(NSString *)requestURL params:(NSString *)params X_WSSE: (NSString *)strXSSE respData:(NSDictionary **)respDic;


//  Upload File
- (BOOL)sendMultipartRequest:(NSString *)strService data:(NSData *)data respData:(NSDictionary **)respDic;
- (BOOL)sendMultipartRequest:(NSString *)strService data:(NSData *)data header: (NSString*) strHeader respData:(NSDictionary **)respDic;
+ (BOOL)uploadFile:(NSString *)strURL filename:(NSString *)filename fileData:(NSString *)fileData respData:(NSDictionary **)respDic;
- (NSData *)makeMultipartBody:(NSDictionary*)dic;
- (void)appendFileToBody:(NSMutableData *)data filenamekey:(NSString*)filenamekey filenamevalue:(NSString*)filenamevalue filedata:(NSData*)filedata;

@end
