//
//  DataKeeper.h
//  OpsightLite
//
//  Created by MacBook on 10/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//  

#import "SingletonClass.h"
#import "Constants.h"
#import "Communication.h"

#import <CoreLocation/CoreLocation.h>

typedef enum _EViewKind {
    MODE_READ = 0,
    MODE_WRITE,
} EViewKind;

@class IGUser;
@class IGItem;
@class IGComment;
@class IGProfileInfo;
@class IGCategory;

@interface DataKeeper : SingletonClass
{
    Communication* communication;
    
    NSArray* m_currentDiscoveredArray;
    NSString* m_strDeviceToken;
    
    NSString* m_strApiKey;
    NSString* m_strErrMessage;
    
    IGUser* m_clsUser;
    
    NSMutableArray* m_Categories;
    NSMutableArray* m_Templates;
    
    NSMutableArray* m_cartItems;
    
    NSMutableArray* m_dictStates;
    
    NSString* m_strStripePublishKey;
    
    BOOL m_FInitLoad;
}

@property (nonatomic, strong) NSMutableArray* m_arrayNewsFeed;
@property (nonatomic, strong) NSMutableArray* m_arrayFeatured;

- (void) loadInit;
- (NSString*) getStripeKey;

//category, product, template
- (NSArray*) getStates;
- (int) getStateIdByKey: (NSString*) strKey;
- (NSArray*) getProductList: (int) nCategoryId;

//blank item list
- (int) getProductIndex: (NSString*) strName Category: (IGCategory*) clsCategory;
- (NSArray*) getCategoryList;
- (IGCategory*) getCategoryByIndex: (int) nCategoryIndex;
- (NSArray*) getTemplateList;
- (NSDictionary*) getProductInfo: (int) nProductId;
- (NSDictionary*) getCategoryDescription: (int) nCategoryId;
//set device token
- (void) saveDeviceToken: (NSString*) tokenString;

//login & signup process
- (BOOL) forgotPassword: (NSString*) strMail;
- (BOOL) loginByFB: (NSString*) accessToken USERID: (NSString*) strUserId;
- (BOOL) defaultLogin: (NSString*) strUser PASSWORD: (NSString*) strPassword;
- (NSString*) getErrMessage;
- (NSString*) getDownloadLink;

- (BOOL) signupProcess: (NSString*) strUserInfo;
- (BOOL) signupProcess: (NSDictionary*) dict Image: (UIImage*) image;
- (IGUser*) getUserInfo;
- (BOOL) isMe: (NSString*) strUsername;
//newsfeed
- (BOOL) loadNewsfeed: (int) nPage;
- (NSArray*) getNewsfeedData;

//featured
- (BOOL) loadFeaturedData: (int) nPage;
- (NSArray*) getFeaturedData;

- (NSArray*) searchHashtags: (NSString*) strKey;
- (NSArray*) searchUsernames: (NSString*) strKey;

- (NSArray*) loadTaggedItems: (NSString*) strTagName PAGE: (int) nPage;
//comment
- (NSMutableArray*) loadCommentList: (NSInteger) nItemId;
- (IGComment*) postComment: (NSInteger) nItemId Text: (NSString*) strComment;
- (BOOL) deleteComment: (NSInteger) nCommentId;

//like
- (BOOL) postLike: (NSInteger) nItemId Mode: (BOOL) FLike;

//profile
- (IGProfileInfo*) getProfileInfo: (NSString*) strUsername;
- (NSDictionary*) getExtraInfo;

- (BOOL) removePhoto;
- (BOOL) changeProfilePicture: (UIImage*) image;

- (NSArray*) getCreationList: (int) nUserIndex Page: (int) nPage;
- (NSArray*) getLikedList: (int) nUserIndex Page: (int) nPage;
- (NSArray*) getLikers: (int) nItemIndex PAGE: (int) nPage;

- (NSArray*) getFollowUsers: (int) nMode USER: (int) nUserIndex PAGE: (int) nPage;
- (BOOL) follow: (NSDictionary*) dictParam;

//upload product

- (IGItem*) uploadProduct: (NSDictionary*) dictProduct;
- (BOOL) deleteItem: (int) nItemId;

- (void) loadCategoryData;
- (void) clearChoseStatus;

#pragma mark - Methods for settings

- (BOOL) changePassword: (NSString*) strPassword;
- (BOOL) saveProfile: (NSDictionary*) dict;
- (NSDictionary*) updateBankInfo: (NSString*) strParam;

#pragma mark - Purchase & Cart Methods

- (BOOL) addItemToCart: (int) nItemId Quantity: (int) nQuantity Additional: (NSString*) strInfo;
- (BOOL) deleteCartItem: (int) nCartId;
- (BOOL) updateCartItem: (int) nCartId Param: (NSString*) strParam;
- (BOOL) loadCart;
- (NSMutableArray*) getCart;
- (int) getCartItemCount;
- (BOOL) itemCheckout: (int) nItemId CardToken: (NSString*) strTokenId ParamInfo: (NSString*) strInfo;
- (BOOL) cartCheckout: (NSString*) strTokenId ParamInfo: (NSString*) strInfo;

- (int) createHybrid: (int) nItemId ProductId: (int) nProductId;

- (NSArray*) getOrders: (int) nPage;
- (NSArray*) getOrderItems: (int) nOrderId;

- (BOOL) withdraw: (float) rAmount;
- (NSArray*) getWithdrawals: (int) nPage;

@end
