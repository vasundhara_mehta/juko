//
//  CUtils.m
//  nur10
//
//  Created by Wony Shin on 10/19/12.
//  Copyright 2013 nur10. All rights reserved.
//

#import "CUtils.h"
#import <QuartzCore/QuartzCore.h>
#import "IGProgressActivity.h"
#import "CGImageRef+CATransform3D.h"

#import <GPUImage.h>
#import "UIImage+CATransform3D.h"


@implementation CUtils

+ (NSString *)getDocumentDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}

+ (NSString *)generateFileName: (BOOL) bMovie {
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setDateFormat:@"MMddyyyy_HH_mm_ss"];
    NSString *date_str = [dateFormatter stringFromDate:currDate];
    NSString *strFileName;
    
    NSInteger randomNumber = arc4random() % 10000;
    
    if (bMovie == YES) {
        strFileName = [NSString stringWithFormat:@"nur10%@.mp4", date_str];
    } else {
        strFileName = [NSString stringWithFormat:@"nur10%@_%d.jpg", date_str, randomNumber];
    }
    
    [strFileName retain];
    
    return strFileName;
}
	
+ (NSString *)getFilePath:(NSString *)fileName
{
	NSArray *fileComponents = [NSArray arrayWithArray:[fileName componentsSeparatedByString:@"."]];
	NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileComponents objectAtIndex:0] ofType:[fileComponents objectAtIndex:1]];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
	BOOL isAtPath = [fileManager fileExistsAtPath:path];
	
	if(isAtPath)
	{
		return path;
	}
	else
	{
		return filePath;
	}
	
}

+ (BOOL)isFileInDocumentDirectory:(NSString *)fileName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL isFileAtPath = [fileManager fileExistsAtPath:path];
	return isFileAtPath;
}

+ (void)copyFileToDocumentDirectory:(NSString *)fileName
{
	NSString *pathOfFile = [self getFilePath:fileName];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *docDir = [self getDocumentDirectory];
	NSString *desFilePath = [docDir stringByAppendingPathComponent:fileName];
	if([self isFileInDocumentDirectory:fileName])
	{
		
	}
	else
	{
		[fileManager copyItemAtPath:pathOfFile toPath:desFilePath error:nil];
	}
}


+ (NSString *)convertToLocalTime:(NSString *)GMTtime
{
    NSString *serverDateString = GMTtime;
    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    [serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromServer = [serverDateFormatter dateFromString:serverDateString];
    [serverDateFormatter release];
    
    NSDateFormatter *dateFormatConverter = [[NSDateFormatter alloc] init];
    [dateFormatConverter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    
    /////////// GMT Logic ////
    NSDate* sourceDate = dateFromServer;
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];
    /////// End GMT Logic /////
    
    NSString *localDate = [dateFormatConverter stringFromDate:destinationDate]; 
    
    return localDate;
    
}

+ (NSString *)convertToLocalTime:(NSString *)strTime timezone: (NSString *) strTimeZone
{
    NSString *serverDateString = strTime;
    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    [serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromServer = [serverDateFormatter dateFromString:serverDateString];
    [serverDateFormatter release];
    
    NSDateFormatter *dateFormatConverter = [[NSDateFormatter alloc] init];
    [dateFormatConverter setDateFormat:@"MM.dd.yyyy"];
    
    /////////// GMT Logic ////
    NSDate* sourceDate = dateFromServer;
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation: strTimeZone];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];
    /////// End GMT Logic /////
    
    NSString *localDate = [dateFormatConverter stringFromDate:destinationDate];
    
    return localDate;
    
}

+ (NSString *)convertToLocalTime1:(NSString *)strTime timezone: (NSString *) strTimeZone
{
    NSString *serverDateString = strTime;
    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    [serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromServer = [serverDateFormatter dateFromString:serverDateString];
    [serverDateFormatter release];
    
    NSDateFormatter *dateFormatConverter = [[NSDateFormatter alloc] init];
    [dateFormatConverter setDateFormat:@"dd.MM.yyyy"];
    
    /////////// GMT Logic ////
    NSDate* sourceDate = dateFromServer;
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation: strTimeZone];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];
    /////// End GMT Logic /////
    
    NSString *localDate = [dateFormatConverter stringFromDate:destinationDate];
    
    return localDate;
    
}

+ (NSString*) getDateString: (NSString*) strDateTime
{
    NSString *serverDateString = strDateTime;
    NSDateFormatter *serverDateFormatter = [[NSDateFormatter alloc] init];
    [serverDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromServer = [serverDateFormatter dateFromString:serverDateString];
    [serverDateFormatter release];
    
    NSDateFormatter *dateFormatConverter = [[NSDateFormatter alloc] init];
    [dateFormatConverter setDateFormat:@"yyyy-MM-dd"];

    NSString *localDate = [dateFormatConverter stringFromDate: dateFromServer];
    
    return localDate;
}

+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}

+ (NSString *)getCurrentTime {
    NSDate* date = [NSDate date];
    
    NSDate *newDate = [date dateByAddingTimeInterval:-60*2];
    
  //  NSDate* newDate = [date addTimeInterval:3600*12];
    NSTimeZone *localtimezone = [NSTimeZone timeZoneWithName:@"Europe/Berlin"];

    
    //Create the dateformatter object
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];

    [formatter setTimeZone:localtimezone];
    
    //Set the required date format
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //Get the string date
    
    NSString* str = [formatter stringFromDate:newDate];
    
    
    
    //Display on the console
  //  NSLog(@"%@", [formatter stringFromDate: newDate]);
    
    return str;
    
}


+ (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)size
{
    float width = size.width;
    float height = size.height;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, image.scale);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    float widthRatio = image.size.width / width;
    float heightRatio = image.size.height / height;
    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
    
    width = image.size.width / divisor;
    height = image.size.height / divisor;
    
    rect.size.width  = width;
    rect.size.height = height;
    
    [image drawInRect: rect];
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return smallImage;
}

+ (NSInteger)getAge:(NSString *)dateOfBirth {
    
    if (dateOfBirth == nil || [dateOfBirth length] < 9) {
        return 0;
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    [df setDateFormat:@"MM/dd/yyyy"];
    NSDate *convertedDate = [df dateFromString:dateOfBirth];
    [df release];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:convertedDate];
    
    if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
        (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
        return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
    } else {
        return [dateComponentsNow year] - [dateComponentsBirth year];
    }
}

// Check if current device is iPhone 5
// LOL, if height of screen is 568, yeah it's just iPhone 5!!!!!
+ (BOOL) isIphone5 {
    
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        return YES;
    }
    
    return NO;
}

// Make Rounded txet
+ (void) makeRoundedText: (UITextField *) textField {
    
    textField.layer.borderColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0].CGColor;
    textField.layer.borderWidth = 1;
    textField.layer.cornerRadius = 10;
    textField.layer.masksToBounds = YES;

}

+ (BOOL) IsEmptyString : (NSString *) str {
    return ([[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length] == 0);
}


+ (CGFloat) getRealHeight: (NSString *) text size: (CGSize) textSize font: (UIFont *) font min_height: (float) minHeight lineBreakMode:(NSLineBreakMode)lineBreakMode {
    
    CGSize realSize = [text sizeWithFont:font constrainedToSize:textSize lineBreakMode:lineBreakMode];
    
    CGFloat ht = MAX(realSize.height, minHeight);
    
    return ht;

}

#pragma mark - MD5
+ (NSString *) createMD5: (NSString *) source {
    const char *cStr = [source UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

+ (NSString*) getNonce : (NSInteger) nLength {
    static char table[] = "abcdefghijklmnopqrstuvwxyz0123456789";
    NSMutableString *output = [NSMutableString stringWithCapacity:nLength];
    
    srandom(time(NULL));
    
    NSInteger i;
    for (i = 0; i < nLength; i++) {
        [output appendFormat:@"%c", table[arc4random() % strlen(table)]];
    }
    
    return output;
}

#pragma mark - URL Encoding

+ (NSString *) URLEncoding: (NSString *) str {
    NSString * encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                   NULL,
                                                                                   (CFStringRef)str,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                   kCFStringEncodingUTF8 );
    
    return encodedString;
}

+ (NSString *) URLDecoding: (NSString *) str {
    return (NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                (CFStringRef) str,
                                                                                CFSTR(""),
                                                                                kCFStringEncodingUTF8);
}

#pragma mark - Get Distance

//+ (double)kilometresBetweenPlace1:(CLLocationCoordinate2D) place1 andPlace2:(CLLocationCoordinate2D) place2 {
//    
//    MKMapPoint  start, finish;
//    
//    start = MKMapPointForCoordinate(place1);
//    finish = MKMapPointForCoordinate(place2);
//    
//    return MKMetersBetweenMapPoints(start, finish) / 1000;
//}

////UI utils/////////
#pragma mark - UI Utils

+ (void) doCircleView: (UIView*) view
{
    [view.layer setCornerRadius: view.frame.size.width/2.0f];
}

#pragma mark - show/hide the Activity
+ (void) showActivity: (IGProgressActivity*) HUD Caption: (NSString*) strCaption
{
    HUD.labelText = strCaption;
    [HUD show: YES];
}

+ (void) hideActivity: (IGProgressActivity*) HUD
{
    [HUD hide: YES];
}

#pragma mark - get value methods from dictionary

+ (NSString*) getStringValueFromDictionary: (NSDictionary*) dict KEY: (NSString*) strKey
{
    if (dict == nil || [dict isKindOfClass: [NSNull class]]) {
        return @"";
    }
    id obj = [dict objectForKey: strKey];
    if (!obj || [obj isKindOfClass: [NSNull class]] || ![obj isKindOfClass: [NSString class]]) {
        return @"";
    }
    return [obj stringByReplacingOccurrencesOfString: STR_EXCHANGE withString: @"&"];
}

+ (int) getIntValueFromDictionary: (NSDictionary*) dict KEY: (NSString*) strKey
{
    if (dict == nil || [dict isKindOfClass: [NSNull class]]) {
        return 0;
    }
    id obj = [dict objectForKey: strKey];
    if (!obj || [obj isKindOfClass: [NSNull class]]) {
        return 0;
    }
    return [obj intValue];
}

+ (float) getFloatValueFromDictionary: (NSDictionary*) dict KEY: (NSString*) strKey
{
    if (dict == nil || [dict isKindOfClass: [NSNull class]]) {
        return 0;
    }
    id obj = [dict objectForKey: strKey];
    if (!obj || [obj isKindOfClass: [NSNull class]]) {
        return 0;
    }
    return [obj floatValue];
}

+ (void) clearView: (UIView*) parent
{
    for (UIView* view in parent.subviews) {
        [view removeFromSuperview];
    }
}

+ (void) clearLayers: (UIView*) parent
{
    for (CALayer* layer in parent.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
}

#pragma mark - Image Process

+ (UIImage *) imageWithView:(UIView *)view
{
    CGSize szSize = view.bounds.size;
    szSize.width = (int)(szSize.width);
    szSize.height = (int)(szSize.height);
    
    UIGraphicsBeginImageContextWithOptions(szSize, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *) imageWithLayer:(CALayer*) layer
{
    CGSize szSize = layer.bounds.size;
    szSize.width = (int)(szSize.width);
    szSize.height = (int)(szSize.height);
    
    UIGraphicsBeginImageContextWithOptions(szSize, layer.opaque, 0.0);
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *) imageWithAllLayer:(UIView *)view
{
    UIView* subView = [[UIView alloc] initWithFrame: view.frame];
    for (CALayer* layer in view.layer.sublayers) {
        CGRect rt = layer.frame;
        
        UIImage* image = [UIImage imageWithCGImage: (CGImageRef)layer.contents];

        CATransform3D transform = layer.transform;
        transform.m34 = -1.0 / 500;
        CGPoint ptAnchor = layer.anchorPoint;
        UIImage* transImage = [image imageWithTransform: transform anchorPoint: ptAnchor];
        
        UIImageView* subImageView = [[UIImageView alloc] initWithFrame: rt];
        subImageView.clipsToBounds = YES;
        subImageView.image = transImage;
        [subView addSubview: subImageView];
    }
    
    [view addSubview: subView];

    CGSize szSize = view.bounds.size;
    szSize.width = (int)(szSize.width);
    szSize.height = (int)(szSize.height);
    
    UIGraphicsBeginImageContextWithOptions(szSize, view.opaque, 0.0);

    [subView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [subView removeFromSuperview];
    
    return img;
}

+ (UIImage*) mergeImage:(UIImage*) first withImage:(UIImage*) second Mode: (CGBlendMode) mode
{
    // get size of the first image
    CGImageRef firstImageRef = first.CGImage;
    CGFloat firstWidth = CGImageGetWidth(firstImageRef);
    CGFloat firstHeight = CGImageGetHeight(firstImageRef);
    
    // get size of the second image
    CGImageRef secondImageRef = second.CGImage;
    CGFloat secondWidth = CGImageGetWidth(secondImageRef);
    CGFloat secondHeight = CGImageGetHeight(secondImageRef);
    
//    if (firstWidth != secondWidth || firstHeight != secondHeight) {
//        return nil;
//    }
    
    // build merged size
    CGSize mergedSize = CGSizeMake(firstWidth, firstHeight);
    
    // capture image context ref
    UIGraphicsBeginImageContextWithOptions(mergedSize, NO, first.scale);
//    UIGraphicsBeginImageContext(mergedSize);
    //Draw images onto the context
    [first drawInRect:CGRectMake(0, 0, firstWidth, firstHeight)];
    //[second drawInRect:CGRectMake(firstWidth, 0, secondWidth, secondHeight)];
    [second drawInRect:CGRectMake(0, 0, secondWidth, secondHeight) blendMode: mode alpha:1.0];
    
    // assign context to new UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end context
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage*) concatImage:(UIImage*) first second:(UIImage*) second
{
    // get size of the first image
    CGImageRef firstImageRef = first.CGImage;
    CGFloat firstWidth = CGImageGetWidth(firstImageRef);
    CGFloat firstHeight = CGImageGetHeight(firstImageRef);
    
    // get size of the second image
    CGImageRef secondImageRef = second.CGImage;
    CGFloat secondWidth = CGImageGetWidth(secondImageRef);
    CGFloat secondHeight = CGImageGetHeight(secondImageRef);
    
    //    if (firstWidth != secondWidth || firstHeight != secondHeight) {
    //        return nil;
    //    }
    
    // build merged size
    CGSize mergedSize = CGSizeMake(firstWidth + secondWidth, firstHeight);
    
    // capture image context ref
    UIGraphicsBeginImageContextWithOptions(mergedSize, NO, first.scale);
    //    UIGraphicsBeginImageContext(mergedSize);
    //Draw images onto the context
    [first drawInRect:CGRectMake(0, 0, firstWidth, firstHeight)];
    [second drawInRect:CGRectMake(firstWidth, 0, secondWidth, secondHeight)];
    
    // assign context to new UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end context
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage*) imageWithImage:(UIImage*) image scaledToSize:(CGSize)newSize
{
//    UIGraphicsBeginImageContextW(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, image.scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage*) imageWithImage: (UIImage*) image cropByRect: (CGRect) clipRect
{
    if (image.scale > 1.0f) {
        clipRect = CGRectMake(clipRect.origin.x * image.scale,
                          clipRect.origin.y * image.scale,
                          clipRect.size.width * image.scale,
                          clipRect.size.height * image.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, clipRect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

+ (UIImage *) fixImage:(UIImage *)image
{
    if (image.imageOrientation == UIImageOrientationUp) return image;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
    
}

+ (UIImage*) maskImage: (UIImage*) image withMask: (UIImage*) maskImage
{
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef), CGImageGetHeight(maskRef), CGImageGetBitsPerComponent(maskRef), CGImageGetBitsPerPixel(maskRef), CGImageGetBytesPerRow(maskRef), CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef maskedImageRef = CGImageCreateWithMask([image CGImage], mask);
    UIImage* maskedImage = [UIImage imageWithCGImage: maskedImageRef];
    
    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    return maskedImage;
}

+ (UIImage*) imageWithMask: (UIImage*) first mask: (UIImage*) second
{
    // get size of the first image
    CGImageRef firstImageRef = first.CGImage;
    CGFloat firstWidth = CGImageGetWidth(firstImageRef);
    CGFloat firstHeight = CGImageGetHeight(firstImageRef);
    
    // get size of the second image
    CGImageRef secondImageRef = second.CGImage;
    CGFloat secondWidth = CGImageGetWidth(secondImageRef);
    CGFloat secondHeight = CGImageGetHeight(secondImageRef);
    
    //    if (firstWidth != secondWidth || firstHeight != secondHeight) {
    //        return nil;
    //    }
    
    // build merged size
    CGSize mergedSize = CGSizeMake(firstWidth, firstHeight);
    
    // capture image context ref
    UIGraphicsBeginImageContextWithOptions(mergedSize, NO, first.scale);
    
    //Draw images onto the context
    [first drawInRect:CGRectMake(0, 0, firstWidth, firstHeight)];
    //[second drawInRect:CGRectMake(firstWidth, 0, secondWidth, secondHeight)];
    [second drawInRect:CGRectMake(0, 0, secondWidth, secondHeight) blendMode:kCGBlendModeOverlay alpha:1.0];
    
    // assign context to new UIImage
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end context
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (CGRect) getRectFromString: (NSString*) strRect
{
    NSArray* array = [strRect componentsSeparatedByString: @","];
    return CGRectMake([array[0] floatValue], [array[1] floatValue], [array[2] floatValue], [array[3] floatValue]);
}


+(UIImage *)changeWhiteColorTransparent: (UIImage *)image
{
    //convert to uncompressed jpg to remove any alpha channels
    //this is a necessary first step when processing images that already have transparency
    image = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1.0)];
    CGImageRef rawImageRef=image.CGImage;
    //RGB color range to mask (make transparent)  R-Low, R-High, G-Low, G-High, B-Low, B-High
    const float colorMasking[6] = {250, 255, 250, 255, 250, 255};
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    
    //iPhone translation
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}

+(UIImage *)changeBlackColorTransparent: (UIImage *)image
{
    CGImageRef rawImageRef=image.CGImage;
    
    const float colorMasking[6] = {0, 0, 0, 0, 0, 0};
    
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    {
        //if in iphone
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    }
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}

+ (UIImage*) circularScaleNCrop: (UIImage*) image Rect: (CGRect) rect
{
    // This function returns a newImage, based on image, that has been:
    // - scaled to fit in (CGRect) rect
    // - and cropped within a circle of radius: rectWidth/2
    
    //Create the bitmap graphics context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.height), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Get the width and heights
    CGFloat imageWidth = image.size.width;
    CGFloat imageHeight = image.size.height;
    CGFloat rectWidth = rect.size.width;
    CGFloat rectHeight = rect.size.height;
    
    //Calculate the scale factor
    CGFloat scaleFactorX = rectWidth/imageWidth;
    CGFloat scaleFactorY = rectHeight/imageHeight;
    
    //Calculate the centre of the circle
    CGFloat imageCentreX = rectWidth/2;
    CGFloat imageCentreY = rectHeight/2;
    
    // Create and CLIP to a CIRCULAR Path
    // (This could be replaced with any closed path if you want a different shaped clip)
    CGFloat radius = rectWidth/2;
    CGContextBeginPath (context);
    CGContextAddArc (context, imageCentreX, imageCentreY, radius, 0, 2*M_PI, 0);
    CGContextClosePath (context);
    CGContextClip (context);
    
    //Set the SCALE factor for the graphics context
    //All future draw calls will be scaled by this factor
    CGContextScaleCTM (context, scaleFactorX, scaleFactorY);
    
    // Draw the IMAGE
    CGRect myRect = CGRectMake(0, 0, imageWidth, imageHeight);
    [image drawInRect:myRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - Hashtag Methods
+(NSArray *)rangesOfHashtagsInString:(NSString *)string
{
    NSRegularExpression *hashtagDetector = [[NSRegularExpression alloc] initWithPattern:@"#\\w\\w*"
                                                                                options:NSRegularExpressionCaseInsensitive
                                                                                  error:nil];
    NSArray *hashtagRanges = [hashtagDetector matchesInString:string
                                                      options:NSMatchingWithoutAnchoringBounds
                                                        range:NSMakeRange(0, string.length)];
    return hashtagRanges;
}

+(NSUInteger)numberOfHashtagsInString:(NSString *)string
{
    //NSRegularExpressionOptions
    //NSMatchingOptions
    NSRegularExpression *hashtagDetector = [[NSRegularExpression alloc] initWithPattern:@"#\\w\\w*"
                                                                                options:NSRegularExpressionCaseInsensitive
                                                                                  error:nil];
    NSUInteger numberOfHashtags = [hashtagDetector numberOfMatchesInString:string
                                                                   options:NSMatchingWithoutAnchoringBounds
                                                                     range:NSMakeRange(0, string.length)];
    return numberOfHashtags;
}

+(NSString*) CapitalText: (NSString*) strText
{
    NSString* str = [NSString stringWithFormat: @"%@%@", [strText substringToIndex: 1].uppercaseString, [strText substringFromIndex:1]];
    return str;
}

@end
