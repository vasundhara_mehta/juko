//
//  CUtils.h
//  nur10
//
//  Created by Wony Shin on 10/19/12.
//  Copyright 2013 nur10. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>

#define STR_EXCHANGE @"###***###"

@class IGProgressActivity;
@interface CUtils : NSObject {

}

+ (NSString *)getFilePath:(NSString *)fileName;
+ (BOOL)isFileInDocumentDirectory:(NSString *)fileName;
+ (void)copyFileToDocumentDirectory:(NSString *)fileName;
+ (NSString *)getDocumentDirectory;
+ (NSString *)generateFileName: (BOOL) bMovie;

+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;

// Time and Date
+ (NSString *)convertToLocalTime:(NSString *)GMTtime;
+ (NSString *)convertToLocalTime:(NSString *)strTime timezone: (NSString *) strTimeZone;
+ (NSString *)convertToLocalTime1:(NSString *)strTime timezone: (NSString *) strTimeZone;
+ (NSString *)getCurrentTime;
+ (NSString*) getDateString: (NSString*) strDateTime;

+ (NSInteger)getAge:(NSString *)dateOfBirth;

// image
+ (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)size;

// iPhone 5
+ (BOOL) isIphone5;

+ (void) makeRoundedText: (UITextField *) textField;
+ (BOOL) IsEmptyString : (NSString *) str;

+ (CGFloat) getRealHeight: (NSString *) text size: (CGSize) textSize font: (UIFont *) font min_height: (float) minHeight lineBreakMode:(NSLineBreakMode)lineBreakMode;

+ (NSString *) createMD5:(NSString *)source;
+ (NSString*) getNonce : (NSInteger) nLength;

+ (NSString *) URLEncoding: (NSString *) str;
+ (NSString *) URLDecoding: (NSString *) str;

//+ (double)kilometresBetweenPlace1:(CLLocationCoordinate2D) place1 andPlace2:(CLLocationCoordinate2D) place2;

////UI utils/////////
#pragma mark - UI Utils

+ (void) doCircleView: (UIView*) view;
+ (void) clearView: (UIView*) parent;
+ (void) clearLayers: (UIView*) parent;
+ (void) showActivity: (IGProgressActivity*) HUD Caption: (NSString*) strCaption;
+ (void) hideActivity: (IGProgressActivity*) HUD;

#pragma mark - get value methods from dictionary

+ (NSString*) getStringValueFromDictionary: (NSDictionary*) dict KEY: (NSString*) strKey;
+ (int) getIntValueFromDictionary: (NSDictionary*) dict KEY: (NSString*) strKey;
+ (float) getFloatValueFromDictionary: (NSDictionary*) dict KEY: (NSString*) strKey;

#pragma mark - Image Process

+ (UIImage *) imageWithView:(UIView *)view;
+ (UIImage *) imageWithAllLayer:(UIView *)view;
+ (UIImage *) imageWithLayer:(CALayer*) layer;

+ (UIImage*) imageWithImage: (UIImage*) image cropByRect: (CGRect) clipRect;
+ (UIImage*) imageWithImage:(UIImage*) image scaledToSize:(CGSize)newSize;
+ (UIImage*) mergeImage:(UIImage*) first withImage:(UIImage*) second Mode: (CGBlendMode) mode;
+ (UIImage*) concatImage:(UIImage*) first second:(UIImage*) second;
+ (UIImage *) fixImage:(UIImage *)image;
+ (UIImage*) maskImage: (UIImage*) image withMask: (UIImage*) maskImage;
+ (UIImage*) imageWithMask: (UIImage*) first mask: (UIImage*) second;

+ (CGRect) getRectFromString: (NSString*) strRect;

+(UIImage *)changeWhiteColorTransparent: (UIImage *)image;
+(UIImage *)changeBlackColorTransparent: (UIImage *)image;

+ (UIImage*) circularScaleNCrop: (UIImage*) image Rect: (CGRect) rect;

#pragma mark - Hashtag methods
+(NSArray *)rangesOfHashtagsInString:(NSString *)string;
+(NSUInteger)numberOfHashtagsInString:(NSString *)string;
+(NSString*) CapitalText: (NSString*) strText;

@end
