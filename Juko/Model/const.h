//
//  const.h
//  Lylu
//
//  Created by Mountain on 11/15/13.
//  Copyright (c) 2013 Su Wu. All rights reserved.
//

#ifndef Juko_const_h
#define Juko_const_h

#pragma mark - debug

#define TEST_FACEBOOK_TOKEN @"CAACcCp8cVzoBALd3xAuaEGv4DaMyK3ZBGofcs1NqQjfRIlW4VrZB0AjSt4DYlwbX5mFnskb6CmLZA4tqmcelXelZCZBOJaygfGEvRDs9nNS88bkEHKIkB47vzkQFTLtLZAumZCXnipttXY8J0kkMtZBHIi2Gs6zetIrBpUPV0PgczZBc20tLqZBCpfeoZCh0Bp7ZB3wZD"

#define TEST_EMAIL @""

//#define TEST_MODE_ADD_DATA

#define _DEBUG
#define _SHOW_LOG

#define TEST_MODE
#undef TEST_MODE
////////////////////////////////////////////////////////////////////////////

#define KEYBOARD_HEIGHT 216

//Macros

#define RELEASE(a) if (a) {[a release]; a = nil;}
#define ALERT_SHOW(a) {UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"" message: a delegate: nil cancelButtonTitle: @"Ok" otherButtonTitles: nil]; [alert show];}

//Constants

#define PAGE_COUNT 10

#define CANVAS_SHIPPING 7.99f
#define ITEM_SHIPPING 5.99f

////////Local Keys//////
#define LOCAL_KEY_LOGIN @"login"
#define LOCAL_KEY_FB_ACCESS_TOKEN @"fb_access_token"
#define LOCAL_KEY_FB_USER_ID @"fb_user_id"

#define LOCAL_KEY_IG_ACCESS_TOKEN @"ig_access_token"
#define LOCAL_KEY_IG_LOGIN @"ig_login"

#define LOCAL_KEY_USERNAME @"username"
#define LOCAL_KEY_PASSWORD @"password"


////////Local Values////
#define LOCAL_VALUE_NO_TOKEN @"no_token"
#define LOCAL_VALUE_NO_USER @"no_user"


///////UITableViewCell Identifiers//////

#define CELL_ID_FEED @"FeedCell"

///////API Keys////////


#pragma mark - notification constants

#define MESSAGE_DISCOVERED @"msg_discover"
#define MESSAGE_DISCOVERED_END @"msg_discover_end"

#define MESSAGE_LIKE @"msg_like"
#define MESSAGE_NOPE @"msg_nope"
#pragma mark - constants

#define MAX_ITEM_COUNT 8


//////////////web service part////////////////
#pragma mark - action keys

//#ifdef TEST_MODE
//#define SERVER_URL @"http://192.168.1.186/juko/api/www/"
//#else
#define SERVER_URL @"http://jukoapp.com/api/www/"
//#endif

///actions

//---login & signup
#define API_FBLOGIN @"user/signinWithFacebook"
#define API_LOGIN @"user/signin"
#define API_SIGNUP @"user/signup"

#define API_CATEGORY @"category"

//---news feed
#define API_NEWS_FEED_FIRST @"item/recent"
#define API_NEWS_FEED @"item/recent/"

//---featured
#define API_FEATURED_FIRST @"item/featured"
#define API_FEATURED @"item/featured/"

#define API_SEARCH_HASHTAG @"hashtag/search/%@/"
#define API_SEARCH_USERNAME @"username/search/%@/"

#define API_TAGGED_ITEMS_FIRST @"item/tagged/%@"
#define API_TAGGED_ITEMS @"item/tagged/%@/%d"

//---comments
#define API_COMMENT_LIST @"item/%d/comment"
#define API_POST_COMMENT @"item/%d/comment"

//---like
#define API_POST_LIKE @"item/%d/like"
#define API_POST_UNLIKE @"item/%d/unlike"

//---profile
#define API_USER_PROFILE @"user/%@"
#define API_USER_EXTRAINFO @"user/extraInfo"

#define API_CREATION_LIST_FIRST @"user/%d/item/recent"
#define API_CREATION_LIST @"user/%d/item/recent/"

#define API_LIKED_LIST_FIRST @"user/%d/item/liked"
#define API_LIKED_LIST @"user/%d/item/liked/"

#define API_FOLLOWERS @"user/%d/follower"
#define API_FOLLOWINGS @"user/%d/following"

#define API_FOLLOW @"user/%d/follow"
#define API_UNFOLLOW @"user/%d/unfollow"

#define API_CREATE @"item/create"

///////request param keys/////////

//-user login-//
#define REQUEST_USER @"username"
#define REQUEST_PASSWORD @"password"
#define REQUEST_DEVICE_TOKEN @"device_token"
#define REQUEST_API_KEY @"API_KEY"

#define REQUEST_FB_TOKEN @"access_token"
#define REQUEST_FB_USER_ID @"user_id"

////////response param keys////////
#define RESPONSE_API_KEY @"api_key"

#define RESPONSE_ERROR_MESSAGE @"message"

//- user info -//
#define RESPONSE_USER @"user"
#define RESPONSE_USER_ID @"id"
#define RESPONSE_USER_NAME @"name"
#define RESPONSE_USER_USERNAME @"username"
#define RESPONSE_USER_EMAIL @"email"
#define RESPONSE_USER_ADDRESS @"address"
#define RESPONSE_USER_FACEBOOK_ID @"facebook"
#define RESPONSE_USER_INSTAGRAM_ID @"instagram"
#define RESPONSE_USER_PHONE @"phone"
#define RESPONSE_USER_AVATAR @"profile_image_url"
#define RESPONSE_USER_AVATAR_THUMB @"profile_image_url_thumb"
#define RESPONSE_USER_ITEM_COUNT @"itemCount"
#define RESPONSE_USER_FOLLOWER_COUNT @"followerCount"
#define RESPONSE_USER_FOLLOWING_COUNT @"followingCount"
#define RESPONSE_USER_STATE @"state"
#define RESPONSE_USER_ZIP @"zip"

//- news feed -//
#define RESPONSE_ITEMS @"items"

#define RESPONSE_COMMENTS @"comments"

/////////////////


//////////////////////////////////////////////
//response code

#define SUCCESS_CODE 200

//////////////////////////////////////////////

#pragma mark - fonts

#define FONT_DEFAULT [UIFont fontWithName: @"HelveticaNeue-Light" size: 17.0f]
#define FONT_LARGE [UIFont fontWithName: @"HelveticaNeue-Light" size: 20.0f]
#define FONT_SMALL [UIFont fontWithName: @"HelveticaNeue-Light" size: 12.0f]

#define FONT_SETTING_CELL [UIFont fontWithName: @"HelveticaNeue-Light" size: 16.0f]

#define FONT_NORMAL [UIFont fontWithName: @"HelveticaNeue" size: 16.0f]

#pragma mark - colors

//#define COLOR_DEFAULT [UIColor colorWithRed:69.0/255.0 green:169.0/255.0 blue:228.0/255.0 alpha:1.0]
#define COLOR_DEFAULT [UIColor colorWithRed:0.0/255.0 green:130.0/255.0 blue:232.0/255.0 alpha:1.0]
#define COLOR_DARK_GRAY [UIColor colorWithRed:87.0/255.0 green:95.0/255.0 blue:98.0/255.0 alpha:1.0]
#define COLOR_DEEP_DARK_GRAY [UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0]
#define COLOR_LIGHT_GRAY [UIColor colorWithRed:190.0/255.0 green:196.0/255.0 blue:199.0/255.0 alpha:1.0]
#define COLOR_BUTTON_TEXT_COLOR [UIColor colorWithRed:159.0/255.0 green:161.0/255.0 blue:163.0/255.0 alpha:1.0]

#pragma capture mode enumeration

#define kRefreshTimeInSeconds 0.01

/////notification constants////
#pragma mark - Notification-Constants

//notifications
#define ID_NOTI_POSTED_COMMENT @"PostedComment"
#define ID_NOTI_LIKED_ITEM @"LikedItem"

//notification constants
#define NOTI_ITEM_ID @"item_id"
///////////////////////////////////////////////


//////////TableViewCell IDS//////////
#define STR_USER_CELL @"UserCell"

//////////////////////////////////////
///blank item
#define CATEGORY_COUNT 10

#define CATEGORY_EDIT_IMAGE @"edit_image"
#define CATEGORY_PUBLISH_IMAGE @"publish_image"
#define CATEGORY_NAME @"name"
#define CATEGORY_PRICE @"price"
#define CATEGORY_ID @"id"
#define CATEGORY_IMAGE @"image"
#define CATEGORY_HAS_PUBLISH_IMAGE @"has_publish_image"
#define CATEGORY_PRODUCTS @"products"

#define MAX_COMMENT_COUNT 5

typedef enum _ECategory
{
    ECATEGORY_CASE = 1,
    ECATEGORY_SKIN,
    ECATEGORY_CANVAS,
    ECATEGORY_MUG,
    ECATEGORY_COASTER,
    ECATEGORY_BLOCKS,
    ECATEGORY_PLAQUES,
    ECATEGORY_MOUSE,
    ECATEGORY_SHIRTS,
} ECategory;

#pragma mark - Share Text

//#define SHARE_DEFAULT_TEXT @"Hey, \nWhether you are an Artist, Casual Photographer, Or Looking For Pure Inspiration. Juko is a revolutionary new social network that allows you to browse, create, share, and sell your latest masterpiece. Drag, drop, and edit your photos directly on our products and share them for the world to see! Join The hottest social network today, Unleash Your inner artistry today! \n"
//#define SHARE_TWITTER_TEXT @"Juko is a revolutionary new social network that allows you to browse/create/share/sell your latest masterpiece.\n"

#define SHARE_DEFAULT_TEXT @"This is the coolest new app for photographers and iPhoneographers alike. With different customizable products as your canvas, the sky is the limit as to how far you will go. If its a birthday mug for mom or your new in-app design business. Juko awaits #getcreating\n"

#define SHARE_TWITTER_TEXT @"This is the coolest new app for photographers and iPhoneographers alike. Juko awaits #getcreating\n"

/////////for payment////////////
#define STPCardErrorUserMessage NSLocalizedString(@"Your card is invalid", @"Error when the card is not valid")

//#ifdef TEST_MODE
//#define STRIPE_PUBLISHABLE_KEY @"pk_test_MFuvNtuDyAeMVDcuCAXPWHQX"
//#else
//#define STRIPE_PUBLISHABLE_KEY @"pk_live_adCy4csIwHSkulF4WKV4kTvj"
//#endif

#endif
