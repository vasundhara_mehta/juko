//
//  IGComment.h
//  Juko
//
//  Created by Mountain on 2/12/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IGItemUser;
@interface IGComment : NSObject

@property int m_nIndex;
@property (nonatomic, strong) NSString* m_strComment;
@property (nonatomic, strong) IGItemUser* m_clsUser;
@property (nonatomic, strong) NSString* m_strTime;

- (id) initWithDictionay: (NSDictionary*) dictComment;

@end
