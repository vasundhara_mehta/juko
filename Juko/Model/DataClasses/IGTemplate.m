//
//  IGTemplate.m
//  Juko
//
//  Created by Mountain on 3/12/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGTemplate.h"

#import "CUtils.h"

@implementation IGTemplate

@synthesize m_Rects, m_nCols, m_nIndex, m_nRows;

- (id) initWithDictionary: (NSDictionary*) dictTemplate
{
    if (self = [super init]) {
        m_nIndex = [CUtils getIntValueFromDictionary: dictTemplate KEY: @"id"];
        m_nRows = [CUtils getIntValueFromDictionary: dictTemplate KEY: @"rows"];
        m_nCols = [CUtils getIntValueFromDictionary: dictTemplate KEY: @"cols"];
        m_Rects = [dictTemplate objectForKey: @"rects"];
    }
    return self;
}

- (CGRect) getRectByIndex: (int) nIndex Invert: (BOOL) FInvert
{
    if (nIndex < m_Rects.count) {
        NSString* strRect = m_Rects[nIndex];
        NSArray* arrayNums = [strRect componentsSeparatedByString: @","];
        if (FInvert) {
            CGRect rect = CGRectMake([arrayNums[0] integerValue], [arrayNums[1] integerValue], [arrayNums[2] integerValue], [arrayNums[3] integerValue]);
            int nValue = rect.origin.x;
            rect.origin.x = rect.origin.y;
            rect.origin.y = m_nCols - nValue - rect.size.width;
            return rect;
        } else {
            return CGRectMake([arrayNums[0] integerValue], [arrayNums[1] integerValue], [arrayNums[2] integerValue], [arrayNums[3] integerValue]);
        }
    }
    return CGRectZero;
}

@end
