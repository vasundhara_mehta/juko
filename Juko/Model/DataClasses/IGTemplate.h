//
//  IGTemplate.h
//  Juko
//
//  Created by Mountain on 3/12/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTemplate : NSObject

@property int m_nIndex;
@property int m_nRows;
@property int m_nCols;
@property (nonatomic, strong) NSArray* m_Rects;

- (id) initWithDictionary: (NSDictionary*) dictTemplate;
- (CGRect) getRectByIndex: (int) nIndex Invert: (BOOL) FInvert;

@end
