//
//  IGItem.h
//  Juko
//
//  Created by Mountain on 2/8/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IGItemUser;
@interface IGItem : NSObject

@property int m_nIndex;
@property int m_nCategoryIndex;
@property (nonatomic, strong) NSString* m_strDescription;
@property int m_nPushlish;
@property int m_nSell;
@property int m_nLikeCount;
@property int m_nCommentCount;
@property BOOL m_FLiked;
@property float m_rPrice;
@property float m_rProfit;
@property (nonatomic, strong) NSString* m_strName;
@property (nonatomic, strong) NSString* m_strRealName;
@property float m_rShippingPrice;
@property (nonatomic, strong) NSString* m_strTime;

@property (nonatomic, strong) IGItemUser* m_clsUser;
@property (nonatomic, strong) NSArray* m_arrayImageUrls;

@property (nonatomic, strong) NSMutableArray* m_arrayComments;

- (id) initWithDictionay: (NSDictionary*) dictItem;

@end
