//
//  IGUser.h
//  Juko
//
//  Created by Mountain on 2/4/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 "api_key" = "NTJlZmI5YWU5MTk0NQ==";
 code = 200;
 user =     {
 address = "4 B Blue Ridge Blvd";
 email = "demo@demo.com";
 facebook = 1054105928;
 instagram = "<null>";
 name = "Josephine Darakjy";
 phone = 9079212010;
 "profile_image_url" = "http://localhost/Juko/common/www/assets/profile_image/\\Liu+Yi+Fei.jpg";
 state = MO;
 username = demoa;
 zip = 10016;
 };

*/
@interface IGUser : NSObject

@property int m_nUserId;
@property (strong, nonatomic) NSString* m_strUsername;
@property (strong, nonatomic) NSString* m_strEmail;
@property (strong, nonatomic) NSString* m_strName;
@property (strong, nonatomic) NSString* m_strAddress;
@property (strong, nonatomic) NSString* m_strFacebookId;
@property (strong, nonatomic) NSString* m_strInstagramId;
@property (strong, nonatomic) NSString* m_strPhone;
@property (strong, nonatomic) NSString* m_strAvatar;
@property (strong, nonatomic) NSString* m_strAvatarThumb;
@property (strong, nonatomic) NSString* m_strZip;
@property (strong, nonatomic) NSString* m_strState;
@property (strong, nonatomic) NSString* m_strCity;
@property (strong, nonatomic) NSString* m_strAdditionalAddress;

@property (nonatomic, strong) NSString* m_strDownloadLink;

- (id) initWithDictionary: (NSDictionary*) dictUser;

@end
