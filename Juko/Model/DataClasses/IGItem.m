//
//  IGItem.m
//  Juko
//
//  Created by Mountain on 2/8/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGItem.h"
#import "IGItemUser.h"
#import "IGComment.h"
#import "CUtils.h"

#import "const.h"

@implementation IGItem

/*              'id' => $item->id,
                'description' => $item->description,
                'publish' => $item->publish,
                'sell' => $item->sell,
                'likeCount' => $item->likeCount,
                'commentCount' => $item->commentCount,
                'user' => array(
                    'id' => $item->user->id,
                    'username' => $item->user->username,
                    'name' => $item->user->profile->name,
                    'profile_image_url' => $item->user->profile->profile_image_url,
                ),*/

- (id) initWithDictionay: (NSDictionary*) dictItem
{
    if (self = [super init]) {
        NSDictionary* dictProduct = [dictItem objectForKey: @"product"];
        NSDictionary* dictCategory = [dictItem objectForKey: @"category"];
        
        _m_nIndex = [[dictItem objectForKey: @"id"] intValue];
        _m_nCategoryIndex = [CUtils getIntValueFromDictionary: dictCategory KEY: @"id"];
        _m_strDescription = [CUtils getStringValueFromDictionary: dictItem KEY: @"description"];
        _m_strName = [CUtils getStringValueFromDictionary: dictProduct KEY: @"name"];
        _m_rShippingPrice = [CUtils getFloatValueFromDictionary: dictProduct KEY: @"shipping_cost"];
        
        NSString* strTempName = [[[_m_strName stringByReplacingOccurrencesOfString: @"Vertical" withString: @""] stringByReplacingOccurrencesOfString: @"Horizontal" withString: @""] stringByReplacingOccurrencesOfString: @"Skin" withString: @""];
        if (_m_nCategoryIndex == ECATEGORY_SHIRTS) {
            _m_strRealName = [NSString stringWithFormat: @"%@ T-Shirt", [strTempName substringToIndex: strTempName.length - 1]];
        } else if (_m_nCategoryIndex == ECATEGORY_CASE) {
            _m_strRealName = [NSString stringWithFormat: @"%@ Case", strTempName];
        } else if (_m_nCategoryIndex == ECATEGORY_SKIN) {
            _m_strRealName = [NSString stringWithFormat: @"%@ Skin", strTempName];
        } else {
            _m_strRealName = [strTempName copy];
        }
                
        _m_nPushlish = [[dictItem objectForKey: @"publish"] intValue];
        _m_nSell = [[dictItem objectForKey: @"sell"] intValue];
        _m_nLikeCount = [[dictItem objectForKey: @"likeCount"] intValue];
        _m_nCommentCount = [[dictItem objectForKey: @"commentCount"] intValue];
        _m_clsUser = [[IGItemUser alloc] initWithDictionary: [dictItem objectForKey: @"user"]];
        _m_arrayImageUrls = [dictItem objectForKey: @"feed_image_url"];
        _m_FLiked = [[dictItem objectForKey: @"like"] boolValue];
        
        _m_strTime = [dictItem objectForKey: @"time_ago_in_words"];
        
        _m_arrayComments = [[NSMutableArray alloc] init];
        NSArray* arrayComments = [dictItem objectForKey: @"comments"];
        
        if (arrayComments) {
            for (int nIdx = (int)arrayComments.count - 1; nIdx > - 1; nIdx --) {
                NSDictionary* dictComment = arrayComments[nIdx];
                IGComment* clsComment = [[IGComment alloc] initWithDictionay: dictComment];
                [_m_arrayComments addObject: clsComment];
            }
        }
        
        _m_rPrice = [CUtils getFloatValueFromDictionary: [dictItem objectForKey: @"price"] KEY: @"amount"];
        _m_rProfit = [CUtils getFloatValueFromDictionary: [dictItem objectForKey: @"price"] KEY: @"profit"];
    }
    return self;
}

@end
