

#import "IGItemUser.h"

/*
                    'id' => $item->user->id,
                    'username' => $item->user->username,
                    'name' => $item->user->profile->name,
                    'profile_image_url' => $item->user->profile->profile_image_url,
 */
@implementation IGItemUser

- (id) initWithDictionary: (NSDictionary*) dictUser
{
    if (self = [super init]) {
        _m_nUserId = [[dictUser objectForKey: @"id"] intValue];
        _m_strUsername = [dictUser objectForKey: @"username"];
        _m_strProfileName = [dictUser objectForKey: @"name"];
        _m_strImageUrl = [dictUser objectForKey: @"profile_image_url"];
        _m_strImageThumbUrl = [dictUser objectForKey: @"profile_image_url_thumb"];
    }
    return self;
}

@end
