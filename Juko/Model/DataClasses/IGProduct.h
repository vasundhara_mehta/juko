//
//  IGProduct.h
//  Juko
//
//  Created by Mountain on 3/12/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGProduct : NSObject

@property BOOL m_FLoaded;
@property int m_nCategoryIndex;
@property int m_nIndex;
@property (nonatomic, retain) NSString* m_strTemplateIds;
@property (nonatomic, retain) NSArray* m_templates; //template ids
@property (nonatomic, retain) NSString* m_strName;
@property (nonatomic, retain) NSString* m_strRealName;
@property (nonatomic, retain) NSString* m_strDescription;
@property float m_rRate;
@property float m_rPrice;
@property (nonatomic, retain) NSString* m_strExampleImage;
@property int m_nWidth;
@property int m_nHeight;

@property CGFloat m_rPrintWidth;
@property CGFloat m_rPrintHeight;

@property (nonatomic, strong) NSString* m_strEditImage;
@property (nonatomic, strong) NSString* m_strPubishImage;
@property float m_rAngle;

@property BOOL m_FChoose;

- (id) initWithDictionary: (NSDictionary*) dictProduct CategoryId: (int) nCategoryId;

@end
