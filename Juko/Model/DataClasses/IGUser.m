//
//  IGUser.m
//  Juko
//
//  Created by Mountain on 2/4/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGUser.h"

#import "const.h"
#import "CUtils.h"

@implementation IGUser

@synthesize m_strAddress, m_strAvatar, m_strEmail, m_strFacebookId, m_strInstagramId, m_strName, m_strPhone, m_strState, m_strUsername, m_strZip, m_strDownloadLink, m_nUserId, m_strAdditionalAddress, m_strCity, m_strAvatarThumb;

- (id) initWithDictionary: (NSDictionary*) dictUser
{
    if (self = [super init]) {
        m_nUserId = [CUtils getIntValueFromDictionary: dictUser KEY: @"id"];
        m_strUsername = [dictUser objectForKey: RESPONSE_USER_USERNAME];
        m_strEmail = [dictUser objectForKey: RESPONSE_USER_EMAIL];
        m_strName = [dictUser objectForKey: RESPONSE_USER_NAME];
        m_strAvatar = [dictUser objectForKey: RESPONSE_USER_AVATAR];
        m_strAvatarThumb = [dictUser objectForKey: RESPONSE_USER_AVATAR_THUMB];
        m_strAddress = [dictUser objectForKey: RESPONSE_USER_ADDRESS];
        m_strFacebookId = [dictUser objectForKey: RESPONSE_USER_FACEBOOK_ID];
        m_strInstagramId = [dictUser objectForKey: RESPONSE_USER_INSTAGRAM_ID];
        m_strPhone = [dictUser objectForKey: RESPONSE_USER_PHONE];
        m_strZip = [dictUser objectForKey: RESPONSE_USER_ZIP];
        m_strState = [dictUser objectForKey: RESPONSE_USER_STATE];
        
        m_strCity = [CUtils getStringValueFromDictionary: dictUser KEY: @"city"];
        m_strAdditionalAddress = [CUtils getStringValueFromDictionary: dictUser KEY: @"additional_address_info"];
        
        m_strDownloadLink = [dictUser objectForKey: @"download_link"];
    }
    return self;
}

@end
