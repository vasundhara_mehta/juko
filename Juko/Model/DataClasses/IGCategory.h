//
//  IGCategory.h
//  Inkgram
//
//  Created by Mountain on 2/27/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGCategory : NSObject

@property int m_nIndex;
@property (nonatomic, strong) NSString* m_strImageName;
@property (nonatomic, strong) NSString* m_strName;
@property (nonatomic, strong) NSString* m_strDescription;
@property (nonatomic, strong) NSString* m_strLocalDescription;
@property float m_rPrice;
@property int m_nPublishCount;
@property (nonatomic, retain) NSMutableArray* m_ProductList;

@property BOOL m_FChoose;

- (id) initWithDictionary: (NSDictionary*) dictItem;

@end
