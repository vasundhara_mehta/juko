

#import <Foundation/Foundation.h>

@interface IGItemUser : NSObject

@property int m_nUserId;
@property (nonatomic, strong) NSString* m_strUsername;
@property (nonatomic, strong) NSString* m_strProfileName;
@property (nonatomic, strong) NSString* m_strImageUrl;
@property (nonatomic, strong) NSString* m_strImageThumbUrl;

- (id) initWithDictionary: (NSDictionary*) dictUser;

@end
