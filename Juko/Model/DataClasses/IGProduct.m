//
//  IGProduct.m
//  Juko
//
//  Created by Mountain on 3/12/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGProduct.h"
#import "CUtils.h"
#import "const.h"

@implementation IGProduct

@synthesize m_templates, m_nCategoryIndex, m_nIndex, m_strRealName, m_FLoaded;
@synthesize m_rAngle, m_strEditImage, m_strPubishImage, m_strTemplateIds;
@synthesize m_strDescription, m_strExampleImage, m_strName, m_nHeight, m_nWidth, m_rPrice, m_rRate, m_FChoose;
@synthesize m_rPrintHeight, m_rPrintWidth;

- (id) initWithDictionary: (NSDictionary*) dictProduct CategoryId: (int) nCategoryId
{
    if (self = [super init]) {
        m_nIndex = [CUtils getIntValueFromDictionary: dictProduct KEY: @"id"];
        m_nCategoryIndex = nCategoryId;
        m_strName = [CUtils getStringValueFromDictionary: dictProduct KEY: @"name"];
        NSString* strTempName = [[[m_strName stringByReplacingOccurrencesOfString: @"Vertical" withString: @""] stringByReplacingOccurrencesOfString: @"Horizontal" withString: @""] stringByReplacingOccurrencesOfString: @"Skin" withString: @""];
        
        if (m_nCategoryIndex == ECATEGORY_SHIRTS) {
            m_strRealName = [NSString stringWithFormat: @"%@ T-Shirt", [strTempName substringToIndex: strTempName.length - 1]];
        } else if (m_nCategoryIndex == ECATEGORY_CASE) {
            m_strRealName = [NSString stringWithFormat: @"%@ Case", strTempName];
        } else if (m_nCategoryIndex == ECATEGORY_SKIN) {
            m_strRealName = [NSString stringWithFormat: @"%@ Skin", strTempName];
        } else {
            m_strRealName = [strTempName copy];
        }

//        if (m_nCategoryIndex == ECATEGORY_SHIRTS) {
//            m_strRealName = [NSString stringWithFormat: @"%@ T-Shirt", [strTempName substringToIndex: strTempName.length - 1]];
//        } else {
//            m_strRealName = [strTempName copy];
//        }

        m_strEditImage = [CUtils getStringValueFromDictionary: dictProduct KEY: @"editname"];
        m_strDescription = [CUtils getStringValueFromDictionary: dictProduct KEY: @"description"];
        m_strExampleImage = [CUtils getStringValueFromDictionary: dictProduct KEY: @"image"];
        
        m_rPrice = [CUtils getFloatValueFromDictionary: dictProduct KEY: @"price"];
        m_nWidth = [CUtils getIntValueFromDictionary: dictProduct KEY: @"width"];
        m_nHeight = [CUtils getIntValueFromDictionary: dictProduct KEY: @"height"];
        
        m_rAngle = [CUtils getFloatValueFromDictionary: dictProduct KEY: @"y_angle"];
        
        m_rPrintWidth = [CUtils getFloatValueFromDictionary: dictProduct KEY: @"print_width"]; //inches
        m_rPrintHeight = [CUtils getFloatValueFromDictionary: dictProduct KEY: @"print_height"]; //inches
        
        m_strTemplateIds = [CUtils getStringValueFromDictionary: dictProduct KEY: @"templates"];
        m_templates = [m_strTemplateIds componentsSeparatedByString: @","];
    }
    return self;
}

@end
