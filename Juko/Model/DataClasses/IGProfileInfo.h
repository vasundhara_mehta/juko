//
//  IGProfileInfo.h
//  Juko
//
//  Created by Mountain on 2/18/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGProfileInfo : NSObject

@property int m_nUserIndex;
@property (strong, nonatomic) NSString* m_strUsername;
@property (strong, nonatomic) NSString* m_strName;
@property (strong, nonatomic) NSString* m_strBio;
@property (strong, nonatomic) NSString* m_strAddress;
@property (strong, nonatomic) NSString* m_strAvatar;
@property (strong, nonatomic) NSString* m_strAvatarThumb;
@property (strong, nonatomic) NSString* m_strZip;
@property (strong, nonatomic) NSString* m_strState;
@property (strong, nonatomic) NSString* m_strCity;
@property (strong, nonatomic) NSString* m_strAdditionalAddress;
@property BOOL m_FFollow;
@property int m_nFollowerCount;
@property int m_nFollowingCount;
@property int m_nCreationCount;
@property int m_nPrivacy;
@property (nonatomic, strong) NSString* m_strEmail;

@property (nonatomic, strong) NSDictionary* m_dictBank;

- (id) initWithDictionary: (NSDictionary*) dictUser;

@end
