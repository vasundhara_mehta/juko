//
//  IGProfileInfo.m
//  Juko
//
//  Created by Mountain on 2/18/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGProfileInfo.h"
#import "const.h"
#import "CUtils.h"

/*
 address = "4 B Blue Ridge Blvd";
 followerCount = 2;
 followingCount = 0;
 name = "Josephine Darakjy";
 "profile_image_url" = "http://192.168.1.186/inkgram/common/www/assets/profile_image/d09db55311bfb366434777143ad87be1.jpg";
 "profile_image_url_thumb" = "http://192.168.1.186/inkgram/common/www/assets/profile_image/d09db55311bfb366434777143ad87be1_thumb.jpg";
 state = MO;
 username = demoa;
 zip = 10016;
 */

@implementation IGProfileInfo

@synthesize m_strAddress, m_strAvatar, m_strName, m_strState, m_strBio, m_strUsername, m_strZip, m_nCreationCount, m_nFollowerCount, m_nFollowingCount, m_strAvatarThumb, m_nUserIndex, m_nPrivacy, m_strEmail, m_dictBank, m_strAdditionalAddress, m_strCity, m_FFollow;

- (id) initWithDictionary: (NSDictionary*) dictUser
{
    if (self = [super init]) {
        m_nUserIndex = [CUtils getIntValueFromDictionary: dictUser KEY: RESPONSE_USER_ID];//[[dictUser objectForKey: RESPONSE_USER_ID] integerValue];
        m_strUsername = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_USERNAME];//[dictUser objectForKey: RESPONSE_USER_USERNAME];
        m_strName = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_NAME];//[dictUser objectForKey: RESPONSE_USER_NAME];
        m_strBio = [CUtils getStringValueFromDictionary: dictUser KEY: @"bio"];
        m_strAvatar = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_AVATAR];//[dictUser objectForKey: RESPONSE_USER_AVATAR];
        m_strAvatarThumb = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_AVATAR_THUMB];//[dictUser objectForKey: RESPONSE_USER_AVATAR_THUMB];
        m_strCity = [CUtils getStringValueFromDictionary: dictUser KEY: @"city"];
        m_strAdditionalAddress = [CUtils getStringValueFromDictionary: dictUser KEY: @"additional_address_info"];
        m_strAddress = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_ADDRESS];//[dictUser objectForKey: RESPONSE_USER_ADDRESS];
        m_strZip = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_ZIP];//[dictUser objectForKey: RESPONSE_USER_ZIP];
        m_strState = [CUtils getStringValueFromDictionary: dictUser KEY: RESPONSE_USER_STATE];//[dictUser objectForKey: RESPONSE_USER_STATE];
        m_nCreationCount = [CUtils getIntValueFromDictionary: dictUser KEY: RESPONSE_USER_ITEM_COUNT];//[[dictUser objectForKey: RESPONSE_USER_ITEM_COUNT] integerValue];
        m_nFollowingCount = [CUtils getIntValueFromDictionary: dictUser KEY: RESPONSE_USER_FOLLOWING_COUNT];//[[dictUser objectForKey: RESPONSE_USER_FOLLOWING_COUNT] integerValue];
        m_nFollowerCount = [CUtils getIntValueFromDictionary: dictUser KEY: RESPONSE_USER_FOLLOWER_COUNT];//[[dictUser objectForKey: RESPONSE_USER_FOLLOWER_COUNT] integerValue];
        m_nPrivacy = [CUtils getIntValueFromDictionary: dictUser KEY: @"privacy"];
        m_strEmail = [CUtils getStringValueFromDictionary: dictUser KEY: @"email"];
        
        m_FFollow = NO;
        id objFollow = [dictUser objectForKey: @"follow"];
        if (objFollow && ![objFollow isKindOfClass: [NSNull class]]) {
            m_FFollow = [objFollow boolValue];
        }
        
        m_dictBank = [dictUser objectForKey: @"bank_account"];
    }
    return self;
}

@end
