

#import "IGCategory.h"
#import "IGProduct.h"
#import "CUtils.h"
#import "const.h"

@implementation IGCategory

@synthesize m_rPrice, m_strName, m_nIndex, m_strImageName, m_ProductList, m_FChoose, m_strDescription, m_strLocalDescription;

- (id) initWithDictionary: (NSDictionary*) dictItem
{
    if (self = [super init]) {
        m_nIndex = [CUtils getIntValueFromDictionary: dictItem KEY: CATEGORY_ID];
        m_rPrice = [CUtils getFloatValueFromDictionary: dictItem KEY: CATEGORY_PRICE];
        m_strImageName = [CUtils getStringValueFromDictionary: dictItem KEY: CATEGORY_IMAGE];
        m_strName = [CUtils getStringValueFromDictionary: dictItem KEY: CATEGORY_NAME];
        m_strDescription = @"";
        m_strLocalDescription = [CUtils getStringValueFromDictionary: dictItem KEY: @"description"];
        
        m_ProductList = [[NSMutableArray alloc] init];
        NSArray* products = [dictItem objectForKey: CATEGORY_PRODUCTS];
        
        for (NSDictionary* dictProduct in products) {
            IGProduct* clsProduct = [[IGProduct alloc] initWithDictionary: dictProduct CategoryId: m_nIndex];
            [m_ProductList addObject: clsProduct];
        }
    }
    return self;
}

@end
