//
//  IGComment.m
//  Juko
//
//  Created by Mountain on 2/12/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGComment.h"
#import "IGItemUser.h"
#import "CUtils.h"

@implementation IGComment

- (id) initWithDictionay: (NSDictionary*) dictComment
{
    if (self = [super init]) {
        if (dictComment != nil) {
            _m_nIndex = [CUtils getIntValueFromDictionary: dictComment KEY: @"id"];
            _m_strComment = [dictComment objectForKey: @"comment"];
            _m_strTime = [dictComment objectForKey: @"time_ago_in_words"];
            if (_m_strTime && _m_strTime.length > 5) {
                _m_strTime = @"1s";
            }
            NSDictionary* dictUser = [dictComment objectForKey: @"user"];
            _m_clsUser = [[IGItemUser alloc] initWithDictionary: dictUser];
        }
    }
    return self;
}

@end
