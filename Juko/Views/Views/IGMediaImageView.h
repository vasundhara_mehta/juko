//
//  IGMediaImageView.h
//  Inkgram
//
//  Created by Mountain on 2/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGMediaImageView : UIView

@property id delegate;

@property (nonatomic, retain) UIImageView* m_imageView;
@property (nonatomic, retain) UIView* bgView;

- (void) configureWithImage: (UIImage*) image;
- (void) configureWithURL: (NSString*) strUrl;

@end
