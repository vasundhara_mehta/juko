//
//  IGLayoutView.m
//  Inkgram
//
//  Created by Mountain on 2/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGLayoutView.h"

#import "IGCategory.h"
#import "IGProduct.h"
#import "IGTemplate.h"

#import "CUtils.h"
#import "DataKeeper.h"
#import "const.h"

#import "TKDragView.h"

#import "UIImage+CATransform3D.h"

#import <GPUImage.h>
#import "MugView.h"

#import "BlockView.h"

#import "UIImage+Trim.h"


#define CONTENT_VIEW_WIDTH 250
#define CONTENT_VIEW_HEIGHT 350

@implementation IGLayoutView
{
    BOOL m_FFirstDraw;
}

@synthesize m_clsTemplate, m_clsProduct, m_nTemplateIndex;
@synthesize bgView, m_bgImageView, m_layoutView, m_maskImageView, m_cameraImageView, m_resultImageView;
@synthesize m_LayoutRects;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    
    return self;
}

- (void) configure: (int) nTemplateId Product: (IGProduct*) clsProduct;
{
    m_LayoutRects = [[NSMutableArray alloc] init];
    
    m_FFirstDraw = YES;
    
    m_clsProduct = clsProduct;
    m_nTemplateIndex = nTemplateId;
    
    bgView = [self viewWithTag: 1];
    
    m_bgImageView = (UIImageView*)[bgView viewWithTag: 100];
    m_layoutView = [bgView viewWithTag: 101];
    m_maskImageView = (UIImageView*)[bgView viewWithTag: 102];
    m_resultImageView = (UIImageView*)[bgView viewWithTag: 104];
    [self getLayoutRects];
}

- (void) getLayoutRects
{
    [m_LayoutRects removeAllObjects];
    
    if (m_nTemplateIndex < 100) {
        DataKeeper* clsModel = [DataKeeper sharedInstance];
        NSArray* templates = [clsModel getTemplateList];
        
        IGTemplate* clsTemplate = [templates objectAtIndex: abs(m_nTemplateIndex)];
        CGSize wndSize = m_bgImageView.bounds.size;
        int nWidth = m_clsProduct.m_nWidth;
        int nHeight = m_clsProduct.m_nHeight;
        if (nWidth >= nHeight) {
            wndSize.height *= ((float)nHeight / (float)nWidth);
        } else {
            wndSize.width *= ((float)nWidth / (float)nHeight);
        }

        CGSize cellSize = (m_nTemplateIndex >= 0) ? CGSizeMake(wndSize.width / (float)clsTemplate.m_nCols, wndSize.height / (float)clsTemplate.m_nRows) : CGSizeMake(wndSize.width / (float)clsTemplate.m_nRows, wndSize.height / (float)clsTemplate.m_nCols);
        
        for (int nIdx = 0; nIdx < clsTemplate.m_Rects.count; nIdx ++) {
            CGRect rect = [clsTemplate getRectByIndex: nIdx Invert: ((m_nTemplateIndex >= 0) ? NO : YES)];
            rect.origin.x *= cellSize.width;
            rect.origin.y *= cellSize.height;
            rect.size.width *= cellSize.width;
            rect.size.height *= cellSize.height;
            
            [m_LayoutRects addObject: [NSString stringWithFormat: @"%f,%f,%f,%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height]];
        }
    } else {
        int nWidth = m_bgImageView.bounds.size.width / 3.0;
        int nHeight = nWidth;
        
        if (m_nTemplateIndex == 100) {
            nHeight = m_bgImageView.bounds.size.width / 2.5;
        }
        
        if ([m_clsProduct.m_strName isEqualToString: @"BlueSweatShirt"]) {
            nWidth *= 0.7;
            nHeight *= 0.7;
        }
        
        CGRect rect = CGRectMake(0, 0, nWidth, nHeight);
        
        [m_LayoutRects addObject: [NSString stringWithFormat: @"%f,%f,%f,%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height]];
    }
}

- (void) drawRect:(CGRect)rect
{
    if (m_FFirstDraw) {
        [self resizeViews];
        
        [self setLayout];
        
        //background setting
        if (m_clsProduct.m_strEditImage == nil || [m_clsProduct.m_strEditImage isEqualToString: @""]) {
            m_clsProduct.m_strEditImage = m_clsProduct.m_strName;
            if (m_clsProduct.m_nCategoryIndex == ECATEGORY_SKIN) {
                m_clsProduct.m_strEditImage = [NSString stringWithFormat: @"skin_%@", m_clsProduct.m_strName];
            }
        }
        switch (m_clsProduct.m_nCategoryIndex) {
            case ECATEGORY_CASE:
                [m_bgImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]]];
                [m_maskImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strEditImage]]];
                break;
            case ECATEGORY_SKIN:
                [m_bgImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]]];
                [m_maskImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strEditImage]]];
                break;
            case ECATEGORY_MUG:
                [m_bgImageView setBackgroundColor: [UIColor colorWithWhite: 0.76 alpha: 1.0]];
                break;
            case ECATEGORY_COASTER:
                if ([m_clsProduct.m_strName rangeOfString: @"Square"].length > 0) { //square coaster
                    [m_bgImageView setBackgroundColor: [UIColor colorWithWhite: 0.8 alpha: 1.0]];
                    
                    [m_bgImageView.layer setCornerRadius: 30];
                    [m_layoutView.layer setCornerRadius: 30];
                } else { //round coaster
                    [m_bgImageView setBackgroundColor: [UIColor colorWithWhite: 0.8 alpha: 1.0]];
                    [m_bgImageView.layer setCornerRadius: m_bgImageView.frame.size.width / 2];
                    [m_layoutView.layer setCornerRadius: m_bgImageView.frame.size.width / 2];
                }
                break;
            case ECATEGORY_CANVAS:
            {
                [m_bgImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strName]]];
                [m_maskImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strName]]];
//                [self setShadow: bgView.layer];
                break;
            }
            case ECATEGORY_PLAQUES:
                [m_bgImageView setBackgroundColor: [UIColor colorWithWhite: 0.8 alpha: 1.0]];
                
                [m_bgImageView.layer setCornerRadius: 5];
                [m_layoutView.layer setCornerRadius: 5];
                break;
            case ECATEGORY_BLOCKS:
                [m_bgImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]]];
                [m_maskImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strEditImage]]];
                break;
            case ECATEGORY_MOUSE:
                [m_bgImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@_addmask.png", m_clsProduct.m_strEditImage]]];
                [m_maskImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strEditImage]]];
                break;
            case ECATEGORY_SHIRTS:
                [m_bgImageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]]];
                if (m_nTemplateIndex == 102) {
                    [CUtils doCircleView: m_layoutView];
                }
                break;
            default:
                break;
        }
        
        m_FFirstDraw = NO;
    }
}

- (void) setShadow: (CALayer*) layer
{
//    layer.borderColor = [UIColor blackColor].CGColor;
//    layer.borderWidth = 1.0;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:layer.bounds];
    [layer setShadowOffset:CGSizeMake(0, 5.0)];
    layer.masksToBounds = NO;
//    [layer setShadowRadius:1.5f];
    [layer setShadowColor:[[UIColor blackColor] CGColor]];
    [layer setShadowOpacity:0.2];
    layer.shadowPath = shadowPath.CGPath;
}

- (void) resizeViews
{
    CGSize wndSize = bgView.frame.size;
    int nWidth = m_clsProduct.m_nWidth;
    int nHeight = m_clsProduct.m_nHeight;
    if (nWidth >= nHeight) {
        wndSize.height *= ((float)nHeight / (float)nWidth);
    } else {
        wndSize.width *= ((float)nWidth / (float)nHeight);
    }
    
    [bgView setFrame: CGRectMake(0, 0, wndSize.width + 4, wndSize.height + 4)];
    bgView.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    
    [m_bgImageView setFrame: CGRectMake(0, 0, wndSize.width, wndSize.height)];
    CGPoint ptCenter = CGPointMake(bgView.frame.size.width / 2, bgView.frame.size.height / 2);
    [m_bgImageView setCenter: ptCenter];
    
    if (m_nTemplateIndex < 100) {
        CGRect rect = m_bgImageView.frame;
        
        [m_layoutView setFrame: rect];
        m_layoutView.layer.masksToBounds = YES;
        
        [m_maskImageView setFrame: rect];
        [m_resultImageView setFrame: rect];
    } else {
        CGRect rect = [CUtils getRectFromString: m_LayoutRects[0]];//m_bgImageView.frame;
        rect.origin.x = ptCenter.x - rect.size.width / 2.0;
        rect.origin.y = ptCenter.y - rect.size.height / 1.6;
        
        [m_layoutView setFrame: rect];
        m_layoutView.layer.masksToBounds = YES;
        
        [m_maskImageView setFrame: rect];
        [m_resultImageView setFrame: rect];
    }
}

- (void) setLayout
{
    [CUtils clearView: m_layoutView];
    
    int nIndex = 0;
    
    for (int nIdx = 0; nIdx < m_LayoutRects.count; nIdx ++) {
        CGRect rect = [CUtils getRectFromString: m_LayoutRects[nIdx]];
        
        UIImageView* imgView = [[UIImageView alloc] initWithFrame: rect];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.autoresizesSubviews = YES;
        imgView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth);
        imgView.tag = nIndex + 1000;
        imgView.clipsToBounds = YES;
//        imgView.userInteractionEnabled = YES;
        [m_layoutView addSubview: imgView];

        UIImageView* gridMain = (UIImageView*)[self viewWithTag: 104];
        UIImageView* imgGrid = [[UIImageView alloc] initWithFrame: rect];
        imgGrid.tag = nIndex + 3000;
        if (m_nTemplateIndex == 102) {
            imgGrid.image = [UIImage imageNamed: @"bg_template_circle_grid.png"];
        } else {
            imgGrid.image = [UIImage imageNamed: @"bg_template_grid.png"];
        }
        [gridMain addSubview: imgGrid];

        UIImageView* imgMaskView = [[UIImageView alloc] initWithFrame: rect];
        imgMaskView.tag = nIndex + 2000;
        imgMaskView.image = [UIImage imageNamed: @"select_mask.png"];
        imgMaskView.alpha = 0;
        [m_layoutView addSubview: imgMaskView];
        
        nIndex ++;
    }
}

- (void) setImageToRect: (UIImage*) image RectIndex: (int) nRectIndex MatchFrames: (NSArray*) frames ParentView:(UIView*) parentView
{
    UIImageView* gridMain = (UIImageView*)[self viewWithTag:104];
    UIImageView* imgGrid = (UIImageView*)[gridMain viewWithTag: nRectIndex + 3000];
    imgGrid.alpha = 0;

    UIImageView* imgContent = (UIImageView*)[m_layoutView viewWithTag: nRectIndex + 1000];
    [imgContent setImage: image];
    
    //create dragview
    TKDragView* orgView = (TKDragView*)[m_layoutView viewWithTag: nRectIndex + 1500];
    if (orgView) {
        [orgView removeFromSuperview];
    }
    
    orgView = [[TKDragView alloc] initWithImage: image startFrame: imgContent.frame goodFrames: frames badFrames: nil andDelegate: self.delegate];
    [orgView setViewIndex: nRectIndex];
    [orgView setLayoutMode: YES];
    [orgView setParentContainer: parentView];
    orgView.tag = nRectIndex + 1500;
    [m_layoutView addSubview: orgView];
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(showFilterView:)];
    gesture.numberOfTapsRequired = 2;
    [orgView addGestureRecognizer: gesture];
    
    //draw Image
    CGRect cropRect = [CUtils getRectFromString: m_LayoutRects[nRectIndex]];
    
    UIImage* realImage = [CUtils imageWithView: imgContent];//[CUtils imageWithImage: image scaledToSize: cropRect.size];
    UIImage* maskImage;
    if (m_clsProduct.m_nCategoryIndex == ECATEGORY_BLOCKS)
        maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]];
    else if (m_clsProduct.m_nCategoryIndex == ECATEGORY_CANVAS)
        maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_addmask.png", m_clsProduct.m_strName]];
    else
        maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGSize szLayout = m_layoutView.frame.size;
    szLayout.width *= realImage.scale;
    szLayout.height *= realImage.scale;
    UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];

    cropRect.origin.x *= realImage.scale;
    cropRect.origin.y *= realImage.scale;
    cropRect.size.width *= realImage.scale;
    cropRect.size.height *= realImage.scale;
    
    maskImage = [CUtils imageWithImage: fitMaskImage cropByRect: cropRect];
    
    UIImage* resultImage;
    if (m_clsProduct.m_nCategoryIndex == ECATEGORY_MOUSE) {
        resultImage = [CUtils mergeImage: realImage withImage: maskImage Mode: kCGBlendModeSoftLight];
    } else if (m_clsProduct.m_nCategoryIndex == ECATEGORY_BLOCKS) {
        resultImage = [CUtils mergeImage: realImage withImage: maskImage Mode: kCGBlendModeMultiply];
    } else {
        resultImage = [CUtils mergeImage: realImage withImage: maskImage Mode: kCGBlendModeNormal];//[CUtils maskImage: realImage withMask: maskImage];
    }
    
    [imgContent setImage: resultImage];
    
    [imgContent.superview bringSubviewToFront: imgContent];
    
    [self setInitRect: orgView];
}

- (void) setImageToRect: (UIImage*) image RectIndex: (int) nRectIndex
{
    UIImageView* gridMain = (UIImageView*)[self viewWithTag:104];
    UIImageView* imgGrid = (UIImageView*)[gridMain viewWithTag: nRectIndex + 3000];
    imgGrid.alpha = 0;

    UIImageView* imgContent = (UIImageView*)[m_layoutView viewWithTag: nRectIndex + 1000];
    [imgContent setImage: image];
    
    //draw Image
    CGRect cropRect = [CUtils getRectFromString: m_LayoutRects[nRectIndex]];
    
    UIImage* realImage = [CUtils imageWithView: imgContent];//[CUtils imageWithImage: image scaledToSize: cropRect.size];
    UIImage* maskImage;
    if (m_clsProduct.m_nCategoryIndex == ECATEGORY_BLOCKS)
        maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]];
    else if (m_clsProduct.m_nCategoryIndex == ECATEGORY_CANVAS)
        maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_addmask.png", m_clsProduct.m_strName]];
    else
        maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGSize szLayout = m_layoutView.frame.size;
    szLayout.width *= realImage.scale;
    szLayout.height *= realImage.scale;
    UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];
    
    cropRect.origin.x *= realImage.scale;
    cropRect.origin.y *= realImage.scale;
    cropRect.size.width *= realImage.scale;
    cropRect.size.height *= realImage.scale;
    
    maskImage = [CUtils imageWithImage: fitMaskImage cropByRect: cropRect];
    
    UIImage* resultImage;
    if (m_clsProduct.m_nCategoryIndex == ECATEGORY_MOUSE || m_clsProduct.m_nCategoryIndex == ECATEGORY_BLOCKS) {
        resultImage = [CUtils mergeImage: realImage withImage: maskImage Mode: kCGBlendModeSoftLight];
    } else {
        resultImage = [CUtils mergeImage: realImage withImage: maskImage Mode: kCGBlendModeNormal];//[CUtils maskImage: realImage withMask: maskImage];
    }

    [imgContent setImage: resultImage];
    
    [imgContent.superview bringSubviewToFront: imgContent];
}

- (void) clearImage: (TKDragView*) dragView
{
    int nIdx = [dragView getViewIndex];
    TKDragView* orgView = (TKDragView*)[m_layoutView viewWithTag: nIdx + 1500];
    if (orgView) {
        [orgView removeFromSuperview];
    }
    
    UIImageView* imgContent = (UIImageView*)[m_layoutView viewWithTag: nIdx + 1000];
    [imgContent setImage: nil];
    
    UIImageView* gridMain = (UIImageView*)[self viewWithTag:104];
    UIImageView* imgGrid = (UIImageView*)[gridMain viewWithTag: nIdx + 3000];
    imgGrid.alpha = 1;
}

- (void) showFilterView: (UITapGestureRecognizer*) gesture
{
    TKDragView* dragView = (TKDragView*)gesture.view;
    if (dragView.m_sourceImage == nil) {
        return;
    }
    if ([_delegate respondsToSelector: @selector(showFilterView:)]) {
        [_delegate performSelectorOnMainThread: @selector(showFilterView:) withObject: dragView waitUntilDone: YES];
    }
}

- (void) setDragView: (TKDragView*) dragView Index: (int) nIndex
{
    UIImageView* imgContent = (UIImageView*)[m_layoutView viewWithTag: nIndex + 1000];
    
    //create dragview
    TKDragView* orgView = (TKDragView*)[m_layoutView viewWithTag: nIndex + 1500];
    if (orgView) {
        [orgView removeFromSuperview];
    }
    
    orgView = [[TKDragView alloc] initWithImage: dragView.m_sourceImage startFrame: imgContent.frame goodFrames: dragView.goodFramesArray badFrames: nil andDelegate: self.delegate];
    [orgView setViewIndex: nIndex];
    [orgView setLayoutMode: YES];
    [orgView setParentContainer: [dragView parentContainer]];
    orgView.tag = nIndex + 1500;
    [m_layoutView addSubview: orgView];
    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(showFilterView:)];
    gesture.numberOfTapsRequired = 2;
    [orgView addGestureRecognizer: gesture];
    
    [imgContent.superview bringSubviewToFront: imgContent];
    
    [self setInitRect: orgView];
}

- (void) setInitRect: (TKDragView*) dragView
{
    CGSize cropSize = dragView.bounds.size;
    
    CGSize imageSize = dragView.m_sourceImage.size;
    CGSize szRatio = CGSizeMake(cropSize.width / imageSize.width, cropSize.height/imageSize.height);
    
    CGSize szNewImageSize;
    if (szRatio.width > szRatio.height) {
        szNewImageSize.width = imageSize.width;
        szNewImageSize.height = cropSize.height / szRatio.width;
    } else {
        szNewImageSize.height = imageSize.height;
        szNewImageSize.width = cropSize.width / szRatio.height;
    }
    
    dragView.m_viewRect = CGRectMake((imageSize.width - szNewImageSize.width) / 2, (imageSize.height - szNewImageSize.height) / 2, szNewImageSize.width, szNewImageSize.height);
}

#pragma mark - ispossible

- (BOOL) isPossibleToFinish
{
    for (int nIdx = 0; nIdx < m_LayoutRects.count; nIdx ++)
    {
        TKDragView* dragView = (TKDragView*)[m_layoutView viewWithTag: nIdx + 1500];
        if (dragView == nil) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - make images for publishing

#define MUG_HEIGHT 250

- (NSDictionary*) completeImage
{
    //get flat image
    UIImage* flatImage;
    
    if (m_clsProduct.m_nCategoryIndex == ECATEGORY_COASTER || m_clsProduct.m_nCategoryIndex == ECATEGORY_PLAQUES || m_clsProduct.m_nCategoryIndex == ECATEGORY_MOUSE) {
        m_layoutView.opaque = NO;
        flatImage = [CUtils imageWithView: m_layoutView];
    } /*else if (m_clsProduct.m_nCategoryIndex == ECATEGORY_CANVAS) {
        flatImage = [CUtils imageWithView: bgView];
    }*/ else if (m_clsProduct.m_nCategoryIndex == ECATEGORY_MUG) {
        CGSize size = m_layoutView.bounds.size;
        float rRatio = MUG_HEIGHT / size.height;
        size.width = size.width * rRatio;
        size.height = MUG_HEIGHT;
        
        UIGraphicsBeginImageContextWithOptions(size, NO, 0);
        
        TKDragView* dragView;
        
        for (int nIdx = 0; nIdx < m_LayoutRects.count; nIdx ++)
        {
            dragView = (TKDragView*)[m_layoutView viewWithTag: nIdx + 1500];
            if (dragView) {
                CGRect viewRect = dragView.m_viewRect;
                viewRect = CGRectMake(viewRect.origin.x * dragView.m_sourceImage.scale, viewRect.origin.y * dragView.m_sourceImage.scale, viewRect.size.width * dragView.m_sourceImage.scale, viewRect.size.height * dragView.m_sourceImage.scale);
                
                UIImage* drawImage = [CUtils imageWithImage: dragView.m_sourceImage cropByRect: viewRect];
                CGRect rect = dragView.frame;
                rect = CGRectMake(rect.origin.x * rRatio, rect.origin.y * rRatio, rect.size.width * rRatio, rect.size.height * rRatio);
                [drawImage drawInRect: rect];
            }
        }
        // grab context
        flatImage = UIGraphicsGetImageFromCurrentImageContext();
        
        // end context
        UIGraphicsEndImageContext();
        /////////////////////////////////////////////////
    } else {
        CGSize size = m_layoutView.bounds.size;
        UIGraphicsBeginImageContextWithOptions(size, NO, 0);
        
        TKDragView* dragView;
        
        for (int nIdx = 0; nIdx < m_LayoutRects.count; nIdx ++)
        {
            dragView = (TKDragView*)[m_layoutView viewWithTag: nIdx + 1500];
            if (dragView) {
                CGRect viewRect = dragView.m_viewRect;
                viewRect = CGRectMake(viewRect.origin.x * dragView.m_sourceImage.scale, viewRect.origin.y * dragView.m_sourceImage.scale, viewRect.size.width * dragView.m_sourceImage.scale, viewRect.size.height * dragView.m_sourceImage.scale);

                UIImage* drawImage = [CUtils imageWithImage: dragView.m_sourceImage cropByRect: viewRect];
                CGRect rect = dragView.frame;
                rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
                [drawImage drawInRect: rect];
            }
        }
        // grab context
        flatImage = UIGraphicsGetImageFromCurrentImageContext();
        
        // end context
        UIGraphicsEndImageContext();
        /////////////////////////////////////////////////
    }
    
    m_maskImageView.alpha = 0;
    NSData* printData = [self getPrintImage];
    m_maskImageView.alpha = 1;
    
    UIImage* publishImage = nil;
    BOOL FShadow = YES;
    switch (m_clsProduct.m_nCategoryIndex) {
        case ECATEGORY_CASE:
            publishImage = [self makePublishImageForPhone: flatImage Mode: kCGBlendModeNormal];
            break;
        case ECATEGORY_SKIN:
            publishImage = [self makePublishImageForSkin: flatImage Mode: kCGBlendModeNormal];
            break;
        case ECATEGORY_CANVAS:
        {
            UIImage* orgImage;
            UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, flatImage.size.width, flatImage.size.height)];
            imgView.opaque = NO;
            
            NSLog(@"%f, %f", flatImage.size.width, flatImage.size.height);
            CALayer *mask = [CALayer layer];
            mask.contents = (id)[m_bgImageView.image CGImage];
            mask.frame = CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height);
            imgView.layer.mask = mask;
            imgView.layer.masksToBounds = YES;
            
            imgView.image = flatImage;
            
            orgImage = [CUtils imageWithView: imgView];
            NSLog(@"img: %f, %f", orgImage.size.width, orgImage.size.height);
            UIImage* sourceImage = [orgImage imageByTrimmingTransparentPixels];
            NSLog(@"trim: %f, %f", sourceImage.size.width, sourceImage.size.height);

            publishImage = [self makePublishImageForCanvas: sourceImage Mode: kCGBlendModeNormal];
            break;
        }
        case ECATEGORY_MUG:
        {
            NSArray* publishImages = [self makePublishImagesForMug: flatImage];
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: publishImages, @"images", printData, @"print_image", [NSNumber numberWithInt: m_nTemplateIndex], @"template_id", [NSNumber numberWithBool: FShadow], @"shadow", m_clsProduct, @"product", nil];
            return dict;
            break;
        }
        case ECATEGORY_COASTER:
            FShadow = NO;
            if ([m_clsProduct.m_strName rangeOfString: @"Square"].length > 0) {
                publishImage = [self makePublishImageForSquareCoaster: flatImage];
            } else {
                publishImage = [self makePublishImageForRoundCoaster: flatImage];
            }
            break;
        case ECATEGORY_PLAQUES:
            FShadow = NO;
            if ([m_clsProduct.m_strName rangeOfString: @"Horizontal"].length > 0) {
                publishImage = [self makePublishImageForHPlaques: flatImage];
            } else {
                publishImage = [self makePublishImageForVPlaques: flatImage];
            }
            break;
        case ECATEGORY_BLOCKS:
        {
            FShadow = NO;
//            UIImageView* imgContent = (UIImageView*)[m_layoutView viewWithTag: m_nTemplateIndex + 1000];
            publishImage = [self makePublishImageForBlock: flatImage];
            break;
        }
        case ECATEGORY_MOUSE:
            FShadow = NO;
            publishImage = [self makePublishImageForMouse: flatImage];
            break;
        case ECATEGORY_SHIRTS:
            FShadow = NO;
            bgView.backgroundColor = [UIColor whiteColor];
            publishImage = [CUtils imageWithView: bgView];
            bgView.backgroundColor = [UIColor clearColor];
        default:
            break;
    }
    
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: publishImage, @"image", printData, @"print_image", [NSNumber numberWithInt: m_nTemplateIndex], @"template_id", [NSNumber numberWithBool: FShadow], @"shadow", m_clsProduct, @"product", nil];
    return dict;
}

#pragma  mark - Get Print Image

#define DPI 72

- (NSData*) getPrintImage
{
    UIImage* flatImage;

    CGSize sizeView = m_layoutView.frame.size;
    CGSize size;
    
    if (m_clsProduct.m_rPrintWidth > 0) {
        size = CGSizeMake(m_clsProduct.m_rPrintWidth * DPI, m_clsProduct.m_rPrintHeight * DPI);
    } else {
        size = m_layoutView.bounds.size;
    }
    
    float rRatio = size.width / sizeView.width;
    
//    if (m_clsProduct.m_nCategoryIndex == ECATEGORY_MUG) {
//        rRatio = MUG_HEIGHT / size.height;
//        size.width = size.width * rRatio;
//        size.height = MUG_HEIGHT;
//    }
    
    
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    TKDragView* dragView;
    
    for (int nIdx = 0; nIdx < m_LayoutRects.count; nIdx ++)
    {
        dragView = (TKDragView*)[m_layoutView viewWithTag: nIdx + 1500];
        if (dragView) {
            CGRect viewRect = dragView.m_viewRect;
            viewRect = CGRectMake(viewRect.origin.x * dragView.m_sourceImage.scale, viewRect.origin.y * dragView.m_sourceImage.scale, viewRect.size.width * dragView.m_sourceImage.scale, viewRect.size.height * dragView.m_sourceImage.scale);
            
            UIImage* drawImage = [CUtils imageWithImage: dragView.m_sourceImage cropByRect: viewRect];
            CGRect rect = dragView.frame;
            rect = CGRectMake(rect.origin.x * rRatio, rect.origin.y * rRatio, rect.size.width * rRatio, rect.size.height * rRatio);
            [drawImage drawInRect: rect];
        }
    }
    // grab context
    flatImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end context
    UIGraphicsEndImageContext();
    /////////////////////////////////////////////////
    
    UIImage* resultImage;
    NSData* data;
    switch (m_clsProduct.m_nCategoryIndex)
    {
        case ECATEGORY_CASE:
        {
            NSString* strName = [NSString stringWithFormat: @"%@", m_clsProduct.m_strName];
            resultImage = [self makePrintImage: flatImage name: strName];
            
            break;
        }
        case ECATEGORY_SKIN:
        {
            NSString* strName = [NSString stringWithFormat: @"skin_%@", m_clsProduct.m_strName];
            resultImage = [self makePrintImage: flatImage name: strName];
            
            break;
        }
        case ECATEGORY_COASTER:
        {
            NSString* strName;
            if ([m_clsProduct.m_strName rangeOfString: @"Round"].length > 0) {
                strName = @"Round Coaster";
            } else {
                strName = @"Square Coaster";
            }
            resultImage = [self makePrintImage: flatImage name: strName];
            break;
        }
        case ECATEGORY_SHIRTS:
            resultImage = [self makePrintImage: flatImage name: [NSString stringWithFormat: @"%d", m_nTemplateIndex]];
            break;
        case ECATEGORY_MUG:
            resultImage = [self makePrintImage: flatImage name: m_clsProduct.m_strName];
            break;
        case ECATEGORY_CANVAS:
        case ECATEGORY_BLOCKS:
        case ECATEGORY_PLAQUES:
            resultImage = [self makePrintImage: flatImage name: m_clsProduct.m_strName];
            break;
        case ECATEGORY_MOUSE:
            resultImage = [self makePrintImage: flatImage name: m_clsProduct.m_strName];
            break;
    }
    
    data = UIImageJPEGRepresentation(resultImage, 0.8);
    
    return data;
}

- (UIImage*) makePrintImage: (UIImage*) orgImage name: (NSString*) strName
{
    CGSize szSize = orgImage.size;
    UIImage* image = orgImage;//[CUtils imageWithImage: orgImage scaledToSize: szSize];

    //    UIImage* imgBorder = [UIImage imageNamed: [NSString stringWithFormat: @"%@_border.png", strName]];
    UIImage* imgMask = [UIImage imageNamed: [NSString stringWithFormat: @"%@_printmask.png", strName]];
    if (imgMask) {
        imgMask = [CUtils imageWithImage: imgMask scaledToSize: szSize];
    }
    
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, szSize.width, szSize.height)];
    view.opaque = NO;
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: view.bounds];
    imgView.opaque = NO;
    [imgView setImage: image];
    
    if (imgMask) {
        CALayer *mask = [CALayer layer];
        mask.contents = (id)[imgMask CGImage];
        mask.frame = CGRectMake(0, 0, szSize.width, szSize.height);
        imgView.layer.mask = mask;
        imgView.layer.masksToBounds = YES;
    }
    
    [view addSubview: imgView];
    
//    UIImageView* imgBorderView = [[UIImageView alloc] initWithFrame: view.bounds];
//    [imgBorderView setImage: imgBorder];
//    [view addSubview: imgBorderView];
    
    UIImage* resultImage = [CUtils imageWithView: view];
    
//    [imgBorderView removeFromSuperview];
    [imgView removeFromSuperview];
    
    return resultImage;
}

#define SIDE_FACTOR 1.0 / 20.0
#define SCALE_FACTOR 1.0
#define MAIN_SCALE 1.0

- (UIImage*) create3LayersForBlock: (UIImage*) leftImage Right: (UIImage*) rightImage Top: (UIImage*) topImage View: (UIView*) view
{
    CGSize mainSize = rightImage.size;
    mainSize.width *= SCALE_FACTOR;
    mainSize.height *= SCALE_FACTOR;
    
    mainSize.width *= MAIN_SCALE;
    mainSize.height *= MAIN_SCALE;
    
    CALayer* leftPage = [CALayer layer];
    leftPage.opacity = 1.0;
    
    CALayer*rightPage = [CALayer layer];
    rightPage.opacity = 1.0;
    
    CALayer* topPage = [CALayer layer];
    topPage.opacity = 1.0;
    topPage.borderWidth = 3;
    topPage.borderColor = [UIColor clearColor].CGColor;
    topPage.shouldRasterize = YES;
    
    leftPage.anchorPoint = (CGPoint){1.0, 0.5};
    rightPage.anchorPoint = (CGPoint){0.0, 0.5};
    topPage.anchorPoint = (CGPoint){0.0, 1.0};

    leftPage.position = (CGPoint){mainSize.width * SIDE_FACTOR, (mainSize.height * SIDE_FACTOR + mainSize.height) / 2};
    rightPage.position = (CGPoint){mainSize.width * SIDE_FACTOR, (mainSize.height * SIDE_FACTOR + mainSize.height) / 2};
    topPage.position = (CGPoint){mainSize.width * SIDE_FACTOR, mainSize.height * SIDE_FACTOR * 1.8};
    
    leftPage.bounds = (CGRect){0, 0, mainSize.width * SIDE_FACTOR, mainSize.height};
    rightPage.bounds = (CGRect){0, 0, mainSize.width, mainSize.height};
    topPage.bounds = (CGRect){0, 0, mainSize.width * 1.1, mainSize.height * SIDE_FACTOR * 2};
    
    leftPage.transform = makePerspectiveTransform(); // uncomment later
    rightPage.transform = makePerspectiveTransform(); // uncomment later
    topPage.transform = makePerspectiveTransform(); // uncomment later
    
    [view.layer addSublayer:topPage];
    [view.layer addSublayer:leftPage];
    [view.layer addSublayer:rightPage];
    
    leftPage.contents = (__bridge id)leftImage.CGImage;
    rightPage.contents = (__bridge id)rightImage.CGImage;
    topPage.contents = (__bridge id)topImage.CGImage;

    CATransform3D transform = leftPage.transform;
    transform = CATransform3DRotate(transform, -45.0 * M_PI / 180.0, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-0.5, 1, 0, 0));
    
    UIImage* lImage, *rImage, *tImage;
    UIImage* img = [CUtils imageWithLayer: leftPage];
    
    lImage = [img imageWithTransform: transform anchorPoint: leftPage.anchorPoint];
    lImage = [lImage imageByTrimmingTransparentPixels];
    
    leftPage.transform = transform;
    
    transform = rightPage.transform;
    transform = CATransform3DRotate(transform, 10.0 * M_PI / 180.0, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-0.5, 1, 0, 0));
    
    img = [CUtils imageWithLayer: rightPage];
    rImage = [img imageWithTransform: transform anchorPoint: rightPage.anchorPoint];
    rImage = [rImage imageByTrimmingTransparentPixels];
    
    rightPage.transform = transform;
    
    transform = topPage.transform;
    
    if ([m_clsProduct.m_strName rangeOfString: @"Horizon"].length > 0) {
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(75.0 * M_PI / 180.0, 1, 0, 0));
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-2.5 * M_PI / 180.0, 0, 0, 1));
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(35.0 * M_PI / 180.0, 0, 1, 0));
    } else {
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(80.0 * M_PI / 180.0, 1, 0, 0));
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-2.0 * M_PI / 180.0, 0, 0, 1));
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(30.0 * M_PI / 180.0, 0, 1, 0));
    }
    
    img = [CUtils imageWithLayer: topPage];
    tImage = [img imageWithTransform: transform anchorPoint: topPage.anchorPoint];
    tImage = [tImage imageByTrimmingTransparentPixels];
    
    topPage.transform = transform;

//    [self addSubview: view];
    
    ///////transformed image////////
    UIView* subView = [[UIView alloc] initWithFrame: view.bounds];
    subView.backgroundColor = [UIColor whiteColor];
    
    UIImageView* subImageView;
    
    subImageView = [[UIImageView alloc] initWithFrame: topPage.frame];
    subImageView.clipsToBounds = YES;
    subImageView.image = tImage;
    [subView addSubview: subImageView];

    subImageView = [[UIImageView alloc] initWithFrame: leftPage.frame];
    subImageView.clipsToBounds = YES;
    subImageView.image = lImage;
    [subView addSubview: subImageView];

    subImageView = [[UIImageView alloc] initWithFrame: rightPage.frame];
    subImageView.clipsToBounds = YES;
    subImageView.contentMode = UIViewContentModeScaleToFill;
    subImageView.image = rImage;
    [subView addSubview: subImageView];
    
    CGRect rect = CGRectMake(leftPage.frame.origin.x, topPage.frame.origin.y, leftPage.frame.size.width + rightPage.frame.size.width, 10.0f + rightPage.frame.size.height);
    
    UIImage* realImage = [CUtils imageWithImage: [CUtils imageWithView: subView] cropByRect: rect];

    [leftPage removeFromSuperlayer];
    [rightPage removeFromSuperlayer];
    [topPage removeFromSuperlayer];
    
    for (UIView* sub in subView.subviews) {
        [sub removeFromSuperview];
    }
    
    return realImage;
}

CATransform3D makePerspectiveTransform()
{
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = 1.0 / -500;
    return transform;
}

/////prev version by using 3d concating the 3 surfaces
/*
- (UIImage*) makePublishImageForBlock: (UIImage*) flatImage
{
    CGSize size = flatImage.size;
    
    UIImage* leftImage = [CUtils imageWithImage: flatImage cropByRect: CGRectMake(0, 0, size.width * SIDE_FACTOR, size.height)];
    UIImage* topImage = [CUtils imageWithImage: flatImage cropByRect: CGRectMake(0, 0, size.width, size.height * SIDE_FACTOR)];
    
    CGRect rect = CGRectMake(0, 0, size.width * (1.0 + SIDE_FACTOR), size.height * (1.0 + SIDE_FACTOR));
    UIView* view = [[UIView alloc] initWithFrame: rect];
    UIImage* realImage;
    realImage = [self create3LayersForBlock: leftImage Right: flatImage Top: topImage View: view];
    
    realImage = [CUtils changeWhiteColorTransparent: realImage];
    realImage = [realImage imageByTrimmingTransparentPixels];
    
    return realImage;
}
*/

#define BLOCK_FACTOR 0.05
- (UIImage*) makePublishImageForBlock: (UIImage*) flatImage
{
    CGFloat yAngle = 15.0 * M_PI / 180.0;
    CGFloat xAngle = 20.0 * M_PI / 180.0;
    
    CGPoint anchorPoint = CGPointMake(0.5, 0.5);
    CATransform3D transform = CATransform3DIdentity;
    if ([m_clsProduct.m_strName rangeOfString: @"Vert"].length > 0) {
        yAngle = 15.0 * M_PI / 180.0;
        transform.m34 = 1 / -3000.0;
    } else if([m_clsProduct.m_strName rangeOfString: @"Hori"].length > 0) {
        yAngle = -10.0 * M_PI / 180.0;
        xAngle = 30.0 * M_PI / 180.0;
        transform.m34 = 1 / -2500.0;
    }
    
    transform = CATransform3DRotate(transform, yAngle, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-xAngle, 1, 0, 0));
    
    UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];
    
    CGSize size = orgImage.size;
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, size.width, size.height)];
    view.opaque = NO;
    [self addSubview: view];
    
    CGRect rect = view.bounds;
    rect.origin.x = - rect.size.width * BLOCK_FACTOR / 2;
    rect.origin.y = - rect.size.height * BLOCK_FACTOR / 2;
    rect.size.width *= (1 + BLOCK_FACTOR);
    rect.size.height *= (1 + BLOCK_FACTOR);
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
    imageView.opaque = NO;
    imageView.image = orgImage;
    imageView.contentMode = UIViewContentModeScaleToFill;
    self.clipsToBounds = NO;
    
    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[removeMaskImage CGImage];
    mask.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
    imageView.layer.mask = mask;
    imageView.layer.masksToBounds = YES;
    
    [view addSubview: imageView];
    
    UIImage* realImage = [CUtils imageWithView: view];
    //    realImage = [realImage imageByTrimmingTransparentPixels];
    
    UIImage* maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGSize szLayout = realImage.size;
    szLayout.width *= realImage.scale;
    szLayout.height *= realImage.scale;
    UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];
    
    UIImage* resultImage = [CUtils mergeImage: realImage withImage: fitMaskImage Mode: kCGBlendModeMultiply];
    
    imageView.image = resultImage;
    resultImage = [CUtils imageWithView: imageView];
    
    resultImage = [resultImage imageByTrimmingTransparentPixels];
    
    [imageView removeFromSuperview];
    [view removeFromSuperview];
    
    return resultImage;
}

- (UIImage*) leftMug: (UIImage*) image Width: (CGFloat) rWidth
{
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, image.size.width + rWidth, image.size.height)];
    view.opaque = NO;
    view.backgroundColor = [UIColor clearColor];
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(rWidth / 2.0, 0, image.size.width, image.size.height)];
    [imgView setImage: image];
    [view addSubview: imgView];
    
    return [CUtils imageWithView: view];
}

- (UIImage*) rightMug: (UIImage*) image Width: (CGFloat) rWidth
{
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, image.size.width + rWidth, image.size.height)];
    view.opaque = NO;
    view.backgroundColor = [UIColor clearColor];
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(rWidth / 2, 0, image.size.width, image.size.height)];
    [imgView setImage: image];
    [view addSubview: imgView];
    
    return [CUtils imageWithView: view];
}

- (NSArray*) makePublishImagesForMug: (UIImage*) flatImage
{
    NSMutableArray* imgArray = [[NSMutableArray alloc] init];
    
    CGSize szMug = flatImage.size;
    
    UIImage* leftImage = [CUtils imageWithImage: flatImage cropByRect: CGRectMake(0, 0, szMug.width / 2, szMug.height)];
    UIImage* rightImage = [CUtils imageWithImage: flatImage cropByRect: CGRectMake(szMug.width / 2, 0, szMug.width / 2, szMug.height)];

    szMug = CGSizeMake(m_clsProduct.m_nWidth / 2, m_clsProduct.m_nHeight);
    //mug left
    MugView* mugView = [[MugView alloc] initWithFrame: CGRectMake(0, 0, (szMug.width / M_PI * 2) / 2, (szMug.height + 120) / 2) Image: leftImage];
    [self addSubview: mugView];
    [mugView configureView];
    
    UIImage* mugLeft = [(GLKView*)mugView snapshot];//[CUtils imageWithView: mugView];
    
    if (m_nTemplateIndex == 8) { // special
        CGSize szImage = mugLeft.size;
        mugLeft = [CUtils imageWithImage: mugLeft cropByRect: CGRectMake(szImage.width / 6.0, 0, szImage.width * 4 / 6.0, szImage.height)];
        mugLeft = [self leftMug: mugLeft Width: szImage.width / 6.0];
    }
    
    //mug right
    mugView.m_flatImage = rightImage;
    [mugView configureView];
    [mugView setNeedsDisplay];
    
    UIImage* mugRight = [(GLKView*)mugView snapshot];//[CUtils imageWithView: mugView];
    if (m_nTemplateIndex == 8) {
        CGSize szImage = mugRight.size;
        mugRight = [CUtils imageWithImage: mugRight cropByRect: CGRectMake(szImage.width / 6.0, 0, szImage.width * 4 / 6.0, szImage.height)];
        mugRight = [self rightMug: mugRight Width: szImage.width / 6.0];
    }
    
    [mugView removeFromSuperview];
    
    ////////////////////////
    CGRect rect = CGRectZero;
    rect.size.width = (szMug.height + 120)/2;
    rect.size.height = (szMug.height + 120)/2;
    UIView* viewWork = [[UIView alloc] initWithFrame: rect];
    viewWork.opaque = NO;
    viewWork.backgroundColor = [UIColor clearColor];
    [self addSubview: viewWork];
    
    CGRect rectTex = CGRectMake(0, 40, (szMug.width / M_PI * 2)/2, szMug.height/2);
    rectTex.origin.x = rect.size.width - rectTex.size.width;
    UIImageView* texView = [[UIImageView alloc] initWithFrame: rectTex];
    texView.opaque = NO;
    texView.contentMode = UIViewContentModeScaleToFill;
    texView.clipsToBounds = YES;
    texView.image = mugLeft;

    CALayer *mask = [CALayer layer];
    mask.contents = (id)[UIImage imageNamed: @"mug_left_mask.png"].CGImage;
    mask.frame = CGRectMake(0, 0, rectTex.size.width, rectTex.size.height);
    texView.layer.mask = mask;
    texView.layer.masksToBounds = YES;
    
    [viewWork addSubview: texView];
    
    UIImageView* resultView = [[UIImageView alloc] initWithFrame: rect];
    resultView.opaque = NO;
    resultView.contentMode = UIViewContentModeScaleToFill;
    resultView.clipsToBounds = YES;
    
    [self addSubview: resultView];

    UIImage* maskImage = [UIImage imageNamed: @"mug_left.png"];

    for (int nIdx = 0; nIdx < 2; nIdx ++) {
        UIImage* img = [CUtils imageWithView: viewWork];
        
        UIImage* realImage = img;
        
        CGSize szLayout = realImage.size;
        szLayout.width *= realImage.scale;
        szLayout.height *= realImage.scale;
        UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];
        
        UIImage* resultImage = [CUtils mergeImage: realImage withImage: fitMaskImage Mode: kCGBlendModeSoftLight];
        resultImage = [CUtils imageWithImage: resultImage scaledToSize: CGSizeMake(resultImage.size.width / 4, resultImage.size.height / 4)];

        [imgArray addObject: resultImage];
        
        texView.image = mugRight;
        rectTex.origin.x = 0;
        texView.frame = rectTex;
        
        CALayer *mask = [CALayer layer];
        mask.contents = (id)[UIImage imageNamed: @"mug_right_mask.png"].CGImage;
        mask.frame = CGRectMake(0, 0, rectTex.size.width, rectTex.size.height);
        texView.layer.mask = mask;
        texView.layer.masksToBounds = YES;

        maskImage = [UIImage imageNamed: @"mug_right.png"];
    }
    
    [resultView removeFromSuperview];
    [texView removeFromSuperview];
    [viewWork removeFromSuperview];
    
    return imgArray;
}

- (UIImage*) makePublishImageForMouse: (UIImage*) flatImage
{
    CGFloat xAngle = 55.0 * M_PI / 180.0;
    CGFloat zAngle = 12.0 * M_PI / 180.0;
    CGFloat yAngle = 12.3 * M_PI / 180.0;
    
    CGPoint anchorPoint = CGPointMake(0.5, 0.5);
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = -1.0 / 500.0;
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(xAngle, 1, 0, 0));
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-zAngle, 0, 0, 1));
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(yAngle, 0, 1, 0));

    
    UIImage* imgShadow = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGRect rect = CGRectMake(0, 0, flatImage.size.width, flatImage.size.height);//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
    UIView* viewWork = [[UIView alloc] initWithFrame: rect];
    viewWork.opaque = NO;
    [self addSubview: viewWork];
    
    rect = CGRectMake(flatImage.size.width / 4, flatImage.size.height / 4, flatImage.size.width / 2, flatImage.size.height / 2);
    UIImageView* imgContent = [[UIImageView alloc] initWithFrame: rect];
    imgContent.layer.cornerRadius = 10;
    imgContent.clipsToBounds = YES;
    imgContent.contentMode = UIViewContentModeScaleAspectFit;
    imgContent.image = flatImage;
    [viewWork addSubview: imgContent];
    
    flatImage = [CUtils imageWithView: viewWork];
    
    [imgContent removeFromSuperview];
    
    UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];
    CGSize orgSize = orgImage.size;
    orgImage = [orgImage imageByTrimmingTransparentPixels];
    orgSize = orgImage.size;
    imgContent.image = orgImage;
    
    rect = CGRectMake(0, 0, orgSize.width / 2, orgSize.height / 2);

    UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
    imageView.clipsToBounds = YES;
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = orgImage;
    [viewWork addSubview: imageView];

    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_mask.png", m_clsProduct.m_strEditImage]];
    removeMaskImage = [CUtils imageWithImage: removeMaskImage scaledToSize: rect.size];
    
    UIImageView* imageMask = [[UIImageView alloc] initWithFrame: rect];
    imageMask.clipsToBounds = YES;
    imageMask.image = removeMaskImage;
    [viewWork addSubview: imageMask];
    
    UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: rect];
    imageShadow.clipsToBounds = YES;
    imageShadow.image = imgShadow;
    [viewWork addSubview: imageShadow];
    
    viewWork.frame = rect;
    
    viewWork.backgroundColor = [UIColor whiteColor];
    UIImage* resultImage = [CUtils imageWithView: viewWork];
    
    [viewWork removeFromSuperview];
    
    return resultImage;
}

- (UIImage*) makePublishImageForHPlaques: (UIImage*) flatImage
{
    CGFloat xAngle = 0.0 * M_PI / 180.0;
    CGFloat yAngle = 0.0 * M_PI / 180.0;
    CGFloat zAngle = 2.0 * M_PI / 180.0;
    
    CGPoint anchorPoint = CGPointMake(0.5, 0.5);
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = -1.0 / 1500.0;
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(xAngle, 1, 0, 0));
    transform = CATransform3DRotate(transform, yAngle, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(zAngle, 0, 0, 1));
    
    UIImage* imgShadow = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGRect rect = CGRectMake(0, 0, imgShadow.size.width, imgShadow.size.height);//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
    UIView* viewWork = [[UIView alloc] initWithFrame: rect];
    viewWork.opaque = NO;
    [self addSubview: viewWork];
    
    rect = CGRectMake(imgShadow.size.width / 6, imgShadow.size.height / 6, imgShadow.size.width * 2 / 3, imgShadow.size.height * 2 / 3);
    UIImageView* imgContent = [[UIImageView alloc] initWithFrame: rect];
    imgContent.clipsToBounds = YES;
    imgContent.contentMode = UIViewContentModeScaleToFill;
    imgContent.image = flatImage;
    [viewWork addSubview: imgContent];
    
    flatImage = [CUtils imageWithView: viewWork];
    
    UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];
    orgImage = [orgImage imageByTrimmingTransparentPixels];
    
    imgContent.image = orgImage;

    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strEditImage]];
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[removeMaskImage CGImage];
    mask.frame = CGRectMake(0, 0, imgContent.bounds.size.width, imgContent.bounds.size.height);
    imgContent.layer.mask = mask;
    imgContent.layer.masksToBounds = YES;
    
    UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: rect];
    imageShadow.clipsToBounds = YES;
    imageShadow.image = imgShadow;
    [viewWork addSubview: imageShadow];
    
    UIImage* resultImage = [CUtils imageWithView: viewWork];
    resultImage = [resultImage imageByTrimmingTransparentPixels];
    
    [imageShadow removeFromSuperview];
    [imgContent removeFromSuperview];
    [viewWork removeFromSuperview];
    
    return resultImage;
}

- (UIImage*) makePublishImageForVPlaques: (UIImage*) flatImage
{
    CGFloat xAngle = 0.0 * M_PI / 180.0;
    CGFloat yAngle = 0.0 * M_PI / 180.0;
    CGFloat zAngle = -2.0 * M_PI / 180.0;
    
    CGPoint anchorPoint = CGPointMake(0.5, 0.5);
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = -1.0 / 1500.0;
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(xAngle, 1, 0, 0));
    transform = CATransform3DRotate(transform, yAngle, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(zAngle, 0, 0, 1));
    
    UIImage* imgShadow = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];

    CGRect rect = CGRectMake(0, 0, imgShadow.size.width, imgShadow.size.height);//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
    UIView* viewWork = [[UIView alloc] initWithFrame: rect];
    viewWork.opaque = NO;
    [self addSubview: viewWork];
    
    rect = CGRectMake(imgShadow.size.width / 8, imgShadow.size.height / 8, imgShadow.size.width * 3 / 4, imgShadow.size.height * 3 / 4);
    UIImageView* imgContent = [[UIImageView alloc] initWithFrame: rect];
    imgContent.clipsToBounds = YES;
    imgContent.contentMode = UIViewContentModeScaleToFill;
    imgContent.image = flatImage;
    [viewWork addSubview: imgContent];
    
    flatImage = [CUtils imageWithView: viewWork];
    
    UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];
    orgImage = [orgImage imageByTrimmingTransparentPixels];
    
    imgContent.image = orgImage;
    
    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_mask.png", m_clsProduct.m_strEditImage]];
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[removeMaskImage CGImage];
    mask.frame = CGRectMake(0, 0, imgContent.bounds.size.width, imgContent.bounds.size.height);
    imgContent.layer.mask = mask;
    imgContent.layer.masksToBounds = YES;
    
    UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: rect];
    imageShadow.clipsToBounds = YES;
    imageShadow.image = imgShadow;
    [viewWork addSubview: imageShadow];
    
    UIImage* resultImage = [CUtils imageWithView: viewWork];
    resultImage = [resultImage imageByTrimmingTransparentPixels];

    [imageShadow removeFromSuperview];
    [imgContent removeFromSuperview];
    [viewWork removeFromSuperview];
    
    return resultImage;
}

- (UIImage*) makePublishImageForSquareCoaster: (UIImage*) flatImage
{
    UIImage* resultImage;
    
    if ([m_clsProduct.m_strName rangeOfString: @"Set"].length > 0) {
        CGRect rect = CGRectMake(8, 8, 304, 304);//bgView.frame;//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
        rect.size.height += 100;
        UIView* viewWork = [[UIView alloc] initWithFrame: rect];
        viewWork.opaque = NO;
        viewWork.clipsToBounds = NO;
        [self addSubview: viewWork];
        
        rect = CGRectMake(rect.size.width / 4, rect.size.height / 4, rect.size.width / 2, rect.size.height / 2);
        
        UIImageView* tmpView = [[UIImageView alloc] initWithFrame: rect];
        tmpView.clipsToBounds = YES;
        tmpView.image = flatImage;
        [viewWork addSubview: tmpView];
        
        UIImage* orgImage = [CUtils imageWithView: viewWork];
        
        [tmpView removeFromSuperview];
        
        CGFloat xAngle = -15.0 * M_PI / 180.0, rRatio = 0;
        CGFloat yAngle = 30.0 * M_PI / 180.0;
        CGFloat zAngle = 14.0 * M_PI / 180.0;
        CGPoint anchorPoint = CGPointMake(0.5, 0.5);
        CATransform3D transform = CATransform3DMakeRotation(xAngle, 1, 0, 0);
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(yAngle, 0, 1, 0));
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-zAngle, 0, 0, 1));
        orgImage = [orgImage imageWithTransform: transform anchorPoint: anchorPoint];
        
        
        rect.size = CGSizeMake(400, 400);
        rect.origin = CGPointMake((304 - rect.size.width) / 2 - 25, (304 - rect.size.height) / 2);
        
        CGRect rectShadow;
        for (int i = 0; i < 4; i ++) {
            rect.origin.x += 10;
            rect.origin.y += 10;
            UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
            imageView.clipsToBounds = YES;
            imageView.image = orgImage;
            
            [viewWork addSubview: imageView];
            
            UIImage* imgShadow = [UIImage imageNamed: @"pub_Square Coaster Set_shadow.png"];
            rRatio = imgShadow.size.height / imgShadow.size.width;
            rectShadow = rect;

            if ([UIScreen mainScreen].bounds.size.height > 500) {
                rectShadow.size.height = 193;
                rectShadow.size.width = rectShadow.size.height / rRatio;
                rectShadow.origin.x += 103;
                rectShadow.origin.y += 117;
            } else {
                rectShadow.size.height = 188;
                rectShadow.size.width = rectShadow.size.height / rRatio;
                rectShadow.origin.x += 104;
                rectShadow.origin.y += 119;
            }
            
            UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: rectShadow];
            imageShadow.clipsToBounds = YES;
            imageShadow.image = imgShadow;
            
            [viewWork addSubview: imageShadow];
        }
        
        CGPoint ptPoint = CGPointMake(rectShadow.origin.x + rectShadow.size.width, rectShadow.origin.y + rectShadow.size.height);
        UIImage* imgStay = [UIImage imageNamed: @"pub_Square Coaster Set_stay.png"];
        rRatio = imgStay.size.height / imgStay.size.width;
        CGRect rectStay;
        rectStay.size.width = 150;
        rectStay.size.height = rectStay.size.width * rRatio;
        rectStay.origin.x = ptPoint.x - 4;
        rectStay.origin.y = ptPoint.y - 45;
        
        UIImageView* imgViewStay = [[UIImageView alloc] initWithFrame: rectStay];
        imgViewStay.clipsToBounds = YES;
        imgViewStay.image = imgStay;
        
        [viewWork addSubview: imgViewStay];
        
        resultImage = [CUtils imageWithView: viewWork];
        resultImage = [resultImage imageByTrimmingTransparentPixels];
        
        [viewWork removeFromSuperview];
    } else {
        CGFloat xAngle = 50.0 * M_PI / 180.0;
        CGFloat zAngle = 8.5 * M_PI / 180.0;
        CGFloat yAngle = 11.0 * M_PI / 180.0;
        
        CGPoint anchorPoint = CGPointMake(0.5, 0.5);
        CATransform3D transform = CATransform3DMakeRotation(-xAngle, 1, 0, 0);
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-zAngle, 0, 0, 1));
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-yAngle, 0, 1, 0));
        
        float rRatio = 0;
        
        CGRect rect = bgView.frame;//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
        UIView* viewWork = [[UIView alloc] initWithFrame: rect];
        viewWork.opaque = NO;
        [self addSubview: viewWork];
        
        rect.origin = CGPointMake(rect.size.width / 4, rect.size.height / 4);
        rect.size = CGSizeMake(rect.size.width / 2, rect.size.height / 2);
        UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
        imageView.clipsToBounds = NO;
        imageView.image = flatImage;
        [viewWork addSubview: imageView];
        
        UIImage* orgImage = [CUtils imageWithView: viewWork];
        orgImage = [orgImage imageWithTransform: transform anchorPoint: anchorPoint];
        
        rect = CGRectMake(-bgView.frame.size.width / 4, -bgView.frame.size.height / 4, bgView.frame.size.width * 3 / 2, bgView.frame.size.height * 3 / 2);
        imageView.frame = rect;
        imageView.image = orgImage;
        
        UIImage* imgShadow = [UIImage imageNamed: @"pub_Square Coaster_mask.png"];
        rRatio = imgShadow.size.height / imgShadow.size.width;
        if ([UIScreen mainScreen].bounds.size.height > 500) {
            rect.size.width = bgView.frame.size.width - 38;
            rect.size.height = rect.size.width * rRatio;
        } else {
            rect.size.width = bgView.frame.size.width - 28;
            rect.size.height = rect.size.width * rRatio;
        }
        UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, rect.size.width * 1.5, rect.size.height * 1.5)];
        imageShadow.clipsToBounds = YES;
        imageShadow.image = imgShadow;
        imageShadow.center = imageView.center;
        
        [viewWork addSubview: imageShadow];
        
        UIImageView* imageBottom = [[UIImageView alloc] initWithFrame: rect];
        imageBottom.center = imageView.center;
        imageBottom.clipsToBounds = YES;
        imageBottom.image = [UIImage imageNamed: @"pub_Square Coaster_shadow.png"];
        [viewWork addSubview: imageBottom];
        
//        CALayer *mask = [CALayer layer];
//        mask.contents = (id)[imgShadow CGImage];
//        mask.frame = imageShadow.frame;
//        viewWork.layer.mask = mask;
//        viewWork.layer.masksToBounds = YES;
        
        resultImage = [CUtils imageWithView: viewWork];
        resultImage = [resultImage imageByTrimmingTransparentPixels];
        
        [viewWork removeFromSuperview];
    }
    
    return resultImage;
}

- (UIImage*) makePublishImageForRoundCoaster: (UIImage*) flatImage
{
//    flatImage = [CUtils circularScaleNCrop: flatImage Rect: CGRectMake(0, 0, flatImage.size.width, flatImage.size.height)];

    UIImage* resultImage;
    
    if ([m_clsProduct.m_strName rangeOfString: @"Set"].length > 0) {
        CGRect rect = bgView.frame;//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
        rect.size.height += 100;
        UIView* viewWork = [[UIView alloc] initWithFrame: rect];
        viewWork.opaque = NO;
        viewWork.clipsToBounds = NO;
        [self addSubview: viewWork];

        CGFloat xAngle = 10.0 * M_PI / 180.0, rRatio = 0;
        CGPoint anchorPoint = CGPointMake(0.5, 0.5);
        CATransform3D transform = CATransform3DMakeRotation(-xAngle, 1, 0, 0);
        transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-1.5*xAngle, 0, 1, 0));
        UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];

        
        rect.size = CGSizeMake(200, 200);
        rect.origin = CGPointMake((bgView.frame.size.width - rect.size.width) / 2 - 25, (bgView.frame.size.height - rect.size.height) / 2);

        for (int i = 0; i < 4; i ++) {
            rect.origin.x += 10;
            rect.origin.y += 10;
            UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
            imageView.clipsToBounds = YES;
            imageView.image = orgImage;
            
            [viewWork addSubview: imageView];
            
            UIImage* imgShadow = [UIImage imageNamed: @"pub_Round Coaster Set_shadow.png"];
            rRatio = imgShadow.size.height / imgShadow.size.width;
            CGRect rectShadow = rect;
            rectShadow.size.height = 190;
            rectShadow.size.width = rect.size.height / rRatio;
            rectShadow.origin.x += 2;
            rectShadow.origin.y += 1;
            
            UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: rectShadow];
            imageShadow.clipsToBounds = YES;
            imageShadow.image = imgShadow;
            
            [viewWork addSubview: imageShadow];
        }
        
        CGPoint ptPoint = CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height);
        UIImage* imgStay = [UIImage imageNamed: @"pub_Round Coaster Set_stay.png"];
        rRatio = imgStay.size.height / imgStay.size.width;
        CGRect rectStay;
        rectStay.size.height = 106;
        rectStay.size.width = rectStay.size.height / rRatio;
        rectStay.origin.x = ptPoint.x - rectStay.size.width / 2 - 13;
        rectStay.origin.y = ptPoint.y - rectStay.size.width / 2 + 30;
        
        UIImageView* imgViewStay = [[UIImageView alloc] initWithFrame: rectStay];
        imgViewStay.clipsToBounds = YES;
        imgViewStay.image = imgStay;
        
        [viewWork addSubview: imgViewStay];
        
        resultImage = [CUtils imageWithView: viewWork];
        resultImage = [resultImage imageByTrimmingTransparentPixels];
        
        [viewWork removeFromSuperview];
    } else {
        CGFloat xAngle = 45.0 * M_PI / 180.0;
        
        CGPoint anchorPoint = CGPointMake(0.5, 0.5);
        CATransform3D transform = CATransform3DMakeRotation(-xAngle, 1, 0, 0);
        UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];

        CGSize szSize = orgImage.size;
        float rRatio = szSize.height / szSize.width;
        
        CGRect rect = bgView.frame;//CGRectMake(0, 0, orgImage.size.width, orgImage.size.height + 10);
        UIView* viewWork = [[UIView alloc] initWithFrame: rect];
        viewWork.opaque = NO;
        [self addSubview: viewWork];

        rect.origin = CGPointZero;
        UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
        imageView.clipsToBounds = YES;
        imageView.image = orgImage;
        
        UIImage* imgShadow = [UIImage imageNamed: @"pub_Round Coaster_shadow.png"];
        rRatio = imgShadow.size.height / imgShadow.size.width;
        rect.size.height = rect.size.width * rRatio;
        
        rect.origin.y = imageView.frame.size.height / 2;
        UIImageView* imageShadow = [[UIImageView alloc] initWithFrame: rect];
        imageShadow.clipsToBounds = YES;
        imageShadow.image = imgShadow;
        
        [viewWork addSubview: imageView];
        [viewWork addSubview: imageShadow];
        
        resultImage = [CUtils imageWithView: viewWork];
        resultImage = [resultImage imageByTrimmingTransparentPixels];
        
        [viewWork removeFromSuperview];
    }
    
    return resultImage;
}

- (UIImage*) makePublishImageForSkin: (UIImage*) flatImage Mode: (CGBlendMode) mode
{
    CGSize size = flatImage.size;
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, size.width, size.height)];
    [self addSubview: view];
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame: view.bounds];
    imageView.image = flatImage;
    self.clipsToBounds = YES;
    
    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", m_clsProduct.m_strEditImage]];
    
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[removeMaskImage CGImage];
    mask.frame = CGRectMake(0, 0, imageView.bounds.size.width, imageView.bounds.size.height);
    imageView.layer.mask = mask;
    imageView.layer.masksToBounds = YES;
    
    [view addSubview: imageView];
    
    UIImage* realImage = [CUtils imageWithView: view];
    
    UIImage* maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGSize szLayout = realImage.size;
    szLayout.width *= realImage.scale;
    szLayout.height *= realImage.scale;
    UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];
    
    UIImage* resultImage = [CUtils mergeImage: realImage withImage: fitMaskImage Mode: mode];
    
    imageView.image = resultImage;
    resultImage = [CUtils imageWithView: imageView];
    
    [view removeFromSuperview];
    
    return resultImage;
}

#define PHONE_FACTOR 0.1
- (UIImage*) makePublishImageForPhone: (UIImage*) flatImage Mode: (CGBlendMode) mode
{
    CGFloat yAngle = 7.5 * M_PI / 180.0;
    CGFloat xAngle = 10.0 * M_PI / 180.0;
    
    CGPoint anchorPoint = CGPointMake(0.5, 0.5);
    CATransform3D transform = CATransform3DIdentity;
    if ([m_clsProduct.m_strName rangeOfString: @"Pad"].length > 0) {
        yAngle = 5.0 * M_PI / 180.;
        transform.m34 = 1 / -500.0;
    } else {
        transform.m34 = 1 / -1500.0;
    }
    transform = CATransform3DRotate(transform, yAngle, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-xAngle, 1, 0, 0));
    
    UIImage* orgImage = [CUtils imageWithImage: flatImage scaledToSize: CGSizeMake(flatImage.size.width * (1 + PHONE_FACTOR), flatImage.size.height * (1 + PHONE_FACTOR))];
    orgImage = [orgImage imageWithTransform: transform anchorPoint: anchorPoint];
    orgImage = [CUtils imageWithImage: orgImage cropByRect: CGRectMake(0, orgImage.size.height * PHONE_FACTOR / 4, orgImage.size.width, orgImage.size.height - orgImage.size.height * PHONE_FACTOR / 2)];

    CGSize size = flatImage.size;
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, size.width, size.height)];
    [self addSubview: view];
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame: view.bounds];
    imageView.image = orgImage;
    imageView.opaque = NO;
    self.clipsToBounds = YES;
    
    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];

    CALayer *mask = [CALayer layer];
    mask.contents = (id)[removeMaskImage CGImage];
    mask.frame = CGRectMake(0, 0, imageView.bounds.size.width, imageView.bounds.size.height);
    imageView.layer.mask = mask;
    imageView.layer.masksToBounds = YES;
    
    [view addSubview: imageView];

    UIImage* realImage = [CUtils imageWithView: view];
    
    UIImage* maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGSize szLayout = realImage.size;
    szLayout.width *= realImage.scale;
    szLayout.height *= realImage.scale;
    UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];
    
    UIImage* resultImage = [CUtils mergeImage: realImage withImage: fitMaskImage Mode: kCGBlendModeMultiply];
    
    imageView.image = resultImage;
    resultImage = [CUtils imageWithView: imageView];
    
    [view removeFromSuperview];
    
    resultImage = [resultImage imageByTrimmingTransparentPixels];
    
    return resultImage;
}

#define MASK_OFFSET 15
#define CANVAS_FACTOR 0.05

- (UIImage*) makePublishImageForCanvas: (UIImage*) flatImage Mode: (CGBlendMode) mode
{
    //y axis rotation
    CGFloat yAngle = 15.0 * M_PI / 180.0;
    CGFloat xAngle = 20.0 * M_PI / 180.0;
    
    CGPoint anchorPoint = CGPointMake(0.5, 0.5);
    CATransform3D transform = CATransform3DIdentity;
    if ([m_clsProduct.m_strName rangeOfString: @"Vert"].length > 0) {
        yAngle = 15.0 * M_PI / 180.0;
        transform.m34 = 1 / -3000.0;
    } else if([m_clsProduct.m_strName rangeOfString: @"Hori"].length > 0) {
        yAngle = 10.0 * M_PI / 180.0;
        xAngle = 25.0 * M_PI / 180.0;
        transform.m34 = 1 / -2500.0;
    } else {
        yAngle = 15.0 * M_PI / 180.0;
        xAngle = 25.0 * M_PI / 180.0;
        transform.m34 = 1 / -6000.0;
    }
    transform = CATransform3DRotate(transform, yAngle, 0, 1, 0);
    transform = CATransform3DConcat(transform, CATransform3DMakeRotation(-xAngle, 1, 0, 0));
    
    UIImage* orgImage = [flatImage imageWithTransform: transform anchorPoint: anchorPoint];
    
    CGSize size = orgImage.size;
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, size.width, size.height)];
    view.opaque = NO;
    [self addSubview: view];
    
    CGRect rect = view.bounds;
    rect.origin.x = -rect.size.width * CANVAS_FACTOR / 2;
    rect.origin.y = -rect.size.height * CANVAS_FACTOR / 2;
    rect.size.width *= (1 + CANVAS_FACTOR);
    rect.size.height *= (1 + CANVAS_FACTOR);
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame: rect];
    imageView.opaque = NO;
    imageView.image = orgImage;
    imageView.contentMode = UIViewContentModeScaleToFill;
    self.clipsToBounds = NO;
    
    UIImage* removeMaskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[removeMaskImage CGImage];
    mask.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
    imageView.layer.mask = mask;
    imageView.layer.masksToBounds = YES;
    
    [view addSubview: imageView];
    
    UIImage* realImage = [CUtils imageWithView: view];
//    realImage = [realImage imageByTrimmingTransparentPixels];
    
    UIImage* maskImage = [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", m_clsProduct.m_strEditImage]];
    
    CGSize szLayout = realImage.size;
    szLayout.width *= realImage.scale;
    szLayout.height *= realImage.scale;
    UIImage* fitMaskImage = [CUtils imageWithImage: maskImage scaledToSize: szLayout];
    
    UIImage* resultImage = [CUtils mergeImage: realImage withImage: fitMaskImage Mode: kCGBlendModeMultiply];
    
    imageView.image = resultImage;
    resultImage = [CUtils imageWithView: imageView];
    
    resultImage = [resultImage imageByTrimmingTransparentPixels];

    [imageView removeFromSuperview];
    [view removeFromSuperview];
    
    return resultImage;
}

@end
