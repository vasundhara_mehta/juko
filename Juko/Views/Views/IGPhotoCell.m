//
//  IGPhotoCell.m
//  Inkgram
//
//  Created by Mountain on 2/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGPhotoCell.h"

#import "UIImageView+WebCache.h"

@implementation IGPhotoCell
{
    BOOL m_FChoose;
    BOOL m_FIsDelete;
    int m_nIndex;
    id m_sourceImage; //url string or uiimage
}

@synthesize m_ImageContent, delegate, m_FEditMode;
@synthesize m_btnChoose, m_btnDelete, reuseIdentifier;

- (id)initWithFrame:(CGRect)frame ContentSize: (CGSize) szSize
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect bgFrame = CGRectMake((frame.size.width - szSize.width) / 2, (frame.size.height - szSize.height) / 2, szSize.width, szSize.height);
        
        UIView *bgView = [[UIView alloc] initWithFrame: bgFrame];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.backgroundColor = [UIColor whiteColor].CGColor;
        
        [bgView.layer setShadowOffset:CGSizeMake(0, 0)];
        [bgView.layer setShadowRadius:1.5f];
        [bgView.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [bgView.layer setShadowOpacity:0.5];
        
        [self addSubview:bgView];
        
        bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        // Image View
        m_ImageContent = [[UIImageView alloc] initWithFrame: bgView.bounds];
        m_ImageContent.clipsToBounds = YES;
        [m_ImageContent setContentMode: UIViewContentModeScaleAspectFill];
        
        [bgView addSubview: m_ImageContent];

        m_btnChoose = [[UIButton alloc] initWithFrame: bgView.bounds];
        [m_btnChoose setImage: [UIImage imageNamed: @"SelectionOverlay~iOS7.png"] forState: UIControlStateSelected];
        
        [bgView addSubview: m_btnChoose];
        [m_btnChoose addTarget: self action: @selector(choosePhoto:) forControlEvents: UIControlEventTouchUpInside];

        CGRect rectBtnDelete = CGRectMake(0, 0, 40, 40);
        rectBtnDelete.origin = CGPointMake(bgFrame.size.width - 40, bgFrame.size.height - 40);
        m_btnDelete = [[UIButton alloc] initWithFrame: rectBtnDelete];
        [self addSubview: m_btnDelete];
        [m_btnDelete addTarget: self action: @selector(deletePhoto:) forControlEvents: UIControlEventTouchUpInside];
        m_btnDelete.alpha = 0;

        m_FChoose = NO;
        m_FIsDelete = NO;
    }
    
    return self;
}

#pragma mark - Set Image to cell

- (void) configureByImage: (UIImage*) image Index: (int) nIndex
{
    m_sourceImage = image;
    m_nIndex = nIndex;
    
    [m_ImageContent setImage: image];
}

- (void) configureByURLString: (NSString*) strURL Index: (int) nIndex
{
    m_sourceImage = strURL;
    m_nIndex = nIndex;
    
    [m_ImageContent setImageWithURL: [NSURL URLWithString: strURL]];
}

- (void) choosePhoto:(id) sender
{
    if (!m_ImageContent.image) {
        return;
    }
    if (m_FEditMode) {
        m_FChoose = !m_FChoose;
        m_btnChoose.selected = m_FChoose;
        
        if (m_FChoose) {
            if ([delegate respondsToSelector: @selector(selectPhoto:)]) {
                [delegate performSelector: @selector(selectPhoto:) withObject: m_ImageContent.image];
            }
        } else {
            if ([delegate respondsToSelector: @selector(deselectPhoto:)]) {
                [delegate performSelector: @selector(deselectPhoto:) withObject: m_ImageContent.image];
            }
        }
    }
}

- (void) deletePhoto: (id) sender
{
    if ([delegate respondsToSelector: @selector(deletePhoto:)]) {
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", m_nIndex], @"index", nil];
        [delegate performSelectorOnMainThread: @selector(deletePhoto:) withObject: dict waitUntilDone: YES];
    }
}

@end
