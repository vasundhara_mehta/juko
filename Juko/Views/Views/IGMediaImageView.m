//
//  IGMediaImageView.m
//  Inkgram
//
//  Created by Mountain on 2/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGMediaImageView.h"

#import "UIImageView+WebCache.h"

@implementation IGMediaImageView

@synthesize bgView, delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect bgFrame = CGRectMake(0, 0, frame.size.width - 0, frame.size.height - 0);
        
        bgView = [[UIView alloc] initWithFrame:bgFrame];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.backgroundColor = [UIColor whiteColor].CGColor;
        
        [self addSubview:bgView];

        [bgView.layer setShadowOffset:CGSizeMake(0, 0)];
        [bgView.layer setShadowRadius:0.5f];
        [bgView.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [bgView.layer setShadowOpacity:0.5];

        // Image View
        _m_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width-0, frame.size.height-0)];
        _m_imageView.clipsToBounds = YES;
        [_m_imageView setContentMode: UIViewContentModeScaleAspectFill];
        
        [self addSubview: _m_imageView];
    }
    
    return self;
}

- (void) configureWithImage: (UIImage*) image
{
    [_m_imageView setImage: image];
}

- (void) configureWithURL: (NSString*) strUrl
{
    [_m_imageView setImageWithURL: [NSURL URLWithString: strUrl]];
}

@end
