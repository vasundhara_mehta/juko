//
//  IGPhotoCell.h
//  Inkgram
//
//  Created by Mountain on 2/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"

@interface IGPhotoCell : UIView<EKResusableCell, UIScrollViewDelegate>
{
    NSString *reuseIdentifier;
}

@property BOOL m_FEditMode;
@property id delegate;

@property (retain, nonatomic) UIImageView *m_ImageContent;
@property (nonatomic, retain) UIButton *m_btnChoose;
@property (nonatomic, retain) UIButton *m_btnDelete;

- (id)initWithFrame:(CGRect)frame ContentSize: (CGSize) szSize;
- (void) configureByImage: (UIImage*) image Index: (int) nIndex;
- (void) configureByURLString: (NSString*) strURL Index: (int) nIndex;

@end
