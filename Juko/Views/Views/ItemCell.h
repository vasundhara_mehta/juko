//
//  FBProductCell.h
//  FussBett
//
//  Created by Mountain on 1/7/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"
#import "DACircularProgressView.h"

@class IGItem;

@interface ItemCell : UIView<EKResusableCell, UIScrollViewDelegate>
{
    NSString *reuseIdentifier;
}

@property BOOL m_FLoaded;
@property (nonatomic, strong) IGItem* m_clsItem;

@property id delegate;

@property (retain, nonatomic) UIImageView *m_ImageView;
@property (retain, nonatomic) UIImageView *imgShadow;
//@property (retain, nonatomic) UIImageView *imgViewLocation;
//@property (nonatomic, readonly) UILabel *labelTitle;
//@property (nonatomic, readonly) UILabel *labelDesc;
//@property (nonatomic, readonly) UILabel *labelPrice;
@property (nonatomic, retain) UIButton *button;
@property (nonatomic, retain) UIButton *btnForLikeTap;
//@property (nonatomic, retain) UIView *viewLabel;
//@property (nonatomic, retain) UIView *viewDistance;
@property (nonatomic, strong) UIImageView* m_ShadowView;
@property (nonatomic, strong) NSArray* m_publishImages;
@property (nonatomic, strong) DACircularProgressView* m_ProgView;

@property (retain, nonatomic) NSString *m_strImgURL;
@property (retain, nonatomic) NSString* m_strOfferURL;

- (void) configureCell: (IGItem*) clsItem;

@end
