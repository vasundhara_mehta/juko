//
//  IGCategoryCell.m
//  Inkgram
//
//  Created by Mountain on 2/27/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGCategoryView.h"


@implementation IGCategoryView

@synthesize m_clsCategory;
@synthesize m_ButtonAction, m_Image_Item, m_Label_Name, m_Label_Price, m_View_Item, m_ButtonInfo;
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configure: (IGCategory*) clsItem
{
    m_clsCategory = clsItem;
    
    m_View_Item = [self viewWithTag: 1];
    
    m_Image_Item = (UIImageView*)[m_View_Item viewWithTag: 100];
    m_Label_Name = (UILabel*)[m_View_Item viewWithTag: 101];
//    m_Label_Price = (UILabel*)[m_View_Item viewWithTag: 102];
    m_ButtonAction = (UIButton*)[m_View_Item viewWithTag: 103];
    
    [m_Image_Item setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", clsItem.m_strImageName]]];
    [m_Label_Name setText: clsItem.m_strName];
//    [m_Label_Price setText: @""];//[NSString stringWithFormat: @"$%.2f", clsItem.m_rPrice]];
    [m_ButtonAction addTarget: self action: @selector(touchItem:) forControlEvents: UIControlEventTouchUpInside];
    
//    m_View_Item.layer.borderColor = [UIColor blackColor].CGColor;
//    m_View_Item.layer.borderWidth = 2;
//    
    [m_View_Item.layer setShadowOffset:CGSizeMake(0, 0)];
    [m_View_Item.layer setShadowRadius:1.5f];
    [m_View_Item.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [m_View_Item.layer setShadowOpacity:0.5];
    
    
    m_ButtonInfo = (UIButton*)[self viewWithTag: 2];
    [m_ButtonInfo addTarget: self action: @selector(touchInfoButton:) forControlEvents: UIControlEventTouchUpInside];
}

- (void) touchItem: (UIButton*) button
{
    if ([delegate canPerformAction: @selector(touchItem:) withSender: self]) {
        [delegate performSelectorOnMainThread: @selector(touchItem:) withObject: m_clsCategory waitUntilDone: YES];
    }
}

- (void) touchInfoButton: (UIButton*) button
{
    if ([delegate canPerformAction: @selector(touchInfoButton:) withSender: self]) {
        [delegate performSelectorOnMainThread: @selector(touchInfoButton:) withObject: m_clsCategory waitUntilDone: YES];
    }
}

@end
