//
//  FBProductCell.m
//  FussBett
//
//  Created by Mountain on 1/7/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "ItemCell.h"
#import "CUtils.h"
#import "const.h"

#import "IGItem.h"

#import "UIImageView+WebCache.h"

#define CONTENT_WIDTH 120
#define CONTENT_HEIGHT 120

#define BUTTON_HEIGHT 20

@implementation ItemCell
{
    BOOL m_FFirst;
}

@synthesize m_clsItem;
@synthesize delegate;

@synthesize reuseIdentifier;
@synthesize button;
@synthesize btnForLikeTap;
//@synthesize labelDesc;
//@synthesize labelPrice;
@synthesize m_strImgURL;
//@synthesize viewLabel;
//@synthesize viewDistance;
@synthesize imgShadow;
//@synthesize m_strOfferURL, imgViewLocation;
@synthesize m_publishImages;
@synthesize m_ImageView;
@synthesize m_ShadowView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect bgFrame = CGRectInset(self.bounds, 0.0f, 0.0f);
        
        UIView *bgView = [[UIView alloc] initWithFrame:bgFrame];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.backgroundColor = [UIColor whiteColor].CGColor;

        [bgView.layer setShadowOffset:CGSizeMake(0, 0)];
        [bgView.layer setShadowRadius:1.5f];
        [bgView.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [bgView.layer setShadowOpacity:0.5];

        [self addSubview:bgView];
        
        bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        // Image View
        m_ImageView = [[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width - CONTENT_WIDTH) / 2, (frame.size.height - CONTENT_HEIGHT) / 2, CONTENT_WIDTH, CONTENT_HEIGHT)];
        m_ImageView.opaque = NO;
        
        [self addSubview: m_ImageView];
        
        self.m_ProgView = [[DACircularProgressView alloc] initWithFrame: CGRectMake(frame.size.width/2 - 22, frame.size.height/2 - 22, 44, 44)];
        [self addSubview: self.m_ProgView];
        
        _m_ProgView.roundedCorners = NO;
        _m_ProgView.trackTintColor = [UIColor lightGrayColor];
        _m_ProgView.progressTintColor = [UIColor grayColor];
        
        self.m_ProgView.hidden = YES;
        
        btnForLikeTap = [UIButton buttonWithType: UIButtonTypeCustom];
        [btnForLikeTap setFrame: self.m_ProgView.frame];
        [btnForLikeTap addTarget:self action:@selector(tapOnce:) forControlEvents: UIControlEventTouchUpInside];

        button = [[UIButton alloc] initWithFrame: self.bounds];
        
        [button setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
        [button addTarget: self action: @selector(touchedProduct:) forControlEvents: UIControlEventTouchUpInside];
        [self addSubview: button];
    }
    
    return self;
}

#pragma mark - Set Image to cell

- (void) configureCell: (IGItem*) clsItem
{
    _m_ProgView.hidden = NO;
    [_m_ProgView setProgress: 0];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (m_ImageView.image) {
//                NSLog(@"%@", clsItem.m_strName);
//                [m_ImageView setImage: nil];
//            }
//        });
//    });
    [CUtils clearView: m_ImageView];
    m_ImageView.image = nil;
    
    m_clsItem = clsItem;
    __weak typeof(self) weakSelf = self;
    if (clsItem.m_arrayImageUrls && clsItem.m_arrayImageUrls.count > 0) {
        [m_ImageView setImageWithURL: [NSURL URLWithString: clsItem.m_arrayImageUrls[0]] placeholderImage: nil options: SDWebImageRetryFailed progress:^(NSUInteger receivedSize, long long expectedSize) {
            // progression tracking code
            CGFloat progress = (double)receivedSize / (double)expectedSize;
            progress = (progress > 1) ? 1 : progress;
            progress = (progress < 0) ? 0 : progress;
            
            [weakSelf.m_ProgView setProgress: progress animated: NO];
        } completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
            weakSelf.m_ProgView.hidden = YES;
            if (image && !error) {
                weakSelf.m_FLoaded = YES;
                if (clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
                    UIImage* frontImage = [CUtils imageWithImage: image cropByRect: CGRectMake(0, 0, image.size.width / 2, image.size.height)];
                    UIImage* backImage = [CUtils imageWithImage: image cropByRect: CGRectMake(image.size.width / 2, 0, image.size.width / 2, image.size.height)];
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: frontImage, backImage, nil];
                } else {
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: image, nil];
                }
                [weakSelf showImage];
                
                [weakSelf.btnForLikeTap setImage: nil forState: UIControlStateNormal];
            } else {
                weakSelf.m_FLoaded = NO;
                [weakSelf.btnForLikeTap setImage: [UIImage imageNamed: @"downloadFailed.png"] forState: UIControlStateNormal];
            }
        }];
    }
}

- (void) tapOnce: (id) sender
{
    if (!_m_FLoaded) {
        __weak typeof(self) weakSelf = self;
        _m_ProgView.hidden = NO;
        [_m_ProgView setProgress: 0];

        [m_ImageView setImageWithURL: [NSURL URLWithString: m_clsItem.m_arrayImageUrls[0]] placeholderImage: nil options: SDWebImageRetryFailed progress:^(NSUInteger receivedSize, long long expectedSize) {
            // progression tracking code
            CGFloat progress = (double)receivedSize / (double)expectedSize;
            progress = (progress > 1) ? 1 : progress;
            progress = (progress < 0) ? 0 : progress;
            
            [weakSelf.m_ProgView setProgress: progress animated: NO];
        } completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
            weakSelf.m_ProgView.hidden = YES;
            if (image && !error) {
                weakSelf.m_FLoaded = YES;
                if (weakSelf.m_clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
                    UIImage* frontImage = [CUtils imageWithImage: image cropByRect: CGRectMake(0, 0, image.size.width / 2, image.size.height)];
                    UIImage* backImage = [CUtils imageWithImage: image cropByRect: CGRectMake(image.size.width / 2, 0, image.size.width / 2, image.size.height)];
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: frontImage, backImage, nil];
                } else {
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: image, nil];
                }
                [(UIButton*)sender setImage: nil forState: UIControlStateNormal];
                
                //                UIView* viewImage = [weakSelf viewWithTag: 4];
                //                [viewImage setBackgroundColor: [UIColor colorWithWhite: 1.0f alpha: 1.0f]];
            } else {
                weakSelf.m_FLoaded = NO;
                [(UIButton*)sender setImage: [UIImage imageNamed: @"downloadFailed.png"] forState: UIControlStateNormal];
            }
        }];
    }
}


- (void) showImage
{
    if (m_publishImages == nil || m_publishImages.count == 0) {
        return;
    }
    
    [CUtils clearView: m_ImageView];
    
    CGPoint ptCenter = m_ImageView.center;
    m_ImageView.frame = CGRectMake((self.bounds.size.width - CONTENT_WIDTH) / 2, (self.bounds.size.height - CONTENT_HEIGHT) / 2, CONTENT_WIDTH, CONTENT_HEIGHT);
//    m_ImageView.center = ptCenter;
    
//    self.m_btnRotate.hidden = YES;
    
    UIImage* image = m_publishImages[0];
    
    CGSize szImage = image.size;
    CGRect rect = m_ImageView.bounds;
    
    if (szImage.width > szImage.height) {
        rect.size.height *= (szImage.height / szImage.width);
    } else {
        rect.size.width *= (szImage.width / szImage.height);
    }
    
    m_ImageView.frame = rect;
    m_ImageView.center = ptCenter;
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: m_ImageView.bounds];
    imgView.clipsToBounds = YES;
    imgView.tag = 1000;
    [m_ImageView addSubview: imgView];
    
    [imgView setImage: image];
    
    //    NSLog(@"%@, %f, %f", m_clsItem.m_strName, image.size.width, image.size.height);
    
    if (m_clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
//        self.m_btnRotate.hidden = NO;
//        m_FBackward = NO;
        [m_ImageView setImage: m_publishImages[0]];
    } else {
        if (m_clsItem.m_nCategoryIndex == ECATEGORY_CASE || m_clsItem.m_nCategoryIndex == ECATEGORY_SKIN || m_clsItem.m_nCategoryIndex == ECATEGORY_CANVAS) {
            imgView.opaque = YES;
            [self setDepthShadow: imgView];
        }
    }
}

- (void) setDropShadow: (UIView*) shadowDropView Rect: (CGRect) rect startPoint: (CGPoint) ptStartPoint endPoint: (CGPoint) ptEndPoint
{
    CAGradientLayer *shadow = [CAGradientLayer layer];
    shadow.frame = rect;
    shadow.startPoint = ptStartPoint;
    shadow.endPoint = ptEndPoint;
    shadow.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.0 alpha:0.1f] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    [self.m_ShadowView.layer addSublayer: shadow];
}

- (void) setDepthShadow: (UIView*) shadowDepth
{
    [shadowDepth.layer setMasksToBounds:NO ];
    [shadowDepth.layer setShadowColor:[[UIColor blackColor ] CGColor ] ];
    [shadowDepth.layer setShadowOpacity:0.3 ];
    [shadowDepth.layer setShadowRadius:6.0 ];
    [shadowDepth.layer setShadowOffset:CGSizeMake( 0 , 0 ) ];
    [shadowDepth.layer setShouldRasterize: NO ];
    
    float rYOffset = 0;
    
    switch (m_clsItem.m_nCategoryIndex)
    {
        case ECATEGORY_CASE:
            rYOffset = 10;
            break;
        case ECATEGORY_SKIN:
            rYOffset = 0;
            break;
        case ECATEGORY_CANVAS:
            //            [self setDropShadow: shadowDepth Rect: CGRectMake(3, shadowDepth.frame.size.height - 40, shadowDepth.frame.size.width - 5, 80) startPoint: CGPointMake(0.0, 0.0) endPoint: CGPointMake(0.3, 1)];
            rYOffset = 40;
            break;
            
        default:
            break;
    }
    UIBezierPath * depthShadowPath = [UIBezierPath bezierPath ];
    [depthShadowPath moveToPoint:CGPointMake( 0 , shadowDepth.frame.size.height ) ];
    [depthShadowPath addLineToPoint:CGPointMake( - 25 , shadowDepth.frame.size.height - 30 ) ];
    
    [depthShadowPath addLineToPoint:CGPointMake( 15 , shadowDepth.frame.size.height - 45 ) ];
    [depthShadowPath addLineToPoint:CGPointMake( shadowDepth.frame.size.width , shadowDepth.frame.size.height - rYOffset ) ];
    [depthShadowPath addLineToPoint:CGPointMake( 0 , shadowDepth.frame.size.height ) ];
    [shadowDepth.layer setShadowPath:[depthShadowPath CGPath ] ];
}

- (void) touchedProduct:(id) sender
{
    if ([self.delegate canPerformAction: @selector(touchedCell:) withSender:nil]) {
        [self.delegate performSelector: @selector(touchedCell:) withObject: m_clsItem afterDelay: 0];
    }
}

@end
