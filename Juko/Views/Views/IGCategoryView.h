//
//  IGCategoryCell.h
//  Inkgram
//
//  Created by Mountain on 2/27/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IGCategory.h"

@interface IGCategoryView : UITableViewCell

@property id delegate;
@property (nonatomic, strong) IGCategory* m_clsCategory;

@property (nonatomic, strong) UIView* m_View_Item;
@property (nonatomic, strong) UILabel* m_Label_Name;
@property (nonatomic, strong) UILabel* m_Label_Price;
@property (nonatomic, strong) UIImageView* m_Image_Item;
@property (nonatomic, strong) UIButton* m_ButtonAction;

@property (nonatomic, strong) UIButton* m_ButtonInfo;

- (void) configure: (IGCategory*) clsItem;

@end
