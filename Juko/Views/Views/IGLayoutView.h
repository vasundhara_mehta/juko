//
//  IGLayoutView.h
//  ;
//
//  Created by Mountain on 2/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>


@class IGProduct;
@class IGTemplate;
@class TKDragView;

@interface IGLayoutView : UIView

@property id delegate;
@property (nonatomic, strong) IGTemplate* m_clsTemplate;
@property (nonatomic, strong) IGProduct* m_clsProduct;
@property int m_nTemplateIndex;

@property (nonatomic, retain) UIView* bgView;
@property (nonatomic, retain) UIImageView* m_bgImageView;
@property (nonatomic, retain) UIView* m_layoutView;
@property (nonatomic, retain) UIImageView* m_maskImageView;
@property (nonatomic, retain) UIImageView* m_cameraImageView;
@property (nonatomic, retain) UIImageView* m_resultImageView;

@property (nonatomic, retain) NSMutableArray* m_LayoutRects;

- (void) configure: (int) nTemplateId Product: (IGProduct*) clsProduct;
- (void) setLayout;
- (NSDictionary*) completeImage;

//- (void) setImageToRect: (UIImage*) image RectIndex: (int) nRectIndex;
//- (void) setImageToRect: (UIImage*) image RectIndex: (int) nRectIndex MatchFrames: (NSArray*) frames;
- (void) setImageToRect: (UIImage*) image RectIndex: (int) nRectIndex MatchFrames: (NSArray*) frames ParentView:(UIView*) parentView;
- (void) setImageToRect: (UIImage*) image RectIndex: (int) nRectIndex;
- (void) setDragView: (TKDragView*) dragView Index: (int) nIndex;
- (void) clearImage: (TKDragView*) dragView;
- (BOOL) isPossibleToFinish;

@end
