//
//  FeedCellView.h
//  Juko
//
//  Created by Mountain on 2/2/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AMAttributedHighlightLabel.h"
#import "STTweetLabel.h"
#import "DACircularProgressView.h"

@class IGItem;

@protocol FeedCellViewDelegate <NSObject>

@optional
- (void) showAllComments: (IGItem*) clsItem;
- (void) showUser: (NSString*) strUser;
- (void) showTag: (NSString*) strTag;

@end

@interface FeedCellView : UITableViewCell<UIGestureRecognizerDelegate>

@property BOOL m_FShowDeleteButton;
@property BOOL m_FLoaded;

@property (nonatomic, strong) UIButton* m_allCommentButton;
@property (nonatomic, strong) UILabel* m_likeLabel;
@property (nonatomic, strong) UIButton* m_likeButton;
@property (nonatomic, strong) UIImageView* m_ImageView;
@property (nonatomic, strong) UIButton* m_btnRotate;
@property (nonatomic, strong) UIImageView* m_ShadowView;
@property (nonatomic, strong) NSArray* m_publishImages;

@property (nonatomic, strong) UIImageView* m_heartImageView;
@property (nonatomic, strong) DACircularProgressView* m_ProgView;

@property (nonatomic, strong) IGItem* m_clsItem;

@property id delegate;

- (void) configureCell: (IGItem*) clsItem;

@end
