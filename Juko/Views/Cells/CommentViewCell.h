//
//  CommentViewCell.h
//  Juko
//
//  Created by Mountain on 2/7/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewCell : UITableViewCell


- (CGFloat) height;

@end
