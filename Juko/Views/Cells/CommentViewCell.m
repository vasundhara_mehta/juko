//
//  CommentViewCell.m
//  Juko
//
//  Created by Mountain on 2/7/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "CommentViewCell.h"

@implementation CommentViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat) height
{
    return 44;
}

@end
