//
//  IGCartCell.m
//  Juko
//
//  Created by Mountain on 4/29/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGCartCell.h"

@implementation IGCartCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) willTransitionToState:(UITableViewCellStateMask)state
{
    if ((state & UITableViewCellStateShowingDeleteConfirmationMask) && (state & UITableViewCellStateShowingEditControlMask)) {
        if ([_delegate respondsToSelector: @selector(removeCell:)]) {
            [_delegate performSelectorOnMainThread: @selector(removeCell:) withObject: self waitUntilDone: YES];
        }
    }
    
    [super willTransitionToState:state];
}

@end
