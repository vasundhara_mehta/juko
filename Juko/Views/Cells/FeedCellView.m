//
//  FeedCellView.m
//  Juko
//
//  Created by Mountain on 2/2/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "FeedCellView.h"
#import "CUtils.h"
#import "DataKeeper.h"
#import "const.h"
#import "IGItem.h"
#import "IGItemUser.h"
#import "IGComment.h"

#import "UIImageView+WebCache.h"


@implementation FeedCellView
{
    BOOL m_FBackward;
}

@synthesize delegate;
@synthesize m_clsItem;

@synthesize m_allCommentButton, m_likeButton, m_likeLabel, m_ImageView, m_btnRotate, m_ShadowView, m_publishImages, m_heartImageView, m_ProgView, m_FLoaded;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureCell: (IGItem*) clsItem
{
    m_clsItem = clsItem;

    m_FBackward = NO;
    //notifications
    [[NSNotificationCenter defaultCenter] removeObserver: self name: ID_NOTI_POSTED_COMMENT object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: ID_NOTI_LIKED_ITEM object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(postedComment:) name: ID_NOTI_POSTED_COMMENT object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(likedItem:) name: ID_NOTI_LIKED_ITEM object: nil];
    ///////////////
    
//    imgView setImage
    UIView* viewImage = [self viewWithTag: 4];
//    [viewImage setBackgroundColor: [UIColor colorWithWhite: 0.86f alpha: 1.0f]];
    
    m_ProgView = (DACircularProgressView*)[viewImage viewWithTag: 102];
    m_ProgView.roundedCorners = NO;
    m_ProgView.trackTintColor = [UIColor lightGrayColor];
    m_ProgView.progressTintColor = [UIColor grayColor];
    
    m_btnRotate = (UIButton*)[viewImage viewWithTag: 103];
    m_heartImageView = (UIImageView*)[viewImage viewWithTag: 104];
    m_heartImageView.alpha = 0;
    
    UIImageView* imgView = (UIImageView*)[viewImage viewWithTag: 100];
    m_ImageView = imgView;
    [CUtils clearView: m_ImageView];
    
    UIButton* btnForLikeTap = (UIButton*)[viewImage viewWithTag: 105];
    
    UITapGestureRecognizer *tapOnce = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnce:)];
    UITapGestureRecognizer *tapTwice = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionLike:)];
    
    tapOnce.numberOfTapsRequired = 1;
    tapTwice.numberOfTapsRequired = 2;

    [tapOnce requireGestureRecognizerToFail:tapTwice];
    
    [btnForLikeTap addGestureRecognizer:tapOnce]; //remove the other button action which calls method `button`
    
    [btnForLikeTap addGestureRecognizer:tapTwice];
    
    if (clsItem.m_arrayImageUrls && clsItem.m_arrayImageUrls.count > 0) {
        
        if (clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
            m_btnRotate.hidden = NO;
            [m_btnRotate removeTarget: self action: @selector(actionRotateMug:) forControlEvents: UIControlEventTouchUpInside];
            [m_btnRotate addTarget: self action: @selector(actionRotateMug:) forControlEvents: UIControlEventTouchUpInside];
        } else {
            m_btnRotate.hidden = YES;
        }
        
        __weak typeof(self) weakSelf = self;
        
//        [SDWebImageDownloader.sharedDownloader downloadImageWithURL: [NSURL URLWithString: clsItem.m_arrayImageUrls[0]]
//                                                            options:0
//                                                           progress:^(NSUInteger receivedSize, long long expectedSize)
//         {
//             // progression tracking code
//             CGFloat progress = (double)receivedSize / (double)expectedSize;
//             if (progress > 1 || progress < 0) {
//                 progress = 0;
//             }
//             [m_ProgView setProgress: progress animated:YES];
//         }
//                                                          completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
//         {
//             if (image && finished)
//             {
//                 if (clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
//                     UIImage* frontImage = [CUtils imageWithImage: image cropByRect: CGRectMake(0, 0, image.size.width / 2, image.size.height)];
//                     UIImage* backImage = [CUtils imageWithImage: image cropByRect: CGRectMake(image.size.width / 2, 0, image.size.width / 2, image.size.height)];
//                     weakSelf.m_publishImages = [NSArray arrayWithObjects: frontImage, backImage, nil];
//                 } else {
//                     weakSelf.m_publishImages = [NSArray arrayWithObjects: image, nil];
//                 }
//                 [weakSelf showImage];
//             }
//         }];
        
        [m_ImageView setImageWithURL: [NSURL URLWithString: clsItem.m_arrayImageUrls[0]] placeholderImage: nil options: SDWebImageRetryFailed progress:^(NSUInteger receivedSize, long long expectedSize) {
            // progression tracking code
            weakSelf.m_ProgView.hidden = NO;
            CGFloat progress = (double)receivedSize / (double)expectedSize;
            progress = (progress > 1) ? 1 : progress;
            progress = (progress < 0) ? 0 : progress;

            [weakSelf.m_ProgView setProgress: progress animated: NO];
        } completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
            weakSelf.m_ProgView.hidden = YES;
            if (image && !error) {
                weakSelf.m_FLoaded = YES;
                if (clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
                    UIImage* frontImage = [CUtils imageWithImage: image cropByRect: CGRectMake(0, 0, image.size.width / 2, image.size.height)];
                    UIImage* backImage = [CUtils imageWithImage: image cropByRect: CGRectMake(image.size.width / 2, 0, image.size.width / 2, image.size.height)];
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: frontImage, backImage, nil];
                } else {
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: image, nil];
                }
                [weakSelf showImage];
                
                [btnForLikeTap setImage: nil forState: UIControlStateNormal];
//                [viewImage setBackgroundColor: [UIColor colorWithWhite: 1.0f alpha: 1.0f]];
            } else {
                weakSelf.m_FLoaded = NO;
                [btnForLikeTap setImage: [UIImage imageNamed: @"downloadFailed.png"] forState: UIControlStateNormal];
            }
        }];
    }
    
    UILabel* labelName = (UILabel*)[viewImage viewWithTag: 101];
    [labelName setText: clsItem.m_strRealName];

    //4 button's actions
    UIView* viewAction = [self viewWithTag: 5];
    UIButton* btnLike = (UIButton*)[viewAction viewWithTag: 101];
    [btnLike addTarget: self action: @selector(showLikers:) forControlEvents: UIControlEventTouchUpInside];
    m_likeButton = btnLike;
    btnLike.selected = clsItem.m_FLiked;
    
    UIButton* btnComment = (UIButton*)[viewAction viewWithTag: 102];
    [btnComment addTarget: self action: @selector(actionComment:) forControlEvents: UIControlEventTouchUpInside];
    UIButton* btnPurchase = (UIButton*)[viewAction viewWithTag: 103];
    [btnPurchase addTarget: self action: @selector(actionPurchase:) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton* btnShare = (UIButton*)[viewAction viewWithTag: 104];
    [btnShare addTarget: self action: @selector(actionShare:) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton* removeButton = (UIButton*)[viewAction viewWithTag: 105];
    [removeButton removeTarget: self action: @selector(remove:) forControlEvents: UIControlEventTouchUpInside];
    [removeButton addTarget: self action: @selector(remove:) forControlEvents: UIControlEventTouchUpInside];
    removeButton.hidden = !self.m_FShowDeleteButton;

    int nButtonCount = 4;
    if (!self.m_FShowDeleteButton) {
        nButtonCount --;
    }
    if (m_clsItem.m_nSell > 0) {
        btnPurchase.hidden = NO;
    } else {
        nButtonCount --;
        btnPurchase.hidden = YES;
    }
    
    ///layout buttons
    
    float rOffsetX = (240.0 / nButtonCount);
    for (int nIdx = 102; nIdx < 106; nIdx ++) {
        UIButton* button = (UIButton*)[viewAction viewWithTag: nIdx];
        if (!button.hidden) {
            button.frame = CGRectMake(rOffsetX, button.frame.origin.y, button.frame.size.width, button.frame.size.height);
            rOffsetX += (240.0 / nButtonCount);
        }
    }
    
    //like & comment count
    m_likeLabel = (UILabel*)[self viewWithTag: 201];
    [m_likeLabel setText: [NSString stringWithFormat: @"%d", clsItem.m_nLikeCount]];
    
    //description(creators comment)
    NSString* strUsername = clsItem.m_clsUser.m_strUsername;
    NSString* strDesc = clsItem.m_strDescription;
    
    UIView* viewCreator = [self viewWithTag: 6]; //creator view
    [self clearView: viewCreator];
    
    CGRect rect, frame;
    CGSize size;
    if (![strDesc isEqualToString: @" "]) {
        NSString* strCreator = [NSString stringWithFormat: @"@%@ %@", strUsername, strDesc];
        
        STTweetLabel *tweetLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 1000.0)];
//        tweetLabel.userInteractionEnabled = NO;
        [tweetLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 15.0f]];
        [tweetLabel setText: strCreator];
        tweetLabel.textAlignment = NSTextAlignmentLeft;
        [viewCreator addSubview:tweetLabel];
        
        size = [tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:tweetLabel.frame.size.width];
        frame = tweetLabel.frame;
        frame.size.height = size.height + 5;
        tweetLabel.frame = frame;
        
        rect = viewCreator.frame;
        rect.size.height = frame.size.height;
        [viewCreator setFrame: rect];
        
        [tweetLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
            if (hotWord == 0) { //handle
                [self showUser: string];
            } else if (hotWord == 1) { //hashtag
                [self showTag: string];
            }
        }];
    }
    
    //comment view
    UIView* viewComment = [self viewWithTag: 7]; //comment view
    [self clearView: viewComment];
    
    rect = viewComment.frame;
    rect.origin.y = viewCreator.frame.origin.y + viewCreator.frame.size.height;
    viewComment.frame = rect;
    
    m_allCommentButton = [[UIButton alloc] initWithFrame: CGRectMake(15, 0, 300, 20)];
    [m_allCommentButton setContentHorizontalAlignment: UIControlContentHorizontalAlignmentLeft];
    [m_allCommentButton setTitleColor: COLOR_LIGHT_GRAY forState: UIControlStateNormal];
    [m_allCommentButton.titleLabel setFont: [UIFont boldSystemFontOfSize: 15]];
    [m_allCommentButton setTitle: [NSString stringWithFormat: @"view all %d comments\n", clsItem.m_nCommentCount] forState: UIControlStateNormal];
    [m_allCommentButton addTarget: self action: @selector(showAllComments:) forControlEvents: UIControlEventTouchUpInside];
    
    [viewComment addSubview: m_allCommentButton];
    
    NSMutableString* strComment = [[NSMutableString alloc] init];

    int nCount = MIN(MAX_COMMENT_COUNT, clsItem.m_arrayComments.count);
    int nStartId = clsItem.m_arrayComments.count - nCount;
    for (int nIdx = nStartId; (nIdx < clsItem.m_arrayComments.count && nCount > 0); nIdx ++) {
        IGComment* clsComment = clsItem.m_arrayComments[nIdx];
        nCount --;
        if ([strComment isEqualToString: @""]) {
            [strComment appendFormat: @"@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
        } else {
            [strComment appendFormat: @"\n@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
        }
    }
    
    STTweetLabel *commentLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 1000.0)];
//    commentLabel.userInteractionEnabled = NO;
    [commentLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Medium" size: 15.0f]];
    [commentLabel setText: strComment];
    commentLabel.textAlignment = NSTextAlignmentLeft;
    [viewComment addSubview:commentLabel];
    
    size = [commentLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:commentLabel.frame.size.width];
    frame = commentLabel.frame;
    frame.size.height = size.height + 10;
    frame.origin.y = 20;
    commentLabel.frame = frame;
    
    rect = viewComment.frame;
    rect.size.height = frame.size.height + 20;
    [viewComment setFrame: rect];
    
    [commentLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
        if (hotWord == 0) { //handle
            [self showUser: string];
        } else if (hotWord == 1) { //hashtag
            [self showTag: string];
        }
    }];

}

- (void) showImage
{
    if (m_publishImages == nil || m_publishImages.count == 0) {
        return;
    }
    
    [CUtils clearView: m_ImageView];
    
    CGPoint ptCenter = m_ImageView.center;
    m_ImageView.frame = CGRectMake(0, 0, 185, 185);
    m_ImageView.center = ptCenter;
    
    self.m_btnRotate.hidden = YES;
    
    UIImage* image = m_publishImages[0];
    
    CGSize szImage = image.size;
    CGRect rect = m_ImageView.bounds;
    
    if (szImage.width > szImage.height) {
        rect.size.height *= (szImage.height / szImage.width);
    } else {
        rect.size.width *= (szImage.width / szImage.height);
    }
    
    m_ImageView.frame = rect;
    m_ImageView.center = ptCenter;
    m_ImageView.image = nil;
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: m_ImageView.bounds];
    imgView.clipsToBounds = YES;
    imgView.tag = 1000;
    [m_ImageView addSubview: imgView];
    
    [imgView setImage: image];
    
//    NSLog(@"%@, %f, %f", m_clsItem.m_strName, image.size.width, image.size.height);

    if (m_clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
        self.m_btnRotate.hidden = NO;
        m_FBackward = NO;
        [imgView setImage: m_publishImages[0]];
    } else {
        if (m_clsItem.m_nCategoryIndex == ECATEGORY_CASE || m_clsItem.m_nCategoryIndex == ECATEGORY_SKIN || m_clsItem.m_nCategoryIndex == ECATEGORY_CANVAS) {
            imgView.opaque = YES;
            [self setDepthShadow: imgView];
        }
    }
}

- (void) setDropShadow: (UIView*) shadowDropView Rect: (CGRect) rect startPoint: (CGPoint) ptStartPoint endPoint: (CGPoint) ptEndPoint
{
    CAGradientLayer *shadow = [CAGradientLayer layer];
    shadow.frame = rect;
    shadow.startPoint = ptStartPoint;
    shadow.endPoint = ptEndPoint;
    shadow.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.0 alpha:0.1f] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    [self.m_ShadowView.layer addSublayer: shadow];
}

- (void) setDepthShadow: (UIView*) shadowDepth
{
    [shadowDepth.layer setMasksToBounds:NO ];
    [shadowDepth.layer setShadowColor:[[UIColor blackColor ] CGColor ] ];
    [shadowDepth.layer setShadowOpacity:0.3 ];
    [shadowDepth.layer setShadowRadius:6.0 ];
    [shadowDepth.layer setShadowOffset:CGSizeMake( 0 , 0 ) ];
    [shadowDepth.layer setShouldRasterize: NO ];
    
    float rYOffset = 0;
    
    switch (m_clsItem.m_nCategoryIndex)
    {
        case ECATEGORY_CASE:
            rYOffset = 10;
            break;
        case ECATEGORY_SKIN:
            rYOffset = 0;
            break;
        case ECATEGORY_CANVAS:
//            [self setDropShadow: shadowDepth Rect: CGRectMake(3, shadowDepth.frame.size.height - 40, shadowDepth.frame.size.width - 5, 80) startPoint: CGPointMake(0.0, 0.0) endPoint: CGPointMake(0.3, 1)];
            rYOffset = 40;
            break;
            
        default:
            break;
    }
    UIBezierPath * depthShadowPath = [UIBezierPath bezierPath ];
    [depthShadowPath moveToPoint:CGPointMake( 0 , shadowDepth.frame.size.height ) ];
    [depthShadowPath addLineToPoint:CGPointMake( - 25 , shadowDepth.frame.size.height - 30 ) ];
    
    [depthShadowPath addLineToPoint:CGPointMake( 15 , shadowDepth.frame.size.height - 45 ) ];
    [depthShadowPath addLineToPoint:CGPointMake( shadowDepth.frame.size.width , shadowDepth.frame.size.height - rYOffset ) ];
    [depthShadowPath addLineToPoint:CGPointMake( 0 , shadowDepth.frame.size.height ) ];
    [shadowDepth.layer setShadowPath:[depthShadowPath CGPath ] ];
}

- (IBAction)actionRotateMug:(id)sender {
    UIImageView* imgView = (UIImageView*)[m_ImageView viewWithTag: 1000];
    m_FBackward = !m_FBackward;
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        if (m_FBackward) {
            [imgView setImage: m_publishImages[1]];
        } else {
            [imgView setImage: m_publishImages[0]];
        }
    }];
}


- (void) clearView: (UIView*) view
{
    for (UIView* subview in view.subviews) {
        [subview removeFromSuperview];
    }
}

#pragma mark - actions

- (void) showAllComments: (UIButton*) button
{
//    int nRow = button.tag;
//    NSString* strRow = [NSString stringWithFormat: @"%d", nRow];
    if ([delegate canPerformAction: @selector(showAllComments:) withSender: nil]) {
        [delegate performSelector: @selector(showAllComments:) withObject: m_clsItem afterDelay: 0];
    }
}

- (void) showUser: (NSString*) strUser
{
    if ([delegate canPerformAction: @selector(showUser:) withSender: nil]) {
        [delegate performSelector: @selector(showUser:) withObject: strUser afterDelay: 0];
    }
}

- (void) showTag: (NSString*) strTag
{
    if ([delegate canPerformAction: @selector(showTag:) withSender: nil]) {
        [delegate performSelector: @selector(showTag:) withObject: strTag afterDelay: 0];
    }
}

- (void) showLikers: (id) sender
{
    if (m_clsItem.m_nLikeCount <= 0) {
        return;
    }
    if ([delegate canPerformAction: @selector(showLikers:) withSender: nil]) {
        [delegate performSelector: @selector(showLikers:) withObject: m_clsItem afterDelay: 0];
    }
}

- (void) tapOnce: (id) sender
{
    if (!m_FLoaded) {
        __weak typeof(self) weakSelf = self;
        m_ProgView.hidden = NO;
        [m_ImageView setImageWithURL: [NSURL URLWithString: m_clsItem.m_arrayImageUrls[0]] placeholderImage: nil options: SDWebImageRetryFailed progress:^(NSUInteger receivedSize, long long expectedSize) {
            // progression tracking code
            CGFloat progress = (double)receivedSize / (double)expectedSize;
            progress = (progress > 1) ? 1 : progress;
            progress = (progress < 0) ? 0 : progress;
            
            [weakSelf.m_ProgView setProgress: progress animated: NO];
        } completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
            weakSelf.m_ProgView.hidden = YES;
            if (image && !error) {
                weakSelf.m_FLoaded = YES;
                if (weakSelf.m_clsItem.m_nCategoryIndex == ECATEGORY_MUG) {
                    UIImage* frontImage = [CUtils imageWithImage: image cropByRect: CGRectMake(0, 0, image.size.width / 2, image.size.height)];
                    UIImage* backImage = [CUtils imageWithImage: image cropByRect: CGRectMake(image.size.width / 2, 0, image.size.width / 2, image.size.height)];
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: frontImage, backImage, nil];
                } else {
                    weakSelf.m_publishImages = [NSArray arrayWithObjects: image, nil];
                }
                [(UIButton*)sender setImage: nil forState: UIControlStateNormal];
                
//                UIView* viewImage = [weakSelf viewWithTag: 4];
//                [viewImage setBackgroundColor: [UIColor colorWithWhite: 1.0f alpha: 1.0f]];
            } else {
                weakSelf.m_FLoaded = NO;
                [(UIButton*)sender setImage: [UIImage imageNamed: @"downloadFailed.png"] forState: UIControlStateNormal];
            }
        }];
    }
}

- (void) actionLike: (id) sender
{
    if ([delegate canPerformAction: @selector(actionLike:) withSender: nil]) {
        if (m_likeButton.selected) {
            [delegate performSelector: @selector(actionUnlike:) withObject: m_clsItem afterDelay: 0];
        } else {
            CGRect rect = m_heartImageView.frame;
            CGPoint ptCenter = m_heartImageView.center;
            rect.size.width = 20;
            rect.size.height = 20;
            m_heartImageView.frame = rect;
            m_heartImageView.center = ptCenter;
            m_heartImageView.alpha = 0;
            [UIView animateWithDuration: 1.0 animations: ^(void) {
                CGPoint ptCenter = m_heartImageView.center;
                CGRect rect = CGRectMake(ptCenter.x - 40, ptCenter.y - 40, 80, 80);
                m_heartImageView.frame = rect;
                m_heartImageView.alpha = 1.0;
            } completion: ^(BOOL finish) {
                m_heartImageView.alpha = 0.0;
            }];
            [delegate performSelector: @selector(actionLike:) withObject: m_clsItem afterDelay: 0];
        }
    }
}

- (void) actionComment: (id) sender
{
    if ([delegate canPerformAction: @selector(actionComment:) withSender: nil]) {
        [delegate performSelector: @selector(actionComment:) withObject: m_clsItem afterDelay: 0];
    }
}

- (void) actionPurchase: (id) sender
{
    if ([delegate canPerformAction: @selector(actionPurchase:) withSender: nil]) {
        [delegate performSelector: @selector(actionPurchase:) withObject: m_clsItem afterDelay: 0];
    }
}

- (void) actionShare: (id) sender
{
    if ([delegate canPerformAction: @selector(actionShare:) withSender: nil]) {
        if (m_publishImages == nil || m_publishImages.count == 0) {
            ALERT_SHOW(@"Image has been downloaded yet");
            return;
        }
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: m_clsItem, @"item", m_publishImages[0], @"image", nil];
        [delegate performSelector: @selector(actionShare:) withObject: dict afterDelay: 0];
    }
}

- (void)selectedMention:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}
- (void)selectedHashtag:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}
- (void)selectedLink:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void) remove: (id) sender
{
    if ([delegate respondsToSelector: @selector(removeItem:)]) {
        [delegate performSelectorOnMainThread: @selector(removeItem:) withObject: m_clsItem waitUntilDone: YES];
    }
}


#pragma mark - observer method

- (void) postedComment: (NSNotification*) noti
{
    NSDictionary* dict = noti.object;
    if (dict && [dict objectForKey: NOTI_ITEM_ID]) {
        int nItemIndex = [[dict objectForKey: NOTI_ITEM_ID] intValue];
        if (nItemIndex == m_clsItem.m_nIndex) {
//            if ([dict objectForKey: @"delete"]) {
//                
//            } else {
//                m_clsItem.m_nCommentCount ++;
//            }
            [m_allCommentButton setTitle: [NSString stringWithFormat: @"view all %d comments\n", m_clsItem.m_nCommentCount] forState: UIControlStateNormal];
            
            if ([delegate respondsToSelector: @selector(refreshCells:)]) {
                [delegate performSelectorOnMainThread: @selector(refreshCells:) withObject: self waitUntilDone: YES];
            }
        }
    }
}

- (void) likedItem: (NSNotification*) noti
{
    NSDictionary* dict = noti.object;
    if (dict && [dict objectForKey: NOTI_ITEM_ID]) {
        int nItemIndex = [[dict objectForKey: NOTI_ITEM_ID] intValue];
        if (nItemIndex == m_clsItem.m_nIndex) {
            if (m_likeButton.selected) {
                m_clsItem.m_nLikeCount --;
                m_likeButton.selected = NO;
            } else {
                m_clsItem.m_nLikeCount ++;
                m_likeButton.selected = YES;
            }
            
            [m_likeLabel setText: [NSString stringWithFormat: @"%d", m_clsItem.m_nLikeCount]];
        }
    }
}

@end
