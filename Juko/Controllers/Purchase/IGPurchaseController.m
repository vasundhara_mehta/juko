//
//  IGPurchaseController.m
//  Juko
//
//  Created by Mountain on 2/7/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGPurchaseController.h"

#import "IGProgressActivity.h"
#import "STPCard.h"
#import "STPToken.h"
#import "Stripe.h"

#import "const.h"
#import "CUtils.h"
#import "DataKeeper.h"

#import "IGCategory.h"
#import "IGProduct.h"
#import "IGUser.h"

#import "PKTextField.h"

typedef void (^STPTokenBlock)(STPToken *token, NSError *error);

@interface IGPurchaseController ()

@end

@implementation IGPurchaseController
{
    float rHeight;
    NSArray* m_arrayData;
    int m_nRow;
    int m_nProductId;
    int m_nStatesId;
    
    IGCategory* m_clsCategory;
    int m_nSizeTag;
    
    NSString* m_strCardToken;
    BOOL m_FVerifyCard;
    
    float m_rTotalPrice;
    int m_nPickerKind; // 0 - normal(product), 1 - state, 2 - country, 3 - shipping mode(speed)
    
    NSArray* m_states;
    NSArray* m_countries;
    NSArray* m_shippingMethods;
    
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_countries = [NSArray arrayWithObjects: @"United States", nil];
        m_shippingMethods = [NSArray arrayWithObjects: @"Standard", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [self.m_LabelPrice setText: [NSString stringWithFormat: @"$%.2f USD", _m_clsItem.m_rPrice]];
    
    float rShippingPrice = _m_clsItem.m_rShippingPrice;//(_m_clsItem.m_nCategoryIndex == ECATEGORY_CANVAS) ? CANVAS_SHIPPING : ITEM_SHIPPING;
    [self.m_LabelShippingPrice setText: [NSString stringWithFormat: @"$%.2f USD", rShippingPrice]];

    [self.m_LabelTotalPrice setText: [NSString stringWithFormat: @"$%.2f USD", (_m_clsItem.m_rPrice + rShippingPrice)]];
    m_rTotalPrice = (_m_clsItem.m_rPrice + rShippingPrice);
    
    self.paymentView = [[PKView alloc] initWithFrame:CGRectMake(15, 5, 290, 45)];
    self.paymentView.delegate = self;
    
    [self.m_ViewPurchase addSubview:self.paymentView];
    
    m_states = [[DataKeeper sharedInstance] getStates];
    
    [self addOptionViews];
}

- (void) addOptionViews
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    int nCategoryIndex = self.m_clsItem.m_nCategoryIndex;
    m_clsCategory = [clsModel getCategoryByIndex: nCategoryIndex];
    
    [self.m_ViewOption addSubview: self.m_ViewChooseSize];
    [self.m_ViewOption addSubview: self.m_ViewChooseProduct];
    
    if (nCategoryIndex == ECATEGORY_CASE || nCategoryIndex == ECATEGORY_SKIN) {
        self.m_ViewChooseSize.hidden = YES;
        UILabel* label = (UILabel*)[self.m_ViewChooseProduct viewWithTag: 100];
        [label setText: self.m_clsItem.m_strRealName];
        
        m_arrayData = m_clsCategory.m_ProductList;
        
        m_nRow = [clsModel getProductIndex: self.m_clsItem.m_strRealName Category: m_clsCategory];
        m_nProductId = m_nRow;
        
        UIButton* button = (UIButton*)[self.m_ViewChooseProduct viewWithTag: 101];
        [button addTarget: self action: @selector(showProducts:) forControlEvents: UIControlEventTouchUpInside];
        
    } else if (nCategoryIndex == ECATEGORY_SHIRTS) {
        self.m_ViewChooseProduct.hidden = YES;
        for (int nIdx = 100; nIdx < 105; nIdx ++) {
            UIButton* button = (UIButton*)[self.m_ViewChooseSize viewWithTag: nIdx];
            button.selected = NO;
            if (nIdx == 102) {
                button.selected = YES;
                m_nSizeTag = 102;
            }
            [button addTarget: self action: @selector(chooseSize:) forControlEvents: UIControlEventTouchUpInside];
            
            UISegmentedControl* segGender = (UISegmentedControl*)[self.m_ViewChooseSize viewWithTag: 105];
            [segGender addTarget: self action: @selector(chooseGender:) forControlEvents: UIControlEventValueChanged];
        }
    } else {
        self.m_ViewChooseProduct.hidden = YES;
        self.m_ViewChooseSize.hidden = YES;
    }
}

- (void) chooseGender: (UISegmentedControl*) segGender
{
    if (segGender.selectedSegmentIndex == 0) {
        UIButton* btnXS = (UIButton*)[self.m_ViewChooseSize viewWithTag: 100];
        btnXS.hidden = NO;
        UIButton* btnXL = (UIButton*)[self.m_ViewChooseSize viewWithTag: 104];
        btnXL.hidden = NO;
    } else {
        UIButton* btnXS = (UIButton*)[self.m_ViewChooseSize viewWithTag: 100];
        btnXS.hidden = YES;
        UIButton* btnXL = (UIButton*)[self.m_ViewChooseSize viewWithTag: 104];
        btnXL.hidden = YES;
    }
}

- (void) showProducts: (UIButton*) button
{
    m_nPickerKind = 0;
    [self showPickerView];
}

- (void) showPickerView
{
    [self.m_pickerView reloadAllComponents];

    self.m_btnPickerNext.enabled = NO;
    if (m_nPickerKind == 1) {
        self.m_btnPickerNext.enabled = YES;
        if (!self.m_btnBilling.selected) {
            m_nStatesId = [[DataKeeper sharedInstance] getStateIdByKey: self.m_txtState.text];
            m_nRow = m_nStatesId;
        } else {
            m_nStatesId = [[DataKeeper sharedInstance] getStateIdByKey: self.m_txtBillState.text];
            m_nRow = m_nStatesId;
        }
        [self.m_pickerView selectRow: m_nRow inComponent: 0 animated: YES];
    } else if (m_nPickerKind == 0) {
        m_nRow = m_nProductId;
        [self.m_pickerView selectRow: m_nRow inComponent: 0 animated: YES];
    }
    
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_View_Picker.frame;
        rect.origin.x = 0;
        rect.origin.y = self.view.frame.size.height - rect.size.height;
        self.m_View_Picker.frame = rect;
    }];
}

///////////////////////////////////////

#pragma mark - UIPickerViewDelegate

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (m_nPickerKind) {
        case 0:
            return m_arrayData.count;
            break;
        case 1:
            return m_states.count;
            break;
        case 2:
            return m_countries.count;
            break;
        case 3:
            return m_shippingMethods.count;
            break;
        default:
            break;
    }
    return 0;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (m_nPickerKind) {
        case 0:
        {
            IGProduct* clsProduct = m_arrayData[row];
            return clsProduct.m_strRealName;
            break;
        }
        case 1:
        {
            NSDictionary* dictState = m_states[row];
            NSString* strKey = [dictState.allKeys objectAtIndex: 0];
            NSString* strState = [dictState objectForKey: strKey];
            
            if (!self.m_btnBilling.selected) {
                if ([strKey isEqualToString: self.m_txtState.text]) {
                    m_nRow = row;
                    m_nStatesId = m_nRow;
                }
            } else {
                if ([strKey isEqualToString: self.m_txtBillState.text]) {
                    m_nRow = row;
                    m_nStatesId = m_nRow;
                }
            }
            
            return strState;
            break;
        }
        case 2:
        {
            m_nRow = 0;
            return m_countries[row];
            break;
        }
        case 3:
        {
            m_nRow = 0;
            return m_shippingMethods[row];
            break;
        }
        default:
            break;
    }
    return @"";
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    m_nRow = row;
    switch (m_nPickerKind) {
        case 0: //product
            m_nProductId = m_nRow;
            break;
        case 1: //states
            m_nStatesId = m_nRow;
            break;
        default:
            break;
    }
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

#pragma mark - Some Button Actions

- (IBAction)chooseProduct:(id)sender {
    if (m_nPickerKind == 0) {
        UILabel* label = (UILabel*)[self.m_ViewChooseProduct viewWithTag: 100];
        
        IGProduct* clsProduct = m_clsCategory.m_ProductList[m_nProductId];
        [label setText: clsProduct.m_strRealName];
        
        [NSThread detachNewThreadSelector: @selector(calcPrice:) toTarget: self withObject: clsProduct];
    } else if (m_nPickerKind == 1) {
        NSDictionary* dictState = m_states[m_nStatesId];
        NSString* strKey = [dictState.allKeys objectAtIndex: 0];

        if (!self.m_btnBilling.selected) {
            [self.m_txtState setText: strKey];
        } else {
            [self.m_txtBillState setText: strKey];
        }
    }
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_View_Picker.frame;
        rect.origin.y = self.view.frame.size.height;
        self.m_View_Picker.frame = rect;
    }];
}

- (IBAction)nextProcess:(id)sender
{
    [self chooseProduct: nil];
    
    UITextField* textField;
    if (!self.m_btnBilling.selected) {
        textField = (UITextField*)[self.m_ViewPurchase viewWithTag: 104];
    } else {
        textField = (UITextField*)[self.m_ViewPurchase viewWithTag: 204];
    }
    
    [textField becomeFirstResponder];
}

- (IBAction)chooseCountry:(id)sender {
    m_nPickerKind = 2;
    [self showPickerView];
}

- (IBAction)chooseShipMode:(id)sender {
    m_nPickerKind = 3;
    [self showPickerView];
}

//////when billing address is not same as shipping address
- (IBAction)actionBillingAddress:(id)sender {
    self.m_btnBilling.selected = !self.m_btnBilling.selected;
    if (self.m_btnBilling.selected) {
        [self.m_LabelAddress setText: @"Billing Information"];
        UIView* viewShip = [self.m_ViewPurchase viewWithTag: 1];
        UIView* viewBill = [self.m_ViewPurchase viewWithTag: 2];
        [UIView animateWithDuration: 0.5 animations: ^(void) {
            CGRect rect = viewShip.frame;
            rect.origin.x = -320;
            viewShip.frame = rect;
            
            rect.origin.x = 0;
            viewBill.frame = rect;
        }];
    } else {
        [self.m_LabelAddress setText: @"Shipping Information"];
        UIView* viewShip = [self.m_ViewPurchase viewWithTag: 1];
        UIView* viewBill = [self.m_ViewPurchase viewWithTag: 2];
        [UIView animateWithDuration: 0.5 animations: ^(void) {
            CGRect rect = viewShip.frame;
            rect.origin.x = 0;
            viewShip.frame = rect;
            
            rect.origin.x = 320;
            viewBill.frame = rect;
        }];
    }
}

- (void) calcPrice: (IGProduct*) clsProduct
{
    if (!clsProduct.m_FLoaded) {
        DataKeeper* clsModel = [DataKeeper sharedInstance];
        NSDictionary* dict = [clsModel getProductInfo: clsProduct.m_nIndex];
        
        clsProduct.m_strDescription = [[CUtils getStringValueFromDictionary: dict KEY: @"description"] copy];
        clsProduct.m_rPrice = [CUtils getFloatValueFromDictionary: dict KEY: @"price"];
    }
    
    float rPrice = clsProduct.m_rPrice;
    rPrice *= 1.25f;//self.m_clsItem.m_rProfit;
    [self.m_LabelPrice setText: [NSString stringWithFormat: @"$%.2f", rPrice]];
    
    float rShippingPrice = _m_clsItem.m_rShippingPrice;//(clsProduct.m_nCategoryIndex == ECATEGORY_CANVAS) ? CANVAS_SHIPPING : ITEM_SHIPPING;

    m_rTotalPrice = (rPrice + rShippingPrice);
    [self.m_LabelTotalPrice setText: [NSString stringWithFormat: @"$%.2f", m_rTotalPrice]];
}

- (void) chooseSize: (UIButton*) btnSize
{
    for (int nIdx = 100; nIdx < 105; nIdx ++) {
        UIButton* button = (UIButton*)[self.m_ViewChooseSize viewWithTag: nIdx];
        button.selected = NO;
    }
    
    btnSize.selected = YES;
    m_nSizeTag = btnSize.tag;
}

///////////////////////////////////////

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    rHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        rHeight = 530;
    }
    
    CGRect rect = self.m_ViewOrder.frame;
    rect.origin.y = rHeight + 100;
    [self.m_ViewOrder setFrame: rect];
    
    rect = self.m_ViewPurchase.frame;
    rect.origin.y = rHeight + 100;
    [self.m_ViewPurchase setFrame: rect];
    
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_ViewOrder.frame;
        rect.origin.y = rHeight - rect.size.height;
        [self.m_ViewOrder setFrame: rect];
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//order view
- (IBAction)showStateView:(id)sender {
    m_nPickerKind = 1;
    
    [self showPickerView];
}

- (IBAction)actionOrderNow:(id)sender {
    
    [self showAddressInfo];
    
    [UIView animateWithDuration: 0.2 animations: ^(void) {
        CGRect rect = self.m_ViewOrder.frame;
        rect.origin.y = rHeight;
        [self.m_ViewOrder setFrame: rect];
    } completion: ^(BOOL finish) {
        [UIView animateWithDuration: 0.3 animations: ^(void) {
            CGRect rect = self.m_ViewPurchase.frame;
            rect.origin.y = rHeight - rect.size.height;
            [self.m_ViewPurchase setFrame: rect];
        }];
    }];
}

- (void) showAddressInfo
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    IGUser* clsUser = [clsModel getUserInfo];
    UITextField* txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 100];
    [txtName setText: clsUser.m_strName];

    UITextField* txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 104];
    [txtCity setText: clsUser.m_strCity];

    UITextField* txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 101];
    [txtAddress setText: clsUser.m_strAddress];
    
    UITextField* txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 102];
    [txtAdditionalAddress setText: clsUser.m_strAdditionalAddress];

    UITextField* txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 103];
    [txtZip setText: clsUser.m_strZip];
    
    [self.m_txtState setText: clsUser.m_strState];
    
    UITextField* txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 105];
    [txtEmail setText: clsUser.m_strEmail];
    
    UITextField* txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 106];
    [txtPhone setText: clsUser.m_strPhone];

    txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 200];
    [txtName setText: clsUser.m_strName];
    
    txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 204];
    [txtCity setText: clsUser.m_strCity];
    
    txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 201];
    [txtAddress setText: clsUser.m_strAddress];
    
    txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 202];
    [txtAdditionalAddress setText: clsUser.m_strAdditionalAddress];
    
    txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 203];
    [txtZip setText: clsUser.m_strZip];
    
    [self.m_txtBillState setText: clsUser.m_strState];
    
    txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 205];
    [txtEmail setText: clsUser.m_strEmail];
    
    txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 206];
    [txtPhone setText: clsUser.m_strPhone];
}

- (IBAction) addToCart:(id)sender {
    BOOL FRes = NO;
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSString* strSize = @"";
    if (m_clsCategory.m_nIndex != ECATEGORY_CASE && m_clsCategory.m_nIndex != ECATEGORY_SKIN) {
        if (m_clsCategory.m_nIndex == ECATEGORY_SHIRTS) {
            UISegmentedControl* segGender = (UISegmentedControl*)[self.m_ViewChooseSize viewWithTag: 105];
            UIButton* button = (UIButton*)[self.m_ViewChooseSize viewWithTag: m_nSizeTag];
            if (segGender.selectedSegmentIndex == 0) {
                strSize = [NSString stringWithFormat: @"Size : %@, Gender : Man", button.titleLabel.text];
            } else {
                strSize = [NSString stringWithFormat: @"Size : %@, Gender : Woman", button.titleLabel.text];
            }
        }
        
        FRes = [clsModel addItemToCart: self.m_clsItem.m_nIndex Quantity: 1 Additional: strSize];
    } else {
        IGProduct* clsProduct = m_clsCategory.m_ProductList[m_nProductId];
        if ([self.m_clsItem.m_strRealName.lowercaseString isEqualToString: clsProduct.m_strRealName.lowercaseString]) {
            FRes = [clsModel addItemToCart: self.m_clsItem.m_nIndex Quantity: 1 Additional: @""];
        } else {
            int nNewItemId = [clsModel createHybrid: self.m_clsItem.m_nIndex ProductId: clsProduct.m_nIndex];
            if (nNewItemId > -1) {
                FRes = [clsModel addItemToCart: nNewItemId Quantity: 1 Additional: @""];
            } else {
                NSLog(@"Can't create new item");
                FRes = NO;
            }
        }
    }
    
    if (FRes) {
        if (_delegate && [_delegate respondsToSelector: @selector(purchased:)]) {
            [self.overlayController dismissOverlay: YES];
            [self performSelector: @selector(delaySend:) withObject: @"Successfully added to your cart" afterDelay: 0.1];
        } else {
            ALERT_SHOW(@"Successfully added to your cart");
            [self.overlayController dismissOverlay: YES];
        }
    } else {
        ALERT_SHOW(@"Failure");
    }
}

- (void) delaySend: (NSString*) strMessage
{
    [_delegate performSelectorOnMainThread: @selector(purchased:) withObject: strMessage waitUntilDone: YES];
}
///////////////////


////purchase view/////
- (IBAction)actionPurchase:(id)sender {
    
    if (!m_FVerifyCard) {
        ALERT_SHOW(@"Please input your card info");
        return;
    }
    if (![self checkAddress]) {
        return;
    }
    
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(purchaseProcess) toTarget: self withObject: nil];
}

- (void) closePurchaseView
{
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = _m_ViewPurchase.frame;
        rect.origin.y = rHeight;
        _m_ViewPurchase.frame = rect;
    } completion: ^(BOOL finish) {
        if (_delegate && [_delegate respondsToSelector: @selector(purchased:)]) {
            [self.overlayController dismissOverlay: YES];
            [self performSelector: @selector(delaySend:) withObject: @"Thank you for your order!" afterDelay: 0.1];
        } else {
            ALERT_SHOW(@"Thank you for your order!");
            [self.overlayController dismissOverlay: YES];
        }
    }];
}

- (void) purchaseProcess
{
    NSString* strParam = [self getPurchaseParam];
    NSString* strFullParam = strParam;
    int nItemIndex = self.m_clsItem.m_nIndex;
    
    BOOL FRes = YES;
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSString* strSize = @"";
    if (m_clsCategory.m_nIndex != ECATEGORY_CASE && m_clsCategory.m_nIndex != ECATEGORY_SKIN) {
        if (m_clsCategory.m_nIndex == ECATEGORY_SHIRTS) {
            UISegmentedControl* segGender = (UISegmentedControl*)[self.m_ViewChooseSize viewWithTag: 105];
            UIButton* button = (UIButton*)[self.m_ViewChooseSize viewWithTag: m_nSizeTag];
            if (segGender.selectedSegmentIndex == 0) {
                strSize = [NSString stringWithFormat: @"Size : %@, Gender : Man", button.titleLabel.text];
            } else {
                strSize = [NSString stringWithFormat: @"Size : %@, Gender : Woman", button.titleLabel.text];
            }
            strFullParam = [NSString stringWithFormat: @"%@&additional_info=%@", strParam, strSize];
        }
    } else {
        IGProduct* clsProduct = m_clsCategory.m_ProductList[m_nProductId];
        if ([self.m_clsItem.m_strRealName.lowercaseString isEqualToString: clsProduct.m_strRealName.lowercaseString]) {
        } else {
            int nNewItemId = [clsModel createHybrid: self.m_clsItem.m_nIndex ProductId: clsProduct.m_nIndex];
            if (nNewItemId > -1) {
                nItemIndex = nNewItemId;
            } else {
                NSLog(@"Can't create new item");
                FRes = NO;
            }
        }
    }
    
    if (FRes) {
        BOOL FResult = [clsModel itemCheckout: nItemIndex CardToken: m_strCardToken ParamInfo: strFullParam];
        [HUD hide: YES];

        if (!FResult) {
            ALERT_SHOW(@"Please check your payment parameters\nAlso you must input card info again");
        } else {
//            ALERT_SHOW(@"Thank you for your order!")
            [self performSelectorOnMainThread: @selector(closePurchaseView) withObject: nil waitUntilDone: YES];
        }
    }
    
    [HUD hide: YES];
}

- (NSString*) getPurchaseParam
{
    NSMutableString* strParam = [[NSMutableString alloc] init];
    
    UITextField* txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 100];
    [strParam appendString: [NSString stringWithFormat: @"name=%@", txtName.text]];
    
    UITextField* txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 104];
    UITextField* txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 101];
    UITextField* txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 102];
    UITextField* txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 103];
    if (txtAdditionalAddress.text == nil || [txtAdditionalAddress.text isEqualToString: @""]) {
        [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
    } else {
        [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Apt/Suite/Bldg : %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtAdditionalAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
    }
    
    UITextField* txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 105];
    [strParam appendString: [NSString stringWithFormat: @"&email=%@", txtEmail.text]];
    
    UITextField* txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 106];
    [strParam appendString: [NSString stringWithFormat: @"&phone=%@", txtPhone.text]];
    
    if (self.m_btnBilling.selected) {
        txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 200];
        [strParam appendString: [NSString stringWithFormat: @"&billing_address=(name: %@", txtName.text]];
        
        UITextField* txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 204];
        UITextField* txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 201];
        UITextField* txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 202];
        UITextField* txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 203];
        if (txtAdditionalAddress.text == nil || [txtAdditionalAddress.text isEqualToString: @""]) {
            [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
        } else {
            [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Apt/Suite/Bldg : %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtAdditionalAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
        }
        
        UITextField* txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 205];
        [strParam appendString: [NSString stringWithFormat: @"&email=%@", txtEmail.text]];
        
        UITextField* txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 206];
        [strParam appendString: [NSString stringWithFormat: @"&phone=%@", txtPhone.text]];
    }
    
    return strParam;
}

#define TEXT_COUNT 7
- (BOOL) checkAddress
{
    for (int nIdx = 0; nIdx < TEXT_COUNT; nIdx ++) {
        UITextField* txtField = (UITextField*)[self.m_ViewPurchase viewWithTag: (100 + nIdx)];
        if (txtField.tag == 102 || txtField.tag == 104) {
            continue;
        }
        if (txtField.text == nil || [txtField.text isEqualToString: @""]) {
            ALERT_SHOW(@"Please fill all information");
            return NO;
        }
        if (self.m_btnBilling.selected) {
            txtField = (UITextField*)[self.m_ViewPurchase viewWithTag: (200 + nIdx)];
            if (txtField.tag == 202 || txtField.tag == 204) {
                continue;
            }
            if (txtField.text == nil || [txtField.text isEqualToString: @""]) {
                ALERT_SHOW(@"Please fill all information");
                return NO;
            }
        }
    }
    if (self.m_txtState.text == nil || [self.m_txtState.text isEqualToString: @""]) {
        ALERT_SHOW(@"Choose State");
        return NO;
    }
    
    if (self.m_btnBilling.selected) {
        if (self.m_txtBillState.text == nil || [self.m_txtBillState.text isEqualToString: @""]) {
            ALERT_SHOW(@"Choose Billing State");
            return NO;
        }
    }
    
    return YES;
}

//////////////////////////////
- (IBAction)actionDissmiss:(id)sender {
    if (self.m_btnDismissKeyboard.hidden == NO) {
        [self dismissKeyboard: nil];
        return;
    }
    if (self.m_View_Picker.frame.origin.y < self.view.frame.size.height) {
        [UIView animateWithDuration: 0.5 animations: ^(void) {
            CGRect rect = self.m_View_Picker.frame;
            rect.origin.y = self.view.frame.size.height;
            self.m_View_Picker.frame = rect;
        }];
        return;
    }
    
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_ViewOrder.frame;
        rect.origin.y = rHeight;
        _m_ViewOrder.frame = rect;

        rect = _m_ViewPurchase.frame;
        rect.origin.y = rHeight;
        _m_ViewPurchase.frame = rect;
    } completion: ^(BOOL finish) {
        [self.overlayController dismissOverlay: YES];
    }];
}

- (IBAction)dismissKeyboard:(id)sender {
    for (int nTag = 100; nTag < 107; nTag ++) {
        UITextField* textField = (UITextField*)[self.m_ViewPurchase viewWithTag: nTag];
        [textField resignFirstResponder];
        textField = (UITextField*)[self.m_ViewPurchase viewWithTag: 100 + nTag];
        [textField resignFirstResponder];
    }
    
    [self.paymentView.cardCVCField resignFirstResponder];
    [self.paymentView.cardExpiryField resignFirstResponder];
    [self.paymentView.cardNumberField resignFirstResponder];
    
    self.m_btnDismissKeyboard.hidden = YES;
    
    CGRect rect = self.m_ViewPurchase.frame;
    rect.origin.y = self.view.frame.size.height - rect.size.height;
    self.m_ViewPurchase.frame = rect;
}

- (void) showDismissKeyboardButton
{
    self.m_btnDismissKeyboard.hidden = NO;
}

- (void) overlayRemoved:(UIXOverlayController *)overlayController
{
    
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    int nTag = textField.tag + 1;
    UITextField* nextField = (UITextField*)[self.m_ViewPurchase viewWithTag: nTag];
    if (nextField) {
        if (nextField.tag == 104 || nextField.tag == 204) {
            [textField resignFirstResponder];
            [self showStateView: nil];
            return YES;
        }
        [nextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        
        CGRect rect = self.m_ViewPurchase.frame;
        rect.origin.y = self.view.frame.size.height - rect.size.height;
        self.m_ViewPurchase.frame = rect;
        
        self.m_btnDismissKeyboard.hidden = YES;
    }
    
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    [self showDismissKeyboardButton];
    
    CGRect rect = textField.superview.frame;
    rect.origin.y += (self.m_ViewPurchase.frame.origin.y + textField.superview.superview.frame.origin.y);
    if (rect.origin.y + rect.size.height > self.view.frame.size.height - KEYBOARD_HEIGHT) {
        float rOffsetY = rect.origin.y + rect.size.height - (self.view.frame.size.height - KEYBOARD_HEIGHT);
        
        rect = self.m_ViewPurchase.frame;
        rect.origin.y -= rOffsetY;
        self.m_ViewPurchase.frame = rect;
    }
}

#pragma mark - PaymentViewDelegate

- (void) paymentView:(PKView *)paymentView withCard:(PKCard *)card isValid:(BOOL)valid
{
    if (!valid) {
        return;
    }
    NSLog(@"Card last4: %@", card.last4);
    NSLog(@"Card expiry: %lu/%lu", (unsigned long)card.expMonth, (unsigned long)card.expYear);
    NSLog(@"Card cvc: %@", card.cvc);
    
    [[NSUserDefaults standardUserDefaults] setValue:card.last4 forKey:@"card.last4"];
    
    [IGProgressActivity showHUDAddedTo:self.view animated:YES];
    
    [self createToken:^(STPToken *token, NSError *error) {
        [IGProgressActivity hideHUDForView:self.view animated:YES];
        
        if (error) {
            [self hasError:error];
        } else {
            [self hasToken:token];
        }
    }];
}

- (void)createToken: (STPTokenBlock)block
{
//    if (pending) return;
    
    if (![self.paymentView isValid]) {
//        NSError *error = [[NSError alloc] initWithDomain:StripeDomain
//                                                    code:STPCardError
//                                                userInfo:@{NSLocalizedDescriptionKey : STPCardErrorUserMessage}];
//        
//        block(nil, error);
        return;
    }
    
//    [self endEditing:YES];
    
    PKCard *card = self.paymentView.card;
    STPCard *scard = [[STPCard alloc] init];
    
    scard.number = card.number;
    scard.expMonth = card.expMonth;
    scard.expYear = card.expYear;
    scard.cvc = card.cvc;
    
//    [self pendingHandler:YES];
    
    NSString* STRIPE_PUBLISHABLE_KEY = [[DataKeeper sharedInstance] getStripeKey];
    
    [Stripe createTokenWithCard:scard
                 publishableKey: STRIPE_PUBLISHABLE_KEY
                     completion:^(STPToken *token, NSError *error) {
//                         [self pendingHandler:NO];
                         block(token, error);
                     }];
    
}

- (void)hasError:(NSError *)error
{
    [self dismissKeyboard: nil];
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                      message:[error localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
    [message show];
}

- (void)hasToken:(STPToken *)token
{
    [self dismissKeyboard: nil];
    
    m_strCardToken = [token.tokenId copy];
    m_FVerifyCard = YES;
    
    NSLog(@"Received token %@", token.tokenId);
//    ALERT_SHOW(@"Success to verify the card");
//
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://example.com"]];
//    request.HTTPMethod = @"POST";
//    NSString *body     = [NSString stringWithFormat:@"stripeToken=%@", token.tokenId];
//    request.HTTPBody   = [body dataUsingEncoding:NSUTF8StringEncoding];
//    
//    [IGProgressActivity showHUDAddedTo:self.view animated:YES];
//    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//                               [IGProgressActivity hideHUDForView:self.view animated:YES];
//                               
//                               //                               if (error) {
//                               //                                   [self hasError:error];
//                               //                               } else {
//                               [self.navigationController popViewControllerAnimated:YES];
//                               //                               }
//                           }];
}


@end
