//
//  IGPurchaseController.h
//  Juko
//
//  Created by Mountain on 2/7/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIXOverlayController.h"
#import "IGItem.h"

#import "PKView.h"

@interface IGPurchaseController : UIXOverlayContentViewController<UIXOverlayControllerDelegate, UITextFieldDelegate, PKViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

//import variables
@property (nonatomic, strong) IGItem* m_clsItem;
@property id delegate;

//UI variables
@property (weak, nonatomic) IBOutlet UIView *m_ViewOrder;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelPrice;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelDest;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelShippingSpeed;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelTotalPrice;
@property (weak, nonatomic) IBOutlet UIButton *m_btnDismissKeyboard;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelShippingPrice;

@property PKView* paymentView;

@property (weak, nonatomic) IBOutlet UIView *m_ViewPurchase;
@property (strong, nonatomic) IBOutlet UIView *m_ViewChooseProduct;
@property (strong, nonatomic) IBOutlet UIView *m_ViewChooseSize;
@property (weak, nonatomic) IBOutlet UIView *m_ViewOption;
@property (weak, nonatomic) IBOutlet UIPickerView *m_pickerView;
@property (weak, nonatomic) IBOutlet UIView *m_View_Picker;

//purchase view
@property (weak, nonatomic) IBOutlet UITextField *m_txtState;
@property (weak, nonatomic) IBOutlet UITextField *m_txtBillState;
@property (weak, nonatomic) IBOutlet UIButton *m_btnBilling;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelAddress;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *m_btnPickerNext;


//actions
- (IBAction)showStateView:(id)sender;

- (IBAction)actionOrderNow:(id)sender;
- (IBAction)actionPurchase:(id)sender;
- (IBAction)actionDissmiss:(id)sender;
- (IBAction)addToCart:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)chooseProduct:(id)sender;
- (IBAction)chooseCountry:(id)sender;
- (IBAction)chooseShipMode:(id)sender;
- (IBAction)nextProcess:(id)sender;


- (IBAction)actionBillingAddress:(id)sender;
@end
