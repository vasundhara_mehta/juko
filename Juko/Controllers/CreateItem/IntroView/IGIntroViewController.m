//
//  IGIntroViewController.m
//  Juko
//
//  Created by Mountain on 4/23/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGIntroViewController.h"

@interface IGIntroViewController ()

@end

@implementation IGIntroViewController
{
    int m_nPage;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.m_ScrollView setContentSize: CGSizeMake(640, self.m_ScrollView.frame.size.height)];
    
    [self actionFirstView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.overlayController dismissOverlay: YES];
}

- (IBAction)changePage:(id)sender {
    m_nPage = self.m_PageCtrl.currentPage;
    [self.m_ScrollView setContentOffset: CGPointMake(320 * m_nPage, 0) animated: YES];
    if (m_nPage == 0) {
        [self actionFirstView];
    } else if (m_nPage == 1) {
        [self actionSecondView];
    }
}

#pragma mark - UIScrollViewDelegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint ptOffset = scrollView.contentOffset;
    m_nPage = (ptOffset.x + 10) / 320;
    self.m_PageCtrl.currentPage = m_nPage;

    if (m_nPage == 0) {
        [self actionFirstView];
    } else if (m_nPage == 1) {
        [self actionSecondView];
    }
}

- (void) actionFirstView
{
    UIView* mainView = [self.m_ScrollView viewWithTag: 1];
    UIView* actionView = [mainView viewWithTag: 100];
    actionView.frame = CGRectMake(200, 320, actionView.frame.size.width, actionView.frame.size.height);
    
    [UIView animateWithDuration: 1.0 animations: ^(void) {
        actionView.frame = CGRectMake(101, 112, actionView.frame.size.width, actionView.frame.size.height);
    }];
}

- (void) actionSecondView
{
    UIView* mainView = [self.m_ScrollView viewWithTag: 2];
    UIView* actionView = [mainView viewWithTag: 100];
    UIImageView* imgTapView = (UIImageView*)[actionView viewWithTag: 200];
    
    imgTapView.alpha = 1.0;
    
    [UIView animateWithDuration: 0.75 delay: 0 options: (UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat) animations: ^(void) {
        [UIView setAnimationRepeatCount: 5];
        imgTapView.alpha = 0.0;
    } completion: ^(BOOL finish) {
        [UIView animateWithDuration: 0.75 animations: ^(void) {
            imgTapView.alpha = 1.0;
        }];
    }];
}

@end
