//
//  IGIntroViewController.h
//  Juko
//
//  Created by Mountain on 4/23/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIXOverlayController.h"

@interface IGIntroViewController : UIXOverlayContentViewController<UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *m_ScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *m_PageCtrl;

- (IBAction)goBack:(id)sender;
- (IBAction)changePage:(id)sender;

@end
