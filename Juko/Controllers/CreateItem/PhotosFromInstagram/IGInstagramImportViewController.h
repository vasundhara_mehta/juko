//
//  IGInstagramImportViewController.h
//  Juko
//
//  Created by Mountain on 3/20/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"

@interface IGInstagramImportViewController : UIViewController<StreamViewDelegate>

@property (nonatomic, strong) id m_delegate;

//members
@property (weak, nonatomic) IBOutlet StreamView *m_albumView;


//actions
- (IBAction)goBack:(id)sender;
- (IBAction)actionDone:(id)sender;

@end
