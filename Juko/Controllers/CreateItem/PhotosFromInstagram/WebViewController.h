//
//  WebViewController.h
//  instagram4iPad
//
//  Created by Markus Emrich on 28.04.12.
//  Copyright (c) 2012 Markus Emrich. All rights reserved.
//


//#import "BaseViewController.h"

static NSString* const INSTAGRAM_AUTH_WAS_SUCCESFULL_NOTIFICATION = @"INSTAGRAM_AUTH_WAS_SUCCESFULL_NOTIFICATION";


@interface WebViewController : UIViewController <UIWebViewDelegate>
{
    IBOutlet UIWebView *mWebView;
    IBOutlet UIActivityIndicatorView* mIndicator;
    
    NSString* mURLToLoad;
    BOOL mShouldSendNotification;
}

@property (nonatomic, readonly) IBOutlet UIWebView* webView;

- (id)initForInstagramAuthentification;
- (id)initWithURLString: (NSString*) urlString;

@end