//
//  IGChooseController.h
//  Inkgram
//
//  Created by Mountain on 2/27/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZCImagePickerController.h"
#import "StreamView.h"
#import "TKDragView.h"

@class IGProduct;
@class IGLayoutView;
@interface IGChooseController : UIViewController<ZCImagePickerControllerDelegate, StreamViewDelegate, TKDragViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    BOOL m_FImportMode;
    IGLayoutView* m_CurrentTemplateView;
    UIImageView* m_selectedImageView;
}

@property (nonatomic, retain) IGProduct* m_clsProduct;

@property (nonatomic, strong) IGLayoutView* m_CurrentTemplateView;

//members
//main view
@property (weak, nonatomic) IBOutlet UILabel *m_LabelProductName;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelPrice;
@property (weak, nonatomic) IBOutlet UIButton *m_btnDismiss;
@property (weak, nonatomic) IBOutlet UIScrollView *m_ScrollView_Layout;
@property (weak, nonatomic) IBOutlet UIScrollView *m_ScrollView_Images;
@property (weak, nonatomic) IBOutlet UIImageView *m_touchView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnCancel;

//image import view
@property (strong, nonatomic) IBOutlet UIView *m_View_Media;
@property (weak, nonatomic) IBOutlet StreamView *m_StreamView_Photos;

//actions

//main view actions
- (IBAction)addMedia:(id)sender;
- (IBAction)dismissImportView:(id)sender;
- (IBAction)cancelDeleteMode:(id)sender;
- (IBAction)finishEdit:(id)sender;

//image import view actions

- (IBAction)photoLibrary:(id)sender;
- (IBAction)photoFromInstagram:(id)sender;
- (IBAction)photoFromFacebook:(id)sender;
- (IBAction)importPhotos:(id)sender;

- (IBAction)showIntroView:(id)sender;

@end
