//
//  IGFilterViewController.m
//  Juko
//
//  Created by Mountain on 3/20/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGFilterViewController.h"

#import "CUtils.h"

#import <GPUImage.h>

#define SCROLL_VIEW_SIZE 250.0

@interface IGFilterViewController ()

@end

@implementation IGFilterViewController
{
    int m_nSelectedFilter;
    GPUImageOutput<GPUImageInput> *filter;
    
    CGFloat m_rZoom;
    CGSize m_originSize;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_nSelectedFilter = -1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (_m_FCircle) {
        [_m_Border setImage: [UIImage imageNamed: @"bg_crop_circle.png"]];
    } else {
        [_m_Border setImage: [UIImage imageNamed: @"bg_crop_border.png"]];
    }
    
    [self customizeView];
    [self configureFitlerButtons];
    
    m_rZoom = 1.0;
}

- (void) customizeView
{
    if (_m_cropSize.width == 0 || _m_cropSize.height == 0) {
        _m_cropSize = CGSizeMake(SCROLL_VIEW_SIZE, SCROLL_VIEW_SIZE);
    }
    
    CGFloat rMax = MAX(_m_cropSize.width, _m_cropSize.height);
    CGSize szNewSize = CGSizeMake(_m_cropSize.width / rMax * SCROLL_VIEW_SIZE, _m_cropSize.height / rMax * SCROLL_VIEW_SIZE);
    _m_ScrollView.frame = CGRectMake(0, 0, szNewSize.width, szNewSize.height);
    
    CGSize szBounds = _m_WorkView.bounds.size;
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        szBounds.height = 299;
    }
    _m_ScrollView.center = CGPointMake(szBounds.width / 2, szBounds.height / 2);
    _m_MainLayer.frame = _m_ScrollView.frame;
    _m_Border.frame = _m_ScrollView.frame;
    
    CGRect rect = _m_ScrollView.frame;
    _m_LeftLayer.frame = CGRectMake(0, rect.origin.y, rect.origin.x, rect.size.height);
    _m_RightLayer.frame = CGRectMake(rect.origin.x + rect.size.width, rect.origin.y, _m_WorkView.frame.size.width - rect.origin.x - rect.size.width + 10, rect.size.height);
    _m_TopLayer.frame = CGRectMake(0, 0, _m_WorkView.frame.size.width, rect.origin.y);
    _m_BottomLayer.frame = CGRectMake(0, rect.origin.y + rect.size.height, _m_WorkView.frame.size.width, _m_WorkView.frame.size.height - rect.origin.y - rect.size.height);
    
    CGSize imageSize = _m_SourceImage.size;
    CGSize szRatio = CGSizeMake(szNewSize.width / imageSize.width, szNewSize.height/imageSize.height);
    
    CGSize szNewImageSize;
    if (szRatio.width > szRatio.height) {
        szNewImageSize.width = szNewSize.width;
        szNewImageSize.height = imageSize.height * szRatio.width;
    } else {
        szNewImageSize.height = szNewSize.height;
        szNewImageSize.width = imageSize.width * szRatio.height;
    }
    
    m_originSize = szNewImageSize;
    
    _m_ImageView.frame = CGRectMake(0, 0, szNewImageSize.width, szNewImageSize.height);
    [_m_ImageView setImage: _m_SourceImage];
    
    [_m_ScrollView setContentSize: _m_ImageView.bounds.size];
    [_m_ScrollView setContentOffset: CGPointMake((_m_ImageView.bounds.size.width - _m_ScrollView.bounds.size.width) / 2, (_m_ImageView.bounds.size.height - _m_ScrollView.bounds.size.height) / 2)];
    
    UIPinchGestureRecognizer* gesture = [[UIPinchGestureRecognizer alloc] initWithTarget: self action: @selector(scaleImage:)];
    [_m_ImageView addGestureRecognizer: gesture];
}

#pragma mark - Zooming Image

- (void) scaleImage:(UIPinchGestureRecognizer*) gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        [self scaleChanged: gesture];
    } else if (gesture.state == UIGestureRecognizerStateEnded) {
        [self endScale: gesture];
    }
}

- (void) endScale: (UIPinchGestureRecognizer*) gesture
{
    CGFloat rScale = m_rZoom * gesture.scale;
    if (rScale < 1.0) {
        m_rZoom = 1;
        return;
    }
    
    m_rZoom = rScale;
}

- (void) scaleChanged: (UIPinchGestureRecognizer*) gesture
{
    CGFloat rScale = m_rZoom * gesture.scale;
    if (rScale < 1.0) {
        return;
    }
    
    CGSize szNewImageSize = CGSizeMake(m_originSize.width * rScale, m_originSize.height * rScale);
    _m_ImageView.frame = CGRectMake(0, 0, szNewImageSize.width, szNewImageSize.height);
    
    [_m_ScrollView setContentSize: _m_ImageView.bounds.size];
}

#define FILTER_ITEM_WIDTH 60
#define FILTER_ITEM_HEIGHT 60
#define MAX_FILTER_COUNT 10

- (void) configureFitlerButtons
{
    CGRect rect = CGRectMake(0, 2, FILTER_ITEM_WIDTH, FILTER_ITEM_HEIGHT);
    for (int nIdx = 0; nIdx < MAX_FILTER_COUNT; nIdx ++) {
        rect.origin.x = nIdx * (FILTER_ITEM_WIDTH + 2) + 2;
        UIButton* btnFilter = [UIButton buttonWithType: UIButtonTypeCustom];
        [btnFilter setImage: [UIImage imageNamed: [NSString stringWithFormat: @"filter_%d", nIdx]] forState: UIControlStateNormal];
        [btnFilter setFrame: rect];
        btnFilter.tag = nIdx + 100;
        [btnFilter addTarget: self action: @selector(touchFilterButton:) forControlEvents: UIControlEventTouchUpInside];
        [self.m_FilterScrollView addSubview: btnFilter];
    }
    
    [self.m_FilterScrollView setContentSize: CGSizeMake(MAX_FILTER_COUNT * (FILTER_ITEM_WIDTH + 2) + 2, FILTER_ITEM_HEIGHT + 4)];
}

- (void) touchFilterButton: (UIButton*) button
{
    if (button.tag == m_nSelectedFilter + 100)
        return;

    m_nSelectedFilter = button.tag - 100;
    
    switch (m_nSelectedFilter) {
        case 0: //default
            filter = [[GPUImageGammaFilter alloc] init];
            break;
        case 1: //brightness
            filter = [[GPUImageBrightnessFilter alloc] init];
            [((GPUImageBrightnessFilter*)filter) setBrightness: 0.45f];
            break;
        case 2: //haze
            filter = [[GPUImageHazeFilter alloc] init];
            break;
        case 3: //saturation
            filter = [[GPUImageSaturationFilter alloc] init];
            [((GPUImageSaturationFilter*)filter) setSaturation: 1.65f];
            break;
        case 4: //saturation
            filter = [[GPUImageOpacityFilter alloc] init];
            [((GPUImageOpacityFilter*)filter) setOpacity: 0.66];
            break;
        case 5: //exposure
            filter = [[GPUImageExposureFilter alloc] init];
            [((GPUImageExposureFilter*)filter) setExposure: 0.87f];
            break;
        case 6: //gray
            filter = [[GPUImageGrayscaleFilter alloc] init];
            break;
        case 7: //tilt
            filter = [[GPUImageTiltShiftFilter alloc] init];
            [(GPUImageTiltShiftFilter *)filter setTopFocusLevel:0.4];
            [(GPUImageTiltShiftFilter *)filter setBottomFocusLevel:0.6];
            [(GPUImageTiltShiftFilter *)filter setFocusFallOffRate:0.2];
            break;
        case 8:  //sepia
            filter = [[GPUImageSepiaFilter alloc] init];
            break;
        case 9: //amatokar
            filter = [[GPUImageAmatorkaFilter alloc] init];
            break;
        default:
            break;
    }
    
    UIImage* imgResult = [filter imageByFilteringImage: _m_OrgImage];
    [self.m_ImageView setImage: imgResult];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (IBAction)doApply:(id)sender {
    if ([_delegate respondsToSelector: @selector(applyFilteredImage:)]) {
        CGFloat rRatio = _m_SourceImage.size.width / _m_ImageView.frame.size.width;
        
        CGSize szCrop = CGSizeMake(_m_ScrollView.frame.size.width * rRatio , _m_ScrollView.frame.size.height * rRatio);
        CGPoint ptLeftTop = CGPointMake(_m_ScrollView.contentOffset.x * rRatio , _m_ScrollView.contentOffset.y * rRatio);
        
        CGRect rect = CGRectMake(ptLeftTop.x, ptLeftTop.y, szCrop.width, szCrop.height);
        UIImage* image = [CUtils imageWithImage: _m_ImageView.image cropByRect: rect];
        
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: _m_ImageView.image, @"whole_image", image, @"image", [NSValue valueWithCGRect: rect], @"rect", nil];
        [_delegate performSelectorOnMainThread: @selector(applyFilteredImage:) withObject:dict waitUntilDone: YES];
    }
    [self dismissViewControllerAnimated: YES completion: nil];
}
@end
