//
//  IGFilterViewController.h
//  Juko
//
//  Created by Mountain on 3/20/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGFilterViewController : UIViewController

//import
@property id delegate;
@property (nonatomic, retain) UIImage* m_SourceImage;
@property (nonatomic, retain) UIImage* m_OrgImage;
@property CGSize m_cropSize;
@property BOOL m_FCircle;

//members

@property (weak, nonatomic) IBOutlet UIScrollView *m_ScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *m_ImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *m_FilterScrollView;
@property (weak, nonatomic) IBOutlet UIView *m_WorkView;

@property (weak, nonatomic) IBOutlet UIImageView *m_TopLayer;
@property (weak, nonatomic) IBOutlet UIImageView *m_LeftLayer;
@property (weak, nonatomic) IBOutlet UIImageView *m_RightLayer;
@property (weak, nonatomic) IBOutlet UIImageView *m_BottomLayer;
@property (weak, nonatomic) IBOutlet UIImageView *m_MainLayer;
@property (weak, nonatomic) IBOutlet UIImageView *m_Border;

//actions
- (IBAction)goBack:(id)sender;
- (IBAction)doApply:(id)sender;


@end
