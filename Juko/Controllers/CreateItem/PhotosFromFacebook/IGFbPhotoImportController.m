//
//  IGFbPhotoImportController.m
//  Juko
//
//  Created by Mountain on 3/18/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGFbPhotoImportController.h"

#import "DataKeeper.h"
#import "const.h"

#import "IGProgressActivity.h"

#import "CUtils.h"

#import "IGPhotoCell.h"

#define FB_PHOTO_LIMIT 20
#define FB_ALBUM_LIMIT 1

#define ADDING_PHOTO_SIZE 70
#define ADDING_PHOTO_PADDING 8

@interface IGFbPhotoImportController ()

@end

@implementation IGFbPhotoImportController
{
    IGProgressActivity* HUD;
    
    NSMutableArray* m_photos;
    
    NSMutableArray* m_nextLinks;
    
    int m_nLinkIndex;
    
    NSMutableArray* m_resultPhotos;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_photos = [[NSMutableArray alloc] init];
        m_nextLinks = [[NSMutableArray alloc] init];
        
        m_nLinkIndex = 0;
        
        m_resultPhotos = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [HUD show: YES];
    
    _m_albumView.scrollsToTop = YES;
    
    _m_albumView.cellPadding = ADDING_PHOTO_PADDING;
    _m_albumView.columnPadding = ADDING_PHOTO_PADDING;
    
    _m_albumView.delegate = self;

    [self initLoad];
}

- (void) initLoad
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strFbAccessToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    NSString* strUrl = [NSString stringWithFormat: @"https://graph.facebook.com/me?fields=albums.limit(%d).fields(photos.limit(%d).fields(source))&access_token=%@", FB_ALBUM_LIMIT, FB_PHOTO_LIMIT, strFbAccessToken];
    
    m_nLinkIndex = 0;
    
    [NSThread detachNewThreadSelector: @selector(loadAlbumPhotos:) toTarget: self withObject: strUrl];
//    [self loadAlbumPhotos: strUrl];
}

- (void) loadNextData
{
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadAlbumPhotos:) toTarget: self withObject: m_nextLinks[m_nLinkIndex]];
}

- (void) loadAlbumPhotos: (NSString*) strUrl
{
    NSDictionary* dictResult = nil;
    Communication* comm = [[Communication alloc] init];
    
    NSURL* url = [NSURL URLWithString: strUrl];
    BOOL FRes = [comm sendRequest: MethodTypeGet requestURL: url.absoluteString params: @"" respData: &dictResult];
    if (FRes) {
        NSLog(@"%@", dictResult);
        NSDictionary* dictAlbum = [dictResult objectForKey: @"albums"];
        if (dictAlbum == nil) {
            dictAlbum = dictResult;
        }
        if (dictAlbum && [dictAlbum objectForKey: @"data"]) {
            NSArray* arrayAlbums = [dictAlbum objectForKey: @"data"];
            for (NSDictionary* dictAlbum in arrayAlbums) {
                NSDictionary* dictPhotos = [dictAlbum objectForKey: @"photos"];
                
                //add photos
                if (dictPhotos && [dictPhotos objectForKey: @"data"]) {
                    NSArray* arrayPhotos = [dictPhotos objectForKey: @"data"];
                    [self addPhotos: arrayPhotos];
                }

                //save next photo link
                NSDictionary* dictPaging = [dictPhotos objectForKey: @"paging"];
                if (dictPaging && [dictPaging objectForKey: @"next"]) {
                    [m_nextLinks addObject: [dictPaging objectForKey: @"next"]];
                }
            }

            //save next album link
            NSDictionary* dictPaging = [dictAlbum objectForKey: @"paging"];
            if (dictPaging && [dictPaging objectForKey: @"next"]) {
                [m_nextLinks addObject: [dictPaging objectForKey: @"next"]];
            }
        }
    }
    
    [HUD hide: YES];

    [self performSelectorOnMainThread: @selector(refreshPhotos) withObject: nil waitUntilDone: YES];
    
}

- (void) addPhotos: (NSArray*) arrayPhotos
{
    for (NSDictionary* dictPhoto in arrayPhotos) {
        [m_photos addObject: [CUtils getStringValueFromDictionary: dictPhoto KEY: @"source"]];
    }
}

- (void) refreshPhotos
{
    [self.m_albumView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button actions

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (IBAction)actionDone:(id)sender {
    if ([self.m_delegate respondsToSelector: @selector(addPhotos:)]) {
        [self.m_delegate performSelectorOnMainThread: @selector(addPhotos:) withObject: m_resultPhotos waitUntilDone: YES];
    }
    [self dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark - albumView delegate


- (NSInteger)numberOfCellsInStreamView:(StreamView *)streamView
{
    if (m_photos == nil) {
        return 0;
    }
    int nCount = m_photos.count;
//    if (m_nLinkIndex < m_nextLinks.count) {
//        nCount ++;
//    }
    return nCount;
}

- (NSInteger)numberOfColumnsInStreamView:(StreamView *)streamView
{
    return 4;
}

- (UIView *)streamView:(StreamView *)stream cellAtIndex:(NSInteger)index
{
    NSString *CellID =  [NSString stringWithFormat: @"MyCell%d", (index%4+1)];
    
    IGPhotoCell *cell;
    
    cell = (IGPhotoCell *)[stream dequeueReusableCellWithIdentifier:CellID];
    
    if (cell == nil) {
        cell = [[IGPhotoCell alloc] initWithFrame:CGRectMake(0, 0, ADDING_PHOTO_SIZE, ADDING_PHOTO_SIZE) ContentSize: CGSizeMake(ADDING_PHOTO_SIZE - 4, ADDING_PHOTO_SIZE - 4)];
        cell.reuseIdentifier = CellID;
    }
    
    cell.delegate = self;
    cell.tag = index + 1000;
    
    [cell configureByURLString: m_photos[index] Index: index];
    cell.m_FEditMode = YES;
    
    if (index == m_photos.count - 1 && m_nextLinks) {
        if (m_nLinkIndex < m_nextLinks.count) {
            [self loadNextData];
            m_nLinkIndex ++;
        }
    }
    
    return cell;
}

- (CGFloat)streamView:(StreamView *)streamView heightForCellAtIndex:(NSInteger)index
{
    return ADDING_PHOTO_SIZE;
}

- (UIView *)headerForStreamView:(StreamView *)streamView
{
    return nil;
    /*
     CSearchCell *header = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     header.label.text = @"This is the header";
     
     return header;
     */
}

- (UIView *)footerForStreamView:(StreamView *)streamView
{
    return nil;
    
    /*
     if (page <= MaxPage) {
     CSearchCell *footer = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     footer.label.text = @"This is the footer";
     
     return footer;
     } else {
     return nil;
     }
     */
}

#pragma mark - Photo Cell delegate methods

- (void) selectPhoto: (UIImage*) photo
{
    [m_resultPhotos addObject: photo];
}

- (void) deselectPhoto: (UIImage*) photo
{
    [m_resultPhotos removeObject: photo];
}

@end
