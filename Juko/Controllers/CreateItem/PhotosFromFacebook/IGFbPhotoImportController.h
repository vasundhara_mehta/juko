//
//  IGFbPhotoImportController.h
//  Juko
//
//  Created by Mountain on 3/18/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"

@interface IGFbPhotoImportController : UIViewController<StreamViewDelegate>

@property (nonatomic, strong) id m_delegate;

//members
@property (weak, nonatomic) IBOutlet StreamView *m_albumView;


//actions
- (IBAction)goBack:(id)sender;
- (IBAction)actionDone:(id)sender;

@end
