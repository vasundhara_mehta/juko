//
//  IGProductKindController.h
//  Juko
//
//  Created by Mountain on 3/11/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGCategory;
@interface IGProductKindController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* m_ProductList;
}

@property (nonatomic, strong) IGCategory* m_clsCategory;

//members
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

//actions

- (IBAction)goBack:(id)sender;

@end
