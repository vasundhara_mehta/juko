

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MugView : GLKView

@property (nonatomic, retain) UIImage* m_flatImage;

- (id) initWithFrame: (CGRect) rect Image: (UIImage*) image;

- (void) configureView;

@end
