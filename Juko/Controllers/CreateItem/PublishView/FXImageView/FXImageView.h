
#import <UIKit/UIKit.h>


@interface FXImageView : UIImageView

+ (NSOperationQueue *)processingQueue;
+ (NSCache *)processedImageCache;

@property (nonatomic, assign, getter = isAsynchronous) BOOL asynchronous;
@property (nonatomic, assign) CGFloat reflectionGap;
@property (nonatomic, assign) CGFloat reflectionScale;
@property (nonatomic, assign) CGFloat reflectionAlpha;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, assign) CGSize shadowOffset;
@property (nonatomic, assign) CGFloat shadowBlur;
@property (nonatomic, assign) CGFloat cornerRadius;
@property (nonatomic, strong) UIImage *processedImage;

@property (nonatomic, copy) UIImage *(^customEffectsBlock)(UIImage *image);
@property (nonatomic, copy) NSString *cacheKey;

- (void)setImageWithContentsOfFile:(NSString *)file;
- (void)setImageWithContentsOfURL:(NSURL *)URL;

@end
