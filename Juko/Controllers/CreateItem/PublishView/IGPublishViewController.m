//
//  IGPublishViewController.m
//  Juko
//
//  Created by Mountain on 3/24/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGPublishViewController.h"

#import <GPUImage.h>
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>

#import "CUtils.h"
#import "const.h"
#import "DataKeeper.h"

#import "UIImage+FX.h"

#import "IGProgressActivity.h"

#import "IGProductDescriptionController.h"
#import "UIXOverlayController.h"
#import "IGPurchaseController.h"
#import "IGAppDelegate.h"

@interface IGPublishViewController ()

@end

@implementation IGPublishViewController
{
    IGProgressActivity* HUD;
    UIDocumentInteractionController* docFile;
    BOOL m_FBackward;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    self.m_ImageView_Publish.layer.allowsEdgeAntialiasing = YES;
    self.m_ImageView_Publish1.layer.allowsEdgeAntialiasing = YES;
    
    if (_m_publishImage == nil) {
        _m_publishImage = [CUtils concatImage: _m_publishImages[1] second: _m_publishImages[0]];
    }
    
//    [self.m_ImagePrint setImage: _m_printImage];
    
    if (_m_clsProduct.m_nCategoryIndex == ECATEGORY_MUG) {
        self.m_btnRotate.hidden = NO;
        m_FBackward = YES;
        [self.m_ImageView_Publish setImage: _m_publishImages[1]];
    } else {
        self.m_btnRotate.hidden = YES;
        if (!self.m_FDropShadow) {
            [self.m_ImageView_Publish1 setImage: _m_publishImage];
        } else {
            UIImage* image = _m_publishImage;
            
            CGSize szImage = image.size;
            CGRect rect = _m_ImageView_Publish.bounds;
            
            CGPoint ptCenter = _m_ImageView_Publish.center;
            if (szImage.width > szImage.height) {
                rect.size.height *= (szImage.height / szImage.width);
            } else {
                rect.size.width *= (szImage.width / szImage.height);
            }
            
            _m_ImageView_Publish.frame = rect;
            _m_ImageView_Publish.center = ptCenter;
            
            ptCenter = _m_ShadowView.center;
            rect = _m_ShadowView.frame;
            rect.size.width = _m_ImageView_Publish.frame.size.width;
            _m_ShadowView.frame = rect;
            _m_ShadowView.center = ptCenter;
            
            [_m_ImageView_Publish setImage: image];
            
            [self setDepthShadow: _m_ImageView_Publish];
        }
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
}

-(UIImage*)addGlowToImage:(UIImage*)imageInput;
{
    CGSize newSize = imageInput.size;
    CGImageRef theImage = imageInput.CGImage;
    
    // expand the size to handle the "glow"
    newSize.width += 6.0;
    newSize.height += 6.0;
    UIGraphicsBeginImageContext(newSize);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginTransparencyLayerWithRect(ctx, CGRectMake(0, 0, newSize.width, newSize.height), NULL);
    CGContextClearRect(ctx, CGRectMake(0, 0, newSize.width, newSize.height));
    
    // you can repeat this process to build glow.
    CGContextDrawImage(ctx, CGRectMake(0, 0, newSize.width, newSize.height), theImage);
    CGContextSetAlpha(ctx, 0.2);
    
    CGContextEndTransparencyLayer(ctx);
    
    // draw the original image into the context, offset to be centered;
    CGRect centerRect = CGRectMake(0, 0, imageInput.size.width, imageInput.size.height);
    centerRect.origin.x += 3.0;
    centerRect.origin.y += 3.0;
    CGContextDrawImage(ctx, centerRect, theImage);
    
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

- (void) setCornerShadow: (UIImageView*) shadowCorner
{
    [shadowCorner.layer setMasksToBounds:NO ];
    [shadowCorner.layer setShadowColor:[[UIColor blackColor ] CGColor ] ];
    [shadowCorner.layer setShadowOpacity:0.65 ];
    [shadowCorner.layer setShadowRadius:6.0 ];
    [shadowCorner.layer setShadowOffset:CGSizeMake( 0 , 0 ) ];
    [shadowCorner.layer setShouldRasterize:YES ];
    
    UIBezierPath * cornerShadowPath = [UIBezierPath bezierPath ];
    [cornerShadowPath moveToPoint:CGPointMake( 3 , 3 ) ];
    [cornerShadowPath addCurveToPoint:CGPointMake( shadowCorner.frame.size.width + 8 , -6 ) controlPoint1:CGPointMake( shadowCorner.frame.size.width / 3 , 3 ) controlPoint2:CGPointMake(  ( shadowCorner.frame.size.width / 3 ) * 2 , 3 )];
    [cornerShadowPath addCurveToPoint:CGPointMake( shadowCorner.frame.size.width - 3 , shadowCorner.frame.size.height - 3 ) controlPoint1:CGPointMake( shadowCorner.frame.size.width - 3 , shadowCorner.frame.size.height / 3 ) controlPoint2:CGPointMake( shadowCorner.frame.size.width - 3 , ( shadowCorner.frame.size.height / 3 ) * 2 ) ];
    [cornerShadowPath addLineToPoint:CGPointMake( 3 , shadowCorner.frame.size.height - 3 ) ];
    [cornerShadowPath addLineToPoint:CGPointMake( 3 , 3 ) ];
    [shadowCorner.layer setShadowPath:[cornerShadowPath CGPath ] ];
}

- (void) setRadialShadow: (UIImageView*) shadowRadial
{
    [shadowRadial.layer setMasksToBounds:NO ];
    [shadowRadial.layer setShadowColor:[[UIColor blackColor ] CGColor ] ];
    [shadowRadial.layer setShadowOpacity:0.65 ];
    [shadowRadial.layer setShadowRadius:6.0 ];
    [shadowRadial.layer setShadowOffset:CGSizeMake( 0 , 0 ) ];
    [shadowRadial.layer setShouldRasterize:YES ];
    [shadowRadial.layer setShadowPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake( 0 , shadowRadial.frame.size.height - 2 , shadowRadial.frame.size.width , 10 ) ] CGPath ] ];
}

- (void) setDropShadow: (UIView*) shadowDropView Rect: (CGRect) rect startPoint: (CGPoint) ptStartPoint endPoint: (CGPoint) ptEndPoint
{
    CAGradientLayer *shadow = [CAGradientLayer layer];
    shadow.frame = rect;
    shadow.startPoint = ptStartPoint;
    shadow.endPoint = ptEndPoint;
    shadow.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.0 alpha:0.1f] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    [self.m_ShadowView.layer addSublayer: shadow];
}

- (void) setDepthShadow: (UIView*) shadowDepth
{
    [shadowDepth.layer setMasksToBounds:NO ];
    [shadowDepth.layer setShadowColor:[[UIColor blackColor ] CGColor ] ];
    [shadowDepth.layer setShadowOpacity:0.3 ];
    [shadowDepth.layer setShadowRadius:6.0 ];
    [shadowDepth.layer setShadowOffset:CGSizeMake( 0 , 0 ) ];
    [shadowDepth.layer setShouldRasterize:YES ];
    
    float rYOffset = 0;
    
    switch (_m_clsProduct.m_nCategoryIndex)
    {
        case ECATEGORY_CASE:
            rYOffset = 10;
            break;
        case ECATEGORY_SKIN:
            rYOffset = 0;
            break;
        case ECATEGORY_CANVAS:
//            [self setDropShadow: shadowDepth Rect: CGRectMake(3, shadowDepth.frame.size.height - 40, shadowDepth.frame.size.width - 5, 80) startPoint: CGPointMake(0.0, 0.0) endPoint: CGPointMake(0.3, 1)];
//            [self setDropShadow: shadowDepth Rect: CGRectMake(shadowDepth.frame.size.width - 82, shadowDepth.frame.size.height - 40, 80, 60) startPoint: CGPointMake(0, 0.0) endPoint: CGPointMake(0.3, 1.0)];
            rYOffset = 40;
            break;
            
        default:
            break;
    }
    UIBezierPath * depthShadowPath = [UIBezierPath bezierPath ];
    [depthShadowPath moveToPoint:CGPointMake( 0 , shadowDepth.frame.size.height ) ];
    [depthShadowPath addLineToPoint:CGPointMake( - 25 , shadowDepth.frame.size.height - 30 ) ];
//    [depthShadowPath addCurveToPoint:CGPointMake( - 40, shadowDepth.frame.size.height - 55 ) controlPoint1:CGPointMake( - 25 , shadowDepth.frame.size.height - 30 ) controlPoint2:CGPointMake( - 15 , shadowDepth.frame.size.height - 75 )];

    [depthShadowPath addLineToPoint:CGPointMake( 15 , shadowDepth.frame.size.height - 45 ) ];
    [depthShadowPath addLineToPoint:CGPointMake( shadowDepth.frame.size.width , shadowDepth.frame.size.height - rYOffset ) ];
    [depthShadowPath addLineToPoint:CGPointMake( 0 , shadowDepth.frame.size.height ) ];
    [shadowDepth.layer setShadowPath:[depthShadowPath CGPath ] ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)actionFbShare:(id)sender {
    if (![self isPossibleToUpload]) {
        ALERT_SHOW(@"Please complete the product information");
        return;
    }

    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    
    if (_m_publishImage) {
        [vc addImage: [self imageForShare: _m_publishImage]];
    } else {
        [vc addImage: [self imageForShare: _m_publishImages[0]]];
        [vc addImage: [self imageForShare: _m_publishImages[1]]];
    }
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

- (IBAction)actionMailShare:(id)sender {
    if (![self isPossibleToUpload]) {
        ALERT_SHOW(@"Please complete the product information");
        return;
    }

    if (![MFMailComposeViewController canSendMail]) {
        ALERT_SHOW(@"Please setup your mail account in Settings of your device");
        return;
    }
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
//    [mail setToRecipients:[NSArray arrayWithObject:@"info@thegrint.com"]];
    [mail setSubject: _m_clsProduct.m_strName];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    if (_m_publishImage) {
        [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: _m_publishImage], 1.0) mimeType: @"image/jpg" fileName: _m_clsProduct.m_strName];
    } else {
        [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: _m_publishImages[1]], 1.0) mimeType: @"image/jpg" fileName: @"image1"];
        [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: _m_publishImages[0]], 1.0) mimeType: @"image/jpg" fileName: @"image2"];
    }
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

- (IBAction)actionIGShare:(id)sender {
    if (![self isPossibleToUpload]) {
        ALERT_SHOW(@"Please complete the product information");
        return;
    }

    UIImage* image;
    
    image = [self imageForShare: [CUtils imageWithView: _m_PublishView]];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];
        NSData *imageData=UIImagePNGRepresentation(image);
        [imageData writeToFile:saveImagePath atomically:YES];
        
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];

        if (docFile == Nil) {
            docFile = [[UIDocumentInteractionController alloc] init];
        }
        docFile.delegate=self;
        docFile.UTI=@"com.instagram.photo";
        
        NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
        docFile.annotation=[NSDictionary dictionaryWithObjectsAndKeys: strText, @"InstagramCaption", nil];
        
        [docFile setURL:imageURL];
        
        [docFile presentOpenInMenuFromRect: CGRectZero inView: self.view animated: YES];
    }
    else
    {
        NSLog (@"Instagram is not available");
    }
}

- (IBAction)actionTwShare:(id)sender {
    if (![self isPossibleToUpload]) {
        ALERT_SHOW(@"Please complete the product information");
        return;
    }
    
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeTwitter];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];

    if (_m_publishImage) {
        [vc addImage: [self imageForShare: _m_publishImage]];
    } else {
        [vc addImage: [self imageForShare: _m_publishImages[0]]];
        [vc addImage: [self imageForShare: _m_publishImages[1]]];
    }
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

#define SHARE_WIDTH 200
#define SHARE_HEIGHT 200
- (UIImage*) imageForShare : (UIImage*) orgImage
{
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, SHARE_WIDTH, SHARE_HEIGHT)];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.opaque = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView setImage: orgImage];
    return [CUtils imageWithView: imgView];
}

- (IBAction)actionBuyIt:(id)sender {
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];

    DataKeeper* clsModel = [DataKeeper sharedInstance];
    IGItem* clsItem = [self uploadProduct];
    if (clsItem == nil) {
        ALERT_SHOW([clsModel getErrMessage]);
    } else {
        UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
        overlay.dismissUponTouchMask = NO;
        
        IGPurchaseController* vc = [[IGPurchaseController alloc] initWithNibName: @"IGPurchaseController" bundle: nil];
        vc.m_clsItem = clsItem;
        vc.delegate = self;
        
        IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
        
        [overlay presentOverlayOnView: appDelegate.window withContent:vc animated: YES];
    }
}

- (void) purchased: (NSString*) strMessage
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"" message: strMessage delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil, nil];
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSArray* vcArrays = [self.navigationController viewControllers];
        [self.navigationController popToViewController: vcArrays[1] animated: YES];
    }
}

- (void) showActivity
{
    [HUD show: YES];
}

- (IBAction)actionDone:(id)sender
{
    if (![self isPossibleToUpload]) {
        ALERT_SHOW(@"Please complete the product information");
        return;
    }
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(doneProcess) toTarget: self withObject: nil];
}

- (void) doneProcess
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    IGItem* clsItem = [self uploadProduct];
    if (clsItem == nil) {
        ALERT_SHOW([clsModel getErrMessage]);
    } else {
        dispatch_sync(dispatch_get_main_queue(),  ^(void) {
            NSArray* vcArrays = [self.navigationController viewControllers];
            [self.navigationController popToViewController: vcArrays[1] animated: NO];

            IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
            
            UINavigationController* nav = appDelegate.m_clsMainFrame.viewControllers[0];
            [appDelegate.m_clsMainFrame setSelectedViewController: nav];
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"refresh_newsfeed" object: nil];
        });
    }
}

- (IBAction)actionRotateMug:(id)sender {
    m_FBackward = !m_FBackward;
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        if (m_FBackward) {
            [self.m_ImageView_Publish setImage: _m_publishImages[1]];
        } else {
            [self.m_ImageView_Publish setImage: _m_publishImages[0]];
        }
    }];
}

- (IBAction)showInfo:(id)sender {
    IGProductDescriptionController* vc = [[IGProductDescriptionController alloc] initWithNibName: @"IGProductDescriptionController" bundle: nil];
    vc.m_clsProduct = _m_clsProduct;
    [self.navigationController pushViewController: vc animated: YES];
}

- (BOOL) isPossibleToUpload
{
//    if (self.m_TextDescription.text == nil || [self.m_TextDescription.text isEqualToString: @""]) {
//        return NO;
//    }
    return YES;
}

- (IGItem*) uploadProduct
{
    NSString* strDescription = @"";
    if (self.m_TextDescription.text && ![self.m_TextDescription.text isEqualToString: @""]) {
        strDescription = [NSString stringWithFormat: @"%@", self.m_TextDescription.text];
    }
    NSString* strTags = [self getTags: strDescription];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    
//    UIImage* image = [CUtils imageWithLayer: _m_ImageView_Publish.layer];
    
    int nTemplateDBId = 0;
    if (_m_nTemplateId == 0) {
        nTemplateDBId = 1;
    } else {
        nTemplateDBId = (_m_nTemplateId / abs(_m_nTemplateId)) * (abs(_m_nTemplateId) + 1);
    }
    
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: _m_publishImage, @"feed_image", _m_dataPrintImage, @"print_image", [NSNumber numberWithInt: nTemplateDBId], @"template_id", [NSNumber numberWithInt: _m_clsProduct.m_nIndex], @"product_id", strDescription, @"description", strTags, @"hashtags", _m_SwitchPublish.isOn ? @"1" : @"0", @"publish", _m_SwitchSell.isOn ? @"1" : @"0", @"sell", nil];

    IGItem* clsItem = [clsModel uploadProduct: dict];
    [HUD hide: YES];
    
    return clsItem;
}

- (NSString*) getTags: (NSString*) strRaw
{
    NSMutableString* strResult = [[NSMutableString alloc] init];
    NSArray *hashtagRanges = [CUtils rangesOfHashtagsInString: strRaw];
    if (hashtagRanges == nil || hashtagRanges.count == 0) {
        return @"";
    }
    NSTextCheckingResult* result = [hashtagRanges objectAtIndex: 0];
    NSRange range = result.range;
    range.location ++;
    range.length --;
    [strResult appendString: [strRaw substringWithRange: range]];
    
    for (int nIdx = 1; nIdx < hashtagRanges.count;  nIdx ++) {
        result = [hashtagRanges objectAtIndex: nIdx];
        range = result.range;
        range.location ++;
        range.length --;
        NSString* strTag = [strRaw substringWithRange: range];
        [strResult appendFormat: @",%@", strTag];
    }
    return strResult;
}


#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = _m_InfoView.frame;
        rect.origin.y += 60;
        _m_InfoView.frame = rect;
    }];

    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = _m_InfoView.frame;
        rect.origin.y -= 60;
        _m_InfoView.frame = rect;
    }];
}

@end
