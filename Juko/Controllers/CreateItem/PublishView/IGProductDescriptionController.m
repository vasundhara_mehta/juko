//
//  IGProductDescriptionController.m
//  Juko
//
//  Created by Mountain on 4/24/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGProductDescriptionController.h"

@interface IGProductDescriptionController ()

@end

@implementation IGProductDescriptionController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.m_nViewKind == 0) {
        [_m_LabelProductName setText: _m_clsProduct.m_strName];
        [_m_TextDescription setText: _m_clsProduct.m_strDescription];
    } else {
        [_m_LabelProductName setText: _m_clsCategory.m_strName];
        
        if (_m_clsCategory.m_strDescription == nil || [_m_clsCategory.m_strDescription isEqualToString: @""]) {
            [_m_TextDescription setText: _m_clsCategory.m_strLocalDescription];
        } else
            [_m_TextDescription setText: _m_clsCategory.m_strDescription];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

@end
