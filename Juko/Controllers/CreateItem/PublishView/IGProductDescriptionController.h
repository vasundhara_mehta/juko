//
//  IGProductDescriptionController.h
//  Juko
//
//  Created by Mountain on 4/24/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGProduct.h"
#import "IGCategory.h"

@interface IGProductDescriptionController : UIViewController

@property int m_nViewKind; //0 - product description, 1 - category description
@property (nonatomic , strong) IGCategory* m_clsCategory;
@property (nonatomic , strong) IGProduct* m_clsProduct;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelProductName;
@property (weak, nonatomic) IBOutlet UITextView *m_TextDescription;

- (IBAction)goBack:(id)sender;

@end
