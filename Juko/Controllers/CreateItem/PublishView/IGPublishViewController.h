//
//  IGPublishViewController.h
//  Juko
//
//  Created by Mountain on 3/24/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGProduct.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface IGPublishViewController : UIViewController<UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIDocumentInteractionControllerDelegate, UIAlertViewDelegate>

//import

@property (nonatomic, strong) NSArray* m_publishImages;
@property (nonatomic, strong) UIImage* m_publishImage;
@property (nonatomic, strong) NSData* m_dataPrintImage;
@property BOOL m_FDropShadow;
@property int m_nTemplateId;

@property (nonatomic, strong) IGProduct* m_clsProduct;

//members
@property (weak, nonatomic) IBOutlet UIImageView *m_ImageView_Publish;
@property (weak, nonatomic) IBOutlet UIImageView *m_ImageView_Publish1;
@property (weak, nonatomic) IBOutlet UIImageView *m_ShadowView;
@property (weak, nonatomic) IBOutlet UIView *m_PublishView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnRotate;
@property (weak, nonatomic) IBOutlet UIImageView *m_ImagePrint;

//publish view
@property (weak, nonatomic) IBOutlet UISwitch *m_SwitchPublish;
@property (weak, nonatomic) IBOutlet UISwitch *m_SwitchSell;
@property (weak, nonatomic) IBOutlet UITextField *m_TextDescription;

@property (weak, nonatomic) IBOutlet UIView *m_InfoView;
//actions

- (IBAction)goBack:(id)sender;

- (IBAction)actionFbShare:(id)sender;
- (IBAction)actionMailShare:(id)sender;
- (IBAction)actionIGShare:(id)sender;
- (IBAction)actionTwShare:(id)sender;


- (IBAction)actionBuyIt:(id)sender;
- (IBAction)actionDone:(id)sender;

- (IBAction)actionRotateMug:(id)sender;
- (IBAction)showInfo:(id)sender;

@end
