//
//  IGChooseController.m
//  Inkgram
//
//  Created by Mountain on 2/27/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGChooseController.h"

#import "IGProduct.h"
#import "IGCategory.h"
#import "IGTemplate.h"

#import "IGMediaImageView.h"
#import "IGLayoutView.h"
#import "IGPhotoCell.h"

#import "CUtils.h"
#import "DataKeeper.h"
#import "const.h"

#import "TKDragView.h"
#import "IGProgressActivity.h"

#import "IGAppDelegate.h"
#import "IGFbPhotoImportController.h"
#import "IGInstagramImportViewController.h"
#import "IGFilterViewController.h"
#import "IGPublishViewController.h"
#import "IGIntroViewController.h"

#import "WebViewController.h"
#import "global.h"

#import "UIXOverlayController.h"
#import "Reachability.h"

#define IMAGE_SIZE 60
#define IMAGE_PADDING 67

#define ADDING_PHOTO_SIZE 70
#define ADDING_PHOTO_PADDING 8

#ifdef TEST_MODE
#define FB_ACCESS_TOKEN @"CAACEdEose0cBAPzMrrXdKoZCDHWDkz0hEjJQEZCpPUvp7svU9eBqiJMGUsDoJ8XEHZC1rfLgXoWIGxaWT79JvTyEJcFmkJZBZBgwfyBQkwutGbpnnvK3iY8XsGjZBqFPlxkf63xsgSjKCcXRsS8ZB6jGfCz9s57Y1aXTdX71ch67PSg4GmM9SxgsUyZARm5op0mXlLNHi62IBwZDZD"
#define FB_USER_ID @"1054105928"
#endif


@interface IGChooseController ()

@end

@implementation IGChooseController
{
    NSMutableArray* m_imageList;
    NSMutableArray* m_addingPhotos;
    
    NSMutableArray* m_layoutFrames;
    
    BOOL m_FGrabImage;
    
    CGPoint m_prevPt;
    
    int m_nCurrentCell;
    
    BOOL m_FGetFbInfo;
    
    TKDragView* m_ImgViewForFilter;
    
    IGProgressActivity* HUD;
}

@synthesize m_CurrentTemplateView, m_clsProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_imageList = [NSMutableArray array];
        m_addingPhotos = [[NSMutableArray alloc] init];
        m_layoutFrames = [[NSMutableArray alloc] init];
        m_nCurrentCell = -1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [_m_LabelProductName setText: m_clsProduct.m_strName];
    [_m_LabelPrice setText: @""];
    
    CGRect rect = _m_View_Media.frame;
    rect.origin.y = self.view.frame.size.height + 100;// - rect.size.height;
    [_m_View_Media setFrame: rect];
    
    _m_StreamView_Photos.scrollsToTop = YES;
    
    _m_StreamView_Photos.cellPadding = ADDING_PHOTO_PADDING;
    _m_StreamView_Photos.columnPadding = ADDING_PHOTO_PADDING;
    
    _m_StreamView_Photos.delegate = self;
    
    m_selectedImageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, ADDING_PHOTO_SIZE, ADDING_PHOTO_SIZE)];
    [self.view addSubview: m_selectedImageView];
    m_selectedImageView.alpha = 0;
    
//    @autoreleasepool
    {
        [self loadLayoutInfo];
    }
    
//    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadExtraProductInfo) toTarget: self withObject: nil];
}

- (void) loadExtraProductInfo
{
    if (!m_clsProduct.m_FLoaded) {
        DataKeeper* clsModel = [DataKeeper sharedInstance];
        NSDictionary* dict = [clsModel getProductInfo: m_clsProduct.m_nIndex];
        
        m_clsProduct.m_strDescription = [[CUtils getStringValueFromDictionary: dict KEY: @"description"] copy];
        m_clsProduct.m_rPrice = [CUtils getFloatValueFromDictionary: dict KEY: @"price"];
        m_clsProduct.m_FLoaded = YES;
    }
    
    [_m_LabelPrice setText: [NSString stringWithFormat: @"$%.2f", m_clsProduct.m_rPrice]];

//    [HUD hide: YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load layout info

- (void) loadLayoutInfo
{
    float rSize = 310;
    float rContentSize = 300;
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        rSize = 222;
        rContentSize = 210;
    }
    
    if (m_clsProduct.m_templates && m_clsProduct.m_templates.count > 0) {
        for (int nIdx = 0; nIdx < m_clsProduct.m_templates.count; nIdx ++) {
            int nTemplateId = [m_clsProduct.m_templates[nIdx] intValue];
            
            IGLayoutView* layoutView = (IGLayoutView*)[[[NSBundle mainBundle] loadNibNamed: @"IGLayoutView" owner: nil options: nil] objectAtIndex: 0];
            [layoutView setFrame: CGRectMake(320 * nIdx + (320 - rContentSize) / 2, (rSize - rContentSize) / 2, rContentSize, rContentSize)];
            layoutView.tag = nIdx + 10000;
            layoutView.delegate = self;
            [_m_ScrollView_Layout addSubview: layoutView];
            [layoutView configure: nTemplateId Product: m_clsProduct];
            
            if (nIdx == 0) {
                [self setCurrentLayout: layoutView];
            }
        }
        
        [_m_ScrollView_Layout setContentSize: CGSizeMake(320 * m_clsProduct.m_templates.count, rSize)];
        _m_ScrollView_Layout.delegate = self;
    }
}

- (void) setCurrentLayout: (IGLayoutView*) layoutView
{
    //add rects for drag view
    m_CurrentTemplateView = layoutView;
    [m_layoutFrames removeAllObjects];
    for (NSString* strRect in layoutView.m_LayoutRects) {
        CGRect rect = [CUtils getRectFromString: strRect];
        [m_layoutFrames addObject: [TKCGRect from: rect forView: layoutView.m_layoutView]];
    }
    
    for (UIView* view in _m_ScrollView_Images.subviews) {
        if ([view isKindOfClass: [TKDragView class]]) {
            ((TKDragView*)view).goodFramesArray = m_layoutFrames;
        }
    }
}

#pragma mark - button Actions

- (IBAction)goBack:(id)sender {
    
    [CUtils clearView: _m_ScrollView_Layout];
    
    [self.navigationController popViewControllerAnimated: YES];
}


- (IBAction)finishEdit:(id)sender {
    if ([m_CurrentTemplateView isPossibleToFinish]) {
        [HUD show: YES];
        [NSThread detachNewThreadSelector: @selector(processForPublish) toTarget: self withObject: nil];
    } else {
        ALERT_SHOW(@"You must complete this template");
    }
}

- (void) processForPublish
{
    NSDictionary* dict = [m_CurrentTemplateView completeImage];
    
    [self performSelectorOnMainThread: @selector(gotoPublishView:) withObject: dict waitUntilDone: YES];
    
    [HUD hide: YES];
}

- (void) gotoPublishView: (NSDictionary*) dict
{
    IGPublishViewController* vc = [[IGPublishViewController alloc] initWithNibName: @"IGPublishViewController" bundle: nil];
    vc.m_publishImage = [dict objectForKey: @"image"];
    vc.m_publishImages = [dict objectForKey: @"images"];
    vc.m_FDropShadow = [[dict objectForKey: @"shadow"] boolValue];
    vc.m_clsProduct = [dict objectForKey: @"product"];
    vc.m_dataPrintImage = [dict objectForKey: @"print_image"];
    vc.m_nTemplateId = [[dict objectForKey: @"template_id"] intValue];
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)addMedia:(id)sender {
    m_FImportMode = YES;
    
    [m_addingPhotos removeAllObjects];
    
    _m_btnDismiss.hidden = NO;
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = _m_View_Media.frame;
        if ([UIScreen mainScreen].bounds.size.height > 500) {
            rect.size.height = 504;
        }
        rect.origin.y = self.view.frame.size.height - rect.size.height;
        [_m_View_Media setFrame: rect];
    }];
}

#pragma mark - show publish view


- (IBAction)dismissImportView:(id)sender {
    m_FImportMode = NO;
    
    _m_btnDismiss.hidden = YES;
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = _m_View_Media.frame;
        rect.origin.y = self.view.frame.size.height + 100;
        [_m_View_Media setFrame: rect];
    }];
}

#pragma mark - Refresh images for texturing

- (void) addTextureImages: (NSArray*) imgList
{
//    [CUtils clearView: _m_ScrollView_Images];
    
	CGRect workingFrame = CGRectMake(7, 32, IMAGE_SIZE, IMAGE_SIZE);
    CGFloat rOffsetX = _m_ScrollView_Images.contentSize.width;
    if (m_imageList.count > 0) {
        workingFrame.origin.x = rOffsetX;
    }
    
    [m_imageList addObjectsFromArray: imgList];
    
    int nIndex = 0;
    for (UIImage* image in imgList) {
        TKDragView* dragView = [[TKDragView alloc] initWithImage: image startFrame: workingFrame goodFrames: m_layoutFrames badFrames: nil andDelegate: self];
        [dragView setViewIndex: nIndex];
        [dragView setParentContainer: _m_ScrollView_Images];
        [_m_ScrollView_Images addSubview: dragView];
        
        workingFrame.origin.x += IMAGE_PADDING;
        nIndex ++;
    }
	
	[_m_ScrollView_Images setContentSize: CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
}

- (void) reloadSourceImages
{
    [CUtils clearView: _m_ScrollView_Images];
    
	CGRect workingFrame = CGRectMake(7, 32, IMAGE_SIZE, IMAGE_SIZE);
//    CGFloat rOffsetX = 0;//_m_ScrollView_Images.contentSize.width;
//    if (m_imageList.count > 0) {
//        workingFrame.origin.x = rOffsetX;
//    }
    
    int nIndex = 0;
    for (UIImage* image in m_imageList) {
        TKDragView* dragView = [[TKDragView alloc] initWithImage: image startFrame: workingFrame goodFrames: m_layoutFrames badFrames: nil andDelegate: self];
        [dragView setViewIndex: nIndex];
        [dragView setParentContainer: _m_ScrollView_Images];
        [_m_ScrollView_Images addSubview: dragView];
        
        if (self.m_btnCancel.hidden == NO) {
            dragView.m_btnDelete.alpha = 1;
            [dragView disableMainGesture];
        }
        
        workingFrame.origin.x += IMAGE_PADDING;
        nIndex ++;
    }
	
	[_m_ScrollView_Images setContentSize: CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
}


#pragma mark - ZCImagePickerControllerDelegate

- (void)zcImagePickerController:(ZCImagePickerController *)imagePickerController didFinishPickingMediaWithInfo:(NSArray *)info {
    [self dismissPickerView];
    
	for (NSDictionary *imageDic in info) {
        [m_addingPhotos addObject: [imageDic objectForKey: UIImagePickerControllerOriginalImage]];
	}
    
    [_m_StreamView_Photos reloadData];
}

- (void)zcImagePickerControllerDidCancel:(ZCImagePickerController *)imagePickerController {
    [self dismissPickerView];
}

#pragma mark - Private Methods

- (void)launchImagePickerViewController {
    ZCImagePickerController *imagePickerController = [[ZCImagePickerController alloc] init];
    imagePickerController.imagePickerDelegate = self;
    imagePickerController.maximumAllowsSelectionCount = 100;
    imagePickerController.mediaType = ZCMediaAllAssets;
    [self presentViewController:imagePickerController animated:YES completion:NULL];
}

- (void)dismissPickerView
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Photo Import View Actions

- (IBAction)photoLibrary:(id)sender
{
    [self launchImagePickerViewController];
}

- (IBAction) actionCamera:(id)sender
{
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.allowsEditing = NO;
    picker.delegate = self;
    [self presentViewController: picker animated: YES completion: nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    CGSize sz = image.size;
    UIImage* fixImage = [CUtils fixImage: [CUtils imageWithImage: image scaledToSize: CGSizeMake(sz.width / 3, sz.height / 3)]];
    
    NSData *data = UIImageJPEGRepresentation(fixImage, 0.8);
    UIImage* img = [UIImage imageWithData:data];
    [self addPhotos:[NSArray arrayWithObject: img]];
    
    [picker dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark - Import Photos from Instagram

- (IBAction)photoFromInstagram:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver: self name: INSTAGRAM_AUTH_WAS_SUCCESFULL_NOTIFICATION object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(showIGImportView:) name: INSTAGRAM_AUTH_WAS_SUCCESFULL_NOTIFICATION object: nil];

    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    [self igLogin];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) igLogin
{
    Reachability *netReach = [Reachability reachabilityWithHostname:@"www.instagram.com"];
    NetworkStatus netStatus = [netReach currentReachabilityStatus];
    if (netStatus==ReachableViaWiFi || ReachableViaWWAN) {
    } else {
        ALERT_SHOW(@"Can't reach to instagram site");
        return;
    }

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey: LOCAL_KEY_IG_LOGIN] && [[defaults objectForKey: LOCAL_KEY_IG_LOGIN] isEqualToString: @"yes"])
    {
        BOOL loggedIn = [self loginSuccessfull];
        
        if (!loggedIn)
        {
            // show oauth login website
            WebViewController* webViewController = [[WebViewController alloc] initForInstagramAuthentification];
            [self presentViewController: webViewController animated: YES completion: nil];
        }
        else
        {
            [defaults setObject: @"yes" forKey: LOCAL_KEY_IG_LOGIN];
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName: INSTAGRAM_AUTH_WAS_SUCCESFULL_NOTIFICATION object: nil];
        }
    }
    else
    {
        // show oauth login website
        WebViewController* webViewController = [[WebViewController alloc] initForInstagramAuthentification];
        [self presentViewController: webViewController animated: YES completion: nil];
    }
    
    [HUD hide: YES];
}

- (BOOL) loginSuccessfull
{
    // check, if auth token was received
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* savedAuthToken = [defaults objectForKey: LOCAL_KEY_IG_ACCESS_TOKEN];
    if (savedAuthToken == nil || [savedAuthToken isEqualToString: @""]) {
        return NO;
    }
    

    return NO;
}

- (void) showIGImportView: (NSNotification*) noti
{
    [self performSelectorOnMainThread: @selector(showIGPhotoImportView) withObject: nil waitUntilDone: YES];
}

- (void) showIGPhotoImportView
{
    IGInstagramImportViewController* vc = [[IGInstagramImportViewController alloc] initWithNibName: @"IGInstagramImportViewController" bundle: nil];
    vc.m_delegate = self;
    [self presentViewController: vc animated: YES completion: nil];
}

#pragma mark - Import Photos from Facebook

- (IBAction)photoFromFacebook:(id)sender {
    m_FGetFbInfo = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: @"GetFbInfo" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(showFbImportView:) name: @"GetFbInfo" object: nil];

    [self fbLogin];
}

//#undef TEST_MODE
- (void) fbLogin
{
#ifndef TEST_MODE
    Reachability *netReach = [Reachability reachabilityWithHostname:@"www.facebook.com"];
    NetworkStatus netStatus = [netReach currentReachabilityStatus];
    if (netStatus==ReachableViaWiFi || ReachableViaWWAN) {
    } else {
        ALERT_SHOW(@"Can't reach to facebook site");
        return;
    }
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.session == nil) {
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_stream", @"publish_actions", @"read_stream", @"read_friendlists", @"email", @"user_photos", nil];
        appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
        
        [FBAppCall handleDidBecomeActiveWithSession:appDelegate.session];
    }
    [FBSession setActiveSession: appDelegate.session];
    if ([appDelegate.session isOpen]) {
        m_FGetFbInfo = YES;

        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject: appDelegate.session.accessTokenData.accessToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
        [defaults synchronize];
        
        [self showFbImportView: nil];
    } else {
        [appDelegate.session openWithBehavior:FBSessionLoginBehaviorWithNoFallbackToWebView completionHandler:^(FBSession* session, FBSessionState status, NSError* error) {
            if ([session isOpen]) {
                m_FGetFbInfo = YES;

                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject: appDelegate.session.accessTokenData.accessToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
                [defaults synchronize];

                [[NSNotificationCenter defaultCenter] postNotificationName: @"GetFbInfo" object: nil];
            } else {
                NSLog(@"Failed to login on Facebook");
                m_FGetFbInfo = YES;
            }
        }];
    }
#else
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: FB_ACCESS_TOKEN forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    [defaults setObject: FB_USER_ID forKey: LOCAL_KEY_FB_USER_ID];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"GetFbInfo" object: nil];
    m_FGetFbInfo = YES;
#endif
}

//- (void) getFBUserInfoAndSignin
//{
//    FBRequestConnection* connection = [[FBRequestConnection alloc] init];
//    connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession | FBRequestConnectionErrorBehaviorAlertUser | FBRequestConnectionErrorBehaviorRetry;
//    FBRequest* request;
//    request = [FBRequest requestWithGraphPath: @"me?field=id" parameters: nil HTTPMethod: @"GET"];
//    
//    [connection addRequest: request completionHandler:^(FBRequestConnection* connection, id result, NSError* error) {
//        if (!error) {
//            FBGraphObject* obj = (FBGraphObject*)result;
//            
//            IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
//            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setObject: appDelegate.session.accessTokenData.accessToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
//            [defaults setObject: [obj objectForKey: @"id"] forKey: LOCAL_KEY_FB_USER_ID];
//            [defaults synchronize];
//        }
//        
//        m_FGetFbInfo = YES;
//    }];
//    [connection start];
//}
//
- (void) showFbImportView: (NSNotification*) noti
{
    [self performSelectorOnMainThread: @selector(showFbImportPhotoView) withObject: nil waitUntilDone: YES];
}

- (void) showFbImportPhotoView
{
    IGFbPhotoImportController* vc = [[IGFbPhotoImportController alloc] initWithNibName: @"IGFbPhotoImportController" bundle: nil];
    vc.m_delegate = self;
    [self presentViewController: vc animated: YES completion: nil];
}

- (void) addPhotos: (NSArray*) photos
{
    if (photos == nil || photos.count == 0) {
        return;
    }
    [m_addingPhotos addObjectsFromArray: photos];
    [_m_StreamView_Photos reloadData];
}

#pragma mark - add all the import photos

- (IBAction)importPhotos:(id)sender {
    [self dismissImportView: nil];
    
    [self addTextureImages: m_addingPhotos];
}

#pragma mark - ShowFilterView

- (void) showFilterView: (TKDragView*) cell
{
    m_ImgViewForFilter = cell;
    
    IGFilterViewController* vc = [[IGFilterViewController alloc] initWithNibName: @"IGFilterViewController" bundle: nil];

    vc.m_SourceImage = cell.m_sourceImage;
    vc.m_OrgImage = cell.m_orgImage;
    vc.m_cropSize = cell.bounds.size;
    
    if (m_CurrentTemplateView.m_nTemplateIndex == 102) //circle rect
        vc.m_FCircle = YES;
    else
        vc.m_FCircle = NO;
    
    vc.delegate = self;
    
    [self presentViewController: vc animated: YES completion: nil];
}

- (void) applyFilteredImage: (NSDictionary*) dict
{
    UIImage* image = [dict objectForKey: @"image"];
    NSValue* clsValue = [dict objectForKey: @"rect"];
    UIImage* wholeImage = [dict objectForKey: @"whole_image"];
    CGRect rect = [clsValue CGRectValue];
    
    m_ImgViewForFilter.m_viewRect = rect;
    m_ImgViewForFilter.m_sourceImage = wholeImage;
    [m_ImgViewForFilter.imageView setImage: wholeImage];

    IGLayoutView* view = (IGLayoutView*)m_ImgViewForFilter.superview.superview.superview;
    int nIndex = m_ImgViewForFilter.tag - 1500;
    [view setImageToRect: image RectIndex: nIndex];
}

#pragma mark - StreamViewDelegate

- (NSInteger)numberOfCellsInStreamView:(StreamView *)streamView
{
    NSInteger nCount = (m_addingPhotos == nil) ? 0 : m_addingPhotos.count;
    
    return nCount;
}

- (NSInteger)numberOfColumnsInStreamView:(StreamView *)streamView
{
    return 4;
}

- (UIView *)streamView:(StreamView *)stream cellAtIndex:(NSInteger)index
{
//    static NSString *CellID1 = @"MyCell1";
//    static NSString *CellID2 = @"MyCell2";
//    static NSString *CellID3 = @"MyCell3";
//    static NSString *CellID4 = @"MyCell4";
    
    NSString *CellID =  [NSString stringWithFormat: @"MyCell%d", (index%4+1)];
    
    IGPhotoCell *cell;
    
    cell = (IGPhotoCell *)[stream dequeueReusableCellWithIdentifier:CellID];
    
    if (cell == nil) {
        cell = [[IGPhotoCell alloc] initWithFrame:CGRectMake(0, 0, ADDING_PHOTO_SIZE, ADDING_PHOTO_SIZE) ContentSize: CGSizeMake(ADDING_PHOTO_SIZE - 4, ADDING_PHOTO_SIZE - 4)];
        cell.reuseIdentifier = CellID;
    }
    
    cell.delegate = self;
    cell.tag = index + 1000;
    
    [cell configureByImage: m_addingPhotos[index] Index: index];
    
    return cell;
}

- (CGFloat)streamView:(StreamView *)streamView heightForCellAtIndex:(NSInteger)index
{
    return ADDING_PHOTO_SIZE;
}

- (UIView *)headerForStreamView:(StreamView *)streamView
{
    return nil;
    /*
     CSearchCell *header = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     header.label.text = @"This is the header";
     
     return header;
     */
}

- (UIView *)footerForStreamView:(StreamView *)streamView
{
    return nil;
    
    /*
     if (page <= MaxPage) {
     CSearchCell *footer = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     footer.label.text = @"This is the footer";
     
     return footer;
     } else {
     return nil;
     }
     */
}

#pragma mark - cell click

- (void) deletePhoto: (NSDictionary*) dict
{
    int nIndex = [CUtils getIntValueFromDictionary: dict KEY: @"index"];
    if (m_addingPhotos.count > nIndex) {
        [m_addingPhotos removeObjectAtIndex: nIndex];
        [_m_StreamView_Photos reloadData];
    }
}

#pragma mark - delete texture item

- (void) deleteTexture: (int) nIndex
{
    
}

#pragma mark - TKDragViewDelegate

- (void) enterDeleteMode
{
    for (UIView* view in _m_ScrollView_Images.subviews) {
        if ([view isKindOfClass: [TKDragView class]]) {
            ((TKDragView*)view).m_btnDelete.alpha = 1;
            [((TKDragView*)view) disableMainGesture];
        }
    }
    
    self.m_btnCancel.hidden = NO;
}

- (IBAction)cancelDeleteMode:(id)sender {
    for (UIView* view in _m_ScrollView_Images.subviews) {
        if ([view isKindOfClass: [TKDragView class]]) {
            ((TKDragView*)view).m_btnDelete.alpha = 0;
            [((TKDragView*)view) enableMainGesture];
        }
    }
    
    self.m_btnCancel.hidden = YES;
}

- (void) deleteSourceImage: (NSDictionary*) dict
{
    int nIndex = [CUtils getIntValueFromDictionary: dict KEY: @"index"];
    if (m_imageList.count > nIndex) {
        [m_imageList removeObjectAtIndex: nIndex];
        [self reloadSourceImages];
    }
}


- (void)dragViewDidStartDragging:(TKDragView *)dragView{
    
    [UIView animateWithDuration:0.2 animations:^{
        dragView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    }];
}

- (void)dragViewDidEndDragging:(TKDragView *)dragView{
    
    if (m_nCurrentCell > -1) {
        UIImageView* imgMaskView = (UIImageView*)[m_CurrentTemplateView.m_layoutView viewWithTag: m_nCurrentCell + 2000];
        imgMaskView.alpha = 0;

        if ([dragView getLayoutMode] && [dragView getViewIndex] == m_nCurrentCell) {
            [m_CurrentTemplateView setDragView: dragView Index: m_nCurrentCell];
            [dragView deleteSelf];
        } else {
            UIImage* image = [dragView getImage];
//            UIImageView* imgContent = (UIImageView*)[m_CurrentTemplateView.m_layoutView viewWithTag: m_nCurrentCell + 1000];
//            imgContent.image = image;
            
            [m_CurrentTemplateView setImageToRect: image RectIndex: m_nCurrentCell MatchFrames: m_layoutFrames ParentView: _m_ScrollView_Layout];
        }
        
        m_nCurrentCell = -1;
    } else if ([dragView getLayoutMode] == YES) { //dragview on template
        [m_CurrentTemplateView clearImage: dragView];
        
        [dragView deleteSelf];
    }
}

- (void) endProcess: (TKDragView*) dragView
{
    
}

- (void)dragViewDidEnterStartFrame:(TKDragView *)dragView{
    
    [UIView animateWithDuration:0.2 animations:^{
        dragView.alpha = 0.5;
    }];
}

- (void)dragViewDidLeaveStartFrame:(TKDragView *)dragView{
    
    [UIView animateWithDuration:0.2 animations:^{
        dragView.alpha = 1.0;
    }];
}


- (void)dragViewDidEnterGoodFrame:(TKDragView *)dragView atIndex:(NSInteger)index
{
    if (index >= m_layoutFrames.count) {
        return;
    }
    TKCGRect* rect = [m_layoutFrames objectAtIndex:index];
    UIView* view = rect.parent;
    UIImageView* imgMaskView = (UIImageView*)[view viewWithTag: index + 2000];
    imgMaskView.alpha = 1;
    [view bringSubviewToFront: imgMaskView];
    
    m_nCurrentCell = index;
}

- (void)dragViewDidLeaveGoodFrame:(TKDragView *)dragView atIndex:(NSInteger)index
{
    if (index >= m_layoutFrames.count) {
        return;
    }
    TKCGRect* rect = [m_layoutFrames objectAtIndex:index];
    UIView* view = rect.parent;
    UIImageView* imgMaskView = (UIImageView*)[view viewWithTag: index + 2000];
    imgMaskView.alpha = 0;
    
    m_nCurrentCell = -1;
}


- (void)dragViewDidEnterBadFrame:(TKDragView *)dragView atIndex:(NSInteger)index{
    
//    UIView *view = [self.badFrames objectAtIndex:index];
//    
//    if (view) view.layer.borderWidth = 4.0f;
}

- (void)dragViewDidLeaveBadFrame:(TKDragView *)dragView atIndex:(NSInteger)index{
//    
//    UIView *view = [self.badFrames objectAtIndex:index];
//    
//    if (view) view.layer.borderWidth = 1.0f;
}


- (void)dragViewWillSwapToEndFrame:(TKDragView *)dragView atIndex:(NSInteger)index{
    
    
    
}

- (void)dragViewDidSwapToEndFrame:(TKDragView *)dragView atIndex:(NSInteger)index{
    
    
//    [UIView animateWithDuration:0.2
//                          delay:0.1
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^{
//                         dragView.transform = CGAffineTransformMakeRotation(M_PI);
//                     }
//                     completion:^(BOOL finished) {
//                         
//                     }];
}


- (void)dragViewWillSwapToStartFrame:(TKDragView *)dragView{
    [UIView animateWithDuration:0.2 animations:^{
        dragView.alpha = 1.0f;
    }];
}

- (void)dragViewDidSwapToStartFrame:(TKDragView *)dragView{
    
}

#pragma mark - UIScrollViewDelegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _m_ScrollView_Layout) {
        float rOffsetX = scrollView.contentOffset.x;
        int nIdx = (rOffsetX - 10) / 320 + 1 + 10000;
        
//        m_CurrentTemplateView = (IGLayoutView*)[_m_ScrollView_Layout viewWithTag: nIdx];
        [self setCurrentLayout: (IGLayoutView*)[_m_ScrollView_Layout viewWithTag: nIdx]];
    }
}

#pragma mark - IGMediaImageView Delegate

- (void) chooseImage: (NSDictionary*) dict
{
}

#pragma mark - Gesture handling

- (void)panDetected:(UIPanGestureRecognizer*)gestureRecognizer{
    switch ([gestureRecognizer state]) {
        case UIGestureRecognizerStateBegan:
            [self panBegan:gestureRecognizer];
            break;
        case UIGestureRecognizerStateChanged:
            [self panMoved:gestureRecognizer];
            break;
        case UIGestureRecognizerStateEnded:
            [self panEnded:gestureRecognizer];
            break;
        default:
            break;
    }
}

- (void) panBegan: (UIPanGestureRecognizer*) gesture
{
    CGPoint pt = [gesture locationInView: _m_touchView];
    if (CGRectContainsPoint(_m_ScrollView_Images.frame, pt)) {
        
        m_FGrabImage = YES;
    }
}

- (void) panMoved: (UIPanGestureRecognizer*) gesture
{
    
}

- (void) panEnded: (UIPanGestureRecognizer*) gesture
{
    
}

#pragma mark - IntroView

- (IBAction)showIntroView:(id)sender {
    IGIntroViewController* vc = [[IGIntroViewController alloc] initWithNibName: @"IGIntroViewController" bundle: nil];
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    [overlay presentOverlayOnView: self.view withContent: vc animated: YES];
}

@end
