//
//  IGProductKindController.m
//  Juko
//
//  Created by Mountain on 3/11/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGProductKindController.h"

#import "DataKeeper.h"
#import "const.h"
#import "CUtils.h"

#import "IGCategory.h"
#import "IGProduct.h"

#import "IGChooseController.h"

#define PRODUCT_CELL @"ProductCell"

@interface IGProductKindController ()

@end

@implementation IGProductKindController

@synthesize m_clsCategory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    m_ProductList = m_clsCategory.m_ProductList;
    
    [self.m_tableView registerNib: [UINib nibWithNibName: PRODUCT_CELL bundle: Nil] forCellReuseIdentifier:PRODUCT_CELL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_ProductList == nil || m_ProductList.count == 0) ? 0 : (m_ProductList.count - 1) / 2 + 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: PRODUCT_CELL];;
    
    int nIndex = indexPath.row * 2;
    [self configureCell: cell Index: nIndex];
    
    cell.tag = indexPath.row + 1000;
    
    return cell;
}

- (void) configureCell: (UITableViewCell*) cell Index: (int) nIndex
{
    UIView* viewLeft = [cell viewWithTag: 1];
    [self setProductView: viewLeft Product: m_ProductList[nIndex]];
    
    UIView* viewRight = [cell viewWithTag: 2];
    if (m_ProductList.count - 1 < nIndex + 1) {
        viewRight.hidden = YES;
    } else {
        [self setProductView: viewRight Product: m_ProductList[nIndex + 1]];
    }
}

- (void) setProductView: (UIView*) view Product: (IGProduct*) clsProduct
{
    view.hidden = NO;
    
    UIImageView* imageView = (UIImageView*)[view viewWithTag: 100];
    UILabel* labelTitle = (UILabel*)[view viewWithTag: 101];
    UILabel* labelPrice = (UILabel*)[view viewWithTag: 102];
    UIButton* button = (UIButton*)[view viewWithTag: 103];
    
    NSString* strImage = clsProduct.m_strExampleImage;//[CUtils getStringValueFromDictionary: product KEY: @"image"];
    
    if (m_clsCategory.m_nIndex == ECATEGORY_SKIN) {
        if ([clsProduct.m_strEditImage isEqualToString: @""]) {
            strImage = clsProduct.m_strName;
            [imageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"skin_%@.png", strImage]]];
        } else {
            strImage = clsProduct.m_strEditImage;
            [imageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", strImage]]];
        }
    } else if (m_clsCategory.m_nIndex == ECATEGORY_CANVAS) {
        strImage = clsProduct.m_strEditImage;//[CUtils getStringValueFromDictionary: product KEY: @"name"];
        if ([strImage isEqualToString: @""]) {
            strImage = clsProduct.m_strName;
        }
        if ([clsProduct.m_strName rangeOfString: @"Canvas-12X12"].length > 0) {
            [imageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@.png", clsProduct.m_strName]]];
        } else {
            [imageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@_addmask.png", strImage]]];
        }
    } else if (m_clsCategory.m_nIndex == ECATEGORY_BLOCKS) {
        strImage = clsProduct.m_strEditImage;//[CUtils getStringValueFromDictionary: product KEY: @"name"];
        if ([strImage isEqualToString: @""]) {
            strImage = clsProduct.m_strName;
        }
        [imageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"pub_%@.png", strImage]]];
    } else {
        strImage = clsProduct.m_strName;//[CUtils getStringValueFromDictionary: product KEY: @"name"];
        if ([strImage isEqualToString: @""]) {
            strImage = @"publish1";
        }
        [imageView setImage: [UIImage imageNamed: [NSString stringWithFormat: @"%@.png", strImage]]];
    }
    
    [labelTitle setText: clsProduct.m_strName];
    [labelPrice setText: [NSString stringWithFormat: @"$%.2f", clsProduct.m_rPrice]];
    
    [button addTarget: self action: @selector(chooseProduct:) forControlEvents: UIControlEventTouchUpInside];
}

- (void) chooseProduct: (UIButton*) button
{
    UIView* view = button.superview;
    UITableViewCell* cell = (UITableViewCell*)view.superview.superview.superview;
    int nIndex = (cell.tag - 1000) * 2 + view.tag - 1;
    
    IGChooseController* vc = [[IGChooseController alloc] initWithNibName: @"IGChooseController" bundle: nil];
    vc.m_clsProduct = m_ProductList[nIndex];//[[IGProduct alloc] initWithDictionary: m_ProductList[nIndex] CategoryId: m_clsCategory.m_nIndex];
    [self.navigationController pushViewController: vc animated: YES];
}

@end
