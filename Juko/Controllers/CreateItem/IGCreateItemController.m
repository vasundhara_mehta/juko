//
//  IGCreateItemController.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGCreateItemController.h"

#import "DataKeeper.h"
#import "CUtils.h"
#import "const.h"
#import "IGProduct.h"

#import "IGCategoryView.h"

#import "IGProductKindController.h"
#import "IGChooseController.h"
#import "IGAppDelegate.h"

#import "IGProductDescriptionController.h"

#import "IGProgressActivity.h"

@interface IGCreateItemController ()

@end

@implementation IGCreateItemController
{
    NSArray* m_CategoryList;
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        self.tabBarItem = [[UITabBarItem alloc] initWithTitle: @"Create" image: nil tag: 0];
        [self.tabBarItem setFinishedSelectedImage: [UIImage imageNamed: @"tabitem3_h"] withFinishedUnselectedImage: [UIImage imageNamed:@"tabitem3"]];
        [[self tabBarItem] setImageInsets: UIEdgeInsetsMake(5, 0, -5, 0)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [self.m_ScrollView setContentInset: UIEdgeInsetsMake(5, 0, 0, 0)];
    
    [self loadData];
}

#define CONTENT_SIZE 320
- (void) loadData
{
//    @autoreleasepool
    {
        m_CategoryList = [[DataKeeper sharedInstance] getCategoryList];
        
        for (int nIdx = 0; nIdx < m_CategoryList.count; nIdx ++) {
            IGCategoryView* itemView = (IGCategoryView*)[[[NSBundle mainBundle] loadNibNamed: @"IGCategoryView" owner: nil options: nil] objectAtIndex: 0];//[[IGCategoryView alloc] init];
            itemView.delegate = self;
            [itemView configure: m_CategoryList[nIdx]];
            
            CGRect rect = CGRectMake(0, nIdx * CONTENT_SIZE, 320, CONTENT_SIZE);
            [itemView setFrame: rect];
            
            [_m_ScrollView addSubview: itemView];
            
            [itemView configure: m_CategoryList[nIdx]];
        }
        
        [_m_ScrollView setContentSize: CGSizeMake(320, CONTENT_SIZE * m_CategoryList.count + 40)];
    }
}

- (void) touchItem: (IGCategory*) clsCategory
{
    if (clsCategory.m_ProductList.count == 1) {
        IGChooseController* vc = [[IGChooseController alloc] initWithNibName: @"IGChooseController" bundle: nil];
        vc.m_clsProduct = clsCategory.m_ProductList[0];//[[IGProduct alloc] initWithDictionary: clsItem.m_ProductList[0] CategoryId: clsItem.m_nIndex];

        IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate.m_clsNavController pushViewController: vc animated: YES];
    } else {
        IGProductKindController* vc = [[IGProductKindController alloc] initWithNibName: @"IGProductKindController" bundle: nil];
        vc.m_clsCategory = clsCategory;
        
        IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
        [appDelegate.m_clsNavController pushViewController: vc animated: YES];
    }
}

- (void) touchInfoButton: (IGCategory*) clsCategory
{
    if (clsCategory.m_strDescription == nil || [clsCategory.m_strDescription isEqualToString: @""]) {
        [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
        DataKeeper* clsModel = [DataKeeper sharedInstance];
        NSDictionary* dictCategory = [clsModel getCategoryDescription: clsCategory.m_nIndex];
        clsCategory.m_strDescription = [CUtils getStringValueFromDictionary: dictCategory KEY: @"description"];
        [HUD hide: YES];
    }
    
    IGProductDescriptionController* vc = [[IGProductDescriptionController alloc] initWithNibName: @"IGProductDescriptionController" bundle: nil];
    vc.m_nViewKind = 1;
    vc.m_clsCategory = clsCategory;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
