//
//  IGNewsFeedController.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGNewsFeedController.h"

//model
#import "const.h"
#import "CUtils.h"
#import "DataKeeper.h"
#import "IGItemUser.h"
#import "IGItem.h"
#import "IGComment.h"

//views
#import "FeedCellView.h"

//relation
#import "IGAppDelegate.h"
#import "IGCommentsViewController.h"
#import "IGTagViewController.h"
#import "IGProfileController.h"
#import "IGPurchaseController.h"
#import "IGLikersViewController.h"

//custom controls
#import "UIImageView+WebCache.h"
#import "AKTabBarController.h"
#import "IGProgressActivity.h"
#import "UIXOverlayController.h"
#import "ODRefreshControl.h"
#import "STTweetLabel.h"

#import <Social/SLComposeViewController.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/SLServiceTypes.h>

#define ITEM_ID_OFFSET 1000

@interface IGNewsFeedController ()

@end

@implementation IGNewsFeedController
{
    FeedCellView* m_CurCell;
    
    IGProgressActivity* HUD;
//    ODRefreshControl* m_RefreshCtrl;
    UIRefreshControl* m_RefreshCtrl;
    
    NSArray* m_arrayData;
    
    BOOL m_FLoading;
    
    UIImage* m_itemImage;
    IGItem* m_clsItem;
    UIDocumentInteractionController* docFile;
    
    int m_nPage;
    BOOL m_FEndData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[self tabBarItem] setFinishedSelectedImage:[UIImage imageNamed: @"tabitem1_h"] withFinishedUnselectedImage:[UIImage imageNamed:@"tabitem1"]];
        [[self tabBarItem] setImageInsets: UIEdgeInsetsMake(5, 0, -5, 0)];
        [[self tabBarItem] setTitlePositionAdjustment: UIOffsetZero];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reloadNewsfeed) name: @"refresh_newsfeed" object: nil];
    
    self.navigationController.navigationBar.hidden = YES;

    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_FEED bundle: nil] forCellReuseIdentifier: CELL_ID_FEED];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];

    m_RefreshCtrl = [[UIRefreshControl alloc] init];
    [m_RefreshCtrl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    [self.m_tableView addSubview: m_RefreshCtrl];
    
    [self showActivity];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
//    [self showActivity];
    m_nPage = 0;
    m_FEndData = NO;
    
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) reloadNewsfeed
{
    [m_RefreshCtrl beginRefreshing];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void) loadData
{
    m_FLoading = YES;
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    m_FEndData = ![clsModel loadNewsfeed: 0];
    if (!m_FEndData) {
        m_nPage ++;
    }
    
    [HUD hide: YES];
    
    m_FLoading = NO;
    [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
}

- (void) nextLoadData
{
    if (m_FEndData)
        return;
    
    m_FLoading = YES;
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    m_FEndData = ![clsModel loadNewsfeed: m_nPage];
    if (!m_FEndData) {
        m_nPage ++;
    }
    
    m_FLoading = NO;
    [self performSelectorOnMainThread: @selector(refreshNextData) withObject: nil waitUntilDone: YES];
}

- (void) refreshData
{
    [m_RefreshCtrl endRefreshing];
    [self.m_tableView reloadData];
    [self actionTopPosition: nil];
}

- (void) refreshNextData
{
    [self.m_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    m_arrayData = [[DataKeeper sharedInstance] getNewsfeedData];
    return (m_arrayData == nil) ? 0 : m_arrayData.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

#define ID_OFFSET_ITEM 1000
- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView = [[[NSBundle mainBundle] loadNibNamed: @"ItemHeaderView" owner: self options: nil] objectAtIndex: 0];
    UIImageView* imgAvatar = (UIImageView*)[headerView viewWithTag: 1];
    UIButton* buttonUser = (UIButton*)[headerView viewWithTag: 2];
    
    if (m_FLoading) {
        return headerView;
    }

    IGItem* clsItem = [m_arrayData objectAtIndex: section];
    IGItemUser* clsItemUser = clsItem.m_clsUser;
    NSString* strUrl = clsItemUser.m_strImageThumbUrl;
    NSURL* url = [NSURL URLWithString: strUrl];
    [imgAvatar setImageWithURL: url completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
        if (error) {
            NSLog(@"%@", error);
        }
    }];
    
    [buttonUser setTitle: clsItemUser.m_strUsername forState: UIControlStateNormal];
    [buttonUser addTarget: self action: @selector(chooseUser:) forControlEvents: UIControlEventTouchUpInside];
    buttonUser.tag = section + ID_OFFSET_ITEM;
    
    UILabel* labelTime = (UILabel*)[headerView viewWithTag: 3];
    UIImageView* imgClock = (UIImageView*)[headerView viewWithTag: 4];
    [labelTime setText: clsItem.m_strTime];
    [labelTime sizeToFit];
    CGRect rect = labelTime.frame;
    rect.origin.x = 310.0 - rect.size.width;
    labelTime.frame = rect;
    
    [imgClock setFrame: CGRectMake(rect.origin.x - imgClock.frame.size.width - 3, imgClock.frame.origin.y, imgClock.frame.size.width, imgClock.frame.size.height)];
    if (clsItem.m_strTime == nil || [clsItem.m_strTime isEqualToString: @""]) {
        imgClock.hidden = YES;
    } else {
        imgClock.hidden = NO;
    }
    
    return headerView;
}

- (void) chooseUser: (UIButton*) button
{
    int nRow = button.tag - ID_OFFSET_ITEM;
    IGItem* clsItem = [m_arrayData objectAtIndex: nRow];
    IGItemUser* clsItemUser = clsItem.m_clsUser;

    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_clsSimpleUser = clsItemUser;
    [self.navigationController pushViewController: vc animated: YES];

//    [self showUser: clsItemUser.m_strUsername];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rHeight = [self calcHeight: indexPath.section];
    return rHeight;
}

- (float) calcHeight: (int) nRow
{
    if (m_FLoading) {
        return 0;
    }

    IGItem* clsItem = [m_arrayData objectAtIndex: nRow];
    IGItemUser* clsItemUser = clsItem.m_clsUser;

    CGSize size = CGSizeZero;
    if ([clsItem.m_strDescription isEqualToString: @""]) {
        
    } else {
        NSString* strDesc = [NSString stringWithFormat: @"%@ %@", clsItemUser.m_strUsername, clsItem.m_strDescription];
        
        STTweetLabel *tweetLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 160.0)];
        [tweetLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 15.0f]];
        [tweetLabel setText: strDesc];
        tweetLabel.textAlignment = NSTextAlignmentLeft;
        
        size = [tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:tweetLabel.frame.size.width];
    }
    
    int nHeight = size.height + 10;

    NSMutableString* strComment = [[NSMutableString alloc] init];
    int nCount = MIN(MAX_COMMENT_COUNT, clsItem.m_arrayComments.count);
    for (int nIdx = clsItem.m_arrayComments.count - 1; (nIdx > -1 && nCount > 0); nIdx --) {
        IGComment* clsComment = clsItem.m_arrayComments[nIdx];
        nCount --;

        if ([strComment isEqualToString: @""]) {
            [strComment appendFormat: @"@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
        } else {
            [strComment appendFormat: @"\n@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
        }
    }
    
    STTweetLabel *commentLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 160.0)];
    [commentLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 15.0f]];
    [commentLabel setText: strComment];
    commentLabel.textAlignment = NSTextAlignmentLeft;
    
    size = [commentLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:commentLabel.frame.size.width];
    nHeight += size.height + 15;
    
    return nHeight + 292;
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] init];
    return view;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_FEED];
    if (m_FLoading) {
        return cell;
    }

    IGItem* clsItem = [m_arrayData objectAtIndex: indexPath.section];

    FeedCellView* feedCell = (FeedCellView*)cell;
    feedCell.delegate = self;
    [feedCell configureCell: clsItem];
    
    feedCell.tag = indexPath.section + ITEM_ID_OFFSET;
    
    if (indexPath.section == m_arrayData.count - 1 && !m_FEndData) {
        [NSThread detachNewThreadSelector: @selector(nextLoadData) toTarget: self withObject: nil];
    }
    
    return feedCell;
}

#pragma mark - FeedCell delegate methods

- (void) refreshCells: (FeedCellView*) cell
{
    int nSection = cell.tag - ITEM_ID_OFFSET;
    NSIndexPath* path = [NSIndexPath indexPathForRow: 0 inSection: nSection];
    [self.m_tableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: path] withRowAnimation: UITableViewRowAnimationAutomatic];
}

- (void) showAllComments: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_READ;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //[self.navigationController pushViewController: vc animated: YES];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) showUser: (NSString*) strUsername
{
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_strUsername = strUsername;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showTag: (NSString*) strTag
{
    NSString* strHashTag = strTag;
    if ([[strTag substringToIndex: 1] isEqualToString: @"#"]) {
        strHashTag = [strTag substringFromIndex: 1];
    }
    IGTagViewController* vc = [[IGTagViewController alloc] initWithNibName: @"IGTagViewController" bundle: nil];
    vc.m_strTag = strHashTag;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - action of item

- (void) actionLike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(likeProcess:) toTarget: self withObject: clsItem];
    //    [self likeProcess: clsItem];
}

- (void) showLikers: (IGItem*) clsItem
{
    IGLikersViewController* vc = [[IGLikersViewController alloc] initWithNibName: @"IGLikersViewController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) likeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: YES];
    if (!FSuccess) {
    }
}

- (void) actionUnlike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(unlikeProcess:) toTarget: self withObject: clsItem];
    //    [self unlikeProcess: clsItem];
}

- (void) unlikeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: NO];
    if (!FSuccess) {
    }
}

- (void) actionComment: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_WRITE;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
//    [self.navigationController pushViewController: vc animated: YES];
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) actionPurchase: (IGItem*) clsItem
{
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    overlay.dismissUponTouchMask = NO;
    
    IGPurchaseController* vc = [[IGPurchaseController alloc] initWithNibName: @"IGPurchaseController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    [overlay presentOverlayOnView: appDelegate.window withContent:vc animated: YES];
}

- (void) actionShare: (NSDictionary*) dict
{
    m_itemImage = [dict objectForKey: @"image"];
    m_clsItem = [dict objectForKey: @"item"];
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle: @"" delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Share on Facebook", @"Share on Mail", @"Share on Instagram", @"Share on Twitter", nil];
    [sheet showInView: self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self actionFbShare];
            break;
        case 1:
            [self actionMailShare];
            break;
        case 2:
            [self actionIGShare];
            break;
        case 3:
            [self actionTwShare];
            break;
        default:
            break;
    }
}

- (IBAction)actionFbShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

- (IBAction)actionMailShare {
    if (![MFMailComposeViewController canSendMail]) {
        ALERT_SHOW(@"Please setup your mail account in Settings of your device");
        return;
    }
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
    //    [mail setToRecipients:[NSArray arrayWithObject:@"info@thegrint.com"]];
    [mail setSubject: m_clsItem.m_strName];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: m_itemImage], 1.0) mimeType: @"image/jpg" fileName: m_clsItem.m_strName];
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

- (IBAction)actionIGShare {
    UIImage* image;
    image = [self imageForShare: m_itemImage];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];
        NSData *imageData=UIImagePNGRepresentation(image);
        [imageData writeToFile:saveImagePath atomically:YES];
        
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        
        if (docFile == Nil) {
            docFile = [[UIDocumentInteractionController alloc] init];
        }
        docFile.delegate=self;
        docFile.UTI=@"com.instagram.photo";
        
        NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
        docFile.annotation=[NSDictionary dictionaryWithObjectsAndKeys: strText, @"InstagramCaption", nil];
        
        [docFile setURL:imageURL];
        
        [docFile presentOpenInMenuFromRect: CGRectZero inView: self.view animated: YES];
    }
    else
    {
        NSLog (@"Instagram is not available");
    }
}

- (IBAction)actionTwShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeTwitter];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

#define SHARE_WIDTH 200
#define SHARE_HEIGHT 200
- (UIImage*) imageForShare : (UIImage*) orgImage
{
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, SHARE_WIDTH, SHARE_HEIGHT)];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.opaque = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView setImage: orgImage];
    return [CUtils imageWithView: imgView];
}

#pragma mark - move scroll to top

- (IBAction)actionTopPosition:(id)sender
{
    [self.m_tableView setContentOffset: CGPointMake(0, 0)];
}

@end
