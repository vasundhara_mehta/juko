//
//  IGCommentsViewController.h
//  Juko
//
//  Created by Mountain on 2/5/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataKeeper.h"
#import "IGItem.h"

@class IGComment;
@interface IGCommentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

//import variables
@property int m_nItemIndex;
@property (nonatomic, strong) IGItem* m_clsItem;
@property EViewKind m_eViewKind;

- (IBAction)goBack:(id)sender;

@end
