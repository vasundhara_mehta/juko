//
//  IGLikersViewController.h
//  Juko
//
//  Created by Mountain on 5/3/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGItem.h"

@interface IGLikersViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IGItem* m_clsItem;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

- (IBAction)goBack:(id)sender;

@end
