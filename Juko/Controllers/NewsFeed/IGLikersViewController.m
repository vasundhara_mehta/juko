//
//  IGLikersViewController.m
//  Juko
//
//  Created by Mountain on 5/3/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGLikersViewController.h"

#import "DataKeeper.h"
#import "const.h"
#import "IGItemUser.h"
#import "CUtils.h"

#import "IGProfileController.h"

#import "UIImageView+WebCache.h"

#import "IGProgressActivity.h"

@interface IGLikersViewController ()

@end

@implementation IGLikersViewController
{
    NSMutableArray* m_arrayUsers;
    
    int m_nPage;
    BOOL m_FEndData;
    
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_arrayUsers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [_m_tableView registerNib: [UINib nibWithNibName: STR_USER_CELL bundle: nil] forCellReuseIdentifier: STR_USER_CELL];
    
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
    
    m_nPage = 0;
}

#pragma mark - load follower/following users

- (void) loadData
{
    if (m_nPage == 0)
        [m_arrayUsers removeAllObjects];
    
    NSArray* arrayRawData;
    arrayRawData = [[DataKeeper sharedInstance] getLikers: _m_clsItem.m_nIndex PAGE: m_nPage];
    
    if (arrayRawData == nil || arrayRawData.count == 0) {
        m_FEndData = YES;
    } else {
        [m_arrayUsers addObjectsFromArray: arrayRawData];
        m_nPage ++;
        
        [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
    }
    
    [HUD hide: YES];
}

- (void) refreshData
{
    [_m_tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_arrayUsers == nil) ? 0 : m_arrayUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: STR_USER_CELL];
    cell.tag = indexPath.row + 10000;
    
    UIImageView* imgAvatar = (UIImageView*)[cell viewWithTag: 1];
    UIButton* btnName = (UIButton*)[cell viewWithTag: 2];
    UIButton* btnAction = (UIButton*)[cell viewWithTag: 3];
    btnAction.hidden = YES;
    
    NSDictionary* dictUser = [m_arrayUsers[indexPath.row] objectForKey: @"user"];
    
    NSString* strAvatarUrl = [CUtils getStringValueFromDictionary: dictUser KEY: @"profile_image_url"];
    [imgAvatar setImageWithURL: [NSURL URLWithString: strAvatarUrl]];
    [btnName setTitle: [CUtils getStringValueFromDictionary: dictUser KEY: @"username"] forState: UIControlStateNormal];
    [btnName addTarget: self action: @selector(showUser:) forControlEvents: UIControlEventTouchUpInside];
    
    if (indexPath.row == m_arrayUsers.count - 1 && !m_FEndData) {
        [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
    }
    
    return cell;
}

- (void) showUser: (UIButton*) button
{
    int nRow = (int)button.superview.superview.superview.tag - 10000;
    NSDictionary* dictUser = [m_arrayUsers[nRow] objectForKey: @"user"];
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_strUsername = [CUtils getStringValueFromDictionary: dictUser KEY: @"username"];
    vc.m_clsUser = nil;
    [self.navigationController pushViewController: vc animated: YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];

    int nRow = (int)indexPath.row;
    NSDictionary* dictUser = [m_arrayUsers[nRow] objectForKey: @"user"];
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_strUsername = [CUtils getStringValueFromDictionary: dictUser KEY: @"username"];
    vc.m_clsUser = nil;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - UIViewController Methods and actions

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

@end
