//
//  IGCommentsViewController.m
//  Juko
//
//  Created by Mountain on 2/5/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGCommentsViewController.h"

//model
#import "DataKeeper.h"
#import "const.h"
#import "IGItemUser.h"
#import "IGComment.h"
#import "IGUser.h"

//view
#import "CommentViewCell.h"

//controller
#import "IGProfileController.h"
#import "IGTagViewController.h"

//custom control
#import "IGProgressActivity.h"
#import "STTweetLabel.h"

#import "JSMessageTextView.h"
#import "JSMessageInputView.h"

#import "NSString+JSMessagesView.h"
#import "UIView+AnimationOptionsForCurve.h"
#import "UIColor+JSMessagesView.h"
#import "UIButton+JSMessagesView.h"

//utility
#import "CUtils.h"
#import "UIImageView+WebCache.h"

#define INPUT_HEIGHT 30.0f
#define ID_CELL_COMMENT @"CommentCell"

@interface IGCommentsViewController () <JSDismissiveTextViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) JSMessageInputView *inputToolbarView;
@property (assign, nonatomic) CGFloat previousTextViewContentHeight;

@property (assign, nonatomic) BOOL isUserScrolling;

- (void)setup;

- (void)sendPressed:(UIButton *)sender;

- (BOOL)shouldAllowScroll;

- (void)handleWillShowKeyboardNotification:(NSNotification *)notification;
- (void)handleWillHideKeyboardNotification:(NSNotification *)notification;
- (void)keyboardWillShowHide:(NSNotification *)notification;

@end



@implementation IGCommentsViewController
{
    IGProgressActivity* HUD;
    NSMutableArray* m_commentList;
}

#pragma mark - Initialization

- (void)setup
{
    if([self.view isKindOfClass:[UIScrollView class]]) {
        // fix for ipad modal form presentations
        ((UIScrollView *)self.view).scrollEnabled = NO;
    }
    
	_isUserScrolling = NO;
    
    CGSize size = self.view.frame.size;
	
    CGRect tableFrame = CGRectMake(0.0f, 64.0f, size.width, size.height - INPUT_HEIGHT - 64);
	_tableView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
	_tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_tableView.dataSource = self;
	_tableView.delegate = self;
    [_tableView registerNib: [UINib nibWithNibName: ID_CELL_COMMENT bundle: nil] forCellReuseIdentifier: ID_CELL_COMMENT];
    
	[self.view addSubview:_tableView];
	
    [self setBackgroundColor: [UIColor whiteColor]];//[UIColor js_messagesBackgroundColor_iOS6]];
    
    CGRect inputFrame = CGRectMake(0.0f, size.height - INPUT_HEIGHT, size.width, INPUT_HEIGHT);
    _inputToolbarView = [[JSMessageInputView alloc] initWithFrame:inputFrame
                                                 textViewDelegate:self
                                                 keyboardDelegate:self
                                             panGestureRecognizer:_tableView.panGestureRecognizer];
    
    UIButton *sendButton;
    sendButton = [UIButton js_defaultSendButton_iOS6];

    sendButton.enabled = NO;
    sendButton.frame = CGRectMake(_inputToolbarView.frame.size.width - 56.0f, 3.0f, 50.0f, 24.0f);
    [sendButton addTarget:self
                   action:@selector(sendPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    [_inputToolbarView setSendButton:sendButton];
    [self.view addSubview:_inputToolbarView];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [CUtils showActivity: HUD Caption: @""];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void) loadData
{
    m_commentList = [[DataKeeper sharedInstance] loadCommentList: _m_nItemIndex];
    
//    [_m_clsItem.m_arrayComments removeAllObjects];
//    [_m_clsItem.m_arrayComments addObjectsFromArray: m_commentList];
    
    [CUtils hideActivity: HUD];
    
    [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
}

- (void) refreshData
{
    [self.tableView reloadData];
    [self scrollToBottomAnimated: YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }

    [self scrollToBottomAnimated: NO];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleWillShowKeyboardNotification:)
												 name:UIKeyboardWillShowNotification
                                               object:nil];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleWillHideKeyboardNotification:)
												 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if (self.m_eViewKind == MODE_WRITE) {
        [_inputToolbarView.textView becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.inputToolbarView resignFirstResponder];
    [self setEditing:NO animated:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    NSLog(@"*** %@: didReceiveMemoryWarning ***", self.class);
}

- (void)dealloc
{
    _tableView = nil;
    _inputToolbarView = nil;
}

#pragma mark - View rotation

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
    [self.tableView setNeedsLayout];
}

#pragma mark - Actions

- (void)sendPressed:(UIButton *)sender
{
    [self finishSend];
//    [self.delegate didSendText:[self.inputToolbarView.textView.text js_stringByTrimingWhitespace]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_commentList == nil) ? 0 : m_commentList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: ID_CELL_COMMENT];
    cell.tag = indexPath.row + 10000;
    
    UIImageView* imgAvatar = (UIImageView*)[cell viewWithTag: 1];
    UIButton* btnName = (UIButton*)[cell viewWithTag: 2];
    UILabel* labelDuration = (UILabel*)[cell viewWithTag: 3];
    UIView* viewComment = [cell viewWithTag: 4];
    [CUtils clearView: viewComment];
//    UILabel* labelSplit = (UILabel*)[cell viewWithTag: 5];
    
    IGComment* clsComment = m_commentList[indexPath.row];
    IGItemUser* clsUser = clsComment.m_clsUser;
    
    [imgAvatar setImageWithURL: [NSURL URLWithString: clsUser.m_strImageThumbUrl]];
    [btnName setTitle: clsUser.m_strUsername forState: UIControlStateNormal];
    [btnName addTarget: self action: @selector(showUser:) forControlEvents: UIControlEventTouchUpInside];
    [labelDuration setText: clsComment.m_strTime];
    
    STTweetLabel *tweetLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 253, 160.0)];
    [tweetLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 14.0f]];
    [tweetLabel setText: clsComment.m_strComment];
    tweetLabel.textAlignment = NSTextAlignmentLeft;
    [viewComment addSubview:tweetLabel];
    
    CGSize size = [tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:tweetLabel.frame.size.width];
    CGRect frame = tweetLabel.frame;
    frame.size.height = size.height + 10;
    tweetLabel.frame = frame;
    
    CGRect rect = viewComment.frame;
    rect.size.height = frame.size.height;
    [viewComment setFrame: rect];
    
//    labelSplit.frame = CGRectMake(50, cell.frame.size.height - 1, 270, 1);
    
    UISwipeGestureRecognizer* leftGesture = [[UISwipeGestureRecognizer alloc] init];
    leftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftGesture addTarget: self action: @selector(showActionView:)];
    [cell addGestureRecognizer: leftGesture];
    
    UISwipeGestureRecognizer* rightGesture = [[UISwipeGestureRecognizer alloc] init];
    rightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [rightGesture addTarget: self action: @selector(swipeRight:)];
    [cell addGestureRecognizer: rightGesture];
    
    UIButton* btnReturn = (UIButton*)[cell viewWithTag: 300];
    [btnReturn addTarget: self action: @selector(actionReturn:) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton* btnDelete = (UIButton*)[cell viewWithTag: 301];
    [btnDelete addTarget: self action: @selector(actionDelete:) forControlEvents: UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - Action View Methods

- (void) showActionView: (UIGestureRecognizer*) gesture
{
    UITableViewCell* cell = (UITableViewCell*)gesture.view;
    if (![self isMe: cell]) {
        return;
    }
    UIView* viewMain = [cell viewWithTag: 100];
    
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = viewMain.frame;
        rect.origin.x = -48;
        viewMain.frame = rect;
    }];
}

- (void) swipeRight: (UIGestureRecognizer*) gesture
{
    UITableViewCell* cell = (UITableViewCell*)gesture.view;
    if (![self isMe: cell]) {
        return;
    }

    UIView* viewMain = [cell viewWithTag: 100];
    
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = viewMain.frame;
        rect.origin.x = 0;
        viewMain.frame = rect;
    }];
}

- (BOOL) isMe: (UITableViewCell*) cell
{
    int nRow = (int)cell.tag - 10000;
    IGComment* clsComment = m_commentList[nRow];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    if ([clsModel isMe: clsComment.m_clsUser.m_strUsername])
        return YES;
    return NO;
}

- (void) actionReturn: (UIButton*) btnReturn
{
    UITableViewCell* cell = (UITableViewCell*)btnReturn.superview.superview.superview.superview;
    UIView* viewMain = [cell viewWithTag: 100];
    
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = viewMain.frame;
        rect.origin.x = 0;
        viewMain.frame = rect;
    }];
}

- (void) actionDelete: (UIButton*) btnDelete
{
    UITableViewCell* cell = (UITableViewCell*)btnDelete.superview.superview.superview.superview;
    UIView* viewMain = [cell viewWithTag: 100];
    
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = viewMain.frame;
        rect.origin.x = 0;
        viewMain.frame = rect;
    }];
    
    int nRow = (int)(cell.tag - 10000);
    
    int nCommentId = ((IGComment*)m_commentList[nRow]).m_nIndex;
    
    [m_commentList removeObjectAtIndex: nRow];
    
    [self reloadForDelete];
    
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", nCommentId], @"id", nil];
    [NSThread detachNewThreadSelector: @selector(deleteComment:) toTarget: self withObject: dict];
}

- (void) reloadForDelete
{
    [self.tableView reloadData];
}

- (void) deleteComment: (NSDictionary*) dict
{
    int nIndex = [[dict objectForKey: @"id"] intValue];
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FRes = [clsModel deleteComment: nIndex];
    if (!FRes) {
        //error case
    }
    
    for (IGComment* clsComment in _m_clsItem.m_arrayComments) {
        if (clsComment.m_nIndex == nIndex) {
            [_m_clsItem.m_arrayComments removeObject: clsComment];
            break;
        }
    }
    
    _m_clsItem.m_nCommentCount --;
    
    NSDictionary* dictNoti = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", _m_nItemIndex], NOTI_ITEM_ID, @"1", @"delete", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_POSTED_COMMENT object: dictNoti];
}

/////////////////////////////////////////
- (void) showUser: (UIButton*) button
{
    int nRow = (int)button.superview.superview.superview.superview.tag - 10000;
    IGComment* clsComment = m_commentList[nRow];
    IGItemUser* clsUser = clsComment.m_clsUser;
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_clsSimpleUser = clsUser;
    vc.m_clsUser = nil;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGComment* clsComment = m_commentList[indexPath.row];
    STTweetLabel *tweetLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 253.0, 160.0)];
    [tweetLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 14.0f]];
    [tweetLabel setText: clsComment.m_strComment];
    tweetLabel.textAlignment = NSTextAlignmentLeft;
    
    CGSize size = [tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:tweetLabel.frame.size.width];
    
    return MAX(size.height, 25) + 30;
}

- (void)finishSend
{
//    [CUtils showActivity: HUD Caption: @""];
    
    NSString* strComment = [self.inputToolbarView.textView.text js_stringByTrimingWhitespace];
    [NSThread detachNewThreadSelector: @selector(actionPost:) toTarget: self withObject: strComment];
    
//    [self addComment: strComment];
    [self performSelectorOnMainThread: @selector(processAfterPost) withObject: nil waitUntilDone: YES];
}

- (void) addComment: (NSString*) strComment
{
    IGComment* comment = [[IGComment alloc] init];
    comment.m_strComment = [strComment copy];
    comment.m_clsUser = [[IGItemUser alloc] init];
    
    IGUser* clsUser = [[DataKeeper sharedInstance] getUserInfo];
    comment.m_clsUser.m_strProfileName = clsUser.m_strName;
    comment.m_clsUser.m_strImageThumbUrl = clsUser.m_strAvatarThumb;
    comment.m_clsUser.m_strImageUrl = clsUser.m_strAvatarThumb;
    comment.m_clsUser.m_strUsername = clsUser.m_strUsername;
    
    [m_commentList addObject: comment];
    
    [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
}

- (void) actionPost: (NSString*) strComment
{
    IGComment* clsComment = [[DataKeeper sharedInstance] postComment: _m_nItemIndex Text: strComment];
    if (clsComment) {
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", _m_nItemIndex], NOTI_ITEM_ID, nil];
        
//        IGComment* clsComment = [[IGComment alloc] init];
//        clsComment.m_strComment = [strComment copy];
//        clsComment.m_clsUser = [[IGItemUser alloc] init];
//        
//        IGUser* clsMe = [[DataKeeper sharedInstance] getUserInfo];
//        clsComment.m_clsUser.m_nUserId = clsMe.m_nUserId;
//        clsComment.m_clsUser.m_strImageThumbUrl = clsMe.m_strAvatar;
//        clsComment.m_clsUser.m_strImageUrl = clsMe.m_strAvatar;
//        clsComment.m_clsUser.m_strProfileName = clsMe.m_strName;
//        clsComment.m_clsUser.m_strUsername = clsMe.m_strUsername;
        
        [_m_clsItem.m_arrayComments addObject: clsComment];
        _m_clsItem.m_nCommentCount ++;
        [m_commentList addObject: clsComment];
        [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
        [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_POSTED_COMMENT object: dict];
//        [self loadData];
    }
    
    
//    [CUtils hideActivity: HUD];
}

- (void) processAfterPost
{
    [self.inputToolbarView.textView setText:nil];
    [self textViewDidChange:self.inputToolbarView.textView];
    [self.tableView reloadData];
    
    int nOffsetY = self.tableView.contentSize.height - self.tableView.frame.size.height;
    if (nOffsetY < 0) {
        nOffsetY = 0;
    }
    [self.tableView setContentOffset: CGPointMake(0, nOffsetY)];
}

- (void)setBackgroundColor:(UIColor *)color
{
    self.view.backgroundColor = color;
    _tableView.backgroundColor = color;
    _tableView.separatorColor = color;
}

- (void)scrollToBottomAnimated:(BOOL)animated
{
	if(![self shouldAllowScroll])
        return;
	
    NSInteger rows = [self.tableView numberOfRowsInSection:0];
    
    if(rows > 0) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionBottom
                                      animated:animated];
    }
}

- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath
			  atScrollPosition:(UITableViewScrollPosition)position
					  animated:(BOOL)animated
{
	if(![self shouldAllowScroll])
        return;
	
	[self.tableView scrollToRowAtIndexPath:indexPath
						  atScrollPosition:position
								  animated:animated];
}

- (BOOL)shouldAllowScroll
{
    if(self.isUserScrolling) {
//        if([self.delegate respondsToSelector:@selector(shouldPreventScrollToBottomWhileUserScrolling)]
//           && [self.delegate shouldPreventScrollToBottomWhileUserScrolling]) {
//            return NO;
//        }
    }
    
    return YES;
}

#pragma mark - Scroll view delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	self.isUserScrolling = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.isUserScrolling = NO;
}

#pragma mark - Text view delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
	
    if(!self.previousTextViewContentHeight)
		self.previousTextViewContentHeight = textView.contentSize.height;
    
    [self scrollToBottomAnimated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView
{
    //    CGFloat maxHeight = [JSMessageInputView maxHeight];
    //
    //    // BEGIN HACK
    //    //
    //        CGSize size = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, maxHeight)];
    //        CGFloat textViewContentHeight = size.height;
    //    //
    //    //  END HACK
    //    //
    //
    //    NSLog(@"prev height : %f", self.previousTextViewContentHeight);
    //
    //    BOOL isShrinking = textViewContentHeight < self.previousTextViewContentHeight;
    //    CGFloat changeInHeight = textViewContentHeight - self.previousTextViewContentHeight;
    //
    //    if(!isShrinking && self.previousTextViewContentHeight == maxHeight) {
    //        changeInHeight = 0;
    //    }
    //    else {
    //        changeInHeight = MIN(changeInHeight, maxHeight - self.previousTextViewContentHeight);
    //    }
    //
    //    if(changeInHeight != 0.0f) {
    //        if(!isShrinking)
    //            [self.inputToolbarView adjustTextViewHeightBy:changeInHeight];
    //
    //        [UIView animateWithDuration:0.25f
    //                         animations:^{
    //                             UIEdgeInsets insets = UIEdgeInsetsMake(0.0f,
    //                                                                    0.0f,
    //                                                                    self.tableView.contentInset.bottom + changeInHeight,
    //                                                                    0.0f);
    //
    //                             self.tableView.contentInset = insets;
    //                             self.tableView.scrollIndicatorInsets = insets;
    //                             [self scrollToBottomAnimated:NO];
    //
    //                             CGRect inputViewFrame = self.inputToolbarView.frame;
    //                             self.inputToolbarView.frame = CGRectMake(0.0f,
    //                                                                      inputViewFrame.origin.y - changeInHeight,
    //                                                                      inputViewFrame.size.width,
    //                                                                      inputViewFrame.size.height + changeInHeight);
    //                         }
    //                         completion:^(BOOL finished) {
    //                             if(isShrinking)
    //                                 [self.inputToolbarView adjustTextViewHeightBy:changeInHeight];
    //                         }];
    //
    //        self.previousTextViewContentHeight = MIN(textViewContentHeight, maxHeight);
    //    }
    
    self.inputToolbarView.sendButton.enabled = ([textView.text js_stringByTrimingWhitespace].length > 0);
}

#pragma mark - Keyboard notifications

- (void)handleWillShowKeyboardNotification:(NSNotification *)notification
{
    [self keyboardWillShowHide:notification];
}

- (void)handleWillHideKeyboardNotification:(NSNotification *)notification
{
    [self keyboardWillShowHide:notification];
}

- (void)keyboardWillShowHide:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0.0f
                        options:[UIView js_animationOptionsForCurve:curve]
                     animations:^{
                         CGFloat keyboardY = [self.view convertRect:keyboardRect fromView:nil].origin.y;
                         
                         CGRect inputViewFrame = self.inputToolbarView.frame;
                         CGFloat inputViewFrameY = keyboardY - inputViewFrame.size.height;
                         
                         // for ipad modal form presentations
                         CGFloat messageViewFrameBottom = self.view.frame.size.height - INPUT_HEIGHT;
                         if(inputViewFrameY > messageViewFrameBottom)
                             inputViewFrameY = messageViewFrameBottom;
						 
                         self.inputToolbarView.frame = CGRectMake(inputViewFrame.origin.x,
																  inputViewFrameY,
																  inputViewFrame.size.width,
																  inputViewFrame.size.height);
                         
                         UIEdgeInsets insets = UIEdgeInsetsMake(0.0f,
                                                                0.0f,
                                                                self.view.frame.size.height - self.inputToolbarView.frame.origin.y - INPUT_HEIGHT,
                                                                0.0f);
                         
                         self.tableView.contentInset = insets;
                         self.tableView.scrollIndicatorInsets = insets;
                     }
                     completion:^(BOOL finished) {
                     }];
}

#pragma mark - Dismissive text view delegate

- (void)keyboardDidScrollToPoint:(CGPoint)point
{
    CGRect inputViewFrame = self.inputToolbarView.frame;
    CGPoint keyboardOrigin = [self.view convertPoint:point fromView:nil];
    inputViewFrame.origin.y = keyboardOrigin.y - inputViewFrame.size.height;
    self.inputToolbarView.frame = inputViewFrame;
}

- (void)keyboardWillBeDismissed
{
    CGRect inputViewFrame = self.inputToolbarView.frame;
    inputViewFrame.origin.y = self.view.bounds.size.height - inputViewFrame.size.height;
    self.inputToolbarView.frame = inputViewFrame;
}

- (void)keyboardWillSnapBackToPoint:(CGPoint)point
{
    CGRect inputViewFrame = self.inputToolbarView.frame;
    CGPoint keyboardOrigin = [self.view convertPoint:point fromView:nil];
    inputViewFrame.origin.y = keyboardOrigin.y - inputViewFrame.size.height;
    self.inputToolbarView.frame = inputViewFrame;
}

- (IBAction) goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated: YES];
}

@end
