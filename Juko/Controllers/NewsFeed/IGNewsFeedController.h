//
//  IGNewsFeedController.h
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface IGNewsFeedController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate>

//member variables

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

- (IBAction)actionTopPosition:(id)sender;

@end
