//
//  IGSplashViewController.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGSplashViewController.h"
#import "IGProgressActivity.h"
#import "const.h"
#import "DataKeeper.h"
#import "CUtils.h"

#import "IGLoginViewController.h"
#import "IGAppDelegate.h"

@interface IGSplashViewController ()

@end

@implementation IGSplashViewController
{
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;

    if ([UIScreen mainScreen].bounds.size.height > 500) {
        [_m_ImgBG setImage: [UIImage imageNamed: @"bg_iphone5.png"]];
    } else {
        [_m_ImgBG setImage: [UIImage imageNamed: @"bg_iphone.png"]];
    }
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    if ([self alreadyLoggedIn]) {
        [self loginProcess];
    } else {
        [self showLoginView];
    }
}

//evaluate that the app already has ever logged in

- (BOOL) alreadyLoggedIn
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    id obj = [defaults objectForKey: LOCAL_KEY_LOGIN];
    if (obj) {
        if ([obj isEqualToString: @"no"]) {
            return NO;
        }
    } else
        return NO;
    
    return YES;
//    return NO;
}

#pragma mark - Login Process

- (void) loginProcess
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strLoginKind = [defaults objectForKey: LOCAL_KEY_LOGIN];
    if ([strLoginKind isEqualToString: @"fb"]) {
        NSString* strToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
        if (strToken == nil || [strToken isEqualToString: LOCAL_VALUE_NO_TOKEN]) {
            [self showLoginView];
        } else {
            [CUtils showActivity: HUD Caption: @"Logging in..."];
            
            [NSThread detachNewThreadSelector: @selector(loginByFB) toTarget: self withObject: nil];
        }
    } else if ([strLoginKind isEqualToString: @"default"]) {
        [CUtils showActivity: HUD Caption: @"Logging in..."];
        [NSThread detachNewThreadSelector: @selector(loginByDefault) toTarget: self withObject: nil];
    }
}

- (void) loginByFB
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    NSString* strUser = [defaults objectForKey: LOCAL_KEY_FB_USER_ID];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FLogin = [clsModel loginByFB: strToken USERID: strUser];
    [CUtils hideActivity: HUD];
    
    if (FLogin) {
        [self performSelectorOnMainThread: @selector(showMainFrame) withObject: nil waitUntilDone: YES];
    } else {
        [self performSelectorOnMainThread: @selector(showLoginView) withObject: nil waitUntilDone: YES];
    }
}

- (void) loginByDefault
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strUsername = [defaults objectForKey: LOCAL_KEY_USERNAME];
    NSString* strPassword = [defaults objectForKey: LOCAL_KEY_PASSWORD];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];

    if ([clsModel defaultLogin: strUsername PASSWORD: strPassword]) {
        [self performSelectorOnMainThread:@selector(showMainFrame) withObject:nil waitUntilDone: YES];
    } else {
        [self performSelectorOnMainThread:@selector(showLoginView) withObject:nil waitUntilDone: YES];
    }
    
    [CUtils hideActivity: HUD];
}

#pragma mark - Show Login View/Show MainFrame

- (void) showLoginView
{
    IGLoginViewController* loginVC = [[IGLoginViewController alloc] initWithNibName: @"IGLoginViewController" bundle: nil];
    [self.navigationController pushViewController: loginVC animated: YES];
}

- (void) showMainFrame
{
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.m_clsMainFrame == nil) {
        [appDelegate createMainFrame];
    }
    [self presentViewController: appDelegate.m_clsNavController animated: YES completion: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
