//
//  IGLoginViewController.h
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GPUImageView.h>

@interface IGLoginViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

//members
@property (weak, nonatomic) IBOutlet UIView *m_CameraView;
@property (weak, nonatomic) IBOutlet UIImageView *m_ImageAvatar;

@property (weak, nonatomic) IBOutlet UIView *m_SigninView;

@property (weak, nonatomic) IBOutlet UITextField *m_TextUser;
@property (weak, nonatomic) IBOutlet UITextField *m_TextPassword;
@property (weak, nonatomic) IBOutlet UIButton *m_btnDismiss;
@property (weak, nonatomic) IBOutlet UIImageView *m_ImgBg;

@property (weak, nonatomic) IBOutlet GPUImageView *m_bgView;
@property (weak, nonatomic) IBOutlet UIScrollView *m_SignupView;
@property (weak, nonatomic) IBOutlet UIView *m_LoginView;

@property (weak, nonatomic) IBOutlet UIButton *m_btnDismissForSignup;
@property (weak, nonatomic) IBOutlet UIButton *m_btnFbLogin;
@property (weak, nonatomic) IBOutlet UIButton *m_btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *m_btnSignUp;

- (IBAction)actionFBLogin:(id)sender;
- (IBAction)actionForgotPassword:(id)sender;
- (IBAction)actionSignin:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)actionSignUp:(id)sender;

- (IBAction)goLoginView:(id)sender;
- (IBAction)signup:(id)sender;
- (IBAction)dismiss:(id)sender;

- (IBAction)choosePhoto:(id)sender;
- (IBAction)goBackFromSigninView:(id)sender;

@end
