//
//  IGSignupViewController.m
//  Juko
//
//  Created by Mountain on 2/4/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGSignupViewController.h"
#import "DataKeeper.h"
#import "const.h"
#import "IGAppDelegate.h"
#import "IGProgressActivity.h"
#import "CUtils.h"


@interface IGSignupViewController ()

@end

@implementation IGSignupViewController
{
    IGProgressActivity* HUD;
    GPUImageStillCamera *stillCamera;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    self.m_btnDismiss.hidden = YES;
    
    stillCamera = [[GPUImageStillCamera alloc] init];
    
    stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    GPUImageGaussianBlurFilter *blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = 5;
    blurFilter.blurPasses = 3;
    
    [stillCamera addTarget: blurFilter];
    GPUImageView *filterView = (GPUImageView *)self.m_CameraView;
    [blurFilter addTarget:filterView];
    
    filterView.fillMode = kGPUImageFillModeStretch;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];

    dispatch_async(dispatch_get_main_queue(), ^{
        @autoreleasepool {
            [stillCamera startCameraCapture];
        }
    });
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    
    [stillCamera stopCameraCapture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextViewDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    int nTag = textField.tag;
    if (nTag == 5) {
        [textField resignFirstResponder];
    } else {
        UITextField* textNext = (UITextField*)[self.view viewWithTag: nTag + 1];
        [textNext becomeFirstResponder];
    }
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect rect = self.view.frame;
    
    CGRect rectText = textField.frame;
    if (rectText.origin.y + rectText.size.height < rect.size.height - KEYBOARD_HEIGHT) {
    } else {
        rect.origin.y = -(rectText.origin.y + rectText.size.height - (rect.size.height - KEYBOARD_HEIGHT) + 5);
    }
    
    [UIView animateWithDuration: 0.2 animations: ^(void) {
        [self.view setFrame: rect];
    }];
}

- (IBAction)dismissKeyboard:(id)sender {
    self.m_btnDismiss.hidden = YES;
    
    for (int nIdx = 1; nIdx < 6; nIdx ++) {
        UITextField* textField = (UITextField*)[self.view viewWithTag: nIdx];
        [textField resignFirstResponder];
    }
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)actionSignup:(id)sender {
    NSMutableString* strUserInfo = [[NSMutableString alloc] init];
    
    NSArray* arrayKeys = [NSArray arrayWithObjects: RESPONSE_USER_NAME, RESPONSE_USER_USERNAME, RESPONSE_USER_EMAIL, REQUEST_PASSWORD, RESPONSE_USER_PHONE, RESPONSE_USER_STATE, RESPONSE_USER_ZIP, nil];
    if ([self isRequiredParam]) {
        for (int nIdx = 1; nIdx <= 5; nIdx ++) {
            UITextField* textField = (UITextField*)[self.view viewWithTag: nIdx];
            if (textField.text && ![textField.text isEqualToString: @""]) {
                if ([strUserInfo isEqualToString: @""]) {
                    [strUserInfo appendString: [NSString stringWithFormat: @"%@=%@", arrayKeys[nIdx - 1], textField.text]];
                } else {
                    [strUserInfo appendString: [NSString stringWithFormat: @"&%@=%@", arrayKeys[nIdx - 1], textField.text]];
                }
            }
        }
        
        [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
        if ([[DataKeeper sharedInstance] signupProcess: strUserInfo]) {
            [self performSelectorOnMainThread:@selector(showMainFrame) withObject:nil waitUntilDone: YES];
        }
        
        [CUtils hideActivity: HUD];
    }
}

- (void) showActivity
{
    [CUtils showActivity: HUD Caption: @"Signup..."];
}

- (void) showMainFrame
{
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.m_clsMainFrame == nil) {
        [appDelegate createMainFrame];
    }
    [self presentViewController: appDelegate.m_clsNavController animated: YES completion: nil];
}

- (BOOL) isRequiredParam
{
    for (int nIdx = 1; nIdx < 8; nIdx ++) {
        UITextField* textField = (UITextField*)[self.view viewWithTag: nIdx];
        if (textField.text == nil || [textField.text isEqualToString: @""]) {
            if (nIdx < 5) {
                ALERT_SHOW(@"You must input name, username, email, password");
                return NO;
            }
        }
    }
    return YES;
}

#pragma mark - Keyboard Show/Hide methods

- (void) keyboardWillBeShown: (NSNotification*) noti
{
    self.m_btnDismiss.hidden = NO;
}

- (void) keyboardWillBeHidden: (NSNotification*) noti
{
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    
    [UIView animateWithDuration: 0.1 animations: ^(void) {
        [self.view setFrame: rect];
    }];
    self.m_btnDismiss.hidden = YES;
}

@end
