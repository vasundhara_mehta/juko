//
//  IGSignupViewController.h
//  Juko
//
//  Created by Mountain on 2/4/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GPUImage.h>


@interface IGSignupViewController : UIViewController

@property (weak, nonatomic) IBOutlet GPUImageView *m_CameraView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnDismiss;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)goBack:(id)sender;
- (IBAction)actionSignup:(id)sender;

@end
