//
//  IGLoginViewController.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGLoginViewController.h"
#import "IGAppDelegate.h"
#import "const.h"
#import "DataKeeper.h"
#import "CUtils.h"
#import "IGProgressActivity.h"
#import "IGSignupViewController.h"
#import "IGEditProfilePictureController.h"

#import "Reachability.h"

#import <GPUImage.h>
#import "UIImageView+WebCache.h"

#ifdef TEST_MODE
#define FB_ACCESS_TOKEN @"CAACEdEose0cBAPzMrrXdKoZCDHWDkz0hEjJQEZCpPUvp7svU9eBqiJMGUsDoJ8XEHZC1rfLgXoWIGxaWT79JvTyEJcFmkJZBZBgwfyBQkwutGbpnnvK3iY8XsGjZBqFPlxkf63xsgSjKCcXRsS8ZB6jGfCz9s57Y1aXTdX71ch67PSg4GmM9SxgsUyZARm5op0mXlLNHi62IBwZDZD"
#define FB_USER_ID @"1054105928"
#endif

@interface IGLoginViewController ()

@end

@implementation IGLoginViewController
{
    IGProgressActivity* HUD;
    GPUImageStillCamera *stillCamera;
    GPUImageGaussianBlurFilter *blurFilter;
    BOOL m_FSignupMode;
    BOOL m_FChangePhoto;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;

    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadInit:)
                                                 name:@"noti_loadinit" object:nil];
    
    [NSThread detachNewThreadSelector: @selector(loadModel) toTarget: self withObject: nil];
//    self.m_btnDismiss.hidden = YES;
    
    [self createCamera];
    m_FSignupMode = NO;
    
//    [CUtils doCircleView: _m_CameraView];
//    [CUtils doCircleView: _m_ImageAvatar];
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[[UIImage imageNamed: @"large_avatar_mask2.png"] CGImage];
    mask.frame = CGRectMake(0, 0, _m_ImageAvatar.bounds.size.width, _m_ImageAvatar.bounds.size.height);
    _m_ImageAvatar.layer.mask = mask;
    _m_ImageAvatar.layer.masksToBounds = YES;
}

- (void) loadModel
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    [clsModel loadInit];
}

- (void) loadInit: (NSNotification*) noti
{
    [self performSelectorOnMainThread: @selector(createCamera) withObject: nil waitUntilDone: YES];
}

- (void) createCamera
{
    @autoreleasepool {
        stillCamera = [[GPUImageStillCamera alloc] init];
        
        stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        
        blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
        blurFilter.blurRadiusInPixels = 5;
        blurFilter.blurPasses = 3;
        
        [stillCamera addTarget: blurFilter];
        GPUImageView *filterView = (GPUImageView *)self.m_bgView;
        [blurFilter addTarget:filterView];
        
        filterView.fillMode = kGPUImageFillModeStretch;
        
        [stillCamera startCameraCapture];
    }
}

- (void) removeCamera
{
    @autoreleasepool {
        [stillCamera stopCameraCapture];
        [blurFilter removeAllTargets];
        [stillCamera removeAllTargets];
        blurFilter = nil;
        stillCamera = nil;
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    self.m_CameraView.hidden = NO;

    if (stillCamera == nil) {
        [self createCamera];
    }
    if (m_FChangePhoto) {
        m_FChangePhoto = NO;
        return;
    }
    
    [self initView];
    
    if ([self alreadyLoggedIn]) {
        [self loginWithSavedInfo];
    } else {
        [self showLoginView];
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeCamera];
    });
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
//    [self performSelectorOnMainThread: @selector(createCamera) withObject: nil waitUntilDone: YES];
}

- (void) initView
{
    CGRect rect = _m_SigninView.frame;
    rect.origin.x = 320;
    _m_SigninView.frame = rect;
    
    rect = _m_SignupView.frame;
    rect.origin.x = 320;
    _m_SignupView.frame = rect;
    
    rect = _m_ImgBg.frame;
    rect.origin.x = 60;
    _m_ImgBg.frame = rect;
    
    if (![self normalInternet]) {
        _m_btnFbLogin.enabled = NO;
        _m_btnLogin.enabled = NO;
        _m_btnSignUp.enabled = NO;
        
        ALERT_SHOW(@"There is no internet connection")
    } else {
        _m_btnFbLogin.enabled = YES;
        _m_btnLogin.enabled = YES;
        _m_btnSignUp.enabled = YES;
    }
}

- (BOOL) normalInternet
{
    Reachability* network = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [network currentReachabilityStatus];
    if (status == NotReachable) {
        return NO;
    }
    return YES;
}

- (void) startCamera
{
//    dispatch_async(dispatch_get_main_queue(), ^{
        @autoreleasepool {
            [stillCamera startCameraCapture];
        }
//    });
}

- (void) showLoginView
{
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_LoginView.frame;
        rect.origin.y = self.view.frame.size.height - rect.size.height;
        rect.origin.x = 0;
        self.m_LoginView.frame = rect;
    }];
}

//evaluate that the app already has ever logged in

- (BOOL) alreadyLoggedIn
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    id obj = [defaults objectForKey: LOCAL_KEY_LOGIN];
    if (obj) {
        if ([obj isEqualToString: @"no"]) {
            return NO;
        }
    } else
        return NO;
    
    return YES;
    //    return NO;
}

#pragma mark - Login Process

- (void) loginWithSavedInfo
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strLoginKind = [defaults objectForKey: LOCAL_KEY_LOGIN];
    if ([strLoginKind isEqualToString: @"fb"]) {
        NSString* strToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
        if (strToken == nil || [strToken isEqualToString: LOCAL_VALUE_NO_TOKEN]) {
            [self showLoginView];
        } else {
            [CUtils showActivity: HUD Caption: @"Logging in..."];
            
            [NSThread detachNewThreadSelector: @selector(loginByFB) toTarget: self withObject: nil];
        }
    } else if ([strLoginKind isEqualToString: @"default"]) {
        [CUtils showActivity: HUD Caption: @"Logging in..."];
        [NSThread detachNewThreadSelector: @selector(loginByDefault) toTarget: self withObject: nil];
    }
}

- (void) loginByFB
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    NSString* strUser = [defaults objectForKey: LOCAL_KEY_FB_USER_ID];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FLogin = [clsModel loginByFB: strToken USERID: strUser];
    [HUD hide: YES];
    
    if (FLogin) {
        [self performSelectorOnMainThread: @selector(showMainFrame) withObject: nil waitUntilDone: YES];
    } else {
        [self performSelectorOnMainThread: @selector(showLoginView) withObject: nil waitUntilDone: YES];
    }
}

- (void) loginByDefault
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strUsername = [defaults objectForKey: LOCAL_KEY_USERNAME];
    NSString* strPassword = [defaults objectForKey: LOCAL_KEY_PASSWORD];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FLogin = [clsModel defaultLogin: strUsername PASSWORD: strPassword];
    
    [HUD hide: YES];
    
    if (FLogin) {
        [self performSelectorOnMainThread:@selector(showMainFrame) withObject:nil waitUntilDone: YES];
    } else {
        [self performSelectorOnMainThread:@selector(showLoginView) withObject:nil waitUntilDone: YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionFBLogin:(id)sender {
//#ifndef TEST_MODE
    self.m_btnFbLogin.userInteractionEnabled = NO;
    [self performSelector: @selector(enableButtons) withObject: Nil afterDelay: 5];
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.session != nil) {
        [appDelegate.session closeAndClearTokenInformation];
    }
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_stream", @"publish_actions", @"read_stream", @"read_friendlists", @"email", @"user_photos", nil];
    appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
    
    [FBAppCall handleDidBecomeActiveWithSession:appDelegate.session];
    
    [FBSession setActiveSession: appDelegate.session];
    if ([appDelegate.session isOpen]) {
        [self getFBUserInfoAndSignin];
    } else {
        [appDelegate.session openWithBehavior:FBSessionLoginBehaviorWithNoFallbackToWebView completionHandler:^(FBSession* session, FBSessionState status, NSError* error) {
            if ([session isOpen]) {
                appDelegate.session = session;
                [FBSession setActiveSession: appDelegate.session];
                [self getFBUserInfoAndSignin];
            } else {
                NSLog(@"Failed to login on Facebook");
                self.m_btnFbLogin.userInteractionEnabled = YES;
            }
        }];
    }
//#else
//    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: FB_ACCESS_TOKEN, LOCAL_KEY_FB_ACCESS_TOKEN, FB_USER_ID, LOCAL_KEY_FB_USER_ID, nil];
//    [self loginProcess: dict];
////    [self showMainFrame];
//#endif
}

- (void) enableButtons
{
    self.m_btnFbLogin.userInteractionEnabled = YES;
}

- (void) getFBUserInfoAndSignin
{
    FBRequestConnection* connection = [[FBRequestConnection alloc] init];
    connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession | FBRequestConnectionErrorBehaviorAlertUser | FBRequestConnectionErrorBehaviorRetry;
    FBRequest* request;
    request = [FBRequest requestWithGraphPath: @"me?field=id" parameters: nil HTTPMethod: @"GET"];
    request.session = [FBSession activeSession];
    
    [self showActivity];

    [connection addRequest: request completionHandler:^(FBRequestConnection* connection, id result, NSError* error) {
        if (!error) {
            FBGraphObject* obj = (FBGraphObject*)result;
            
            IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject: appDelegate.session.accessTokenData.accessToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
            [defaults setObject: [obj objectForKey: @"id"] forKey: LOCAL_KEY_FB_USER_ID];
            [defaults synchronize];

            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: appDelegate.session.accessTokenData.accessToken, LOCAL_KEY_FB_ACCESS_TOKEN, [obj objectForKey: @"id"], LOCAL_KEY_FB_USER_ID, nil];
            
            [self loginProcess: dict];
            self.m_btnFbLogin.userInteractionEnabled = YES;
        } else {
            self.m_btnFbLogin.userInteractionEnabled = YES;
            ALERT_SHOW(@"Please check your network status");
        }
        
        [HUD hide: YES];
    }];
    [connection start];
}

- (IBAction)actionForgotPassword:(id)sender {
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle: @"Enter your mail address"];
    
    [dialog setMessage:@" "];
    [dialog addButtonWithTitle:@"Cancel"];
    [dialog addButtonWithTitle:@"Ok"];
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    [dialog textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [dialog setTransform: moveUp];
    [dialog show];
}

#pragma mark - alertview delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSString* strMail = [[alertView textFieldAtIndex: 0].text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        if (strMail == nil || [strMail isEqualToString: @""])
            return;
        
        DataKeeper* clsModel = [DataKeeper sharedInstance];
        if ([clsModel forgotPassword: strMail]) {
            ALERT_SHOW(@"Please check your mail inbox");
        } else {
            ALERT_SHOW(@"No user exist");
        }
    }
}


- (IBAction)actionSignin:(id)sender {
    m_FSignupMode = NO;
    [self showSigninView];
}

- (void) showSigninView
{
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = self.m_LoginView.frame;
        rect.origin.x = -320;
        self.m_LoginView.frame = rect;
        
        rect = self.m_ImgBg.frame;
        rect.origin.x -= 320;
        self.m_ImgBg.frame = rect;

        rect = self.m_SigninView.frame;
        rect.origin.x = 0;
        self.m_SigninView.frame = rect;
    } completion: ^(BOOL finish) {
        [self.m_TextUser becomeFirstResponder];
    }];
}

- (void) signin
{
    if (![self isPossibleToSignin]) {
        return;
    }
    
    self.m_btnLogin.userInteractionEnabled = NO;
    
    //[NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: LOCAL_VALUE_NO_TOKEN forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    [defaults setObject: LOCAL_VALUE_NO_USER forKey: LOCAL_KEY_FB_USER_ID];
    [defaults synchronize];
    
    BOOL FRes = [[DataKeeper sharedInstance] defaultLogin: self.m_TextUser.text PASSWORD: self.m_TextPassword.text];
    [HUD hide: YES];

    if (FRes) {
        [defaults setObject: self.m_TextUser.text forKey: LOCAL_KEY_USERNAME];
        [defaults setObject: self.m_TextPassword.text forKey: LOCAL_KEY_PASSWORD];
        [defaults setObject: @"default" forKey: LOCAL_KEY_LOGIN];
        [defaults synchronize];
        
        [self performSelectorOnMainThread:@selector(showMainFrame) withObject:nil waitUntilDone: YES];
    } else {
        [defaults setObject: @"" forKey: LOCAL_KEY_USERNAME];
        [defaults setObject: @"" forKey: LOCAL_KEY_PASSWORD];
        [defaults setObject: @"no" forKey: LOCAL_KEY_LOGIN];
        [defaults synchronize];
        
        ALERT_SHOW(@"Failed to log in.\nPlease confirm the information for logging in");
    }
    
    self.m_btnLogin.userInteractionEnabled = YES;
}

- (BOOL) isPossibleToSignin
{
    if (self.m_TextUser.text == nil || [[self.m_TextUser.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString: @""]) {
        ALERT_SHOW(@"You must input the username/email");
        return NO;
    }
    if (self.m_TextPassword.text == nil || [self.m_TextPassword.text isEqualToString: @""]) {
        ALERT_SHOW(@"You must input the password");
        return NO;
    }
    return YES;
}

- (IBAction)dismissKeyboard:(id)sender {
//    self.m_btnDismiss.hidden = YES;
    [self.m_TextUser resignFirstResponder];
    [self.m_TextPassword resignFirstResponder];
}

- (IBAction)actionSignUp:(id)sender {
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = self.m_LoginView.frame;
        rect.origin.x = -320;
        self.m_LoginView.frame = rect;
        
        rect = self.m_ImgBg.frame;
        rect.origin.x -= 320;
        self.m_ImgBg.frame = rect;
        
        rect = self.m_SignupView.frame;
        rect.origin.x = 0;
        self.m_SignupView.frame = rect;
    }];
    
    m_FSignupMode = YES;
    
    for (int nIdx = 1; nIdx <= 5; nIdx ++) {
        UITextField* textField = (UITextField*)[self.m_SignupView viewWithTag: nIdx];
        textField.text = @"";
    }
}

- (IBAction)goLoginView:(id)sender {
    [self dismiss: nil];
    [self dismissKeyboard: nil];
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = self.m_LoginView.frame;
        rect.origin.x = 0;
        self.m_LoginView.frame = rect;
        
        rect = self.m_ImgBg.frame;
        rect.origin.x += 320;
        self.m_ImgBg.frame = rect;
        
        rect = self.m_SignupView.frame;
        rect.origin.x = 320;
        self.m_SignupView.frame = rect;
    }];
    
    m_FSignupMode = NO;
}

- (IBAction) signup:(id)sender {
    NSMutableString* strUserInfo = [[NSMutableString alloc] init];
    
    NSArray* arrayKeys = [NSArray arrayWithObjects: RESPONSE_USER_NAME, RESPONSE_USER_USERNAME, RESPONSE_USER_EMAIL, REQUEST_PASSWORD, RESPONSE_USER_PHONE, RESPONSE_USER_STATE, RESPONSE_USER_ZIP, nil];
    if ([self isRequiredParam]) {
        UIImage* image = _m_ImageAvatar.image;
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        for (int nIdx = 1; nIdx <= 5; nIdx ++) {
            UITextField* textField = (UITextField*)[self.m_SignupView viewWithTag: nIdx];
            if (textField.text && ![textField.text isEqualToString: @""]) {
                if (!image) {
                    if ([strUserInfo isEqualToString: @""]) {
                        [strUserInfo appendString: [NSString stringWithFormat: @"%@=%@", arrayKeys[nIdx - 1], textField.text]];
                    } else {
                        [strUserInfo appendString: [NSString stringWithFormat: @"&%@=%@", arrayKeys[nIdx - 1], textField.text]];
                    }
                } else {
                    [dict setObject: textField.text forKey: arrayKeys[nIdx - 1]];
                }
            }
        }
        
        [NSThread detachNewThreadSelector: @selector(showActivityForSignup) toTarget: self withObject: nil];

        BOOL FRes = NO;
        if (image) {
            FRes = [[DataKeeper sharedInstance] signupProcess: dict Image: image];
        } else {
            FRes = [[DataKeeper sharedInstance] signupProcess: strUserInfo];
        }
        [CUtils hideActivity: HUD];

        if (FRes) {
            ALERT_SHOW(@"Success to sign up");
            [self goLoginView: nil];
            return;
        } else {
//            NSString* strErrMsg = [[DataKeeper sharedInstance] getErrMessage];
//            if (strErrMsg == nil || [strErrMsg isEqualToString: @""]) {
//                [CUtils hideActivity: HUD];
//                ALERT_SHOW(@"You must verify the registration in your mail");
//                [self goLoginView: nil];
//                return;
//            }
//            ALERT_SHOW(strErrMsg);
            ALERT_SHOW(@"Fail to signup\nYour username or email may be already exist");
        }
        
    }
}

- (IBAction)dismiss:(id)sender {
    self.m_btnDismissForSignup.hidden = YES;
    
    for (int nIdx = 1; nIdx < 5; nIdx ++) {
        UITextField* textField = (UITextField*)[self.m_SignupView viewWithTag: nIdx];
        [textField resignFirstResponder];
    }
}

#pragma mark - Choose Photo
- (IBAction)choosePhoto:(id)sender
{
    IGEditProfilePictureController* vc = [[IGEditProfilePictureController alloc] initWithNibName: @"IGEditProfilePictureController" bundle: nil];
    vc.delegate = self;
    
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    overlay.dismissUponTouchMask = NO;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    [overlay presentOverlayOnView: appDelegate.window withContent: vc animated: YES];
}

- (void) chooseButton: (NSDictionary*) dict
{
    m_FChangePhoto = YES;
    
    int nButtonIndex = [[dict objectForKey: @"index"] intValue];
    
    switch (nButtonIndex) {
        case 1: //remove photo
        {
            [CUtils showActivity: HUD Caption: @""];
            [NSThread detachNewThreadSelector: @selector(removePicture) toTarget:self withObject: nil];
            break;
        }
        case 2: // Import from Facebook
        {
            [HUD show: YES];
            [NSThread detachNewThreadSelector: @selector(getFbPicture) toTarget: self withObject: nil];
            break;
        }
        case 3: // Import from Twitter
            break;
        case 4: // Take Photo
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.allowsEditing = NO;
            picker.delegate = self;
            [self presentViewController: picker animated: YES completion: nil];
            break;
        }
        case 5: // Choose from Library
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.allowsEditing = NO;
            picker.delegate = self;
            [self presentViewController: picker animated: YES completion: nil];
            
            break;
        }
        default:
            break;
    }
}

- (void) removePicture
{
    [self.m_ImageAvatar setImage: [UIImage imageNamed: @"large_avatar_mask2.png"]];
    
    [CUtils hideActivity: HUD];
}

- (void) getFbPicture
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    id fbToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    if (fbToken == nil || [fbToken isEqualToString: LOCAL_VALUE_NO_TOKEN]) {
        [self performSelectorOnMainThread: @selector(fbLogin) withObject: nil waitUntilDone: YES];
    } else {
        id fbId = [defaults objectForKey: LOCAL_KEY_FB_USER_ID];
        if (fbId && ![fbId isEqualToString: LOCAL_VALUE_NO_USER]) {
            [self performSelectorOnMainThread: @selector(setFbPicture) withObject: nil waitUntilDone: YES];
        } else {
            [self performSelectorOnMainThread: @selector(fbLogin) withObject: nil waitUntilDone: YES];
        }
    }
}

- (void) fbLogin
{
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.session == nil) {
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_stream", @"publish_actions", @"read_stream", @"read_friendlists", @"email", @"user_photos", nil];
        appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
        
        [FBAppCall handleDidBecomeActiveWithSession:appDelegate.session];
    }
    [FBSession setActiveSession: appDelegate.session];
    
    [appDelegate.session openWithBehavior:FBSessionLoginBehaviorWithNoFallbackToWebView completionHandler:^(FBSession* session, FBSessionState status, NSError* error) {
        if ([session isOpen]) {
            [self getFBUserID];
        } else {
            NSLog(@"Failed to login on Facebook");
        }
        [HUD hide: YES];
    }];
}

- (void) getFbUserPicture: (NSNotification*) noti
{
    [self performSelectorOnMainThread: @selector(showFbImportPhotoView) withObject: nil waitUntilDone: YES];
}

- (void) getFBUserID
{
    FBRequestConnection* connection = [[FBRequestConnection alloc] init];
    connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession | FBRequestConnectionErrorBehaviorAlertUser | FBRequestConnectionErrorBehaviorRetry;
    FBRequest* request;
    request = [FBRequest requestWithGraphPath: @"me?field=id" parameters: nil HTTPMethod: @"GET"];
    
    [connection addRequest: request completionHandler:^(FBRequestConnection* connection, id result, NSError* error) {
        if (!error) {
            FBGraphObject* obj = (FBGraphObject*)result;
            
            IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject: appDelegate.session.accessTokenData.accessToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
            [defaults setObject: [obj objectForKey: @"id"] forKey: LOCAL_KEY_FB_USER_ID];
            [defaults synchronize];
            
            [self performSelectorOnMainThread: @selector(setFbPicture) withObject: nil waitUntilDone: YES];
        }
    }];
    [connection start];
}

- (void) setFbPicture
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strFbId = [defaults objectForKey: LOCAL_KEY_FB_USER_ID];
    NSURL* fbPictureUrl = [NSURL URLWithString: [NSString stringWithFormat: @"https://graph.facebook.com/%@/picture?width=200&height=200", strFbId]];
    
    [self.m_ImageAvatar setImageWithURL: fbPictureUrl];// completed: ^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
//        [self setAvatarImage: image];
//    }];
    
    [HUD hide: YES];
}


- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    CGSize sz = image.size;
    UIImage* fixImage = [CUtils fixImage: [CUtils imageWithImage: image scaledToSize: CGSizeMake(sz.width / 3, sz.height / 3)]];
    
    NSData *data = UIImageJPEGRepresentation(fixImage, 0.8);
    UIImage* img = [UIImage imageWithData:data];
    
    [self.m_ImageAvatar setImage: img];
    
//    [NSThread detachNewThreadSelector: @selector(setAvatarImage:) toTarget: self withObject: img];
    
    [picker dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark - back from signin view

- (IBAction)goBackFromSigninView:(id)sender {
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect rect = self.m_LoginView.frame;
        rect.origin.x = 0;
        self.m_LoginView.frame = rect;
        
        rect = self.m_ImgBg.frame;
        rect.origin.x += 320;
        self.m_ImgBg.frame = rect;

        rect = self.m_SigninView.frame;
        rect.origin.x = 320;
        self.m_SigninView.frame = rect;
    }];
    
    [self dismissKeyboard: nil];
}

- (void) showActivity
{
    [CUtils showActivity: HUD Caption: @"Logging in..."];
}

- (void) loginProcess: (NSDictionary*) dictFBInfo
{
    //    FBSession* session = ((AppDelegate*)[UIApplication sharedApplication].delegate).session;
//    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    
    NSString* strToken = [dictFBInfo objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    NSString* strUserId = [dictFBInfo objectForKey: LOCAL_KEY_FB_USER_ID];
    
    NSLog(@"access token : %@", strToken);
    BOOL FSuccess = [[DataKeeper sharedInstance] loginByFB: strToken USERID: strUserId];
    if (!FSuccess) {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject: LOCAL_VALUE_NO_TOKEN forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
        [defaults setObject: LOCAL_VALUE_NO_USER forKey: LOCAL_KEY_FB_USER_ID];
        [defaults setObject: @"no" forKey: LOCAL_KEY_LOGIN];
        [defaults synchronize];
        
//        [CUtils hideActivity: HUD];
        
        return;
    } else {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject: strToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
        [defaults setObject: strUserId forKey: LOCAL_KEY_FB_USER_ID];
        [defaults setObject: @"fb" forKey: LOCAL_KEY_LOGIN];
        [defaults synchronize];
    }
    
    [self performSelectorOnMainThread:@selector(showMainFrame) withObject:nil waitUntilDone: YES];
}

- (void) showMainFrame
{
    [HUD hide: YES];
    
    [self performSelectorOnMainThread: @selector(removeCamera) withObject: nil waitUntilDone: YES];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.m_clsMainFrame = nil;
    
    [appDelegate createMainFrame];
    
    [appDelegate.m_clsMainFrame setSelectedIndex: 0];
    [self.navigationController pushViewController: appDelegate.m_clsMainFrame animated: YES];
}

#pragma mark - UITextViewDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (!m_FSignupMode) {
        if (textField == self.m_TextUser) {
            [self.m_TextPassword becomeFirstResponder];
        } else {
            [textField resignFirstResponder];
            
            [self showActivity];
            [NSThread detachNewThreadSelector: @selector(signin) toTarget: self withObject: nil];
        }
    } else {
        int nTag = textField.tag;
        if (nTag == 4) {
            [textField resignFirstResponder];
        } else {
            UITextField* textNext = (UITextField*)[self.m_SignupView viewWithTag: nTag + 1];
            [textNext becomeFirstResponder];
        }
    }
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (!m_FSignupMode) {
//        CGRect rect = self.m_LoginView.frame;
//        
//        if (textField == self.m_TextUser) {
//            rect.origin.y = -50;
//        } else {
//            rect.origin.y = -100;
//        }
//        
//        [UIView animateWithDuration: 0.5 animations: ^(void) {
//            [self.m_LoginView setFrame: rect];
//        }];
    } else {
        CGRect rect = self.view.frame;
        
        CGRect rectText = textField.frame;
        rectText.origin.y += textField.superview.frame.origin.y;
        if (rectText.origin.y + rectText.size.height < rect.size.height - KEYBOARD_HEIGHT) {
        } else {
            rect.origin.y = -(rectText.origin.y + rectText.size.height - (rect.size.height - KEYBOARD_HEIGHT) + 5);
        }
        
        [UIView animateWithDuration: 0.2 animations: ^(void) {
            [self.m_SignupView setFrame: rect];
        }];
    }
}

#pragma mark - Keyboard Show/Hide methods

- (void) keyboardWillBeShown: (NSNotification*) noti
{
    if (!m_FSignupMode) {
//        self.m_btnDismiss.hidden = NO;
    } else {
        self.m_btnDismissForSignup.hidden = NO;
    }
}

- (void) keyboardWillBeHidden: (NSNotification*) noti
{
    if (!m_FSignupMode) {
//        CGRect rect = self.m_LoginView.frame;
//        rect.origin.y = 0;
//        
//        [UIView animateWithDuration: 0.5 animations: ^(void) {
//            [self.m_LoginView setFrame: rect];
//        }];
//        self.m_btnDismiss.hidden = YES;
    } else {
        CGRect rect = self.m_SignupView.frame;
        rect.origin.y = 0;
        
        [UIView animateWithDuration: 0.5 animations: ^(void) {
            [self.m_SignupView setFrame: rect];
        }];
        self.m_btnDismissForSignup.hidden = YES;
    }
}

- (void) showActivityForSignup
{
    [CUtils showActivity: HUD Caption: @"Signup..."];
}

- (BOOL) isRequiredParam
{
    for (int nIdx = 1; nIdx < 8; nIdx ++) {
        UITextField* textField = (UITextField*)[self.m_SignupView viewWithTag: nIdx];
        if (textField.text == nil || [textField.text isEqualToString: @""]) {
            if (nIdx < 5) {
                ALERT_SHOW(@"You must input name, username, email, password");
                return NO;
            }
        }
    }
    return YES;
}


@end
