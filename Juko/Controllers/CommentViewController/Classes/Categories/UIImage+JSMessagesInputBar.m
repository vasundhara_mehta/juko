

#import "UIImage+JSMessagesInputBar.h"

@implementation UIImage (JSMessagesInputBar)

+ (UIImage *)js_inputBar_iOS6
{
//    return [[UIImage imageNamed:@"input-bar"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    return [UIImage imageNamed:@"input-bar"];
}

+ (UIImage *)js_inputField_iOS6
{
    return [UIImage imageNamed:@"input-field"];//[[UIImage imageNamed:@"input-field"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 0.0f, 18.0f, 18.0f)];
}

@end
