

#import "NSString+JSMessagesView.h"

@implementation NSString (JSMessagesView)

- (NSString *)js_stringByTrimingWhitespace
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSUInteger)js_numberOfLines
{
    return [self componentsSeparatedByString:@"\n"].count + 1;
}

@end