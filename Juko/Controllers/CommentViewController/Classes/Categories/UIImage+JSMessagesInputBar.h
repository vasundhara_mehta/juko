

#import <UIKit/UIKit.h>

@interface UIImage (JSMessagesInputBar)

+ (UIImage *)js_inputBar_iOS6;

+ (UIImage *)js_inputField_iOS6;

@end
