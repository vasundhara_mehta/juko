

#import <Foundation/Foundation.h>

@interface NSString (JSMessagesView)

- (NSString *)js_stringByTrimingWhitespace;

- (NSUInteger)js_numberOfLines;

@end