

#import <UIKit/UIKit.h>

@interface UIView (AnimationOptionsForCurve)

+ (UIViewAnimationOptions)js_animationOptionsForCurve:(UIViewAnimationCurve)curve;

@end