

#import <UIKit/UIKit.h>

@interface UIColor (JSMessagesView)

+ (UIColor *)js_messagesBackgroundColor_iOS6;

+ (UIColor *)js_messagesTimestampColor_iOS6;

@end