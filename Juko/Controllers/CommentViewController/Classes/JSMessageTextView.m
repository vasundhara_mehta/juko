
#import "JSMessageTextView.h"
#import "JSBubbleView.h"

@implementation JSMessageTextView

#pragma mark - Init

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.backgroundColor = [UIColor whiteColor];
        self.scrollIndicatorInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 8.0f);
        [self setTextContainerInset: UIEdgeInsetsMake(5, 0, 0, 0)];
        self.contentInset = UIEdgeInsetsZero;
        self.scrollEnabled = YES;
        self.scrollsToTop = NO;
        self.userInteractionEnabled = YES;
        self.font = [UIFont systemFontOfSize:12.0f];
        self.textColor = [UIColor blackColor];
        self.keyboardAppearance = UIKeyboardAppearanceDefault;
        self.keyboardType = UIKeyboardTypeTwitter;
        self.returnKeyType = UIReturnKeyDefault;
        self.textAlignment = NSTextAlignmentLeft;
        self.opaque = NO;
        
        [self.layer setCornerRadius: 3];
    }
    return self;
}

#pragma mark - Text view

- (NSInteger)numberOfLinesOfText
{
    return [JSMessageTextView numberOfLinesForMessage:self.text];
}

+ (NSInteger)maxCharactersPerLine
{
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) ? 33 : 109;
}

+ (NSInteger)numberOfLinesForMessage:(NSString *)txt
{
    return (txt.length / [JSMessageTextView maxCharactersPerLine]) + 1;
}

@end
