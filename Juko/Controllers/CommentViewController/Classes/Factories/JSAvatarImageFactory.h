

#import <Foundation/Foundation.h>

extern CGFloat const kJSAvatarImageSize;

typedef NS_ENUM(NSUInteger, JSAvatarImageStyle) {
    JSAvatarImageStyleClassic,
    JSAvatarImageStyleFlat
};


typedef NS_ENUM(NSUInteger, JSAvatarImageShape) {
    JSAvatarImageShapeCircle,
    JSAvatarImageShapeSquare
};


@interface JSAvatarImageFactory : NSObject

+ (UIImage *)avatarImageNamed:(NSString *)filename
                        style:(JSAvatarImageStyle)style
                        shape:(JSAvatarImageShape)shape;
+ (UIImage *)avatarImage:(NSString *) urlString
                   style:(JSAvatarImageStyle)style
                   shape:(JSAvatarImageShape)shape;

@end
