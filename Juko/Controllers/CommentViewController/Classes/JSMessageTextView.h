

#import "JSDismissiveTextView.h"

@interface JSMessageTextView : JSDismissiveTextView

- (NSInteger)numberOfLinesOfText;

+ (NSInteger)maxCharactersPerLine;

+ (NSInteger)numberOfLinesForMessage:(NSString *)txt;

@end
