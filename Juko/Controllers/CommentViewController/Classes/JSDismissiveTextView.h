

#import <UIKit/UIKit.h>

@protocol JSDismissiveTextViewDelegate <NSObject>

@optional
- (void)keyboardDidShow;
- (void)keyboardDidScrollToPoint:(CGPoint)point;
- (void)keyboardWillBeDismissed;
- (void)keyboardWillSnapBackToPoint:(CGPoint)point;

@end



@interface JSDismissiveTextView : UITextView

@property (weak, nonatomic) id<JSDismissiveTextViewDelegate> keyboardDelegate;
@property (strong, nonatomic) UIPanGestureRecognizer *dismissivePanGestureRecognizer;

@end
