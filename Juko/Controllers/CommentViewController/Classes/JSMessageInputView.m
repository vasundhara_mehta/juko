

#import "JSMessageInputView.h"
#import "JSBubbleView.h"

#import "NSString+JSMessagesView.h"
#import "UIImage+JSMessagesInputBar.h"

#define SEND_BUTTON_WIDTH 55.0f

@interface JSMessageInputView ()

- (void)setup;
- (void)setupTextView;

@end



@implementation JSMessageInputView

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame
             textViewDelegate:(id<UITextViewDelegate>)delegate
             keyboardDelegate:(id<JSDismissiveTextViewDelegate>)keyboardDelegate
         panGestureRecognizer:(UIPanGestureRecognizer *)pan
{
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
        _textView.delegate = delegate;
        _textView.keyboardDelegate = keyboardDelegate;
        _textView.dismissivePanGestureRecognizer = pan;
    }
    return self;
}

- (void)dealloc
{
    _textView = nil;
    _sendButton = nil;
}

- (BOOL)resignFirstResponder
{
    [self.textView resignFirstResponder];
    return [super resignFirstResponder];
}

#pragma mark - Setup

- (void)setup
{
//    self.image = [UIImage js_inputBar_iOS6];
    self.backgroundColor = [UIColor colorWithRed: 0.9 green: 0.9 blue: 0.9 alpha:1.0];
    self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
    self.opaque = YES;
    self.userInteractionEnabled = YES;
    [self setupTextView];
}

- (void)setupTextView
{
    CGFloat width = self.frame.size.width - SEND_BUTTON_WIDTH - 11;
    CGFloat height = [JSMessageInputView textViewLineHeight];
    
    _textView = [[JSMessageTextView  alloc] initWithFrame:CGRectMake(4.0f, 3.0f, width, height)];
	
//    UIImageView *inputFieldBack = [[UIImageView alloc] initWithFrame:CGRectMake(_textView.frame.origin.x - 1.0f,
//                                                                                3.0f,
//                                                                                _textView.frame.size.width + 2.0f,
//                                                                                self.frame.size.height - 6)];
//    inputFieldBack.image = [UIImage js_inputField_iOS6];
//    inputFieldBack.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
//    inputFieldBack.backgroundColor = [UIColor clearColor];
//    [self addSubview:inputFieldBack];
    [self addSubview:_textView];
}

#pragma mark - Setters

- (void)setSendButton:(UIButton *)btn
{
    if(_sendButton)
        [_sendButton removeFromSuperview];
    
    _sendButton = btn;
    [self addSubview:_sendButton];
}

#pragma mark - Message input view

- (void)adjustTextViewHeightBy:(CGFloat)changeInHeight
{
    CGRect prevFrame = self.textView.frame;
    
    int numLines = MAX([self.textView numberOfLinesOfText],
                       [self.textView.text js_numberOfLines]);
    
    self.textView.frame = CGRectMake(prevFrame.origin.x,
                                     prevFrame.origin.y,
                                     prevFrame.size.width,
                                     prevFrame.size.height + changeInHeight);
    
    self.textView.contentInset = UIEdgeInsetsMake((numLines >= 6 ? 0.0f : 0.0f),
                                                  0.0f,
                                                  (numLines >= 6 ? 0.0f : 0.0f),
                                                  0.0f);
    
    self.textView.scrollEnabled = (numLines >= 4);
    
    if(numLines >= 6) {
        CGPoint bottomOffset = CGPointMake(0.0f, self.textView.contentSize.height - self.textView.bounds.size.height);
        [self.textView setContentOffset:bottomOffset animated:YES];
    }
}

+ (CGFloat)textViewLineHeight
{
    return 24.0f; // for fontSize 16.0f
}

+ (CGFloat)maxLines
{
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) ? 4.0f : 8.0f;
}

+ (CGFloat)maxHeight
{
    return ([JSMessageInputView maxLines] + 1.0f) * [JSMessageInputView textViewLineHeight];
}

@end