//
//  IGFollowViewController.m
//  Juko
//
//  Created by Mountain on 2/22/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGFollowViewController.h"

#import "DataKeeper.h"
#import "const.h"
#import "IGItemUser.h"
#import "CUtils.h"

#import "IGProfileController.h"

#import "UIImageView+WebCache.h"

#import "IGProgressActivity.h"

@interface IGFollowViewController ()

@end

@implementation IGFollowViewController
{
    NSMutableArray* m_arrayUsers;
    
    int m_nPage;
    BOOL m_FEndData;
    
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_arrayUsers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    if (_m_nViewKind == 1) {
        [_m_Label_Title setText: @"Following"];
    }
    
    [_m_tableView registerNib: [UINib nibWithNibName: STR_USER_CELL bundle: nil] forCellReuseIdentifier: STR_USER_CELL];
    
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
    
    m_nPage = 0;
}

#pragma mark - load follower/following users

- (void) loadData
{
    if (m_nPage == 0)
        [m_arrayUsers removeAllObjects];

    NSArray* arrayRawData;
    arrayRawData = [[DataKeeper sharedInstance] getFollowUsers: _m_nViewKind USER: _m_nUserIndex PAGE: m_nPage];

    if (arrayRawData == nil || arrayRawData.count == 0) {
        m_FEndData = YES;
    } else {
        for (NSDictionary* dict in arrayRawData) {
            NSDictionary* dictUser = [dict objectForKey: @"user"];
            IGItemUser* clsUser = [[IGItemUser alloc] initWithDictionary: dictUser];
            
            BOOL FFollow = YES;
            id objFollow = [dict objectForKey: @"follow"];
            if (objFollow && ![objFollow isKindOfClass: [NSNull class]]) {
                FFollow = [objFollow boolValue];
            }

            NSDictionary* dictInfo;
            DataKeeper* clsModel = [DataKeeper sharedInstance];
            if ([clsModel isMe: _m_clsProfile.m_strUsername]) {
                if (_m_nViewKind == 0) {
                    dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys: clsUser, @"user", [NSNumber numberWithBool: FFollow], @"follow", nil];
                } else {
                    dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys: clsUser, @"user", [NSNumber numberWithBool: FFollow], @"follow", nil];
                }
            } else {
                dictInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys: clsUser, @"user", [NSNumber numberWithBool: FFollow], @"follow", nil];
            }

            [m_arrayUsers addObject: dictInfo];
        }
        m_nPage ++;
        
        [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
    }
    
    [HUD hide: YES];
}

- (void) refreshData
{
    [_m_tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_arrayUsers == nil) ? 0 : m_arrayUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: STR_USER_CELL];
    cell.tag = indexPath.row + 10000;
    
    UIImageView* imgAvatar = (UIImageView*)[cell viewWithTag: 1];
    UIButton* btnName = (UIButton*)[cell viewWithTag: 2];
    UIButton* btnAction = (UIButton*)[cell viewWithTag: 3];

    IGItemUser* clsUser = [m_arrayUsers[indexPath.row] objectForKey: @"user"];
    
    [imgAvatar setImageWithURL: [NSURL URLWithString: clsUser.m_strImageThumbUrl]];
    [btnName setTitle: clsUser.m_strUsername forState: UIControlStateNormal];
    [btnName addTarget: self action: @selector(showUser:) forControlEvents: UIControlEventTouchUpInside];

    [btnAction addTarget: self action: @selector(follow:) forControlEvents: UIControlEventTouchUpInside];

    BOOL FFollow = [[m_arrayUsers[indexPath.row] objectForKey: @"follow"] boolValue];
    if (FFollow) {
        [btnAction setTitle: @"Unfollow" forState: UIControlStateNormal];
        [btnAction setBackgroundImage: [UIImage imageNamed: @"btn_profile_blue.png"] forState: UIControlStateNormal];
    } else {
        [btnAction setTitle: @"Follow" forState: UIControlStateNormal];
        [btnAction setBackgroundImage: [UIImage imageNamed: @"btn_profile_common.png"] forState: UIControlStateNormal];
    }
    
    if (indexPath.row == m_arrayUsers.count - 1) {
        if (!m_FEndData) {
            [HUD show: YES];
            [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
        }
    }
    
    return cell;
}

- (void) showUser: (UIButton*) button
{
    int nRow = (int)button.superview.superview.superview.tag - 10000;
    IGItemUser* clsUser = [m_arrayUsers[nRow] objectForKey: @"user"];
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_clsSimpleUser = clsUser;
    vc.m_clsUser = nil;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) follow: (UIButton*) button
{
    int nRow = (int)button.superview.superview.superview.tag - 10000;
    
    NSMutableDictionary* dictInfo = m_arrayUsers[nRow];
//    IGItemUser* clsUser = [dictInfo objectForKey: @"user"];
    BOOL FFollow = [[dictInfo objectForKey: @"follow"] boolValue];
    
    if (FFollow) { //unfollow
        [button setTitle: @"Follow" forState: UIControlStateNormal];
        [button setBackgroundImage: [UIImage imageNamed: @"btn_profile_common.png"] forState: UIControlStateNormal];
    } else { //follow
        [button setTitle: @"Unfollow" forState: UIControlStateNormal];
        [button setBackgroundImage: [UIImage imageNamed: @"btn_profile_blue.png"] forState: UIControlStateNormal];
    }
    
    FFollow = !FFollow;
    [dictInfo setObject: [NSNumber numberWithBool: FFollow] forKey: @"follow"];

    NSDictionary* dictParam = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", nRow], @"row", nil];
    [NSThread detachNewThreadSelector: @selector(followProcess:) toTarget: self withObject: dictParam];
}

- (void) followProcess: (NSDictionary*) dictParam
{
    int nRow = [[dictParam objectForKey: @"row"] intValue];
    
    NSDictionary* dictInfo = m_arrayUsers[nRow];
    IGItemUser* clsUser = [dictInfo objectForKey: @"user"];
    BOOL FFollow = [[dictInfo objectForKey: @"follow"] boolValue];
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsUser.m_nUserId], @"userIndex", [NSNumber numberWithBool: FFollow], @"follow", nil];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    [clsModel follow: dict];
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

#pragma mark - UIViewController Methods and actions

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender
{
//    if ([[DataKeeper sharedInstance] isMe: _m_clsProfile.m_strUsername]) {
//        [[NSNotificationCenter defaultCenter] postNotificationName: @"reload_profile" object: nil];
//    }
    [self.navigationController popViewControllerAnimated: YES];
}


@end
