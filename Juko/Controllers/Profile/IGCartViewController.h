//
//  IGCartViewController.h
//  Juko
//
//  Created by Mountain on 4/29/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGCartViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnEdit;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelSubTotal;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelTotal;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelShipping;
@property (weak, nonatomic) IBOutlet UIToolbar *m_Toolbar;

- (IBAction)goBack:(id)sender;

- (IBAction)actionEdit:(id)sender;
- (IBAction)actionCheckout:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;

@end
