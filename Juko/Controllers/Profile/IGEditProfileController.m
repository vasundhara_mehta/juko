//
//  IGEditProfileController.m
//  Juko
//
//  Created by Mountain on 5/2/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGEditProfileController.h"

#import "DataKeeper.h"
#import "CUtils.h"
#import "const.h"

#import "IGProgressActivity.h"
#import "GCPlaceholderTextView.h"

#import "IGChangePasswordController.h"
#import "IGShippingAddressViewController.h"


#define CELL_ID_SWITCH @"SwitchCell"
#define CELL_ID_BIO @"BioCell"
#define CELL_ID1 @"Cell1"

@interface IGEditProfileController ()

@end

@implementation IGEditProfileController
{
    NSArray* m_strSectionTitles;
    NSArray* m_titles;
    
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        m_strSectionTitles = [NSArray arrayWithObjects: @"", @"", @"PRIVATE INFORMATION", @"", nil];
        m_titles = @[@[@"Username", @"Email", @"Bio"], @[@"Change Password"], @[@"Shipping Address"], @[@"Privacy"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_SWITCH bundle: nil] forCellReuseIdentifier: CELL_ID_SWITCH];
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_BIO bundle: nil] forCellReuseIdentifier: CELL_ID_BIO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showActivity
{
    [HUD show: YES];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)actionSave:(id)sender {
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];

    NSString* strBio = _m_clsProfile.m_strBio;
    if (strBio == Nil) {
        strBio = @" ";
    }
    
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: strBio, @"bio", nil];
    
    if (![clsModel saveProfile: dict]) {
        NSLog(@"Fail to save bio");
    }
    
    [HUD hide: YES];

    [self goBack: nil];
}

#pragma mark - UITableViewDelegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return m_strSectionTitles.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 3;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return 1;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 320, 40)];
    UILabel* label = [[UILabel alloc] initWithFrame: CGRectMake(10, 0, 320, 40)];
    label.textColor = COLOR_DEFAULT;
    label.text = m_strSectionTitles[section];
    [view addSubview: label];
    return view;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 2) { // bio
//        NSString* strBio = _m_clsProfile.m_strBio;
//        CGSize maximumLabelSize = CGSizeMake(300, 9999);
//        
//        CGSize expectedLabelSize = [strBio sizeWithFont: FONT_NORMAL
//                                      constrainedToSize: maximumLabelSize
//                                          lineBreakMode: NSLineBreakByWordWrapping];
//        return MAX(expectedLabelSize.height + 14, 44);
        return 132.0;
    } else {
        return 44.0;
    }
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    
    if (indexPath.section == 3) { //privacy cell
        cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_SWITCH];
        UILabel* label = (UILabel*)[cell viewWithTag: 1];
        [label setText: @"Privacy"];

        UISwitch* ctrlSwitch = (UISwitch*)[cell viewWithTag: 2];
        [ctrlSwitch setOn: (_m_clsProfile.m_nPrivacy > 0) ? YES : NO];
        [ctrlSwitch removeTarget: self action: @selector(setPrivacy:) forControlEvents: UIControlEventValueChanged];
        [ctrlSwitch addTarget: self action: @selector(setPrivacy:) forControlEvents: UIControlEventValueChanged];
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_BIO];
        UITextView* txtView = (UITextView*)[cell viewWithTag: 100];
        txtView.delegate = self;
        ((GCPlaceholderTextView*)txtView).placeholder = @"Bio";
        txtView.textColor = COLOR_DARK_GRAY;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID1];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CELL_ID1];
        }
        [cell.textLabel setText: m_titles[indexPath.section][indexPath.row]];
        cell.textLabel.textColor = COLOR_DARK_GRAY;
    }
    
    if (indexPath.section == 1 || indexPath.section == 2) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [self showAccountInfo: indexPath CELL: cell];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    
    if (indexPath.section == 1) { //change password
        [self showPasswordView];
    } else if (indexPath.section == 2) { //shipping address
        [self showShippingAddressView];
    }
}

- (void) showPasswordView
{
    IGChangePasswordController* vc = [[IGChangePasswordController alloc] initWithNibName: @"IGChangePasswordController" bundle: nil];
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showShippingAddressView
{
    IGShippingAddressViewController* vc = [[IGShippingAddressViewController alloc] initWithNibName: @"IGShippingAddressViewController" bundle: nil];
    vc.m_clsProfile = self.m_clsProfile;
    [self.navigationController pushViewController: vc animated: YES];
}


- (void) showAccountInfo: (NSIndexPath*) indexPath CELL: (UITableViewCell*) cell
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell.textLabel setText: [NSString stringWithFormat: @"Username - %@", _m_clsProfile.m_strUsername]];
        } else if (indexPath.row == 1) {
            [cell.textLabel setText: [NSString stringWithFormat: @"Email - %@", _m_clsProfile.m_strEmail]];
        } else if (indexPath.row == 2) {
            UITextView* txtView = (UITextView*)[cell viewWithTag: 100];
            txtView.text = _m_clsProfile.m_strBio;
        }
    }
}

- (void) setPrivacy: (UISwitch*) ctrlSwitch
{
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    if (![clsModel saveProfile: [NSDictionary dictionaryWithObjectsAndKeys: (ctrlSwitch.isOn) ? @"1" : @"0", @"privacy", nil]]) {
        ALERT_SHOW(@"Failed to set privacy status");
    }
    
    _m_clsProfile.m_nPrivacy = ctrlSwitch.isOn;
    
    [HUD hide: YES];
}

#pragma mark - UITextViewDelegate


#define MAX_BIO_LENGTH 145

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (text && [text isEqualToString: @"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

- (void) textViewDidChange:(UITextView *)textView
{
    _m_clsProfile.m_strBio = [textView.text stringByReplacingOccurrencesOfString: @"\n" withString: @" "];
    if (_m_clsProfile.m_strBio && _m_clsProfile.m_strBio.length > MAX_BIO_LENGTH) {
        _m_clsProfile.m_strBio = [_m_clsProfile.m_strBio substringToIndex: MAX_BIO_LENGTH];
    }
    textView.text = _m_clsProfile.m_strBio;
    
//    CGSize maximumLabelSize = CGSizeMake(300, 9999);
//    
//    CGSize expectedLabelSize = [_m_clsProfile.m_strBio sizeWithFont: FONT_NORMAL
//                                  constrainedToSize: maximumLabelSize
//                                      lineBreakMode: NSLineBreakByWordWrapping];
//    float rHeight = MAX(expectedLabelSize.height + 14, 44);
//    CGRect rect = textView.frame;
//    rect.size.height = rHeight;
//    textView.frame = rect;
    
//    [self.m_tableView reloadSections: [NSIndexSet indexSetWithIndex: 0] withRowAnimation: UITableViewRowAnimationAutomatic];
}

@end
