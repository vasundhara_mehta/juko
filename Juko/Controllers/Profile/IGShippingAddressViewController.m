//
//  IGShippingAddressViewController.m
//  Juko
//
//  Created by Mountain on 4/19/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGShippingAddressViewController.h"

#import "const.h"
#import "DataKeeper.h"
#import "IGUser.h"

#import "IGProgressActivity.h"

#define MAX_TEXTFIELD_COUNT 4

@interface IGShippingAddressViewController ()

@end

@implementation IGShippingAddressViewController
{
    IGProgressActivity* HUD;
    NSArray* m_states;
    int m_nRow;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_states = [[DataKeeper sharedInstance] getStates];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    DataKeeper* clsModel = [DataKeeper sharedInstance];
    IGUser* clsUser = [clsModel getUserInfo];
    
    for (int nIdx = 1; nIdx <= MAX_TEXTFIELD_COUNT; nIdx ++) {
        UITextField* txtField = (UITextField*)[self.view viewWithTag: nIdx];
        txtField.delegate = self;
        
        if (nIdx == 1) {
            [txtField setText: clsUser.m_strCity];
        } else if (nIdx == 2) {
            [txtField setText: clsUser.m_strAddress];
        } else if (nIdx == 3) {
            [txtField setText: clsUser.m_strAdditionalAddress];
        } else if (nIdx == 4) {
            [txtField setText: clsUser.m_strZip];
        }
    }
    
    [self.m_txtState setText: clsUser.m_strState];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    CGRect rect = self.m_View_Picker.frame;
    rect.origin.y = self.view.frame.size.height;
    self.m_View_Picker.frame = rect;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)actionSave:(id)sender {
    if (![self check]) {
        ALERT_SHOW(@"Please fill all information")
        return;
    }
    
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSArray* arrayFields = @[@"city", @"address", @"additional_address_info", @"zip", @"state"];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    for (int nIdx = 1; nIdx <= MAX_TEXTFIELD_COUNT; nIdx ++) {
        UITextField* txtField = (UITextField*)[self.view viewWithTag: nIdx];
        [dict setObject: txtField.text forKey: arrayFields[nIdx - 1]];
    }
    
    [dict setObject: self.m_txtState.text forKey: arrayFields[MAX_TEXTFIELD_COUNT]];
    
    if (![clsModel saveProfile: dict]) {
        ALERT_SHOW(@"Please input the correct information");
    } else {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"" message: @"Success to change shipping address" delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [alert show];
        
        IGUser* clsUser = [clsModel getUserInfo];
        
        clsUser.m_strState = [self.m_txtState.text copy];
        clsUser.m_strCity = [[dict objectForKey: arrayFields[0]] copy];
        clsUser.m_strAddress = [[dict objectForKey: arrayFields[1]] copy];
        clsUser.m_strAdditionalAddress = [[dict objectForKey: arrayFields[2]] copy];
        clsUser.m_strZip = [[dict objectForKey: arrayFields[3]] copy];
    }
    [HUD hide: YES];
}

- (IBAction)chooseState:(id)sender {
    NSDictionary* dictState = m_states[m_nRow];
    NSString* strKey = [dictState.allKeys objectAtIndex: 0];
//    NSString* strState = [dictState objectForKey: strKey];
    
    [self.m_txtState setText: strKey];
    
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_View_Picker.frame;
        rect.origin.y = self.view.frame.size.height;
        self.m_View_Picker.frame = rect;
    }];
}

- (void) dismissKeyboard
{
    for (int nIdx = 1; nIdx <= MAX_TEXTFIELD_COUNT; nIdx ++) {
        UITextField* txtField = (UITextField*)[self.view viewWithTag: nIdx];
        [txtField resignFirstResponder];
    }
}

- (IBAction)showPickerView:(id)sender {
    [self dismissKeyboard];
    [self.m_pickerView reloadAllComponents];
    
    [self.m_pickerView selectRow: m_nRow inComponent: 0 animated: YES];
    
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_View_Picker.frame;
        rect.origin.x = 0;
        rect.origin.y = self.view.frame.size.height - rect.size.height - 50;
        self.m_View_Picker.frame = rect;
    }];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated: YES];
}

- (BOOL) check
{
    for (int nIdx = 1; nIdx <= MAX_TEXTFIELD_COUNT; nIdx ++) {
        UITextField* txtField = (UITextField*)[self.view viewWithTag: nIdx];
        if (txtField.tag == 3)
            continue;
        
        if (txtField.text == nil || [txtField.text isEqualToString: @""]) {
            return NO;
        }
    }
    
    if (self.m_txtState.text == nil || [self.m_txtState.text isEqualToString: @""]) {
        return NO;
    }
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == MAX_TEXTFIELD_COUNT) {
        [textField resignFirstResponder];
        [self chooseState: nil];
    } else {
        int nTag = textField.tag + 1;
        if (nTag <= MAX_TEXTFIELD_COUNT) {
            UITextField* txt = (UITextField*)[self.view viewWithTag: nTag];
            [txt becomeFirstResponder];
        }
    }
    return YES;
}

#pragma mark - UIPickerViewDelegate

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return m_states.count;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary* dictState = m_states[row];
    NSString* strKey = [dictState.allKeys objectAtIndex: 0];
    NSString* strState = [dictState objectForKey: strKey];
    
    if ([strKey isEqualToString: self.m_txtState.text]) {
        m_nRow = row;
    }
    
    return strState;
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    m_nRow = row;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}



@end
