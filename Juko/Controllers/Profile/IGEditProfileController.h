//
//  IGEditProfileController.h
//  Juko
//
//  Created by Mountain on 5/2/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IGProfileInfo.h"
@interface IGEditProfileController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

@property (nonatomic, retain) IGProfileInfo* m_clsProfile;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

- (IBAction)goBack:(id)sender;
- (IBAction)actionSave:(id)sender;

@end
