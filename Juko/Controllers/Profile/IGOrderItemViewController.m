//
//  IGOrderItemViewController.m
//  Juko
//
//  Created by Mountain on 5/2/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGOrderItemViewController.h"
#import "DataKeeper.h"
#import "const.h"
#import "CUtils.h"

#import "IGCartCell.h"

#import "IGProgressActivity.h"
#import "UIImageView+WebCache.h"

#define CELL_ID @"CartCell"

@interface IGOrderItemViewController ()

@end

@implementation IGOrderItemViewController
{
    IGProgressActivity* HUD;
    
    NSArray* m_orderItems;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID bundle: nil] forCellReuseIdentifier: CELL_ID];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadOrderItems) toTarget:self withObject: nil];
}

- (void) loadOrderItems
{
    m_orderItems = [[DataKeeper sharedInstance] getOrderItems: _m_nOrderId];
    [HUD hide: YES];
    
    [self performSelectorOnMainThread: @selector(reloadAllData) withObject: nil waitUntilDone: YES];
}

- (void) reloadAllData
{
    [self.m_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_orderItems ? m_orderItems.count : 0);
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID];
    cell.tag = indexPath.row + 10000;
    
    IGCartCell* cartCell = (IGCartCell*) cell;
    cartCell.delegate = self;
    
    NSDictionary* dictItem = m_orderItems[indexPath.row];
    [self configureCell: cell data: dictItem];
    
    return cell;
}

- (void) configureCell: (UITableViewCell*) cell data: (NSDictionary*) dictCartItem
{
    UIImageView* imgView = (UIImageView*)[cell viewWithTag: 1];
    UILabel* labelName = (UILabel*)[cell viewWithTag: 2];
    UILabel* labelPrice = (UILabel*)[cell viewWithTag: 3];
    UITextField* txtQuantity = (UITextField*)[cell viewWithTag: 4];
    txtQuantity.hidden = YES;
    UILabel* labelQuantity = (UILabel*)[cell viewWithTag: 5];
    
    NSString* strImageUrl= @"";
    NSArray* arrayUrl = [[dictCartItem objectForKey: @"item"] objectForKey: @"feed_image_url"];
    if (arrayUrl && arrayUrl.count > 0) {
        strImageUrl = arrayUrl[0];
    }
    [imgView setImageWithURL: [NSURL URLWithString: strImageUrl]];
    
    NSDictionary* dictProduct = [dictCartItem objectForKey: @"product"];
    NSDictionary* dictCategory = [dictCartItem objectForKey: @"category"];
    
    NSString* strName = [CUtils getStringValueFromDictionary: dictProduct KEY: @"name"];
    int nCategoryIndex = [CUtils getIntValueFromDictionary: dictCategory KEY: @"id"];
    
    NSString* strTitle = @"";
    NSString* strAddition = [CUtils getStringValueFromDictionary: dictCartItem KEY: @"additional_info"];
    if (strAddition && ![strAddition isEqualToString: @""]) {
        strTitle = [NSString stringWithFormat: @"%@ (%@)", [self getRealProductName: strName CategoryIndex: nCategoryIndex], strAddition];
    } else {
        strTitle = [self getRealProductName: strName CategoryIndex: nCategoryIndex];
    }
    
    [labelName setText: strTitle];
    
    NSDictionary* dictPrice = [[dictCartItem objectForKey: @"item"] objectForKey: @"price"];
    float rPrice = [CUtils getFloatValueFromDictionary: dictPrice KEY: @"amount"];
    [labelPrice setText: [NSString stringWithFormat: @"$%.2f", rPrice]];
    
    int nQuantity = [CUtils getIntValueFromDictionary: dictCartItem KEY: @"quantity"];
    [labelQuantity setText: [NSString stringWithFormat: @"%d", nQuantity]];
    
}

- (NSString*) getRealProductName: (NSString*) strName CategoryIndex: (int) nCategoryIndex
{
    NSString* strRealName;
    NSString* strTempName = [[[strName stringByReplacingOccurrencesOfString: @"Vertical" withString: @""] stringByReplacingOccurrencesOfString: @"Horizontal" withString: @""] stringByReplacingOccurrencesOfString: @"Skin" withString: @""];
    if (nCategoryIndex == ECATEGORY_SHIRTS) {
        strRealName = [NSString stringWithFormat: @"%@ T-Shirt", [strTempName substringToIndex: strTempName.length - 1]];
    } else {
        strRealName = [strTempName copy];
    }
    
    return strRealName;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
}


@end
