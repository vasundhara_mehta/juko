//
//  IGEditProfilePictureController.h
//  Juko
//
//  Created by Mountain on 4/21/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIXOverlayController.h"

@interface IGEditProfilePictureController : UIXOverlayContentViewController

@property id delegate;

- (IBAction)actionChoose:(id)sender;

- (IBAction)cancel:(id)sender;

@end
