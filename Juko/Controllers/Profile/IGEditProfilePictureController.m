//
//  IGEditProfilePictureController.m
//  Juko
//
//  Created by Mountain on 4/21/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGEditProfilePictureController.h"

@interface IGEditProfilePictureController ()

@end

@implementation IGEditProfilePictureController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionChoose:(id)sender {
    if (sender) {
        int nTag = ((UIButton*)sender).tag;
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", nTag], @"index", nil];
        if (_delegate && [_delegate respondsToSelector: @selector(chooseButton:)]) {
            [_delegate performSelector: @selector(chooseButton:) withObject: dict afterDelay: 0.0f];
        }
    }
    [self.overlayController dismissOverlay: YES];
}

- (IBAction)cancel:(id)sender {
    [self.overlayController dismissOverlay: YES];
}

@end
