//
//  IGShippingAddressViewController.h
//  Juko
//
//  Created by Mountain on 4/19/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGProfileInfo.h"

@interface IGShippingAddressViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
/////////import///////////////
@property (nonatomic, strong) IGProfileInfo* m_clsProfile;

////////////////
@property (weak, nonatomic) IBOutlet UIView *m_View_Picker;
@property (weak, nonatomic) IBOutlet UIPickerView *m_pickerView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnState;
@property (weak, nonatomic) IBOutlet UITextField *m_txtState;

- (IBAction)goBack:(id)sender;
- (IBAction)actionSave:(id)sender;
- (IBAction)chooseState:(id)sender;
- (IBAction)showPickerView:(id)sender;

@end
