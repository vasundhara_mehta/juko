//
//  IGWithdrawalListController.m
//  Juko
//
//  Created by Mountain on 5/3/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGWithdrawalListController.h"

#import "DataKeeper.h"
#import "const.h"
#import "CUtils.h"

#define CELL_ID @"WithdrawCell"

@interface IGWithdrawalListController ()

@end

@implementation IGWithdrawalListController
{
    BOOL m_FEndData;
    int m_nPage;
    
    NSMutableArray* m_arrayData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        m_arrayData = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID bundle: nil] forCellReuseIdentifier: CELL_ID];
    
    m_FEndData = NO;
    m_nPage = 0;
    
    [NSThread detachNewThreadSelector: @selector(loadMoreData) toTarget: self withObject: nil];
}

- (void) loadMoreData
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    
    NSArray* array = [clsModel getWithdrawals: m_nPage];
    if (array && array.count > 0) {
        [m_arrayData addObjectsFromArray: array];
        [self performSelectorOnMainThread: @selector(reloadAllData) withObject: self waitUntilDone: YES];
        m_nPage ++;
    } else {
        m_FEndData = YES;
    }
}

- (void) reloadAllData
{
    [self.m_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_arrayData ? m_arrayData.count : 0);
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID];
    
    [self configureCell: cell data: m_arrayData[indexPath.row]];
    
    if (indexPath.row == m_arrayData.count - 1 && !m_FEndData) {
        [NSThread detachNewThreadSelector: @selector(loadMoreData) toTarget: self withObject: nil];
    }
    
    return cell;
}

- (void) configureCell: (UITableViewCell*) cell data: (NSDictionary*) dict
{
    UILabel* labelDate = (UILabel*)[cell viewWithTag: 1];
    UILabel* labelAmount = (UILabel*)[cell viewWithTag: 2];
    UILabel* labelStatus = (UILabel*)[cell viewWithTag: 3];
    
    NSString* strTime = [CUtils getStringValueFromDictionary: dict KEY: @"create_at"];
    NSString* strDate = [CUtils getDateString: strTime];
    [labelDate setText: strDate];
    
    float rAmount = [CUtils getFloatValueFromDictionary: dict KEY: @"amount"];
    [labelAmount setText: [NSString stringWithFormat: @"$%.2f", rAmount]];
    
    [labelStatus setText: [CUtils getStringValueFromDictionary: dict KEY: @"status"]];
}

@end
