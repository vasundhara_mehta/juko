//
//  IGSettingViewController.m
//  Juko
//
//  Created by Mountain on 2/11/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGSettingViewController.h"
#import "const.h"
#import "DataKeeper.h"
#import "CUtils.h"
#import "IGAppDelegate.h"

#import "IGProgressActivity.h"

//#import "IGInviteFromContactsController.h"
#import "IGCartViewController.h"
#import "IGBankViewController.h"
#import "IGMyOrderViewController.h"
#import "IGWithdrawalListController.h"

#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>

#define CELL_ID1 @"cell"
#define CELL_ID_REFERRALS @"ReferralsCell"
#define CELL_ID_BANK @"BankCell"

@interface IGSettingViewController ()

@end

@implementation IGSettingViewController
{
    NSArray* arraySection;
    NSDictionary* m_dictRowTitles;
    
    IGProgressActivity* HUD;
    
    float m_rbalance;
    int m_nReferrals;
    
    BOOL m_FLoadCart;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arraySection = [NSArray arrayWithObjects: @"Cart", @"Virtual Wallet", @"Referrals", @"Support", @"", nil];
        NSArray* arrayRow0 = @[@"", @"My Orders"];
        NSArray* arrayRow2 = @[@"My Wallet", @"Withdrawals", @"My Payment Account"];
        NSArray* arrayRow3 = @[@"Invite People", @"Referrals"];
        NSArray* arrayRow4 = @[@"Term of Use", @"Contact Us", @"Report a Problem"];
        NSArray* arrayRow5 = @[@"Logout"];
        
        m_dictRowTitles = [NSDictionary dictionaryWithObjectsAndKeys: arrayRow0, @"1", arrayRow2, @"2", arrayRow3, @"3", arrayRow4, @"4", arrayRow5, @"5", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_REFERRALS bundle: nil] forCellReuseIdentifier: CELL_ID_REFERRALS];
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_BANK bundle: nil] forCellReuseIdentifier: CELL_ID_BANK];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.m_tableView];
    [self.view addSubview: HUD];
    
//    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(getExtraInfo) toTarget:self withObject: nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
}

- (void) getExtraInfo
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSDictionary* dictInfo = [clsModel getExtraInfo];
    
    if (dictInfo == nil) {
        NSLog(@"%@", [clsModel getErrMessage]);
    } else {
        m_rbalance = [CUtils getFloatValueFromDictionary: dictInfo KEY: @"balance"];
        m_nReferrals = [CUtils getIntValueFromDictionary: dictInfo KEY: @"referrals"];
    }
    
    BOOL FRes = [clsModel loadCart];
    if (!FRes) {
        NSLog(@"No items");
    }
    m_FLoadCart = YES;
    
//    [HUD hide: YES];
    
    [self performSelectorOnMainThread: @selector(reloadAllData) withObject: nil waitUntilDone: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - UITableViewDelegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return arraySection.count;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 320, 44)];
    [view setBackgroundColor: [UIColor colorWithWhite: 0.9 alpha: 1.0]];
    UILabel* label = [[UILabel alloc] initWithFrame: CGRectMake(10, 0, 320, 44)];
    [label setFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 15.0]];
    [label setTextColor: COLOR_DEFAULT];
    [label setText: arraySection[section]];
    [view addSubview: label];
    return view;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 3;
            break;
        case 2:
            return 2;
            break;
        case 3:
            return 3;
            break;
        case 4:
            return 1;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 1) {
        return 74;
    }
    return 44;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    
    NSArray* titles = [m_dictRowTitles objectForKey: [NSString stringWithFormat: @"%d", indexPath.section + 1]];
    if (indexPath.section == 2 && indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_REFERRALS];
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_BANK];
        [cell.textLabel setText: titles[indexPath.row]];
        cell.textLabel.textColor = COLOR_DARK_GRAY;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID1];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CELL_ID1];
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        [cell.textLabel setText: titles[indexPath.row]];
        cell.textLabel.textColor = COLOR_DARK_GRAY;
    }

    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 0) {
                [cell.textLabel setText: @"Loading..."];
                [NSThread detachNewThreadSelector: @selector(showCartItemCount:) toTarget: self withObject: cell];
            }
            break;
//        case 1:
//            [self showAccountInfo: indexPath CELL: cell];
//            break;
        case 1:
            [self showWalletInfo: indexPath CELL: cell];
            break;
        case 2:
            [self showReferralInfo: indexPath CELL: cell];
            break;
        case 3:
            break;
        case 4:
            break;
        default:
            break;
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                UITableViewCell* cell = [tableView cellForRowAtIndexPath: indexPath];
                if ([cell.textLabel.text isEqualToString: @"Loading..."]) {
                    return;
                }
                [self showCart: nil];
            } else if (indexPath.row == 1) {
                [self showMyOrderView];
            }
            break;
        }
//        case 1: //account
//            if (indexPath.row == 2) { //change password
//                [self showPasswordView];
//            } else if (indexPath.row == 3) { //shipping address
//                [self showShippingAddressView];
//            }
//            break;
        case 1: //virtual wallet
            if (indexPath.row == 0) { // withdraw
                if (![self isPossibleToWithdraw]) {
                    ALERT_SHOW(@"Please setup your bank account")
                    return;
                }
                [self withdrawProcess];
            } else if (indexPath.row == 1) { //withdrawals
                [self showWithdrawalsView];
            } else if (indexPath.row == 2) { //setup bank account
                [self showBankView];
            }
            break;
        case 2:
            if (indexPath.row == 0) {
                UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle: @"Invite People" delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Invite From Facebook", @"Invite From Twitter", @"Invite By Email", nil];
                [actionSheet showInView: self.view];
            }
            break;
        case 3:
            if (indexPath.row == 0) { //terms of use
                
            } else if (indexPath.row == 1) { //contact us
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                
                mail.mailComposeDelegate = self;
                
                [mail setToRecipients: [NSArray arrayWithObjects: @"support@jukoapp.com", nil]];
                [mail setSubject: @"Contact Us"];
                
                NSString* strText = @"";
                [mail setMessageBody: strText isHTML:NO];
                
                [self presentViewController: mail animated: YES completion: nil];
            } else if (indexPath.row == 2) { //report problem
                
            }
            break;
        case 4:
            [self logout];
            break;
        default:
            break;
    }
}

- (void) showWithdrawalsView
{
    IGWithdrawalListController* vc = [[IGWithdrawalListController alloc] initWithNibName: @"IGWithdrawalListController" bundle: nil];
    [self.navigationController pushViewController: vc animated: YES];
}

- (BOOL) isPossibleToWithdraw
{
    NSDictionary* dictBank = self.m_clsProfile.m_dictBank;
    NSString* strAccountNumber = [CUtils getStringValueFromDictionary: dictBank KEY: @"account_number"];
    if (strAccountNumber == nil || [strAccountNumber isEqualToString: @""]) {
        return NO;
    }
    
    return YES;
}

- (void) withdrawProcess
{
    UIAlertView* dialog = [[UIAlertView alloc] init];
    dialog.tag = 45;
    [dialog setDelegate:self];
    [dialog setTitle:@"Withdraw"];
    
    [dialog setMessage:@"Please input amount of money\n that you are going to withdraw."];
    [dialog addButtonWithTitle: @"Cancel"];
    [dialog addButtonWithTitle: @"Withdraw"];
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [dialog textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeSentences;
    [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [dialog setTransform: moveUp];
    [dialog show];
}

#pragma mark - alertview delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 45) {
        if (buttonIndex == 1) {
            NSString* strText = [[alertView textFieldAtIndex: 0].text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            if (strText == nil || [strText isEqualToString: @""])
                return;
            float rAmount = [strText floatValue];
            if (rAmount <= 0 || rAmount > m_rbalance) {
                ALERT_SHOW(@"Invalid amount of withdrawal money")
                return;
            }
            [NSThread detachNewThreadSelector: @selector(withdrawMoney:) toTarget: self withObject: [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithFloat: rAmount], @"amount", nil]];
        }
    }
}

- (void) withdrawMoney: (NSDictionary*) dictParam
{
    float rAmount = [CUtils getFloatValueFromDictionary: dictParam KEY: @"amount"];
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FRes = [clsModel withdraw: rAmount];
    if (!FRes) {
        NSLog(@"Fail to withdraw");
    }
    [self getExtraInfo];
}

- (void) reloadAllData
{
    [self.m_tableView reloadData];
}

- (void) showMyOrderView
{
    IGMyOrderViewController* vc = [[IGMyOrderViewController alloc] initWithNibName: @"IGMyOrderViewController" bundle: nil];
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) showBankView
{
    IGBankViewController* vc = [[IGBankViewController alloc] initWithNibName: @"IGBankViewController" bundle: nil];
    vc.m_clsProfile = self.m_clsProfile;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) showCart: (UIButton*) button
{
    IGCartViewController* vc = [[IGCartViewController alloc] initWithNibName: @"IGCartViewController" bundle: nil];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

#pragma mark - Show information

- (void) showCartItemCount: (UITableViewCell*) cell
{
    while (!m_FLoadCart) {
        usleep(100);
    }
    
    [self performSelectorOnMainThread: @selector(showCartItemCountOnLabel:) withObject: cell waitUntilDone: YES];
}

- (void) showCartItemCountOnLabel: (UITableViewCell*) cell
{
    int nCartItemCount = [[DataKeeper sharedInstance] getCartItemCount];
    [cell.textLabel setText: [NSString stringWithFormat: @"%d items", nCartItemCount]];
}

- (void) showWalletInfo: (NSIndexPath*) indexPath CELL: (UITableViewCell*) cell
{
    if (indexPath.row == 0) {
        NSLog(@"balance %f", m_rbalance);
        [cell.textLabel setText: [NSString stringWithFormat: @"$%.2f", m_rbalance]];
    } else if (indexPath.row == 2) {
        UIButton* btnCheck = (UIButton*)[cell viewWithTag: 1];
        NSDictionary* dictBank = self.m_clsProfile.m_dictBank;
        int nVerify = [CUtils getIntValueFromDictionary: dictBank KEY: @"verified"];
        if (nVerify > 0) {
            btnCheck.hidden = NO;
        } else {
            btnCheck.hidden = YES;
        }
    }
}

- (void) showReferralInfo: (NSIndexPath*) indexPath CELL: (UITableViewCell*) cell
{
    if (indexPath.row == 1) {
        UILabel* label = (UILabel*)[cell viewWithTag: 1];
        int nProfit = 20 + (MIN(m_nReferrals, 500) / 100);
        [label setText: [NSString stringWithFormat: @"Profit(%d%%)", nProfit]];
        
        UIProgressView* progress = (UIProgressView*)[cell viewWithTag: 2];
        float rPercent = (float)(nProfit - 20) / 5.0;
        [progress setProgress: rPercent];
        
        label = (UILabel*)[cell viewWithTag: 3];
        [label setText: [NSString stringWithFormat: @"Referrals(%d)", m_nReferrals]];

        label = (UILabel*)[cell viewWithTag: 5];
        [label setText: [NSString stringWithFormat: @"%d", (nProfit - 20) * 100]];

        label = (UILabel*)[cell viewWithTag: 6];
        progress = (UIProgressView*)[cell viewWithTag: 4];
        if (nProfit == 25) {
            [label setText: @""];
            [progress setProgress: 1.0];
        } else {
            [label setText: [NSString stringWithFormat: @"%d", (nProfit - 19) * 100]];
            [progress setProgress: (float)((MIN(m_nReferrals, 500) - (nProfit - 20) * 100) / 100.0)];
        }
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self inviteFromFbOrTw: NO];
            break;
        case 1:
            [self inviteFromFbOrTw: YES];
            break;
        case 2:
            [self inviteFromContacts];
            break;
        default:
            break;
    }
}

- (void) inviteFromContacts
{
//    IGInviteFromContactsController* vc = [[IGInviteFromContactsController alloc] initWithNibName: @"IGInviteFromContactsController" bundle: nil];
//    [self.navigationController pushViewController: vc animated: YES];
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
    [mail setToRecipients: nil];
    [mail setSubject: @"Invite"];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

- (void) inviteFromFbOrTw: (BOOL) FTwitter
{
    NSString* strLink = [[DataKeeper sharedInstance] getDownloadLink];
    NSString* strText = [NSString stringWithFormat: @"%@ %@", SHARE_DEFAULT_TEXT, strLink];
    
    SLComposeViewController* vc;

    if (FTwitter) {
        strText = [NSString stringWithFormat: @"%@ %@", SHARE_TWITTER_TEXT, strLink];
        vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeTwitter];
//        [vc addImage: [UIImage imageNamed: @"icon_1024.png"]];
    } else {
        vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
//        [vc addImage: [UIImage imageNamed: @"icon_1024.png"]];
    }
    
    [vc setInitialText: strText];

    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
    
//    [FBWebDialogs presentRequestsDialogModallyWithSession: nil
//                                                  message: strText
//                                                    title: @"Invitation"
//                                               parameters:nil
//                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                                                      if (error) {
//                                                          // Case A: Error launching the dialog or sending request.
//                                                          NSLog(@"Error sending request.");
//                                                      } else {
//                                                          if ([resultURL.absoluteString rangeOfString:@"request="].location == NSNotFound) {
//                                                              // Case B: User clicked the "x" icon
//                                                              NSLog(@"User canceled request.");
//                                                          } else {
//                                                              NSLog(@"Request Sent.");
//                                                          }
//                                                      }}];
}

#pragma mark - Logout

- (void) logout
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: @"no" forKey: LOCAL_KEY_LOGIN];
    [defaults synchronize];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController popToRootViewControllerAnimated: YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"noti_loadinit" object:nil];
}

@end
