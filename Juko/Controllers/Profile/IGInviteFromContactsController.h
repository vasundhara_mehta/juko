//
//  IGInviteFromContactsController.h
//  Juko
//
//  Created by Mountain on 4/22/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface IGInviteFromContactsController : UIViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
- (IBAction)goBack:(id)sender;
- (IBAction)actionInvite:(id)sender;

@end
