//
//  IGInviteFromContactsController.m
//  Juko
//
//  Created by Mountain on 4/22/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGInviteFromContactsController.h"

#import "const.h"
#import "CUtils.h"
#import "DataKeeper.h"

#import <AddressBook/ABAddressBook.h>
#import <AddressBook/ABPerson.h>
#import <AddressBook/ABRecord.h>
#import <AddressBook/ABMultiValue.h>

#define CELL_ID_CONTACT @"ContactCell"
#define MAX_INVITE_COUNT 10
#define MAX_CONTACT_COUNT 10000

@interface IGInviteFromContactsController ()

@end

@implementation IGInviteFromContactsController
{
    NSMutableArray* m_arrayContacts;
    int m_nChooseCount;
    
    int m_pnFlag[MAX_CONTACT_COUNT];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_arrayContacts = [[NSMutableArray alloc] init];
        m_nChooseCount = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_CONTACT bundle: nil] forCellReuseIdentifier: CELL_ID_CONTACT];
    
    [self actionFromContacts];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Get Contacts

- (void) actionFromContacts
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                NSLog(@"Access granted!");
                [self loadContacts: addressBookRef];
            } else {
                NSLog(@"Access denied!");
            }
        });
    } else {
        [self loadContacts: addressBookRef];
    }
}

- (void) loadContacts: (ABAddressBookRef) addressBookRef
{
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBookRef);
    
    for(int i = 0; i < numberOfPeople; i++) {
        
        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
        
        NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
        NSLog(@"Name:%@ %@", firstName, lastName);
        
        ABMultiValueRef email = ABRecordCopyValue(person, kABPersonEmailProperty);
        if (ABMultiValueGetCount(email) > 0) {
            NSMutableArray* arrayEmails = [[NSMutableArray alloc] init];
            for (CFIndex i = 0; i < ABMultiValueGetCount(email); i++) {
                NSString *strMailAddress = (__bridge NSString *) ABMultiValueCopyValueAtIndex(email, i);
                [arrayEmails addObject: [strMailAddress copy]];
                NSLog(@"mail:%@", strMailAddress);
            }
            
            NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [firstName copy], @"firstname", [lastName copy], @"lastname", arrayEmails, @"emails", nil];
            [m_arrayContacts addObject: dict];
        }
    }
    memset(m_pnFlag, 0, sizeof(int) * MAX_CONTACT_COUNT);
    
    [self.m_tableView reloadData];
}


#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return m_arrayContacts.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = (UITableViewCell*)[self.m_tableView dequeueReusableCellWithIdentifier: CELL_ID_CONTACT];
    
    cell.tag = indexPath.row + 10000;
    
    UILabel* label = (UILabel*)[cell viewWithTag: 100];
    UIImageView* imgCheck = (UIImageView*)[cell viewWithTag: 101];
    
    if (m_pnFlag[indexPath.row] == 0) {
        [label setTextColor: [UIColor colorWithWhite: 0.3 alpha: 1.0]];
        imgCheck.image = nil;
    } else {
        [label setTextColor: COLOR_DEFAULT];
        imgCheck.image = [UIImage imageNamed: @"mark_contact_check.png"];
    }
    
    NSDictionary* dict = m_arrayContacts[indexPath.row];
    label.text = [NSString stringWithFormat: @"%@ %@", [CUtils getStringValueFromDictionary: dict KEY: @"firstname"], [CUtils getStringValueFromDictionary: dict KEY: @"lastname"]];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    UITableViewCell* cell = [tableView cellForRowAtIndexPath: indexPath];
    
    UILabel* label = (UILabel*)[cell viewWithTag: 100];
    UIImageView* imgCheck = (UIImageView*)[cell viewWithTag: 101];

    NSLog(@"%d", m_pnFlag[indexPath.row]);
    m_pnFlag[indexPath.row] = 1 - m_pnFlag[indexPath.row];
    if (m_pnFlag[indexPath.row] == 0) {
        [label setTextColor: [UIColor colorWithWhite: 0.3 alpha: 1.0]];
        imgCheck.image = nil;
        m_nChooseCount --;
    } else {
        
        if (m_nChooseCount > MAX_INVITE_COUNT) {
            m_pnFlag[indexPath.row] = 1 - m_pnFlag[indexPath.row];
            ALERT_SHOW(@"You may sent the invitation to %d people at once.")
            return;
        }
        
        [label setTextColor: COLOR_DEFAULT];
        imgCheck.image = [UIImage imageNamed: @"mark_contact_check.png"];
        m_nChooseCount ++;
    }
}

#pragma mark - back

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)actionInvite:(id)sender {
    if (![MFMailComposeViewController canSendMail]) {
        ALERT_SHOW(@"Please setup your mail account in Settings of your device");
        return;
    }
    
    NSMutableArray* arrayMails = [NSMutableArray array];
    for (int nIdx = 0; nIdx < m_arrayContacts.count; nIdx ++) {
        NSLog(@"flag: %d", m_pnFlag[nIdx]);
        if (m_pnFlag[nIdx] > 0) {
            NSArray* mails = [m_arrayContacts[nIdx] objectForKey: @"emails"];
            if (mails && mails.count > 0) {
                [arrayMails addObjectsFromArray: mails];
            }
        }
    }
    
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
    [mail setToRecipients: arrayMails];
    [mail setSubject: @"Recommendation"];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

@end
