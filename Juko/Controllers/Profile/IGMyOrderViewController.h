//
//  IGMyOrderViewController.h
//  Juko
//
//  Created by Mountain on 5/2/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGMyOrderViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

@end
