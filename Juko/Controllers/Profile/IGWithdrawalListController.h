//
//  IGWithdrawalListController.h
//  Juko
//
//  Created by Mountain on 5/3/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGWithdrawalListController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

@end
