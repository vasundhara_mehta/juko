//
//  IGProfileController.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGProfileController.h"

#import "const.h"
#import "DataKeeper.h"
#import "CUtils.h"
#import "IGUser.h"
#import "IGItemUser.h"
#import "IGComment.h"
#import "IGProfileInfo.h"

#import "ItemCell.h"
#import "FeedCellView.h"

#import "IGItemViewController.h"
#import "IGCommentsViewController.h"
#import "IGTagViewController.h"
#import "IGAppDelegate.h"
#import "IGPurchaseController.h"
#import "IGSettingViewController.h"
#import "IGFollowViewController.h"
#import "IGEditProfilePictureController.h"
#import "IGEditProfileController.h"
#import "IGLikersViewController.h"

#import "STTweetLabel.h"
#import "UIXOverlayController.h"
#import "IGProgressActivity.h"

#import "UIImageView+WebCache.h"
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>

@interface IGProfileController ()

@end

@implementation IGProfileController
{
    BOOL m_FSelect_Tile;
    IGProfileInfo* m_clsProfile;
    
    IGProgressActivity* HUD;
    
    UIRefreshControl* m_RefreshCtrl;
    
    NSMutableArray* m_creationList;
    NSMutableArray* m_likedList;
    
    NSArray* m_arrayData;
    
    BOOL m_FEndCreationList;
    int m_nCreationPage;
    BOOL m_FEndLikedList;
    int m_nLikedPage;
    
    BOOL m_FChangePicture;
    
    UIDocumentInteractionController* docFile;
    IGItem* m_clsItem;
    UIImage* m_itemImage;
    
    BOOL m_FFinishLoading;
    BOOL m_FInitLoaded;
}

@synthesize m_strUsername;
@synthesize m_nDataKind;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.tabBarItem setFinishedSelectedImage: [UIImage imageNamed: @"tabitem4_h"] withFinishedUnselectedImage: [UIImage imageNamed:@"tabitem4"]];
        [[self tabBarItem] setImageInsets: UIEdgeInsetsMake(5, 0, -5, 0)];
        
        m_creationList = [[NSMutableArray alloc] init];
        m_likedList = [[NSMutableArray alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(observerForRemoveItem:) name:@"delete_item" object: nil];
    }
    return self;
}

- (void) setM_strUsername:(NSString *) strUsername
{
    if ([[strUsername substringToIndex: 1] isEqualToString: @"@"]) {
        m_strUsername = [[strUsername substringFromIndex: 1] copy];
    } else {
        m_strUsername = [strUsername copy];
    }
}

- (void) reloadProfile:(UIRefreshControl *)refreshControl
{
    m_FChangePicture = YES;
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self configureView];
    
    [CUtils showActivity: HUD Caption: @""];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void) layoutViews
{
    [self.m_Label_Bio setText: m_clsProfile.m_strBio];
    
    CGSize maximumLabelSize = CGSizeMake(302, FLT_MAX);
    
    CGSize expectedLabelSize = [m_clsProfile.m_strBio sizeWithFont: self.m_Label_Bio.font constrainedToSize:maximumLabelSize lineBreakMode: self.m_Label_Bio.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = self.m_Label_Bio.frame;
    newFrame.size.height = expectedLabelSize.height + 5;
    self.m_Label_Bio.frame = newFrame;

    CGRect rect = _m_View_Desc.frame;
    rect.size.height = self.m_Label_Bio.frame.origin.y + self.m_Label_Bio.frame.size.height + 5;
    _m_View_Desc.frame = rect;
    
    rect = _m_View_Buttons.frame;
    rect.origin.y = _m_View_Desc.frame.size.height + _m_View_Desc.frame.origin.y;
    _m_View_Buttons.frame = rect;
    
    rect = _m_View_Tab.frame;
    float rPrevHeight = rect.size.height;
    rect.size.height = _m_View_Desc.frame.size.height + _m_View_Buttons.frame.size.height;
    _m_View_Tab.frame = rect;
    
    if (rPrevHeight != rect.size.height) {
        [self.m_tableView beginUpdates];
        [self.m_tableView setTableHeaderView: _m_View_Tab];
        [self.m_tableView endUpdates];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
 
    m_FFinishLoading = YES;
    
    if (m_FChangePicture) {
        m_FChangePicture = NO;
        return;
    }
    
    [self layoutViews];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.m_tableView beginUpdates];
        [self.m_tableView setTableHeaderView: _m_View_Tab];
        [self.m_tableView endUpdates];
    });
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}

- (void) loadData
{
    m_FInitLoaded = NO;
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    if (_m_nViewKind == 1) { //me
        _m_clsUser = [clsModel getUserInfo];
        _m_clsSimpleUser = nil;
    }
    
    if (_m_clsSimpleUser) {
        m_clsProfile = [clsModel getProfileInfo: _m_clsSimpleUser.m_strUsername];
    } else if (_m_clsUser) {
        m_clsProfile = [clsModel getProfileInfo: _m_clsUser.m_strUsername];
    } else if (m_strUsername) {
        m_clsProfile = [clsModel getProfileInfo: m_strUsername];
    }
    
    if ([[DataKeeper sharedInstance] isMe: m_clsProfile.m_strUsername]) {
        _m_ButtonFollow.hidden = NO;
        [_m_ButtonFollow setTitle: @"Edit Profile" forState: UIControlStateNormal];
    } else {
        _m_ButtonFollow.hidden = NO;
        
        if (m_clsProfile.m_FFollow) {
            [_m_ButtonFollow setTitle: @"Unfollow" forState: UIControlStateNormal];
            [_m_ButtonFollow  setBackgroundImage: [UIImage imageNamed: @"btn_profile_blue.png"] forState: UIControlStateNormal];
        } else {
            [_m_ButtonFollow setTitle: @"Follow" forState: UIControlStateNormal];
            [_m_ButtonFollow  setBackgroundImage: [UIImage imageNamed: @"btn_profile_common.png"] forState: UIControlStateNormal];
        }
    }
    
    m_FInitLoaded = YES;

    m_FEndCreationList = NO;
    m_FEndLikedList = NO;
    m_nCreationPage = 0;
    m_nLikedPage = 0;
    
    [m_likedList removeAllObjects];
    [m_creationList removeAllObjects];
    
    [self loadItemData];
    
    [CUtils hideActivity: HUD];
    
    [self performSelectorOnMainThread: @selector(showProfile) withObject: nil waitUntilDone: YES];
}

- (void) loadItemData
{
    if (!m_FInitLoaded) {
        return;
    }
    
    
    m_FFinishLoading = NO;
    
    if (m_nDataKind == 0) {
        [self getNextCreationList];
    } else {
        [self getNextLikedList];
    }
    m_FFinishLoading = YES;

    [self performSelectorOnMainThread: @selector(showItemData) withObject: nil waitUntilDone: YES];
}

- (void) getNextCreationList
{
    if (m_FEndCreationList) {
        return;
    }
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSArray* arrayCreationList = [clsModel getCreationList: m_clsProfile.m_nUserIndex Page: m_nCreationPage];
    if (arrayCreationList == nil || arrayCreationList.count == 0) {
        m_FEndCreationList = YES;
    } else {
        [m_creationList addObjectsFromArray: arrayCreationList];
        m_nCreationPage ++;
    }
}

- (void) getNextLikedList
{
    if (m_FEndLikedList) {
        return;
    }
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSArray* arrayLikedList = [clsModel getLikedList: m_clsProfile.m_nUserIndex Page: m_nLikedPage];
    if (arrayLikedList == nil || arrayLikedList.count == 0) {
        m_FEndLikedList = YES;
    } else {
        [m_likedList addObjectsFromArray: arrayLikedList];
        m_nLikedPage ++;
    }
}

- (void) showProfile
{
    [_m_Image_Avatar setImageWithURL:[NSURL URLWithString: m_clsProfile.m_strAvatarThumb] placeholderImage: nil options: SDWebImageRetryFailed];
    [_m_Label_Username setText: m_clsProfile.m_strName];
    [_m_Label_UserID setText: m_clsProfile.m_strUsername.uppercaseString];
    
    //[_m_Label_Location setText: m_clsProfile.m_strAddress];
    [_m_Label_Creations setText: [NSString stringWithFormat: @"%d", m_clsProfile.m_nCreationCount]];
    [_m_Label_Followers setText: [NSString stringWithFormat: @"%d", m_clsProfile.m_nFollowerCount]];
    [_m_Label_Following setText: [NSString stringWithFormat: @"%d", m_clsProfile.m_nFollowingCount]];
    
    [self layoutViews];
    
    [m_RefreshCtrl endRefreshing];
}

- (void) configureView
{
    [_m_ButtonFollow setTitle: @"Loading..." forState: UIControlStateNormal];
    
    if (_m_nViewKind == 1) {
        self.m_btnBack.hidden = YES;
    } else {
        self.m_btnSettings.hidden = YES;
    }
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_FEED bundle: nil] forCellReuseIdentifier: CELL_ID_FEED];
    
//    self.m_tableView.alpha = 0;
    
    [self.m_Button_Creation setTitleColor: COLOR_DEEP_DARK_GRAY forState: UIControlStateNormal];
    [self.m_Button_Creation setTitleColor: COLOR_DEFAULT forState: UIControlStateSelected];
    
    [self.m_ButtonLiked setTitleColor: COLOR_DEEP_DARK_GRAY forState: UIControlStateNormal];
    [self.m_ButtonLiked setTitleColor: COLOR_DEFAULT forState: UIControlStateSelected];

    m_nDataKind = 0;
    self.m_Button_Creation.selected = YES;
    self.m_ImgSlide.frame = CGRectMake(8, 0, self.m_ImgSlide.frame.size.width, self.m_ImgSlide.frame.size.height);
    
    m_FSelect_Tile = YES;
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    m_RefreshCtrl = [[UIRefreshControl alloc] init];
    [m_RefreshCtrl addTarget:self action:@selector(reloadProfile:) forControlEvents:UIControlEventValueChanged];
    [self.m_tableView addSubview: m_RefreshCtrl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showItemData
{
//    if (_m_btnScroll.selected) {
        [_m_tableView reloadData];
//    } else {
//        [_m_StreamView reloadData];
//    }
}

//#pragma mark - StreamView Delegate
//
//- (NSInteger)numberOfCellsInStreamView:(StreamView *)streamView
//{
//    if (m_nDataKind == 0) {
//        m_arrayData = m_creationList;
//    } else {
//        m_arrayData = m_likedList;
//    }
//    
//    NSInteger nCount = (m_arrayData ? m_arrayData.count : 0);
//    
//    return nCount;
//}
//
//- (NSInteger)numberOfColumnsInStreamView:(StreamView *)streamView
//{
//    return 2;
//}
//
//- (UIView *)streamView:(StreamView *)stream cellAtIndex:(NSInteger)index
//{
//    static NSString *CellID1 = @"MyCell1";
//    static NSString *CellID2 = @"MyCell2";
//    
//    BOOL redCell = index % 2 == 0;
//    NSString *CellID =  redCell ? CellID2 : CellID1;
//    
//    ItemCell *cell;
//    
//    cell = (ItemCell *)[stream dequeueReusableCellWithIdentifier:CellID];
//    
//    if (cell == nil) {
//        cell = [[ItemCell alloc] initWithFrame:CGRectMake(0, 0, 154, 154)];
//        cell.reuseIdentifier = CellID;
//    }
//    
//    cell.delegate = self;
//    cell.tag = index + 1000;
//    
//    [cell configureCell: m_arrayData[index]];
//    
//    return cell;
//}
//
//- (CGFloat)streamView:(StreamView *)streamView heightForCellAtIndex:(NSInteger)index
//{
//    return 154;
//}
//
//- (UIView *)headerForStreamView:(StreamView *)streamView
//{
//    return nil;
//    /*
//     CSearchCell *header = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
//     header.label.text = @"This is the header";
//     
//     return header;
//     */
//}
//
//- (UIView *)footerForStreamView:(StreamView *)streamView
//{
//    return nil;
//    
//    /*
//     if (page <= MaxPage) {
//     CSearchCell *footer = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
//     footer.label.text = @"This is the footer";
//     
//     return footer;
//     } else {
//     return nil;
//     }
//     */
//}

#pragma mark - cell click

- (void) touchedCell: (id) sender
{
    IGItem* clsItem = (IGItem*)sender;
    IGItemViewController* vc = [[IGItemViewController alloc] initWithNibName: @"IGItemViewController" bundle: nil];
    vc.m_clsFeaturedItem = clsItem;
    vc.m_FShowDeleteButton = NO;
    if (m_nDataKind == 0 && [[DataKeeper sharedInstance] isMe: m_clsProfile.m_strUsername]) {
        vc.m_FShowDeleteButton = YES;
    }
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - Change View Mode

- (IBAction)changeViewMode:(id)sender {
    UIButton* button = (UIButton*)sender;
    if (button.selected) {
        return;
    }
    
    [self changeViewModeByButton: button];
}

- (void) changeViewModeByButton: (UIButton*) button
{
    if (button == _m_btnScroll) {
        _m_btnScroll.selected = YES;
        _m_btnTile.selected = NO;
    } else {
        _m_btnScroll.selected = NO;
        _m_btnTile.selected = YES;
    }
    
    [self.m_tableView reloadData];
    
//    float rHeight = _m_View_Tab.frame.size.height;
//    [_m_View_Tab setFrame: CGRectMake(0, 64, _m_View_Tab.frame.size.width, _m_View_Tab.frame.size.height)];
//    
//    [_m_StreamView setContentInset: UIEdgeInsetsMake(rHeight, 0, 50, 0)];
//    [_m_tableView setContentInset: UIEdgeInsetsMake(rHeight, 0, 50, 0)];
//
//    [_m_tableView setContentOffset: CGPointMake(0, -rHeight)];
//    [_m_StreamView setContentOffset: CGPointMake(0, -rHeight)];
}

- (IBAction)changeDataKind:(id)sender {
    UIButton* button = (UIButton*) sender;
    if ((m_nDataKind == 0 && button == self.m_Button_Creation) || (m_nDataKind == 1 && button == self.m_ButtonLiked)) {
        return;
    }
    m_nDataKind = 1 - m_nDataKind;
    if (m_nDataKind == 0) {
        self.m_Button_Creation.selected = YES;
        self.m_ButtonLiked.selected = NO;
        
        [UIView animateWithDuration: 0.3 animations: ^(void) {
            self.m_ImgSlide.frame = CGRectMake(8, 0, self.m_ImgSlide.frame.size.width, self.m_ImgSlide.frame.size.height);
        }];
    } else {
        self.m_Button_Creation.selected = NO;
        self.m_ButtonLiked.selected = YES;
        [UIView animateWithDuration: 0.3 animations: ^(void) {
            self.m_ImgSlide.frame = CGRectMake(159, 0, self.m_ImgSlide.frame.size.width, self.m_ImgSlide.frame.size.height);
        }];
    }
    
    BOOL FTile = self.m_btnTile.selected;
    
    if (m_FSelect_Tile) {
        [self changeViewMode: self.m_btnTile];
    } else {
        [self changeViewMode: self.m_btnScroll];
    }
    
    m_FSelect_Tile = FTile;
    
    [NSThread detachNewThreadSelector: @selector(loadItemData) toTarget: self withObject: nil];
}

- (IBAction)actionSettings:(id)sender
{
    IGSettingViewController* vc = [[IGSettingViewController alloc] initWithNibName: @"IGSettingViewController" bundle: nil];
    vc.m_clsProfile = m_clsProfile;
    
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)showFollowers:(id)sender
{
    IGFollowViewController* vc = [[IGFollowViewController alloc] initWithNibName: @"IGFollowViewController" bundle: nil];
    vc.m_nUserIndex = m_clsProfile.m_nUserIndex;
    vc.m_nViewKind = 0;
    vc.m_clsProfile = m_clsProfile;
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)showFollowing:(id)sender
{
    IGFollowViewController* vc = [[IGFollowViewController alloc] initWithNibName: @"IGFollowViewController" bundle: nil];
    vc.m_nUserIndex = m_clsProfile.m_nUserIndex;
    vc.m_nViewKind = 1;
    vc.m_clsProfile = m_clsProfile;
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)actionFollow:(id)sender
{
    if ([_m_ButtonFollow.titleLabel.text isEqualToString: @"Edit Profile"]) {
        [self showEditProfileView];
        return;
    }
    
    m_clsProfile.m_FFollow = !m_clsProfile.m_FFollow;
    if (m_clsProfile.m_FFollow) {
        [_m_ButtonFollow setTitle: @"Unfollow" forState: UIControlStateNormal];
        [_m_ButtonFollow setBackgroundImage: [UIImage imageNamed: @"btn_profile_blue.png"] forState: UIControlStateNormal];
    } else {
        [_m_ButtonFollow setTitle: @"Follow" forState: UIControlStateNormal];
        [_m_ButtonFollow setBackgroundImage: [UIImage imageNamed: @"btn_profile_common.png"] forState: UIControlStateNormal];
    }

    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSDictionary* dictParam = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", m_clsProfile.m_nUserIndex], @"userIndex", [NSNumber numberWithBool: m_clsProfile.m_FFollow], @"follow", nil];
    [NSThread detachNewThreadSelector: @selector(follow:) toTarget: clsModel withObject: dictParam];
}

- (void) showEditProfileView
{
    IGEditProfileController* vc = [[IGEditProfileController alloc] initWithNibName: @"IGEditProfileController" bundle: nil];
    vc.m_clsProfile = m_clsProfile;
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)changeProfilePhoto:(id)sender {
    NSString* strUsername = [[DataKeeper sharedInstance] getUserInfo].m_strUsername;
    if ([strUsername isEqualToString: m_clsProfile.m_strUsername]) { //me
        IGEditProfilePictureController* vc = [[IGEditProfilePictureController alloc] initWithNibName: @"IGEditProfilePictureController" bundle: nil];
        vc.delegate = self;
        
        UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
        overlay.dismissUponTouchMask = NO;
        
        IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
        
        [overlay presentOverlayOnView: appDelegate.window withContent: vc animated: YES];
    }
}

- (void) chooseButton: (NSDictionary*) dict
{
    int nButtonIndex = [[dict objectForKey: @"index"] intValue];
    
    m_FChangePicture = YES;
    
    switch (nButtonIndex) {
        case 1: //remove photo
        {
            [CUtils showActivity: HUD Caption: @""];
            [NSThread detachNewThreadSelector: @selector(removePicture) toTarget:self withObject: nil];
            break;
        }
        case 2: // Import from Facebook
        {
            [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
            [self getFbPicture];
            break;
        }
        case 3: // Import from Twitter
            break;
        case 4: // Take Photo
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.allowsEditing = NO;
            picker.delegate = self;
            [self presentViewController: picker animated: YES completion: nil];
            break;
        }
        case 5: // Choose from Library
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.allowsEditing = NO;
            picker.delegate = self;
            [self presentViewController: picker animated: YES completion: nil];

            break;
        }
        default:
            break;
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    CGSize sz = image.size;
    UIImage* fixImage = [CUtils fixImage: [CUtils imageWithImage: image scaledToSize: CGSizeMake(sz.width / 3, sz.height / 3)]];
    
    NSData *data = UIImageJPEGRepresentation(fixImage, 0.8);
    UIImage* img = [UIImage imageWithData:data];
    
    [self.m_Image_Avatar setImage: img];
    
    [NSThread detachNewThreadSelector: @selector(setAvatarImage:) toTarget: self withObject: img];
    
    [picker dismissViewControllerAnimated: YES completion: nil];
}


- (void) showActivity
{
    [HUD show: YES];
}

- (void) removePicture
{
    [[DataKeeper sharedInstance] removePhoto];
    [self.m_Image_Avatar setImage: [UIImage imageNamed: @"nouser.jpg"]];
    
    [CUtils hideActivity: HUD];
}

- (void) getFbPicture
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    id fbToken = [defaults objectForKey: LOCAL_KEY_FB_ACCESS_TOKEN];
    if (fbToken == nil || [fbToken isEqualToString: LOCAL_VALUE_NO_TOKEN]) {
        [self performSelectorOnMainThread: @selector(fbLogin) withObject: nil waitUntilDone: YES];
    } else {
        id fbId = [defaults objectForKey: LOCAL_KEY_FB_USER_ID];
        if (fbId && ![fbId isEqualToString: LOCAL_VALUE_NO_USER]) {
            [self performSelectorOnMainThread: @selector(setFbPicture) withObject: nil waitUntilDone: YES];
        } else {
            [self performSelectorOnMainThread: @selector(fbLogin) withObject: nil waitUntilDone: YES];
        }
    }
}

- (void) fbLogin
{
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.session == nil) {
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_stream", @"publish_actions", @"read_stream", @"read_friendlists", @"email", @"user_photos", nil];
        appDelegate.session = [[FBSession alloc] initWithPermissions:permissions];
        
        [FBAppCall handleDidBecomeActiveWithSession:appDelegate.session];
    }
    [FBSession setActiveSession: appDelegate.session];

    [appDelegate.session openWithBehavior:FBSessionLoginBehaviorWithNoFallbackToWebView completionHandler:^(FBSession* session, FBSessionState status, NSError* error) {
        if ([session isOpen]) {
            [self getFBUserID];
        } else {
            NSLog(@"Failed to login on Facebook");
        }
    }];
}

- (void) getFbUserPicture: (NSNotification*) noti
{
    [self performSelectorOnMainThread: @selector(showFbImportPhotoView) withObject: nil waitUntilDone: YES];
}

- (void) getFBUserID
{
    FBRequestConnection* connection = [[FBRequestConnection alloc] init];
    connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession | FBRequestConnectionErrorBehaviorAlertUser | FBRequestConnectionErrorBehaviorRetry;
    FBRequest* request;
    request = [FBRequest requestWithGraphPath: @"me?field=id" parameters: nil HTTPMethod: @"GET"];
    
    [connection addRequest: request completionHandler:^(FBRequestConnection* connection, id result, NSError* error) {
        if (!error) {
            FBGraphObject* obj = (FBGraphObject*)result;
            
            IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject: appDelegate.session.accessTokenData.accessToken forKey: LOCAL_KEY_FB_ACCESS_TOKEN];
            [defaults setObject: [obj objectForKey: @"id"] forKey: LOCAL_KEY_FB_USER_ID];
            [defaults synchronize];
            
            [self performSelectorOnMainThread: @selector(setFbPicture) withObject: nil waitUntilDone: YES];
        }
    }];
    [connection start];
}

- (void) setFbPicture
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* strFbId = [defaults objectForKey: LOCAL_KEY_FB_USER_ID];
    NSURL* fbPictureUrl = [NSURL URLWithString: [NSString stringWithFormat: @"https://graph.facebook.com/%@/picture?width=200&height=200", strFbId]];
    
    [self.m_Image_Avatar setImageWithURL: fbPictureUrl completed: ^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
        [self setAvatarImage: image];
    }];
}

- (void) setAvatarImage: (UIImage*) image
{
    [[DataKeeper sharedInstance] changeProfilePicture: image];
    [HUD hide: YES];
}

#pragma mark - UITableViewDelegate

#define ID_OFFSET_ITEM 1000
#define GALLERY_CELL_SIZE 154

- (void) chooseUser: (UIButton*) button
{
    int nRow = button.tag - ID_OFFSET_ITEM;
    IGItem* clsItem = [m_arrayData objectAtIndex: nRow];
    IGItemUser* clsItemUser = clsItem.m_clsUser;
    
    if ([clsItemUser.m_strUsername isEqualToString: m_clsProfile.m_strUsername]) {
        return;
    }
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_clsSimpleUser = clsItemUser;
    [self.navigationController pushViewController: vc animated: YES];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (m_nDataKind == 0) {
        m_arrayData = m_creationList;
    } else {
        m_arrayData = m_likedList;
    }
    
    int nCount = (m_arrayData) ? m_arrayData.count : 0;

    if (!self.m_btnScroll.selected) {
        if (nCount > 0) {
            nCount = (nCount - 1) / 2 + 1;
        }
    }
    
    return nCount;
}

//- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return _m_View_Tab.frame.size.height;
//    }
//    return 0;
//}
//
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rHeight = [self calcHeight: indexPath.row];
    return rHeight;
}

- (float) calcHeight: (int) nRow
{
    if (!_m_btnScroll.selected) {
        return GALLERY_CELL_SIZE + 4;
    } else {
        IGItem* clsItem = [m_arrayData objectAtIndex: nRow];
        IGItemUser* clsItemUser = clsItem.m_clsUser;
        
        CGSize size = CGSizeZero;
        if ([clsItem.m_strDescription isEqualToString: @""]) {
            
        } else {
            NSString* strDesc = [NSString stringWithFormat: @"%@ %@", clsItemUser.m_strUsername, clsItem.m_strDescription];
            
            STTweetLabel *tweetLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 1000.0)];
            [tweetLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 15.0f]];
            [tweetLabel setText: strDesc];
            tweetLabel.textAlignment = NSTextAlignmentLeft;
            
            size = [tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:tweetLabel.frame.size.width];
        }
        
        int nHeight = size.height + 10;
        
        NSMutableString* strComment = [[NSMutableString alloc] init];
        int nCount = MIN(MAX_COMMENT_COUNT, clsItem.m_arrayComments.count);
        for (int nIdx = clsItem.m_arrayComments.count - 1; (nIdx > -1 && nCount > 0); nIdx --) {
            IGComment* clsComment = clsItem.m_arrayComments[nIdx];
            nCount --;
            
            if ([strComment isEqualToString: @""]) {
                [strComment appendFormat: @"@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
            } else {
                [strComment appendFormat: @"\n@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
            }
        }
        
        STTweetLabel *commentLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 1000.0)];
        [commentLabel setText: strComment];
        commentLabel.textAlignment = NSTextAlignmentLeft;
        
        size = [commentLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:commentLabel.frame.size.width];
        nHeight += size.height + 15;
        
        return nHeight + 292 + 44;
    }
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] init];
    return view;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!m_FFinishLoading) {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath: indexPath];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"Temp"];
        }
        return cell;
    }
    UITableViewCell* dataCell;
    if (self.m_btnScroll.selected) {
        dataCell = [self getScrollCell: tableView indexPath: indexPath];
    } else {
        dataCell = [self getGalleryCell: tableView indexPath: indexPath];
    }
    
    return dataCell;
}

- (UITableViewCell*) getGalleryCell: (UITableView*) tableView indexPath: (NSIndexPath*) indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: @"GalleryCell"];
    
    ItemCell* leftCell = nil, *rightCell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"GalleryCell"];

        float rOffset = (self.view.frame.size.width - GALLERY_CELL_SIZE * 2) / 3;
        leftCell = [[ItemCell alloc] initWithFrame:CGRectMake(rOffset, rOffset/2, GALLERY_CELL_SIZE, GALLERY_CELL_SIZE)];
        leftCell.delegate = self;
        leftCell.tag = 1000;
        [cell addSubview: leftCell];
        
        rightCell = [[ItemCell alloc] initWithFrame:CGRectMake(rOffset * 2 + GALLERY_CELL_SIZE, rOffset/2, GALLERY_CELL_SIZE, GALLERY_CELL_SIZE)];
        rightCell.delegate = self;
        rightCell.tag = 1001;
        [cell addSubview: rightCell];
    } else {
        leftCell = (ItemCell*)[cell viewWithTag: 1000];
        rightCell = (ItemCell*)[cell viewWithTag: 1001];
    }
    
    if ([leftCell isKindOfClass: [ItemCell class]]) {
        [leftCell configureCell: m_arrayData[indexPath.row * 2]];
    }
    if ((indexPath.row + 1) * 2 <= m_arrayData.count) {
        rightCell.hidden = NO;
        if ([rightCell isKindOfClass: [ItemCell class]])
            [rightCell configureCell: m_arrayData[indexPath.row * 2 + 1]];
    } else
        rightCell.hidden = YES;

    return cell;
}

- (UITableViewCell*) getScrollCell: (UITableView*) tableView indexPath: (NSIndexPath*) indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_FEED];
    
    UIView* headerView = [[[NSBundle mainBundle] loadNibNamed: @"ItemHeaderView" owner: self options: nil] objectAtIndex: 0];
    UIImageView* imgAvatar = (UIImageView*)[headerView viewWithTag: 1];
    UIButton* buttonUser = (UIButton*)[headerView viewWithTag: 2];
    
    IGItem* clsItem = [m_arrayData objectAtIndex: indexPath.row];
    IGItemUser* clsItemUser = clsItem.m_clsUser;
    NSString* strUrl = clsItemUser.m_strImageThumbUrl;
    NSURL* url = [NSURL URLWithString: strUrl];
    [imgAvatar setImageWithURL: url placeholderImage: nil options: SDWebImageRetryFailed];
    
    [buttonUser setTitle: clsItemUser.m_strUsername forState: UIControlStateNormal];
    
    [buttonUser addTarget: self action: @selector(chooseUser:) forControlEvents: UIControlEventTouchUpInside];
    buttonUser.tag = indexPath.row + ID_OFFSET_ITEM;
    
    UILabel* labelTime = (UILabel*)[headerView viewWithTag: 3];
    UIImageView* imgClock = (UIImageView*)[headerView viewWithTag: 4];
    [labelTime setText: clsItem.m_strTime];
    [labelTime sizeToFit];
    CGRect rect = labelTime.frame;
    rect.origin.x = 310.0 - rect.size.width;
    labelTime.frame = rect;
    
    [imgClock setFrame: CGRectMake(rect.origin.x - imgClock.frame.size.width - 3, imgClock.frame.origin.y, imgClock.frame.size.width, imgClock.frame.size.height)];
    if (clsItem.m_strTime == nil || [clsItem.m_strTime isEqualToString: @""]) {
        imgClock.hidden = YES;
    } else {
        imgClock.hidden = NO;
    }
    
    
    FeedCellView* feedCell = (FeedCellView*)cell;
    feedCell.delegate = self;
    feedCell.m_FShowDeleteButton = NO;
    if (m_nDataKind == 0 && [[DataKeeper sharedInstance] isMe: m_clsProfile.m_strUsername]) {
        feedCell.m_FShowDeleteButton = YES;
    }
    [feedCell configureCell: clsItem];
    
    feedCell.tag = indexPath.row + ID_OFFSET_ITEM;
    
    UITableViewCell* dataCell = [tableView dequeueReusableCellWithIdentifier: @"Cell1"];
    if (dataCell == nil) {
        dataCell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"Cell"];
        dataCell.clipsToBounds = YES;
    }
    
    rect = CGRectMake(0, 44, feedCell.frame.size.width, feedCell.frame.size.height);
    feedCell.frame = rect;
    [dataCell addSubview: feedCell];
    
    headerView.frame = CGRectMake(0, 0, 320, 44);
    [dataCell addSubview: headerView];

    return dataCell;
}

#pragma mark - FeedCell delegate methods

- (void) refreshCells: (FeedCellView*) cell
{
    int nRow = cell.tag - ID_OFFSET_ITEM;
    NSIndexPath* path = [NSIndexPath indexPathForRow: nRow inSection: 0];
    [self.m_tableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: path] withRowAnimation: UITableViewRowAnimationAutomatic];
}

- (void) showAllComments: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_READ;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //[self.navigationController pushViewController: vc animated: YES];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) showUser: (NSString*) strUsername
{
    NSString* strRealUsername;
    if ([[strUsername substringToIndex: 1] isEqualToString: @"@"]) {
        strRealUsername = [[strUsername substringFromIndex: 1] copy];
    } else {
        strRealUsername = [strUsername copy];
    }

    if ([m_clsProfile.m_strUsername isEqualToString: strRealUsername]) {
        return;
    }
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_strUsername = strUsername;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showTag: (NSString*) strTag
{
    NSString* strHashTag = strTag;
    if ([[strTag substringToIndex: 1] isEqualToString: @"#"]) {
        strHashTag = [strTag substringFromIndex: 1];
    }
    IGTagViewController* vc = [[IGTagViewController alloc] initWithNibName: @"IGTagViewController" bundle: nil];
    vc.m_strTag = strHashTag;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - action of item

- (void) actionLike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(likeProcess:) toTarget: self withObject: clsItem];
    //    [self likeProcess: clsItem];
}

- (void) showLikers: (IGItem*) clsItem
{
    IGLikersViewController* vc = [[IGLikersViewController alloc] initWithNibName: @"IGLikersViewController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) likeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: YES];
    if (!FSuccess) {
    }
}

- (void) actionUnlike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(unlikeProcess:) toTarget: self withObject: clsItem];
    //    [self unlikeProcess: clsItem];
}

- (void) unlikeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: NO];
    if (!FSuccess) {
    }
}

- (void) actionComment: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_WRITE;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //    [self.navigationController pushViewController: vc animated: YES];
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) actionPurchase: (IGItem*) clsItem
{
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    overlay.dismissUponTouchMask = NO;
    
    IGPurchaseController* vc = [[IGPurchaseController alloc] initWithNibName: @"IGPurchaseController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    [overlay presentOverlayOnView: appDelegate.window withContent:vc animated: YES];
}

- (void) actionShare: (NSDictionary*) dict
{
    m_itemImage = [dict objectForKey: @"image"];
    m_clsItem = [dict objectForKey: @"item"];
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle: @"" delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Share on Facebook", @"Share on Mail", @"Share on Instagram", @"Share on Twitter", nil];
    [sheet showInView: self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self actionFbShare];
            break;
        case 1:
            [self actionMailShare];
            break;
        case 2:
            [self actionIGShare];
            break;
        case 3:
            [self actionTwShare];
            break;
        default:
            break;
    }
}

- (IBAction)actionFbShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

- (IBAction)actionMailShare {
    if (![MFMailComposeViewController canSendMail]) {
        ALERT_SHOW(@"Please setup your mail account in Settings of your device");
        return;
    }
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
    //    [mail setToRecipients:[NSArray arrayWithObject:@"info@thegrint.com"]];
    [mail setSubject: m_clsItem.m_strName];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: m_itemImage], 1.0) mimeType: @"image/jpg" fileName: m_clsItem.m_strName];
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

- (IBAction)actionIGShare {
    UIImage* image;
    image = [self imageForShare: m_itemImage];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];
        NSData *imageData=UIImagePNGRepresentation(image);
        [imageData writeToFile:saveImagePath atomically:YES];
        
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        
        if (docFile == Nil) {
            docFile = [[UIDocumentInteractionController alloc] init];
        }
        docFile.delegate=self;
        docFile.UTI=@"com.instagram.photo";
        
        NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
        docFile.annotation=[NSDictionary dictionaryWithObjectsAndKeys: strText, @"InstagramCaption", nil];
        
        [docFile setURL:imageURL];
        
        [docFile presentOpenInMenuFromRect: CGRectZero inView: self.view animated: YES];
    }
    else
    {
        NSLog (@"Instagram is not available");
    }
}

- (IBAction)actionTwShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeTwitter];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

#define SHARE_WIDTH 200
#define SHARE_HEIGHT 200
- (UIImage*) imageForShare : (UIImage*) orgImage
{
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, SHARE_WIDTH, SHARE_HEIGHT)];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.opaque = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView setImage: orgImage];
    return [CUtils imageWithView: imgView];
}

- (void) removeItem: (IGItem*) clsItem
{
    if (clsItem == nil)
        return;
    
    m_clsItem = clsItem;
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"" message: @"Are you sure to remove this item?" delegate: self cancelButtonTitle: @"Yes" otherButtonTitles: @"No", nil];
    alert.tag = 100;
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) { //delete
        [self deleteItem: m_clsItem];
    }
}

- (void) observerForRemoveItem: (NSNotification*) noti
{
    m_clsItem = noti.object;
    [self deleteItem: m_clsItem];
}

- (void) deleteItem: (IGItem*) clsItem
{
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FRes = [clsModel deleteItem: clsItem.m_nIndex];
    
    if (!FRes) {
        [HUD hide: YES];
        ALERT_SHOW(@"Fail to delete");
    } else {
        m_FEndCreationList = NO;
        m_FEndLikedList = NO;
        m_nCreationPage = 0;
        m_nLikedPage = 0;
        
        [m_likedList removeAllObjects];
        [m_creationList removeAllObjects];

        [self loadItemData];
        [HUD hide: YES];
        
        m_clsProfile.m_nCreationCount --;
        [_m_Label_Creations setText: [NSString stringWithFormat: @"%d", m_clsProfile.m_nCreationCount]];
    }
}
//
//#pragma mark - UIScrollViewDelegate
//
//- (void) scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if (!m_FFinishLoading) {
//        return;
//    }
//    if (_m_btnScroll.selected && scrollView == _m_tableView) {
//        CGRect rect = _m_View_Tab.frame;
//        rect.origin.y = 64.0 - (scrollView.contentOffset.y + scrollView.contentInset.top);
//        
//        _m_View_Tab.frame = rect;
//    } else if (!_m_btnScroll.selected && scrollView == _m_StreamView) {
////        NSLog(@"%f, %f", scrollView.contentOffset.y, scrollView.contentInset.top);
//        CGRect rect = _m_View_Tab.frame;
//        rect.origin.y = 64.0 - (scrollView.contentOffset.y + scrollView.contentInset.top);
//        
//        _m_View_Tab.frame = rect;
//    }
//}
//
//- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    if (_m_btnScroll.selected && scrollView == _m_tableView) {
//        CGRect rect = _m_View_Tab.frame;
//        rect.origin.y = 64.0 - (scrollView.contentOffset.y + scrollView.contentInset.top);
//        
//        _m_View_Tab.frame = rect;
//    } else if (!_m_btnScroll.selected && scrollView == _m_StreamView) {
//        CGRect rect = _m_View_Tab.frame;
//        rect.origin.y = 64.0 - (scrollView.contentOffset.y + scrollView.contentInset.top);
//        
//        _m_View_Tab.frame = rect;
//    }
//}

@end
