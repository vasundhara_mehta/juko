//
//  IGCheckoutController.h
//  Juko
//
//  Created by Mountain on 4/30/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGItem.h"

#import "PKView.h"

@interface IGCheckoutController : UIViewController<UITextFieldDelegate, PKViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property PKView* paymentView;

//UI member variables
@property (weak, nonatomic) IBOutlet UIView *m_ViewPurchase;
@property (weak, nonatomic) IBOutlet UITextField *m_txtState;
@property (weak, nonatomic) IBOutlet UITextField *m_txtBillState;
@property (weak, nonatomic) IBOutlet UIView *m_View_Picker;
@property (weak, nonatomic) IBOutlet UIPickerView *m_pickerView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnDismissKeyboard;
@property (weak, nonatomic) IBOutlet UIButton *m_btnBilling;
@property (weak, nonatomic) IBOutlet UILabel *m_LabelAddress;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *m_btnPickerNext;

//action methods
- (IBAction)showStates:(id)sender;
- (IBAction)chooseState:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)actionPurchase:(id)sender;
- (IBAction)actionBillingAddress:(id)sender;
- (IBAction)nextProcess:(id)sender;

@end
