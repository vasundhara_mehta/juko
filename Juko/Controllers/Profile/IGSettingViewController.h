//
//  IGSettingViewController.h
//  Juko
//
//  Created by Mountain on 2/11/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MFMailComposeViewController.h>
#import "IGProfileInfo.h"

@interface IGSettingViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) IGProfileInfo* m_clsProfile;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

@end
