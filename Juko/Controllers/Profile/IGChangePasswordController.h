//
//  IGChangePasswordController.h
//  Juko
//
//  Created by Mountain on 4/19/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGChangePasswordController : UIViewController<UIAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *m_TxtPassword;
@property (weak, nonatomic) IBOutlet UITextField *m_TxtConfirmPassword;


- (IBAction)changePassword:(id)sender;
- (IBAction)goBack:(id)sender;

@end
