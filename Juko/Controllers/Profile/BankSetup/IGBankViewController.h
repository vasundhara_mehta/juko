//
//  IGBankViewController.h
//  Juko
//
//  Created by Mountain on 4/30/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGProfileInfo.h"

@interface IGBankViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) IGProfileInfo* m_clsProfile;

@property (weak, nonatomic) IBOutlet UITextField *m_TxtName;
@property (weak, nonatomic) IBOutlet UITextField *m_TxtRoutingNumber;
@property (weak, nonatomic) IBOutlet UITextField *m_TxtAccountNumber;

- (IBAction)goBack:(id)sender;
- (IBAction)save:(id)sender;

@end
