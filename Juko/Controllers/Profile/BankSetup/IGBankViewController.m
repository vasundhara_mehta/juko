//
//  IGBankViewController.m
//  Juko
//
//  Created by Mountain on 4/30/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGBankViewController.h"
#import "CUtils.h"
#import "const.h"
#import "DataKeeper.h"

#import "IGProgressActivity.h"

@interface IGBankViewController ()

@end

@implementation IGBankViewController
{
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    NSDictionary* dictBank = self.m_clsProfile.m_dictBank;
    [self.m_TxtName setText: [CUtils getStringValueFromDictionary: dictBank KEY: @"full_name"]];
    [self.m_TxtRoutingNumber setText: [CUtils getStringValueFromDictionary: dictBank KEY: @"routing_number"]];
    [self.m_TxtAccountNumber setText: [CUtils getStringValueFromDictionary: dictBank KEY: @"account_number"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)save:(id)sender
{
    if (![self checkParam]) {
        ALERT_SHOW(@"Please input your bank account information")
        return;
    }
    
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(saveBankInfo) toTarget: self withObject: nil];
}

- (BOOL) checkParam
{
    if (self.m_TxtName.text == nil || [self.m_TxtName.text isEqualToString: @""]) {
        return NO;
    }
    if (self.m_TxtRoutingNumber.text == nil || [self.m_TxtRoutingNumber.text isEqualToString: @""]) {
        return NO;
    }
    if (self.m_TxtAccountNumber.text == nil || [self.m_TxtAccountNumber.text isEqualToString: @""]) {
        return NO;
    }
    return YES;
}

- (void) saveBankInfo
{
    NSString* strParam = [self getParam];
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    NSDictionary* dictBank = [clsModel updateBankInfo: strParam];
    [HUD hide: YES];
    
    if (dictBank) {
        self.m_clsProfile.m_dictBank = [NSDictionary dictionaryWithObjectsAndKeys: self.m_TxtName.text, @"full_name", self.m_TxtRoutingNumber.text, @"routing_number", self.m_TxtAccountNumber.text, @"account_number", nil];
        [self performSelectorOnMainThread: @selector(goBack:) withObject: nil waitUntilDone: YES];
    } else {
        ALERT_SHOW(@"Fail to update the bank account\nPlease input valid account information")
    }
}

- (NSString*) getParam
{
    NSString* strParam = [NSString stringWithFormat: @"bank_account_full_name=%@&bank_routing_number=%@&bank_account_number=%@", self.m_TxtName.text, self.m_TxtRoutingNumber.text, self.m_TxtAccountNumber.text];
    return strParam;
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.m_TxtName) {
        [self.m_TxtRoutingNumber becomeFirstResponder];
    } else if (textField == self.m_TxtRoutingNumber) {
        [self.m_TxtAccountNumber becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

@end
