//
//  IGProfileController.h
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"
#import <MessageUI/MFMailComposeViewController.h>

@class IGItemUser;
@class IGUser;
@interface IGProfileController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

//import variable

@property (nonatomic, strong) IGItemUser* m_clsSimpleUser;
@property (nonatomic, strong) IGUser* m_clsUser;

//////////////////
@property int m_nViewKind;

@property int m_nDataKind; //0 - creations, 1 - liked

@property (nonatomic, strong) NSString* m_strUsername;

@property (weak, nonatomic) IBOutlet UIButton *m_btnBack;
@property (weak, nonatomic) IBOutlet UIButton *m_btnSettings;
@property (weak, nonatomic) IBOutlet UIImageView *m_Image_Avatar;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_Username;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_Bio;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_Creations;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_Followers;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_Following;
@property (weak, nonatomic) IBOutlet UIButton *m_ButtonFollow;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_UserID;
@property (weak, nonatomic) IBOutlet UIView *m_View_Desc;
@property (weak, nonatomic) IBOutlet UIView *m_View_Buttons;

@property (weak, nonatomic) IBOutlet UIView *m_View_Tab;
@property (weak, nonatomic) IBOutlet UIButton *m_Button_Creation;
@property (weak, nonatomic) IBOutlet UIButton *m_ButtonLiked;
@property (weak, nonatomic) IBOutlet UIImageView *m_ImgSlide;

@property (weak, nonatomic) IBOutlet UIButton *m_btnScroll;
@property (weak, nonatomic) IBOutlet UIButton *m_btnTile;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

- (IBAction)goBack:(id)sender;


- (IBAction)changeViewMode:(id)sender;
- (IBAction)changeDataKind:(id)sender;
- (IBAction)actionSettings:(id)sender;
- (IBAction)showFollowers:(id)sender;
- (IBAction)showFollowing:(id)sender;
- (IBAction)actionFollow:(id)sender;
- (IBAction)changeProfilePhoto:(id)sender;

@end
