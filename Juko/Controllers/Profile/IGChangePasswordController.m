//
//  IGChangePasswordController.m
//  Juko
//
//  Created by Mountain on 4/19/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGChangePasswordController.h"
#import "const.h"
#import "DataKeeper.h"

#import "IGProgressActivity.h"

@interface IGChangePasswordController ()

@end

@implementation IGChangePasswordController
{
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePassword:(id)sender {
    NSString* strPassword = _m_TxtPassword.text;
    NSString* strCPassword = _m_TxtConfirmPassword.text;
    
    if (strPassword == nil || strCPassword == nil || [strPassword isEqualToString: @""] || [strCPassword isEqualToString: @""]) {
        ALERT_SHOW(@"Password can't be null");
        return;
    } else if (![strCPassword isEqualToString: strPassword]) {
        ALERT_SHOW(@"Confirm Password must be the same as password");
        return;
    }
    
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    if (![clsModel changePassword: strPassword]) {
        ALERT_SHOW(@"Failed to change your password");
        return;
    } else {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Success to change password" message: @"You may use your username and new password" delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [alert show];
    }
    [HUD hide: YES];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _m_TxtPassword) {
        [_m_TxtConfirmPassword becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
