//
//  IGCheckoutController.m
//  Juko
//
//  Created by Mountain on 4/30/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGCheckoutController.h"
#import "IGProgressActivity.h"
#import "STPCard.h"
#import "STPToken.h"
#import "Stripe.h"

#import "const.h"
#import "CUtils.h"
#import "DataKeeper.h"

#import "IGCategory.h"
#import "IGProduct.h"
#import "IGUser.h"

#import "PKTextField.h"

typedef void (^STPTokenBlock)(STPToken *token, NSError *error);

@interface IGCheckoutController ()

@end

@implementation IGCheckoutController
{
    float rHeight;
    NSArray* m_arrayData;
    int m_nRow;
    
    IGCategory* m_clsCategory;
    int m_nSizeTag;
    
    NSString* m_strCardToken;
    BOOL m_FVerifyCard;
    
    float m_rTotalPrice;
    int m_nPickerKind; // 0 - normal, 1 - state
    
    NSArray* m_states;
    
    IGProgressActivity* HUD;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    self.paymentView = [[PKView alloc] initWithFrame:CGRectMake(15, 5, 290, 45)];
    self.paymentView.delegate = self;
    
    [self.m_ViewPurchase addSubview:self.paymentView];
    
    m_states = [[DataKeeper sharedInstance] getStates];
    
    [self showAddressInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

///////////////////////////////////////

#pragma mark - UIPickerViewDelegate

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    if (m_nPickerKind == 0) {
//        return m_arrayData.count;
//    }
    return m_states.count;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary* dictState = m_states[row];
    NSString* strKey = [dictState.allKeys objectAtIndex: 0];
    NSString* strState = [dictState objectForKey: strKey];
    
    if (!self.m_btnBilling.selected) {
        if ([strKey isEqualToString: self.m_txtState.text]) {
            m_nRow = row;
        }
    } else {
        if ([strKey isEqualToString: self.m_txtBillState.text]) {
            m_nRow = row;
        }
    }
    
    return strState;
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    m_nRow = row;
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


///////////////////////////////////////


#pragma mark - Button Actions

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)showStates:(id)sender {
    [self.m_pickerView reloadAllComponents];
    
    if (!self.m_btnBilling.selected) {
        m_nRow = [[DataKeeper sharedInstance] getStateIdByKey: self.m_txtState.text];
    } else {
        m_nRow = [[DataKeeper sharedInstance] getStateIdByKey: self.m_txtBillState.text];
    }

    [self.m_pickerView selectRow: m_nRow inComponent: 0 animated: YES];
    
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_View_Picker.frame;
        rect.origin.x = 0;
        rect.origin.y = self.view.frame.size.height - rect.size.height;
        self.m_View_Picker.frame = rect;
    }];
}

- (IBAction)chooseState:(id)sender
{
    NSDictionary* dictState = m_states[m_nRow];
    NSString* strKey = [dictState.allKeys objectAtIndex: 0];
    
    if (!self.m_btnBilling.selected) {
        [self.m_txtState setText: strKey];
    } else {
        [self.m_txtBillState setText: strKey];
    }
   
    [UIView animateWithDuration: 0.5 animations: ^(void) {
        CGRect rect = self.m_View_Picker.frame;
        rect.origin.y = self.view.frame.size.height;
        self.m_View_Picker.frame = rect;
    }];
}

- (IBAction)nextProcess:(id)sender
{
    [self chooseState: nil];
    
    UITextField* textField;
    if (!self.m_btnBilling.selected) {
        textField = (UITextField*)[self.m_ViewPurchase viewWithTag: 104];
    } else {
        textField = (UITextField*)[self.m_ViewPurchase viewWithTag: 204];
    }
    
    [textField becomeFirstResponder];
}


#pragma mark - Methods

- (void) showAddressInfo
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    IGUser* clsUser = [clsModel getUserInfo];
    UITextField* txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 100];
    [txtName setText: clsUser.m_strName];
    
    UITextField* txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 104];
    [txtCity setText: clsUser.m_strCity];
    
    UITextField* txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 101];
    [txtAddress setText: clsUser.m_strAddress];
    
    UITextField* txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 102];
    [txtAdditionalAddress setText: clsUser.m_strAdditionalAddress];
    
    UITextField* txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 103];
    [txtZip setText: clsUser.m_strZip];
    
    [self.m_txtState setText: clsUser.m_strState];
    
    UITextField* txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 105];
    [txtEmail setText: clsUser.m_strEmail];
    
    UITextField* txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 106];
    [txtPhone setText: clsUser.m_strPhone];
    
    txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 200];
    [txtName setText: clsUser.m_strName];
    
    txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 204];
    [txtCity setText: clsUser.m_strCity];
    
    txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 201];
    [txtAddress setText: clsUser.m_strAddress];
    
    txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 202];
    [txtAdditionalAddress setText: clsUser.m_strAdditionalAddress];
    
    txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 203];
    [txtZip setText: clsUser.m_strZip];
    
    [self.m_txtBillState setText: clsUser.m_strState];
    
    txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 205];
    [txtEmail setText: clsUser.m_strEmail];
    
    txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 206];
    [txtPhone setText: clsUser.m_strPhone];
}

- (IBAction)actionPurchase:(id)sender
{
    if (!m_FVerifyCard) {
        ALERT_SHOW(@"Please input your card info");
        return;
    }
    if (![self checkAddress]) {
        return;
    }

    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(purchaseProcess) toTarget: self withObject: nil];
}

- (void) closePurchaseView
{
    NSArray* vcArray = self.navigationController.viewControllers;
    [self.navigationController popToViewController: vcArray[vcArray.count - 3] animated: YES];
}

- (void) purchaseProcess
{
    NSString* strParam = [self getPurchaseParam];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FResult = [clsModel cartCheckout: m_strCardToken ParamInfo: strParam];
    [HUD hide: YES];
    
    if (!FResult) {
        ALERT_SHOW(@"Please check your payment parameters\nAlso you must input card info again");
    } else {
        ALERT_SHOW(@"Thank you for your order!");
        [NSThread detachNewThreadSelector: @selector(reloadCart) toTarget: self withObject: nil];
    }
}

- (void) reloadCart
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FRes = [clsModel loadCart];
    if (!FRes) {
        NSLog(@"Fail to reload cart");
    }

    [self performSelectorOnMainThread: @selector(closePurchaseView) withObject: nil waitUntilDone: YES];
}

- (NSString*) getPurchaseParam
{
    NSMutableString* strParam = [[NSMutableString alloc] init];
    
    UITextField* txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 100];
    [strParam appendString: [NSString stringWithFormat: @"name=%@", txtName.text]];
    
    UITextField* txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 104];
    UITextField* txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 101];
    UITextField* txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 102];
    UITextField* txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 103];
    if (txtAdditionalAddress.text == nil || [txtAdditionalAddress.text isEqualToString: @""]) {
        [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
    } else {
        [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Apt/Suite/Bldg : %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtAdditionalAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
    }
    
    UITextField* txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 105];
    [strParam appendString: [NSString stringWithFormat: @"&email=%@", txtEmail.text]];
    
    UITextField* txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 106];
    [strParam appendString: [NSString stringWithFormat: @"&phone=%@", txtPhone.text]];
    
    if (self.m_btnBilling.selected) {
        txtName = (UITextField*)[self.m_ViewPurchase viewWithTag: 200];
        [strParam appendString: [NSString stringWithFormat: @"&billing_address=(name: %@", txtName.text]];
        
        UITextField* txtCity = (UITextField*)[self.m_ViewPurchase viewWithTag: 204];
        UITextField* txtAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 201];
        UITextField* txtAdditionalAddress = (UITextField*)[self.m_ViewPurchase viewWithTag: 202];
        UITextField* txtZip = (UITextField*)[self.m_ViewPurchase viewWithTag: 203];
        if (txtAdditionalAddress.text == nil || [txtAdditionalAddress.text isEqualToString: @""]) {
            [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
        } else {
            [strParam appendString: [NSString stringWithFormat: @"&address=Address: %@, Apt/Suite/Bldg : %@, Zip : %@, State: %@, City: %@", txtAddress.text, txtAdditionalAddress.text, txtZip.text, self.m_txtState.text, txtCity.text]];
        }
        
        UITextField* txtEmail = (UITextField*)[self.m_ViewPurchase viewWithTag: 205];
        [strParam appendString: [NSString stringWithFormat: @"&email=%@", txtEmail.text]];
        
        UITextField* txtPhone = (UITextField*)[self.m_ViewPurchase viewWithTag: 206];
        [strParam appendString: [NSString stringWithFormat: @"&phone=%@", txtPhone.text]];
    }
    
    return strParam;
}

#define TEXT_COUNT 7
- (BOOL) checkAddress
{
    for (int nIdx = 0; nIdx < TEXT_COUNT; nIdx ++) {
        UITextField* txtField = (UITextField*)[self.m_ViewPurchase viewWithTag: (100 + nIdx)];
        if (txtField.tag == 102 || txtField.tag == 104) {
            continue;
        }
        if (txtField.text == nil || [txtField.text isEqualToString: @""]) {
            ALERT_SHOW(@"Please fill all information");
            return NO;
        }
        if (self.m_btnBilling.selected) {
            txtField = (UITextField*)[self.m_ViewPurchase viewWithTag: (200 + nIdx)];
            if (txtField.tag == 202 || txtField.tag == 204) {
                continue;
            }
            if (txtField.text == nil || [txtField.text isEqualToString: @""]) {
                ALERT_SHOW(@"Please fill all information");
                return NO;
            }
        }
    }
    if (self.m_txtState.text == nil || [self.m_txtState.text isEqualToString: @""]) {
        ALERT_SHOW(@"Choose State");
        return NO;
    }
    
    if (self.m_btnBilling.selected) {
        if (self.m_txtBillState.text == nil || [self.m_txtBillState.text isEqualToString: @""]) {
            ALERT_SHOW(@"Choose Billing State");
            return NO;
        }
    }
    
    return YES;
}

//////////////////////////////

- (IBAction)dismissKeyboard:(id)sender {
    for (int nTag = 100; nTag < 107; nTag ++) {
        UITextField* textField = (UITextField*)[self.m_ViewPurchase viewWithTag: nTag];
        [textField resignFirstResponder];
    }
    
    [self.paymentView.cardCVCField resignFirstResponder];
    [self.paymentView.cardExpiryField resignFirstResponder];
    [self.paymentView.cardNumberField resignFirstResponder];
    
    self.m_btnDismissKeyboard.hidden = YES;
    
    CGRect rect = self.m_ViewPurchase.frame;
    rect.origin.y = 70;
    self.m_ViewPurchase.frame = rect;
}

- (void) showDismissKeyboardButton
{
    self.m_btnDismissKeyboard.hidden = NO;
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    int nTag = textField.tag + 1;
    UITextField* nextField = (UITextField*)[self.m_ViewPurchase viewWithTag: nTag];
    if (nextField) {
        if (nextField.tag == 104 || nextField.tag == 204) {
            [textField resignFirstResponder];
            [self showStates: nil];
            return YES;
        }
        [nextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        
        CGRect rect = self.m_ViewPurchase.frame;
        rect.origin.y = 70;
        self.m_ViewPurchase.frame = rect;
        
        self.m_btnDismissKeyboard.hidden = YES;
    }
    
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    [self showDismissKeyboardButton];
    
    CGRect rect = textField.superview.frame;
    rect.origin.y += (self.m_ViewPurchase.frame.origin.y + textField.superview.superview.frame.origin.y);
    if (rect.origin.y + rect.size.height > self.view.frame.size.height - KEYBOARD_HEIGHT) {
        float rOffsetY = rect.origin.y + rect.size.height - (self.view.frame.size.height - KEYBOARD_HEIGHT);
        
        rect = self.m_ViewPurchase.frame;
        rect.origin.y -= rOffsetY;
        self.m_ViewPurchase.frame = rect;
    }
}

#pragma mark - PaymentViewDelegate

- (void) paymentView:(PKView *)paymentView withCard:(PKCard *)card isValid:(BOOL)valid
{
    NSLog(@"Card last4: %@", card.last4);
    NSLog(@"Card expiry: %lu/%lu", (unsigned long)card.expMonth, (unsigned long)card.expYear);
    NSLog(@"Card cvc: %@", card.cvc);
    
    [[NSUserDefaults standardUserDefaults] setValue:card.last4 forKey:@"card.last4"];
    
    [IGProgressActivity showHUDAddedTo:self.view animated:YES];
    
    [self createToken:^(STPToken *token, NSError *error) {
        [IGProgressActivity hideHUDForView:self.view animated:YES];
        
        if (error) {
            [self hasError:error];
        } else {
            [self hasToken:token];
        }
    }];
}

- (void)createToken: (STPTokenBlock)block
{
    //    if (pending) return;
    
    if (![self.paymentView isValid]) {
//        NSError *error = [[NSError alloc] initWithDomain:StripeDomain
//                                                    code:STPCardError
//                                                userInfo:@{NSLocalizedDescriptionKey : STPCardErrorUserMessage}];
//        
//        block(nil, error);
        return;
    }
    
    //    [self endEditing:YES];
    
    PKCard *card = self.paymentView.card;
    STPCard *scard = [[STPCard alloc] init];
    
    scard.number = card.number;
    scard.expMonth = card.expMonth;
    scard.expYear = card.expYear;
    scard.cvc = card.cvc;
    
    //    [self pendingHandler:YES];
    
    NSString* STRIPE_PUBLISHABLE_KEY = [[DataKeeper sharedInstance] getStripeKey];
    
    [Stripe createTokenWithCard:scard
                 publishableKey:STRIPE_PUBLISHABLE_KEY
                     completion:^(STPToken *token, NSError *error) {
                         //                         [self pendingHandler:NO];
                         block(token, error);
                     }];
    
}

- (void)hasError:(NSError *)error
{
    [self dismissKeyboard: nil];
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                      message:[error localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
    [message show];
}

- (void)hasToken:(STPToken *)token
{
    [self dismissKeyboard: nil];
    
    m_strCardToken = [token.tokenId copy];
    m_FVerifyCard = YES;
    
    NSLog(@"Received token %@", token.tokenId);
}

//////when billing address is not same as shipping address
- (IBAction)actionBillingAddress:(id)sender
{
    self.m_btnBilling.selected = !self.m_btnBilling.selected;
    if (self.m_btnBilling.selected) {
        [self.m_LabelAddress setText: @"Billing Information"];
        UIView* viewShip = [self.m_ViewPurchase viewWithTag: 1];
        UIView* viewBill = [self.m_ViewPurchase viewWithTag: 2];
        [UIView animateWithDuration: 0.5 animations: ^(void) {
            CGRect rect = viewShip.frame;
            rect.origin.x = -320;
            viewShip.frame = rect;
            
            rect.origin.x = 0;
            viewBill.frame = rect;
        }];
    } else {
        [self.m_LabelAddress setText: @"Shipping Information"];
        UIView* viewShip = [self.m_ViewPurchase viewWithTag: 1];
        UIView* viewBill = [self.m_ViewPurchase viewWithTag: 2];
        [UIView animateWithDuration: 0.5 animations: ^(void) {
            CGRect rect = viewShip.frame;
            rect.origin.x = 0;
            viewShip.frame = rect;
            
            rect.origin.x = 320;
            viewBill.frame = rect;
        }];
    }
}

@end
