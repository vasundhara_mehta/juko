//
//  IGCartViewController.m
//  Juko
//
//  Created by Mountain on 4/29/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGCartViewController.h"
#import "DataKeeper.h"
#import "const.h"
#import "CUtils.h"

#import "IGCartCell.h"

#import "IGProgressActivity.h"
#import "UIImageView+WebCache.h"

#import "IGCheckoutController.h"

#define CELL_ID @"CartCell"

@interface IGCartViewController ()

@end

@implementation IGCartViewController
{
    IGProgressActivity* HUD;
    
    NSMutableArray* m_cartItems;
    
    UITextField* m_textField;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID bundle: nil] forCellReuseIdentifier: CELL_ID];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(showKeyboard:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(hideKeyboard:) name: UIKeyboardWillHideNotification object: nil];
    
    m_cartItems = [[DataKeeper sharedInstance] getCart];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: UIKeyboardWillHideNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: UIKeyboardWillShowNotification object: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)actionEdit:(id)sender {
    if ([self.m_btnEdit.titleLabel.text isEqualToString: @"Edit"]) {
        [self.m_btnEdit setTitle: @"Done" forState: UIControlStateNormal];
        
        [self.m_tableView setEditing:YES animated:YES];
        [self.m_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        [self.m_btnEdit setTitle: @"Edit" forState: UIControlStateNormal];
        
        [self.m_tableView setEditing:NO animated:YES];
        [self.m_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (IBAction)actionCheckout:(id)sender {
    if (m_cartItems == nil || m_cartItems.count == 0) {
        return;
    }
    IGCheckoutController* vc = [[IGCheckoutController alloc] initWithNibName: @"IGCheckoutController" bundle: nil];
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)dismissKeyboard:(id)sender {
    [m_textField resignFirstResponder];
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSString*) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"";
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self calcTotal];
    return (m_cartItems ? m_cartItems.count : 0);
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID];
    cell.tag = indexPath.row + 10000;
    
    IGCartCell* cartCell = (IGCartCell*) cell;
    cartCell.delegate = self;
    
    NSDictionary* dictItem = m_cartItems[indexPath.row];
    [self configureCell: cell data: dictItem];
    
    return cell;
}

- (void) removeCell: (UITableViewCell*) cell
{
    NSIndexPath* indexPath = [self.m_tableView indexPathForCell: cell];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    int nItemId = [CUtils getIntValueFromDictionary: m_cartItems[indexPath.row] KEY: @"id"];
    
    [NSThread detachNewThreadSelector: @selector(showActivity) toTarget: self withObject: nil];
    BOOL FRes = [clsModel deleteCartItem: nItemId];
    [HUD hide: YES];

    if (FRes) {
        [m_cartItems removeObjectAtIndex: indexPath.row];
        
        [self.m_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    } else {
        ALERT_SHOW(@"Failed to remove the cart item");
    }
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) configureCell: (UITableViewCell*) cell data: (NSDictionary*) dictCartItem
{
    UIImageView* imgView = (UIImageView*)[cell viewWithTag: 1];
    UILabel* labelName = (UILabel*)[cell viewWithTag: 2];
    UILabel* labelPrice = (UILabel*)[cell viewWithTag: 3];
    UITextField* txtQuantity = (UITextField*)[cell viewWithTag: 4];
    
    NSString* strImageUrl= @"";
    NSArray* arrayUrl = [[dictCartItem objectForKey: @"item"] objectForKey: @"feed_image_url"];
    if (arrayUrl && arrayUrl.count > 0) {
        strImageUrl = arrayUrl[0];
    }
    [imgView setImageWithURL: [NSURL URLWithString: strImageUrl]];
    
    NSDictionary* dictProduct = [dictCartItem objectForKey: @"product"];
    NSDictionary* dictCategory = [dictCartItem objectForKey: @"category"];
    
    NSString* strName = [CUtils getStringValueFromDictionary: dictProduct KEY: @"name"];
    int nCategoryIndex = [CUtils getIntValueFromDictionary: dictCategory KEY: @"id"];
    
    NSString* strTitle = @"";
    NSString* strAddition = [CUtils getStringValueFromDictionary: dictCartItem KEY: @"additional_info"];
    if (strAddition && ![strAddition isEqualToString: @""]) {
        strTitle = [NSString stringWithFormat: @"%@ (%@)", [self getRealProductName: strName CategoryIndex: nCategoryIndex], strAddition];
    } else {
        strTitle = [self getRealProductName: strName CategoryIndex: nCategoryIndex];
    }
    
    [labelName setText: strTitle];
    
    NSDictionary* dictPrice = [[dictCartItem objectForKey: @"item"] objectForKey: @"price"];
    float rPrice = [CUtils getFloatValueFromDictionary: dictPrice KEY: @"amount"];
    [labelPrice setText: [NSString stringWithFormat: @"$%.2f", rPrice]];
    
    int nQuantity = [CUtils getIntValueFromDictionary: dictCartItem KEY: @"quantity"];
    [txtQuantity setText: [NSString stringWithFormat: @"%d", nQuantity]];
    
    txtQuantity.delegate = self;
    txtQuantity.keyboardType = UIKeyboardTypeNumberPad;
}

- (NSString*) getRealProductName: (NSString*) strName CategoryIndex: (int) nCategoryIndex
{
    NSString* strRealName;
    NSString* strTempName = [[[strName stringByReplacingOccurrencesOfString: @"Vertical" withString: @""] stringByReplacingOccurrencesOfString: @"Horizontal" withString: @""] stringByReplacingOccurrencesOfString: @"Skin" withString: @""];
    if (nCategoryIndex == ECATEGORY_SHIRTS) {
        strRealName = [NSString stringWithFormat: @"%@ T-Shirt", [strTempName substringToIndex: strTempName.length - 1]];
    } else {
        strRealName = [strTempName copy];
    }
    
    return strRealName;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
}

#pragma mark - UITextfieldDelegate

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    m_textField = textField;
    [textField setSelectedTextRange: [textField textRangeFromPosition: textField.beginningOfDocument toPosition: textField.endOfDocument]];
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    UITableViewCell* cell = (UITableViewCell*)textField.superview.superview.superview;
    int nRow = (int)cell.tag - 10000;
    
    NSDictionary* dictCartItem = m_cartItems[nRow];
    int nQuantity = [CUtils getIntValueFromDictionary: dictCartItem KEY: @"quantity"];
    int nNewQuantity = (int)[textField.text integerValue];
    
    if (nNewQuantity == 0) {
        [self removeCell: cell];
    } else if (nNewQuantity != nQuantity) {
        [NSThread detachNewThreadSelector: @selector(updateCartItem:) toTarget: self withObject: cell];
    }
}

- (void) updateCartItem: (UITableViewCell*) cell
{
    int nRow = (int)cell.tag - 10000;
    
    NSDictionary* dictCartItem = m_cartItems[nRow];
    
    UITextField* textField = (UITextField*)[cell viewWithTag: 4];
    int nNewQuantity = [textField.text intValue];
    
    int nCartItemId = [CUtils getIntValueFromDictionary: dictCartItem KEY: @"id"];
    NSString* strParam = [NSString stringWithFormat: @"quantity=%d", nNewQuantity];
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    BOOL FRes = [clsModel updateCartItem: nCartItemId Param: strParam];
    if (FRes) {
        [dictCartItem setValue: [NSString stringWithFormat: @"%d", nNewQuantity] forKey: @"quantity"];
    } else {
        int nQuantity = [CUtils getIntValueFromDictionary: dictCartItem KEY: @"quantity"];
        [textField setText: [NSString stringWithFormat: @"%d", nQuantity]];
    }
    
    [self calcTotal];
}

- (void) showKeyboard: (NSNotification*) noti
{
    CGRect keyboardRect = [[noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect toolRect = self.m_Toolbar.frame;
    toolRect.origin.y = self.view.frame.size.height;
    self.m_Toolbar.frame = toolRect;
    
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect toolRect = self.m_Toolbar.frame;
        toolRect.origin.y = self.view.frame.size.height - keyboardRect.size.height - toolRect.size.height;
        self.m_Toolbar.frame = toolRect;
    }];
}

- (void) hideKeyboard: (NSNotification*) noti
{
    [UIView animateWithDuration: 0.3 animations: ^(void) {
        CGRect toolRect = self.m_Toolbar.frame;
        toolRect.origin.y = self.view.frame.size.height;
        self.m_Toolbar.frame = toolRect;
    }];
}

#pragma mark - Calculation Total Money

- (void) calcTotal
{
    float rSubtotal = 0.0f;
    float rShipping = 0.0f;
    
    for (NSDictionary* dictCartItem in m_cartItems) {
        NSDictionary* dictItem = [dictCartItem objectForKey: @"item"];
        float rPrice = [CUtils getFloatValueFromDictionary: [dictItem objectForKey: @"price"] KEY: @"amount"];
        int nQuantity = [CUtils getIntValueFromDictionary: dictCartItem KEY: @"quantity"];
        rSubtotal += (rPrice * nQuantity);
        
        NSDictionary* dictProduct = [dictCartItem objectForKey: @"product"];
        rShipping += ([CUtils getFloatValueFromDictionary: dictProduct KEY: @"shipping_cost"] * nQuantity);
    }
    
    [self.m_LabelSubTotal setText: [NSString stringWithFormat: @"Subtotal: $%.2f", rSubtotal]];
    [self.m_LabelShipping setText: [NSString stringWithFormat: @"Shipping: $%.2f", rShipping]];
    [self.m_LabelTotal setText: [NSString stringWithFormat: @"Total: $%.2f", (rSubtotal + rShipping)]];
}

@end
