//
//  IGFollowViewController.h
//  Juko
//
//  Created by Mountain on 2/22/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGProfileInfo.h"

@interface IGFollowViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

//import

@property int m_nViewKind; //0 - followers, 1 - following
@property int m_nUserIndex;
@property (nonatomic, strong) IGProfileInfo* m_clsProfile;

//members

@property (weak, nonatomic) IBOutlet UILabel *m_Label_Title;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

//actions
- (IBAction)goBack:(id)sender;


@end
