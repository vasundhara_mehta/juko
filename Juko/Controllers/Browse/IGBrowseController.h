//
//  IGBrowseController.h
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"

@interface IGBrowseController : UIViewController<StreamViewDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet StreamView *m_BrowseView;
@property (weak, nonatomic) IBOutlet UISearchBar *m_searchField;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

//self variables

@property (nonatomic, strong) NSString* m_strCellID;

@end
