//
//  IGFilterController.m
//  Juko
//
//  Created by Mountain on 3/11/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGFilterController.h"
#import "DataKeeper.h"
#import "const.h"
#import "IGCategory.h"

@interface IGFilterController ()

@end

@implementation IGFilterController
{
    NSArray* m_categories;
    
    BOOL m_FChangeData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.m_ImageBackground setImage: _m_bgImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender
{
    if (m_FChangeData && [_delegate respondsToSelector: @selector(setFilterCategory)]) {
        [_delegate performSelectorOnMainThread: @selector(setFilterCategory) withObject: nil waitUntilDone: YES];
    }
    
    [self.overlayController dismissOverlay: YES];
}

#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    m_categories = [clsModel getCategoryList];
    return (m_categories == nil) ? 0 : m_categories.count;
}

#define CELL_HEIGHT 40
#define CHECK_WIDTH 24
#define CHECK_HEIGHT 18

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: @"cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"cell"];
        UIImageView* imgCheck = [[UIImageView alloc] initWithFrame: CGRectMake(cell.frame.size.width - 50, 11, CHECK_WIDTH, CHECK_HEIGHT)];
        imgCheck.clipsToBounds = YES;
        imgCheck.contentMode = UIViewContentModeScaleAspectFit;
        imgCheck.image = [UIImage imageNamed: @"mark_check.png"];
        [cell addSubview: imgCheck];
        imgCheck.alpha = 0;
        imgCheck.tag = 100;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    IGCategory* clsCategory = m_categories[indexPath.row];
    if (clsCategory.m_FChoose) {
        [cell.textLabel setTextColor: [UIColor colorWithRed: 27.0/255.0 green: 163.0/255.0 blue: 203.0/255.0 alpha: 1.0]];
        ((UIImageView*)[cell viewWithTag: 100]).alpha = 1.0;
    } else {
        [cell.textLabel setTextColor: [UIColor colorWithRed: 64.0/255.0 green: 64.0/255.0 blue: 64.0/255.0 alpha: 1.0]];
        ((UIImageView*)[cell viewWithTag: 100]).alpha = 0.0;
    }
    cell.textLabel.text = clsCategory.m_strName;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    m_FChangeData = YES;
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    IGCategory* clsCategory = m_categories[indexPath.row];
    clsCategory.m_FChoose = !clsCategory.m_FChoose;
    UITableViewCell* cell = [tableView cellForRowAtIndexPath: indexPath];
    if (clsCategory.m_FChoose) {
        [cell.textLabel setTextColor: [UIColor colorWithRed: 27.0/255.0 green: 163.0/255.0 blue: 203.0/255.0 alpha: 1.0]];
        ((UIImageView*)[cell viewWithTag: 100]).alpha = 1.0;
    } else {
        [cell.textLabel setTextColor: [UIColor colorWithRed: 64.0/255.0 green: 64.0/255.0 blue: 64.0/255.0 alpha: 1.0]];
        ((UIImageView*)[cell viewWithTag: 100]).alpha = 0.0;
    }
}

@end
