//
//  IGItemViewController.m
//  Juko
//
//  Created by Mountain on 2/8/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGItemViewController.h"

#import "DataKeeper.h"
#import "const.h"
#import "CUtils.h"
#import "IGItemUser.h"

#import "FeedCellView.h"

#import "IGCommentsViewController.h"
#import "IGProfileController.h"
#import "IGTagViewController.h"
#import "IGAppDelegate.h"
#import "IGPurchaseController.h"
#import "IGLikersViewController.h"

#import "UIImageView+WebCache.h"

#import <Social/SLServiceTypes.h>
#import <Social/SLComposeViewController.h>

#import "IGProgressActivity.h"

@interface IGItemViewController ()

@end

@implementation IGItemViewController
{
    IGItem* m_clsItem;
    UIImage* m_itemImage;
    UIDocumentInteractionController* docFile;
    
    IGProgressActivity* HUD;
}

@synthesize m_clsFeaturedItem;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    CGRect rect;
    UIView* headerView = [[[NSBundle mainBundle] loadNibNamed: @"ItemHeaderView" owner: self options: nil] objectAtIndex: 0];
    [self.m_ItemView addSubview: headerView];
    rect = headerView.frame;
    rect.origin = CGPointMake(0, 0);
    [headerView setFrame: rect];
    
    FeedCellView* cell = (FeedCellView*)[[[NSBundle mainBundle] loadNibNamed: @"FeedCell" owner: self options: nil] objectAtIndex: 0];
    cell.m_FShowDeleteButton = self.m_FShowDeleteButton;
    cell.delegate = self;
    [self.m_ItemView addSubview: cell];
    rect = cell.frame;
    rect.origin.y = headerView.frame.size.height;
    [cell setFrame: rect];
    
    _m_HeaderView = headerView;
    _m_ItemCell = cell;
    
    [self configure];
}

- (void) configure
{
    //user info configuration
    UIImageView* avatarView = (UIImageView*)[_m_HeaderView viewWithTag: 1];
    [avatarView setImageWithURL: [NSURL URLWithString: m_clsFeaturedItem.m_clsUser.m_strImageThumbUrl]];
    UIButton* btnName = (UIButton*)[_m_HeaderView viewWithTag: 2];
    [btnName addTarget: self action: @selector(clickUser:) forControlEvents: UIControlEventTouchUpInside];
    [btnName setTitle: m_clsFeaturedItem.m_clsUser.m_strUsername forState: UIControlStateNormal];
    
    UILabel* labelTime = (UILabel*)[_m_HeaderView viewWithTag: 3];
    UIImageView* imgClock = (UIImageView*)[_m_HeaderView viewWithTag: 4];
    [labelTime setText: m_clsFeaturedItem.m_strTime];
    [labelTime sizeToFit];
    CGRect rect = labelTime.frame;
    rect.origin.x = 310.0 - rect.size.width;
    labelTime.frame = rect;

    [imgClock setFrame: CGRectMake(rect.origin.x - imgClock.frame.size.width - 3, imgClock.frame.origin.y, imgClock.frame.size.width, imgClock.frame.size.height)];
    if (m_clsFeaturedItem.m_strTime == nil || [m_clsFeaturedItem.m_strTime isEqualToString: @""]) {
        imgClock.hidden = YES;
    } else {
        imgClock.hidden = NO;
    }


    //featured item configuration
    [_m_ItemCell configureCell: m_clsFeaturedItem];
    
    [_m_ItemView setContentSize: CGSizeMake(_m_ItemCell.frame.size.width, _m_ItemCell.frame.size.height + 10)];
}

- (void) clickUser: (id) sender
{
    [self showUser: m_clsFeaturedItem.m_clsUser.m_strUsername];
}

#pragma mark - FeedCell delegate methods

- (void) refreshCells: (FeedCellView*) cell
{
    [cell configureCell: m_clsFeaturedItem];
}


- (void) showAllComments: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_READ;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //[self.navigationController pushViewController: vc animated: YES];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) showUser: (NSString*) strUsername
{
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_strUsername = strUsername;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showTag: (NSString*) strTag
{
    NSString* strHashTag = strTag;
    if ([[strTag substringToIndex: 1] isEqualToString: @"#"]) {
        strHashTag = [strTag substringFromIndex: 1];
    }
    IGTagViewController* vc = [[IGTagViewController alloc] initWithNibName: @"IGTagViewController" bundle: nil];
    vc.m_strTag = strHashTag;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - action of item

- (void) actionLike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(likeProcess:) toTarget: self withObject: clsItem];
    //    [self likeProcess: clsItem];
}

- (void) showLikers: (IGItem*) clsItem
{
    IGLikersViewController* vc = [[IGLikersViewController alloc] initWithNibName: @"IGLikersViewController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) likeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: YES];
    if (!FSuccess) {
    }
}

- (void) actionUnlike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(unlikeProcess:) toTarget: self withObject: clsItem];
    //    [self unlikeProcess: clsItem];
}

- (void) unlikeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: NO];
    if (!FSuccess) {
    }
}

- (void) actionComment: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_WRITE;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //    [self.navigationController pushViewController: vc animated: YES];
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) actionPurchase: (IGItem*) clsItem
{
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    overlay.dismissUponTouchMask = NO;
    
    IGPurchaseController* vc = [[IGPurchaseController alloc] initWithNibName: @"IGPurchaseController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    [overlay presentOverlayOnView: appDelegate.window withContent:vc animated: YES];
}

- (void) actionShare: (NSDictionary*) dict
{
    m_itemImage = [dict objectForKey: @"image"];
    m_clsItem = [dict objectForKey: @"item"];
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle: @"" delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Share on Facebook", @"Share on Mail", @"Share on Instagram", @"Share on Twitter", nil];
    [sheet showInView: self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self actionFbShare];
            break;
        case 1:
            [self actionMailShare];
            break;
        case 2:
            [self actionIGShare];
            break;
        case 3:
            [self actionTwShare];
            break;
        default:
            break;
    }
}

- (IBAction)actionFbShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

- (IBAction)actionMailShare {
    if (![MFMailComposeViewController canSendMail]) {
        ALERT_SHOW(@"Please setup your mail account in Settings of your device");
        return;
    }
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
    //    [mail setToRecipients:[NSArray arrayWithObject:@"info@thegrint.com"]];
    [mail setSubject: m_clsItem.m_strName];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: m_itemImage], 1.0) mimeType: @"image/jpg" fileName: m_clsItem.m_strName];
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

- (IBAction)actionIGShare {
    UIImage* image;
    image = [self imageForShare: m_itemImage];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];
        NSData *imageData=UIImagePNGRepresentation(image);
        [imageData writeToFile:saveImagePath atomically:YES];
        
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        
        if (docFile == Nil) {
            docFile = [[UIDocumentInteractionController alloc] init];
        }
        docFile.delegate=self;
        docFile.UTI=@"com.instagram.photo";
        
        NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
        docFile.annotation=[NSDictionary dictionaryWithObjectsAndKeys: strText, @"InstagramCaption", nil];
        
        [docFile setURL:imageURL];
        
        [docFile presentOpenInMenuFromRect: CGRectZero inView: self.view animated: YES];
    }
    else
    {
        NSLog (@"Instagram is not available");
    }
}

- (IBAction)actionTwShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeTwitter];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

#define SHARE_WIDTH 200
#define SHARE_HEIGHT 200
- (UIImage*) imageForShare : (UIImage*) orgImage
{
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, SHARE_WIDTH, SHARE_HEIGHT)];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.opaque = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView setImage: orgImage];
    return [CUtils imageWithView: imgView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - remove item

- (void) removeItem: (IGItem*) clsItem
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"" message: @"Are you sure to remove this item?" delegate:self cancelButtonTitle: @"Yes" otherButtonTitles: @"No", nil];
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) { //yes
        [self goBack: nil];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"delete_item" object: m_clsFeaturedItem];
    }
}

@end
