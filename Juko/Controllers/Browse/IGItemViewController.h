//
//  IGItemViewController.h
//  Juko
//
//  Created by Mountain on 2/8/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "IGItem.h"


@class FeedCellView;
@interface IGItemViewController : UIViewController<UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

//import variables
@property(nonatomic, strong) IGItem* m_clsFeaturedItem;
@property BOOL m_FShowDeleteButton;

//UI variables
@property (weak, nonatomic) IBOutlet UIScrollView *m_ItemView;
@property (nonatomic, strong) UIView* m_HeaderView;
@property (nonatomic, strong) FeedCellView* m_ItemCell;

//action
- (IBAction)goBack:(id)sender;

@end
