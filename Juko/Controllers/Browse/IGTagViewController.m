//
//  IGTagViewController.m
//  Juko
//
//  Created by Mountain on 2/5/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGTagViewController.h"

#import "const.h"
#import "DataKeeper.h"
#import "IGItem.h"
#import "IGUser.h"
#import "IGItemUser.h"
#import "IGComment.h"

#import "CUtils.h"

#import "ItemCell.h"
#import "STTweetLabel.h"
#import "FeedCellView.h"

#import "IGItemViewController.h"
#import "IGCommentsViewController.h"
#import "IGProfileController.h"
#import "IGLikersViewController.h"
#import "IGAppDelegate.h"
#import "IGPurchaseController.h"
#import "UIXOverlayController.h"
#import "IGProgressActivity.h"

#import "IGFilterController.h"

#import "UIImageView+WebCache.h"
#import "UIImage+StackBlur.h"

#import <Social/SLServiceTypes.h>
#import <Social/SLComposeViewController.h>

@interface IGTagViewController ()

@end

@implementation IGTagViewController
{
    IGProgressActivity* HUD;
    NSMutableArray* m_arrayData;
    BOOL m_FEndData;
    int m_nPage;
    
    IGItem* m_clsItem;
    UIImage* m_itemImage;
    UIDocumentInteractionController* docFile;
}

@synthesize m_strTag;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        m_arrayData = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    _m_StreamView.scrollsToTop = YES;
    
    _m_StreamView.cellPadding = 4.0f;
    _m_StreamView.columnPadding = 4.0f;
    
    _m_StreamView.delegate = self;
    
    [_m_StreamView setContentInset: UIEdgeInsetsMake(0, 0, 50, 0)];
    
    [self.m_tableView registerNib: [UINib nibWithNibName: CELL_ID_FEED bundle: nil] forCellReuseIdentifier: CELL_ID_FEED];
    
    self.m_tableView.alpha = 0;
    
    self.m_Label_Title.text = [NSString stringWithFormat: @"#%@", m_strTag];
    
    [[DataKeeper sharedInstance] clearChoseStatus];

    m_nPage = 0;
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadItemData) toTarget: self withObject: nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
}

- (void) loadItemData
{
    if (m_nPage == 0) {
        [m_arrayData removeAllObjects];
    }
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    
    NSArray* arrayRawItems = [clsModel loadTaggedItems: m_strTag PAGE: m_nPage];
    if (arrayRawItems == nil || arrayRawItems.count == 0) {
        m_FEndData = YES;
    } else {
        m_nPage ++;
    }
    
    for (NSDictionary* dictItem in arrayRawItems) {
        IGItem* clsItem = [[IGItem alloc] initWithDictionay: [dictItem objectForKey: @"item"]];
        [m_arrayData addObject: clsItem];
    }
    
    [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
}

- (void) refreshData
{
    if (_m_btnScroll.selected) {
        [_m_tableView reloadData];
    } else {
        [_m_StreamView reloadData];
    }
    [HUD hide: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - StreamView Delegate

- (NSInteger)numberOfCellsInStreamView:(StreamView *)streamView
{
    NSInteger nCount = m_arrayData ? m_arrayData.count : 0;
    
    return nCount;
}

- (NSInteger)numberOfColumnsInStreamView:(StreamView *)streamView
{
    return 2;
}

- (UIView *)streamView:(StreamView *)stream cellAtIndex:(NSInteger)index
{
    static NSString *CellID1 = @"MyCell1";
    static NSString *CellID2 = @"MyCell2";
    
    BOOL redCell = index % 2 == 0;
    NSString *CellID =  redCell ? CellID2 : CellID1;
    
    ItemCell *cell;
    
    cell = (ItemCell *)[stream dequeueReusableCellWithIdentifier:CellID];
    
    if (cell == nil) {
        cell = [[ItemCell alloc] initWithFrame:CGRectMake(0, 0, 154, 154)];
        cell.reuseIdentifier = CellID;
    }
    
    cell.delegate = self;
    cell.tag = index + 1000;
    
    [cell configureCell: [m_arrayData objectAtIndex: index]];
    
    if (index == m_arrayData.count - 1) {
        if (!m_FEndData) {
            [NSThread detachNewThreadSelector:@selector(loadItemData) toTarget: self withObject: nil];
        }
    }
    
    return cell;
}

- (CGFloat)streamView:(StreamView *)streamView heightForCellAtIndex:(NSInteger)index
{
    return 154;
}

- (UIView *)headerForStreamView:(StreamView *)streamView
{
    return nil;
    /*
     CSearchCell *header = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     header.label.text = @"This is the header";
     
     return header;
     */
}

- (UIView *)footerForStreamView:(StreamView *)streamView
{
    return nil;
    
    /*
     if (page <= MaxPage) {
     CSearchCell *footer = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     footer.label.text = @"This is the footer";
     
     return footer;
     } else {
     return nil;
     }
     */
}

#pragma mark - cell click

- (void) touchedCell: (id) sender
{
    IGItem* clsItem = (IGItem*)sender;
    //int nIndex = ((UIView*)sender).superview.tag - 1000;
    //IGItem* clsItem =[dataSource.m_products objectAtIndex: nIndex];
    IGItemViewController* vc = [[IGItemViewController alloc] initWithNibName: @"IGItemViewController" bundle: nil];
    vc.m_clsFeaturedItem = clsItem;
    [self.navigationController pushViewController: vc animated: YES];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

#pragma mark - Change View Mode

- (IBAction)changeViewMode:(id)sender {
    UIButton* button = (UIButton*)sender;
    if (button.selected) {
        return;
    }
    
    if (button == _m_btnScroll) {
        _m_btnScroll.selected = YES;
        _m_btnTile.selected = NO;
        
        self.m_StreamView.alpha = 0;
        self.m_tableView.alpha = 1;
        
        [self.m_tableView reloadData];
    } else {
        _m_btnScroll.selected = NO;
        _m_btnTile.selected = YES;

        self.m_StreamView.alpha = 1;
        self.m_tableView.alpha = 0;
        
        [self.m_StreamView reloadData];
    }
}

#pragma mark - FilterView

- (IBAction)showFilterView:(id)sender {
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    overlay.dismissUponTouchMask = NO;
    
    IGFilterController* vc = [[IGFilterController alloc] initWithNibName: @"IGFilterController" bundle: nil];
    vc.delegate = self;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    UIImage *blurredImage = [[self getViewImage: CGRectMake(0, self.view.frame.size.height - 315, 320, 315)] stackBlur: 10];
    vc.m_bgImage = blurredImage;
    
    [overlay presentOverlayOnView: appDelegate.window withContent:vc animated: YES];
}

- (void) setFilterCategory
{
    m_nPage = 0;
    [HUD show: YES];
    [NSThread detachNewThreadSelector: @selector(loadItemData) toTarget: self withObject: nil];
}

- (UIImage*) getViewImage: (CGRect) rect
{
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //
    return [CUtils imageWithImage: image cropByRect: rect];
}

#pragma mark - UITableViewDelegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return m_arrayData ? m_arrayData.count : 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

#define ID_OFFSET_ITEM 1000
- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView = [[[NSBundle mainBundle] loadNibNamed: @"ItemHeaderView" owner: self options: nil] objectAtIndex: 0];
    UIImageView* imgAvatar = (UIImageView*)[headerView viewWithTag: 1];
    UIButton* buttonUser = (UIButton*)[headerView viewWithTag: 2];
    
    IGItem* clsItem = [m_arrayData objectAtIndex: section];
    IGItemUser* clsItemUser = clsItem.m_clsUser;
    NSString* strUrl = clsItemUser.m_strImageThumbUrl;
    NSURL* url = [NSURL URLWithString: strUrl];
    [imgAvatar setImageWithURL: url completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType) {
        if (error) {
            NSLog(@"%@", error);
        }
    }];
    
    [buttonUser setTitle: clsItemUser.m_strUsername forState: UIControlStateNormal];
    
    [buttonUser addTarget: self action: @selector(chooseUser:) forControlEvents: UIControlEventTouchUpInside];
    buttonUser.tag = section + ID_OFFSET_ITEM;
    
    UILabel* labelTime = (UILabel*)[headerView viewWithTag: 3];
    UIImageView* imgClock = (UIImageView*)[headerView viewWithTag: 4];
    [labelTime setText: clsItem.m_strTime];
    [labelTime sizeToFit];
    CGRect rect = labelTime.frame;
    rect.origin.x = 310.0 - rect.size.width;
    labelTime.frame = rect;

    [imgClock setFrame: CGRectMake(rect.origin.x - imgClock.frame.size.width - 3, imgClock.frame.origin.y, imgClock.frame.size.width, imgClock.frame.size.height)];
    if (clsItem.m_strTime == nil || [clsItem.m_strTime isEqualToString: @""]) {
        imgClock.hidden = YES;
    } else {
        imgClock.hidden = NO;
    }
    
    return headerView;
}

- (void) chooseUser: (UIButton*) button
{
    int nRow = (int)button.tag - ID_OFFSET_ITEM;
    IGItem* clsItem = [m_arrayData objectAtIndex: nRow];
    IGItemUser* clsItemUser = clsItem.m_clsUser;
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_clsSimpleUser = clsItemUser;
    [self.navigationController pushViewController: vc animated: YES];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rHeight = [self calcHeight: (int)indexPath.row];
    return rHeight;
}

- (float) calcHeight: (int) nRow
{
    IGItem* clsItem = [m_arrayData objectAtIndex: nRow];
    IGItemUser* clsItemUser = clsItem.m_clsUser;
    
    CGSize size = CGSizeZero;
    if ([clsItem.m_strDescription isEqualToString: @""]) {
        
    } else {
        NSString* strDesc = [NSString stringWithFormat: @"%@ %@", clsItemUser.m_strUsername, clsItem.m_strDescription];
        
        STTweetLabel *tweetLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 160.0)];
        [tweetLabel setCustomFont: [UIFont fontWithName: @"HelveticaNeue-Bold" size: 15.0f]];
        [tweetLabel setText: strDesc];
        tweetLabel.textAlignment = NSTextAlignmentLeft;
        
        size = [tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:tweetLabel.frame.size.width];
    }
    
    int nHeight = size.height + 10;
    
    NSMutableString* strComment = [[NSMutableString alloc] init];
    int nCount = (int)MIN(MAX_COMMENT_COUNT, clsItem.m_arrayComments.count);
    for (int nIdx = (int)clsItem.m_arrayComments.count - 1; (nIdx > -1 && nCount > 0); nIdx --) {
        IGComment* clsComment = clsItem.m_arrayComments[nIdx];
        nCount --;
        
        if ([strComment isEqualToString: @""]) {
            [strComment appendFormat: @"@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
        } else {
            [strComment appendFormat: @"\n@%@ %@", clsComment.m_clsUser.m_strUsername, clsComment.m_strComment];
        }
    }
    
    STTweetLabel *commentLabel = [[STTweetLabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 160.0)];
    [commentLabel setText: strComment];
    commentLabel.textAlignment = NSTextAlignmentLeft;
    
    size = [commentLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:commentLabel.frame.size.width];
    nHeight += size.height + 15;
    
    return nHeight + 292;
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] init];
    return view;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: CELL_ID_FEED];
    
    IGItem* clsItem = [m_arrayData objectAtIndex: indexPath.section];
    
    FeedCellView* feedCell = (FeedCellView*)cell;
    feedCell.delegate = self;
    [feedCell configureCell: clsItem];
    
    feedCell.tag = indexPath.section + ID_OFFSET_ITEM;
    
    return feedCell;
}

#pragma mark - FeedCell delegate methods

- (void) refreshCells: (FeedCellView*) cell
{
    int nSection = (int)cell.tag - ID_OFFSET_ITEM;
    NSIndexPath* path = [NSIndexPath indexPathForRow: 0 inSection: nSection];
    [self.m_tableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: path] withRowAnimation: UITableViewRowAnimationAutomatic];
}

- (void) showAllComments: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_READ;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //[self.navigationController pushViewController: vc animated: YES];
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) showUser: (NSString*) strUsername
{
    NSString* strRealUsername;
    if ([[strUsername substringToIndex: 1] isEqualToString: @"@"]) {
        strRealUsername = [[strUsername substringFromIndex: 1] copy];
    } else {
        strRealUsername = [strUsername copy];
    }
    
    IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    vc.m_strUsername = strUsername;
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) showTag: (NSString*) strTag
{
    NSString* strHashTag = strTag;
    if ([[strTag substringToIndex: 1] isEqualToString: @"#"]) {
        strHashTag = [strTag substringFromIndex: 1];
    }
    IGTagViewController* vc = [[IGTagViewController alloc] initWithNibName: @"IGTagViewController" bundle: nil];
    vc.m_strTag = strHashTag;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - action of item

- (void) actionLike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];

    [NSThread detachNewThreadSelector: @selector(likeProcess:) toTarget: self withObject: clsItem];
//    [self likeProcess: clsItem];
}

- (void) showLikers: (IGItem*) clsItem
{
    IGLikersViewController* vc = [[IGLikersViewController alloc] initWithNibName: @"IGLikersViewController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) likeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: YES];
    if (!FSuccess) {
    }
}

- (void) actionUnlike: (IGItem*) clsItem
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"%d", clsItem.m_nIndex], NOTI_ITEM_ID, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: ID_NOTI_LIKED_ITEM object: dict];
    
    [NSThread detachNewThreadSelector: @selector(unlikeProcess:) toTarget: self withObject: clsItem];
//    [self unlikeProcess: clsItem];
}

- (void) unlikeProcess: (IGItem*) clsItem
{
    BOOL FSuccess = [[DataKeeper sharedInstance] postLike: clsItem.m_nIndex Mode: NO];
    if (!FSuccess) {
    }
}

- (void) actionComment: (IGItem*) clsItem
{
    IGCommentsViewController* vc = [[IGCommentsViewController alloc] initWithNibName: @"IGCommentsViewController" bundle: nil];
    vc.m_eViewKind = MODE_WRITE;
    vc.m_nItemIndex = clsItem.m_nIndex;
    vc.m_clsItem = clsItem;
    //    [self.navigationController pushViewController: vc animated: YES];
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.m_clsNavController pushViewController: vc animated: YES];
}

- (void) actionPurchase: (IGItem*) clsItem
{
    UIXOverlayController* overlay = [[UIXOverlayController alloc] init];
    overlay.dismissUponTouchMask = NO;
    
    IGPurchaseController* vc = [[IGPurchaseController alloc] initWithNibName: @"IGPurchaseController" bundle: nil];
    vc.m_clsItem = clsItem;
    
    IGAppDelegate* appDelegate = (IGAppDelegate*)[UIApplication sharedApplication].delegate;
    
    [overlay presentOverlayOnView: appDelegate.window withContent:vc animated: YES];
}

- (void) actionShare: (NSDictionary*) dict
{
    m_itemImage = [dict objectForKey: @"image"];
    m_clsItem = [dict objectForKey: @"item"];
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle: @"" delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Share on Facebook", @"Share on Mail", @"Share on Instagram", @"Share on Twitter", nil];
    [sheet showInView: self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self actionFbShare];
            break;
        case 1:
            [self actionMailShare];
            break;
        case 2:
            [self actionIGShare];
            break;
        case 3:
            [self actionTwShare];
            break;
        default:
            break;
    }
}

- (IBAction)actionFbShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

- (IBAction)actionMailShare {
    if (![MFMailComposeViewController canSendMail]) {
        ALERT_SHOW(@"Please setup your mail account in Settings of your device");
        return;
    }
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    
    mail.mailComposeDelegate = self;
    
    //    [mail setToRecipients:[NSArray arrayWithObject:@"info@thegrint.com"]];
    [mail setSubject: m_clsItem.m_strName];
    
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [mail setMessageBody: strText isHTML:NO];
    
    [mail addAttachmentData: UIImageJPEGRepresentation([self imageForShare: m_itemImage], 1.0) mimeType: @"image/jpg" fileName: m_clsItem.m_strName];
    [self presentViewController: mail animated: YES completion: nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated: YES completion: nil];
    
    if(result == MFMailComposeResultSent){
        ALERT_SHOW(@"Mail has been sent");
    }
}

- (IBAction)actionIGShare {
    UIImage* image;
    image = [self imageForShare: m_itemImage];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];
        NSData *imageData=UIImagePNGRepresentation(image);
        [imageData writeToFile:saveImagePath atomically:YES];
        
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        
        if (docFile == Nil) {
            docFile = [[UIDocumentInteractionController alloc] init];
        }
        docFile.delegate=self;
        docFile.UTI=@"com.instagram.photo";
        
        NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
        docFile.annotation=[NSDictionary dictionaryWithObjectsAndKeys: strText, @"InstagramCaption", nil];
        
        [docFile setURL:imageURL];
        
        [docFile presentOpenInMenuFromRect: CGRectZero inView: self.view animated: YES];
    }
    else
    {
        NSLog (@"Instagram is not available");
    }
}

- (IBAction)actionTwShare {
    SLComposeViewController* vc = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeTwitter];
    NSString* strText = [NSString stringWithFormat: @"%@%@", SHARE_DEFAULT_TEXT, [[DataKeeper sharedInstance] getDownloadLink]];
    [vc setInitialText: strText];
    [vc addImage: [self imageForShare: m_itemImage]];
    vc.completionHandler = ^(SLComposeViewControllerResult result)  {
        [self dismissViewControllerAnimated: YES completion:nil];
    };
    [self presentViewController: vc animated: YES completion: nil];
}

#define SHARE_WIDTH 200
#define SHARE_HEIGHT 200
- (UIImage*) imageForShare : (UIImage*) orgImage
{
    UIImageView* imgView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, SHARE_WIDTH, SHARE_HEIGHT)];
    imgView.backgroundColor = [UIColor whiteColor];
    imgView.opaque = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView setImage: orgImage];
    return [CUtils imageWithView: imgView];
}


@end
