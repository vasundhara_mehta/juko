//
//  IGFilterController.h
//  Juko
//
//  Created by Mountain on 3/11/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIXOverlayController.h"

@interface IGFilterController : UIXOverlayContentViewController<UITableViewDataSource, UITableViewDelegate>

//import
@property (nonatomic, retain) UIImage* m_bgImage;
@property id delegate;

//members

@property (weak, nonatomic) IBOutlet UIImageView *m_ImageBackground;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

//actions

- (IBAction)dismiss:(id)sender;

@end
