//
//  IGTagViewController.h
//  Juko
//
//  Created by Mountain on 2/5/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamView.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface IGTagViewController : UIViewController<StreamViewDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) NSString* m_strTag;
@property (weak, nonatomic) IBOutlet StreamView *m_StreamView;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet UIButton *m_btnTile;
@property (weak, nonatomic) IBOutlet UIButton *m_btnScroll;
@property (weak, nonatomic) IBOutlet UILabel *m_Label_Title;

- (IBAction)changeViewMode:(id)sender;
- (IBAction)showFilterView:(id)sender;

@end
