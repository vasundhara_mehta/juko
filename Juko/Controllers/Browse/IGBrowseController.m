//
//  IGBrowseController.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGBrowseController.h"

//model
#import "IGItem.h"
#import "const.h"
#import "DataKeeper.h"
#include "CUtils.h"

//view
#import "ItemCell.h"

//other controllers
#import "IGItemViewController.h"
#import "IGProfileController.h"
#import "IGTagViewController.h"

//custom controls
#import "IGProgressActivity.h"

@interface IGBrowseController ()

@end

@implementation IGBrowseController
{
    IGProgressActivity* HUD;
    NSArray* m_arrayData;
    
    NSArray* m_arraySearchResults;
    UIRefreshControl* m_RefreshCtrl;
    
    BOOL m_FLoading;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        self.tabBarItem = [[UITabBarItem alloc] initWithTitle: @"Browse" image: nil tag: 0];
        [self.tabBarItem setFinishedSelectedImage: [UIImage imageNamed: @"tabitem2_h"] withFinishedUnselectedImage: [UIImage imageNamed:@"tabitem2"]];
        [[self tabBarItem] setImageInsets: UIEdgeInsetsMake(5, 0, -5, 0)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.hidden = YES;
    
    HUD = [[IGProgressActivity alloc] initWithView: self.view];
    [self.view addSubview: HUD];
    
    _m_BrowseView.scrollsToTop = YES;
    
    _m_BrowseView.cellPadding = 4.0f;
    _m_BrowseView.columnPadding = 4.0f;
    
    _m_BrowseView.delegate = self;
    
    [_m_BrowseView setContentInset: UIEdgeInsetsMake(0, 0, 50, 0)];
    
    self.m_tableView.alpha = 0;
    [self.m_tableView registerNib: [UINib nibWithNibName: @"NameCell" bundle: nil] forCellReuseIdentifier: @"NameCell"];
    [self.m_tableView registerNib: [UINib nibWithNibName: @"TagCell" bundle: nil] forCellReuseIdentifier: @"TagCell"];
    
    
    _m_strCellID = @"NameCell";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self showActivity];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
    
    m_RefreshCtrl = [[UIRefreshControl alloc] init];
    [m_RefreshCtrl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    [self.m_BrowseView addSubview: m_RefreshCtrl];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    m_arrayData = [[DataKeeper sharedInstance] getFeaturedData];
}

- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    //    [self showActivity];
    [NSThread detachNewThreadSelector: @selector(loadData) toTarget: self withObject: nil];
}

- (void) showActivity
{
    [HUD show: YES];
}

- (void) loadData
{
    m_FLoading = YES;
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    [clsModel loadFeaturedData: 0];
    
    [HUD hide: YES];
    
    m_FLoading = NO;
    
    [self performSelectorOnMainThread: @selector(refreshData) withObject: nil waitUntilDone: YES];
}

- (void) refreshData
{
    m_arrayData = [[DataKeeper sharedInstance] getFeaturedData];

    [m_RefreshCtrl endRefreshing];
    [self.m_BrowseView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - StreamView Delegate

- (NSInteger)numberOfCellsInStreamView:(StreamView *)streamView
{
    NSInteger nCount = (m_arrayData == nil) ? 0 : m_arrayData.count;
    
    return nCount;
}

- (NSInteger)numberOfColumnsInStreamView:(StreamView *)streamView
{
    return 2;
}

- (UIView *)streamView:(StreamView *)stream cellAtIndex:(NSInteger)index
{
    ItemCell *cell;
    static NSString *CellID1 = @"MyCell1";
    static NSString *CellID2 = @"MyCell2";
    
    BOOL redCell = index % 2 == 0;
    NSString *CellID =  redCell ? CellID2 : CellID1;

    cell = (ItemCell *)[stream dequeueReusableCellWithIdentifier:CellID];
    
    if (cell == nil) {
        cell = [[ItemCell alloc] initWithFrame:CGRectMake(0, 0, 154, 154)];
        cell.reuseIdentifier = CellID;
    }

    if (m_FLoading) {
        return cell;
    }
    
    cell.delegate = self;
    cell.tag = index + 1000;
    
    [cell configureCell: m_arrayData[index]];
    
    return cell;
}

- (CGFloat)streamView:(StreamView *)streamView heightForCellAtIndex:(NSInteger)index
{
    return 154;
}

- (UIView *)headerForStreamView:(StreamView *)streamView
{
    return nil;
    /*
     CSearchCell *header = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     header.label.text = @"This is the header";
     
     return header;
     */
}

- (UIView *)footerForStreamView:(StreamView *)streamView
{
    return nil;
    
    /*
     if (page <= MaxPage) {
     CSearchCell *footer = [[CSearchCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - stream.columnPadding * 2, 60)];
     footer.label.text = @"This is the footer";
     
     return footer;
     } else {
     return nil;
     }
     */
}

#pragma mark - cell click

- (void) touchedCell: (id) sender
{
    IGItem* clsItem = (IGItem*)sender;
    //int nIndex = ((UIView*)sender).superview.tag - 1000;
    //IGItem* clsItem =[dataSource.m_products objectAtIndex: nIndex];
    IGItemViewController* vc = [[IGItemViewController alloc] initWithNibName: @"IGItemViewController" bundle: nil];
    vc.m_clsFeaturedItem = clsItem;
    [self.navigationController pushViewController: vc animated: YES];
}

#pragma mark - UISearchBarDelegate

- (BOOL) searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString* strCellName;
    NSString* strText = [NSString stringWithFormat: @"%@%@", searchBar.text, text];
    if (strText && ![strText isEqualToString: @""]) {
        if ([[strText substringToIndex:1] isEqualToString: @"#"])
            strCellName = @"TagCell";
        else
            strCellName = @"NameCell";
        
        _m_strCellID = strCellName;
    }
    return YES;
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (self.m_tableView.alpha == 0) {
        self.m_tableView.alpha = 1; //some animation?
    }
    // require to get the data source
    [self searchProcess: searchText];
    [self.m_tableView reloadData];
}

- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    self.m_BrowseView.hidden = YES;
    searchBar.showsCancelButton = YES;
    return YES;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;
    self.m_tableView.alpha = 0;
    searchBar.text = @"";
    
    self.m_BrowseView.hidden = NO;
}

#pragma mark - search username or tag

- (void) searchProcess: (NSString*) strKey
{
    if (![_m_strCellID isEqualToString: @"NameCell"]) { //username search
        if ([strKey isEqualToString: @""] || [strKey isEqualToString: @"#"]) {
            return;
        }
        NSString* strTag;
        if ([[strKey substringToIndex: 1] isEqualToString: @"#"]) {
            strTag = [[strKey substringFromIndex: 1] copy];
        } else {
            strTag = strKey;
        }
        m_arraySearchResults = [[DataKeeper sharedInstance] searchHashtags: strTag];
    } else { //hashtag search
        m_arraySearchResults = [[DataKeeper sharedInstance] searchUsernames: strKey];
    }
    
    [self performSelectorOnMainThread: @selector(refreshSearchResult) withObject: Nil waitUntilDone: YES];
}

- (void) refreshSearchResult
{
    [self.m_tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (m_arraySearchResults) ? m_arraySearchResults.count : 0;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: _m_strCellID];
    
    if ([_m_strCellID isEqualToString: @"NameCell"]) {
        NSDictionary* dictUsername = m_arraySearchResults[indexPath.row];
        UILabel* labelName = (UILabel*)[cell viewWithTag: 1];
        [labelName setText: [CUtils getStringValueFromDictionary: dictUsername KEY: @"username"]];
    } else {
        NSDictionary* dictHashtag = m_arraySearchResults[indexPath.row];
        UILabel* labelName = (UILabel*)[cell viewWithTag: 1];
        [labelName setText: [CUtils getStringValueFromDictionary: dictHashtag KEY: @"name"]];
        
        UILabel* labelCount = (UILabel*)[cell viewWithTag: 2];
        [labelCount setText: [NSString stringWithFormat: @"%d items", [CUtils getIntValueFromDictionary: dictHashtag KEY: @"itemCount"]]];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UIView*) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    [self.m_searchField resignFirstResponder];
    
    if ([_m_strCellID isEqualToString: @"NameCell"]) {
        NSDictionary* dictUsername = m_arraySearchResults[indexPath.row];
        IGProfileController* vc = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
        vc.m_strUsername = [CUtils getStringValueFromDictionary: dictUsername KEY: @"username"];
        [self.navigationController pushViewController: vc animated: YES];
    } else { //tag cell
         NSDictionary* dictHashtag = m_arraySearchResults[indexPath.row];
        IGTagViewController* vc = [[IGTagViewController alloc] initWithNibName: @"IGTagViewController" bundle: nil];
        vc.m_strTag = [CUtils getStringValueFromDictionary: dictHashtag KEY: @"name"];
        [self.navigationController pushViewController: vc animated: YES];
    }
}

#pragma mark - KEYBOARD show/hide DELEGATE

-(void) keyboardWillShow: (id) sender
{
    CGRect rect = self.m_tableView.frame;
    rect.size.height = self.view.frame.size.height - KEYBOARD_HEIGHT - rect.origin.y + 25;
    self.m_tableView.frame = rect;
}

-(void) keyboardWillHide: (id) sender
{
    CGRect rect = self.m_tableView.frame;
    rect.size.height = self.view.frame.size.height - rect.origin.y;
    self.m_tableView.frame = rect;
}

@end
