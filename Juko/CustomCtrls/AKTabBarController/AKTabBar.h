

#import "AKTab.h"

// The various positions for the AKTabBar within the AKTabBarView.
// The default position is AKTabBarPositionBottom.
typedef NS_ENUM(NSInteger, AKTabBarPosition) {
    AKTabBarPositionBottom = 0,
    AKTabBarPositionTop,
};

@class AKTabBar;

@protocol AKTabBarDelegate <NSObject>

@required

// Used by the TabBarController to be notified when a tab is pressed
- (void)tabBar:(AKTabBar *)AKTabBarDelegate didSelectTabAtIndex:(NSInteger)index;

@end

@interface AKTabBar : UIView

- (id)initWithFrame:(CGRect)frame
        fixedHeight:(BOOL)hasFixedHeight
           position:(AKTabBarPosition)position;

@property (nonatomic, strong) NSArray *tabs;
@property (nonatomic, strong) AKTab *selectedTab;
@property (nonatomic, assign) id <AKTabBarDelegate> delegate;

// Tab top embos Color
@property (nonatomic, strong) UIColor *edgeColor;

// Top embos Color. optional, default to edgeColor
@property (nonatomic, strong) UIColor *topEdgeColor;

// Tabs selected colors.
@property (nonatomic, strong) NSArray *tabColors;

// Tab background image
//@property (nonatomic, strong) NSString *backgroundImageName;

//
@property (nonatomic, assign) CGFloat tabWidth;

- (void)tabSelected:(AKTab *)sender;

@end
