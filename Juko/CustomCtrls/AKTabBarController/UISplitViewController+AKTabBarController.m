

#import "UISplitViewController+AKTabBarController.h"
#import "UIViewController+AKTabBarController.h"

@class UIViewController;

@implementation UISplitViewController (AKTabBarController)

- (UIViewController *)ak_masterViewController {
	return [[self viewControllers] objectAtIndex:0];
}

- (NSString *)tabImageName 
{
	return [[self ak_masterViewController] tabImageName];
}

- (NSString *)activeTabImageName;
{
	return [[self ak_masterViewController] activeTabImageName];
}

- (NSString *)tabTitle;
{
	return [[self ak_masterViewController] tabTitle];
}

@end
