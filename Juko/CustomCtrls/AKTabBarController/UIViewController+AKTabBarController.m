

#import "UIViewController+AKTabBarController.h"

#import "AKTabBarController.h"

@implementation UIViewController (AKTabBarController)

- (NSString *)tabImageName
{
	return nil;
}

- (NSString *)activeTabImageName
{
  return nil;
}

- (NSString *)tabBackgroundImageName
{
    return nil;
}

- (NSString *)tabTitle
{
	return nil;
}

- (AKTabBarController *)akTabBarController
{
    UIViewController *parent = self.parentViewController;
    Class cls = [AKTabBarController class];
    while(parent) {
        if([parent isKindOfClass:cls]) {
            return (AKTabBarController*) parent;
        }
        parent = parent.parentViewController;
    }

    return nil;
}

@end
