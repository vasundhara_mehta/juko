
#import "AKTab.h"

// cross fade animation duration.
static const float kAnimationDuration = 0.15;

// Padding of the content
static const float kPadding = 0.0;

// Margin between the image and the title
static const float kMargin = 0.0;

// Margin at the top
static const float kTopMargin = 0.0;

@interface AKTab ()

// Permits the cross fade animation between the two images, duration in seconds.
- (void)animateContentWithDuration:(CFTimeInterval)duration;

@end

@implementation AKTab
{
    BOOL isTabIconPresent;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self) {
        self.contentMode = UIViewContentModeScaleAspectFit;
        self.backgroundColor = [UIColor clearColor];
        _titleIsHidden = NO;
        isTabIconPresent = NO;
    }
    return self;
}

#pragma mark - Touche handeling

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self animateContentWithDuration:kAnimationDuration];
}

#pragma mark - Animation

- (void)animateContentWithDuration:(CFTimeInterval)duration
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"contents"];
    animation.duration = duration;
    [self.layer addAnimation:animation forKey:@"contents"];
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (void)drawBackground:(CGContextRef)ctx
                inRect:(CGRect)rect {
    if (!self.selected) {
        if (_tabImageWithName) {
            UIImage *backgroundImage = [UIImage imageNamed:_tabImageWithName];
            if(!UIEdgeInsetsEqualToEdgeInsets(self.backgroundImageCapInsets, UIEdgeInsetsZero)) {
                [[backgroundImage resizableImageWithCapInsets:self.backgroundImageCapInsets] drawInRect:rect];
            } else {
                self.backgroundColor = [UIColor clearColor];
                [[UIColor colorWithPatternImage:backgroundImage] set];
                CGContextFillRect(ctx, rect);
            }
        }
        else {
            [[UIColor colorWithPatternImage:[UIImage imageNamed:@"AKTabBarController.bundle/noise-pattern"]] set];
            CGContextFillRect(ctx, rect);
        }
    } else {
        if (_activeImageWithName) {
            UIImage *backgroundImage = [UIImage imageNamed:_activeImageWithName];
            if(!UIEdgeInsetsEqualToEdgeInsets(self.backgroundImageCapInsets, UIEdgeInsetsZero)) {
                [[backgroundImage resizableImageWithCapInsets:self.backgroundImageCapInsets] drawInRect:rect];
            } else {
                self.backgroundColor = [UIColor clearColor];
                [[UIColor colorWithPatternImage:backgroundImage] set];
                CGContextFillRect(ctx, rect);
            }
        }
        else {
            [[UIColor colorWithPatternImage:[UIImage imageNamed:@"AKTabBarController.bundle/noise-pattern"]] set];
            CGContextFillRect(ctx, rect);
        }
    }
}

- (void)drawRect:(CGRect)rect
{
    
}
@end