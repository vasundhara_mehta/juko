
#import "AKTabBarView.h"

@implementation AKTabBarView

#pragma mark - Setters

- (void)setTabBar:(AKTabBar *)tabBar
{
    if (_tabBar != tabBar)
    {
        [_tabBar removeFromSuperview];
        _tabBar = tabBar;
        [self addSubview:tabBar];
    }
}


- (void)setContentView:(UIView *)contentView
{
    if (_contentView != contentView)
    {
        [_contentView removeFromSuperview];
        _contentView = contentView;
        _contentView.frame = CGRectMake(0, 0, self.bounds.size.width, self.tabBar.frame.origin.y);
        [self addSubview:_contentView];
        [self sendSubviewToBack:_contentView];
        [_contentView setNeedsDisplay];
        [self setNeedsLayout];
    }
}


- (void)setTabBarPosition:(AKTabBarPosition)tabBarPosition
{
    _tabBarPosition = tabBarPosition;
    [self setNeedsLayout];
}

#pragma mark - Layout & Drawing

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect rect = (CGRect) {
        .origin.x = _tabBar.frame.origin.x,
        .origin.y = (self.tabBarPosition == AKTabBarPositionTop) ? 0.0
                                                                 : CGRectGetHeight(self.bounds) - CGRectGetHeight(_tabBar.bounds),
        .size = _tabBar.frame.size
    };
    
    _tabBar.frame = rect;

    rect = (CGRect) {
        .origin.x = 0,
        .origin.y = (self.tabBarPosition == AKTabBarPositionTop) ? CGRectGetMaxY(_tabBar.frame) : 0,
        .size.width = CGRectGetWidth(self.bounds),
        .size.height = CGRectGetHeight(self.bounds) - ((!_isTabBarHidding) ? CGRectGetHeight(_tabBar.bounds) : 0)
    };
    
    _contentView.frame = rect;
    [_contentView setNeedsLayout];
}




@end
