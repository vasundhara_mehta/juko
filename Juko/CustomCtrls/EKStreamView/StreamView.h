
#import <UIKit/UIKit.h>

@protocol EKResusableCell <NSObject>

@property (nonatomic, retain) NSString *reuseIdentifier;

@end



@interface StreamViewCellInfo : NSObject 

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) NSUInteger index;

// You SHOULD ONLY access this property when this object is in visibleCellInfo!
@property (nonatomic, weak) UIView<EKResusableCell> *cell;

@end



@class StreamView;


@protocol StreamViewDelegate <UIScrollViewDelegate>

- (NSInteger)numberOfCellsInStreamView:(StreamView *)streamView;
- (NSInteger)numberOfColumnsInStreamView:(StreamView *)streamView;
- (UIView<EKResusableCell> *)streamView:(StreamView *)streamView cellAtIndex:(NSInteger)index;
- (CGFloat)streamView:(StreamView *)streamView heightForCellAtIndex:(NSInteger)index;

@optional

- (UIView *)headerForStreamView:(StreamView *)streamView;
- (UIView *)footerForStreamView:(StreamView *)streamView;
- (void)streamView:(StreamView *)streamView willDisplayCell:(UIView<EKResusableCell> *)cell forIndex:(NSInteger)index;
@optional

@end


@interface StreamViewUIScrollViewDelegate : NSObject<UIScrollViewDelegate>
@property (nonatomic,weak) StreamView *streamView;

@end

typedef enum {
    StreamViewScrollPositionNone,
    StreamViewScrollPositionTop,
    StreamViewScrollPositionMiddle,
    StreamViewScrollPositionBottom
} StreamViewScrollPosition;

@interface StreamView : UIScrollView
{
    NSMutableArray
    *cellHeightsByIndex,    // 1d
    *cellHeightsByColumn,   // 2d
    *rectsForCells,         // 2d StreamViewCellInfo
    *infoForCells;          // 1d
    
    NSMutableDictionary *cellCache; // reuseIdentifier => NSMutableArray
    NSSet *visibleCellInfo;
    CGFloat columnWidth;
    StreamViewUIScrollViewDelegate *delegateObj;
}

@property (nonatomic, weak) id<StreamViewDelegate> delegate;
@property (nonatomic, assign) CGFloat columnPadding;
@property (nonatomic, assign) CGFloat cellPadding;

@property (nonatomic, readonly) CGFloat columnWidth;

@property (nonatomic, readonly) UIView *headerView, *footerView;
@property (nonatomic, readonly) UIView *contentView;

- (id<EKResusableCell>)dequeueReusableCellWithIdentifier:(NSString *)identifier;
- (void)reloadData;
- (void)scrollToCellAtIndex:(NSUInteger)index atScrollPosition:(StreamViewScrollPosition)scrollPosition animated:(BOOL)animated;
@end
