//
//  IGAppDelegate.m
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import "IGAppDelegate.h"
#import "IGLoginViewController.h"

#import "IGNewsFeedController.h"
#import "IGBrowseController.h"
#import "IGCreateItemController.h"
#import "IGProfileController.h"

#import "AKTabBarController.h"
#import "DataKeeper.h"
#import "IGUser.h"

@implementation IGAppDelegate

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    NSString* strURL = [url absoluteString];
    NSLog(@"url path: %@", strURL);
    
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:self.session fallbackHandler:^(FBAppCall *call) {
                            NSLog(@"In fallback handler");
                        }];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.splashController = [[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:nil];
    _m_clsNavController = [[UINavigationController alloc] initWithRootViewController: self.splashController];
    self.window.rootViewController = _m_clsNavController;
    [self.window makeKeyAndVisible];
    
    [UIMenuController sharedMenuController].menuVisible = NO;
    
    return YES;
}

- (void) createMainFrame
{
//    _m_clsMainFrame = [[AKTabBarController alloc] initWithTabBarHeight: 49];
    
    IGNewsFeedController* feedVC = [[IGNewsFeedController alloc] initWithNibName: @"IGNewsFeedController" bundle: nil];
    IGBrowseController* browseVC = [[IGBrowseController alloc] initWithNibName: @"IGBrowseController" bundle: nil];
    IGCreateItemController* itemVC = [[IGCreateItemController alloc] initWithNibName: @"IGCreateItemController" bundle: nil];
    IGProfileController* profileVC = [[IGProfileController alloc] initWithNibName: @"IGProfileController" bundle: nil];
    profileVC.m_nViewKind = 1;
    
    DataKeeper* clsModel = [DataKeeper sharedInstance];
    profileVC.m_clsUser = [clsModel getUserInfo];
    
    NSArray* arrayTabs = @[[[UINavigationController alloc] initWithRootViewController: feedVC]
                           , [[UINavigationController alloc] initWithRootViewController: browseVC]
                           , [[UINavigationController alloc] initWithRootViewController: itemVC]
                           , [[UINavigationController alloc] initWithRootViewController: profileVC]];
    
    _m_clsMainFrame = [[UITabBarController alloc] init];
    
    [_m_clsMainFrame setViewControllers: arrayTabs];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 6.5) {
        [_m_clsMainFrame.tabBar setTranslucent: YES];
    }
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor clearColor]];
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setShadowImage: [[UIImage alloc] init]];
//    _m_clsMainFrame.automaticallyAdjustsScrollViewInsets = NO;
    
//    _m_clsNavController = [[UINavigationController alloc] initWithRootViewController: _m_clsMainFrame];
//    [_m_clsNavController setNavigationBarHidden: YES];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Get UIDeviceToken

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APN device token: %@", deviceToken);
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *deviceTokenString = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                                   ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                                   ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                                   ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    [[DataKeeper sharedInstance] saveDeviceToken: deviceTokenString];
}

#pragma mark - push notification
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"received notification");
    NSLog(@"------\n push data : %@ --------\n", userInfo);
    
    //    {
    //        aps =     {
    //            alert = "{\"action\":\"message\",\"user_id\":\"5\",\"user_name\":\"Jin Zhang\",\"like_id\":\"27\",\"message\":\"hello, ppppppp\",\"time\":\"2013-12-14 12:58:52\"}";
    //            sound = default;
    //        };
    //    }
    
    NSDictionary *payload = [userInfo objectForKey:@"aps"];
    
    NSDictionary* alert = [payload objectForKey:@"alert"];
    NSString* action = [alert objectForKey: @"action"];
//    if ([action isEqualToString: @"message"]) { //message push
//        if ([mainFrame.centerController isKindOfClass: [MessageViewController class]]) {
//            MessageViewController* vc = (MessageViewController*)mainFrame.centerController;
//            CTrade* pTrade = vc.m_pTrade;
//            
//            int nLikeId = [[alert objectForKey: @"like_id"] integerValue];
//            if (nLikeId == pTrade.m_nLikeId) {
//                [vc showNotification: alert];
//            }
//        }
//    } else if ([action isEqualToString: @"request_accepted"]) {
//        [self.rightController actionRefresh: nil];
//    }
    
    
}


@end
