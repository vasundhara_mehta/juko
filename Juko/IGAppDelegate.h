//
//  IGAppDelegate.h
//  Juko
//
//  Created by Mountain on 1/28/14.
//  Copyright (c) 2014 SuWu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@class IGLoginViewController;
//@class AKTabBarController;

@interface IGAppDelegate : UIResponder <UIApplicationDelegate>

//member variables
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IGLoginViewController *splashController;

//@property (strong, nonatomic) AKTabBarController* m_clsMainFrame;
@property (strong, nonatomic) UITabBarController* m_clsMainFrame;
@property (strong, nonatomic) UINavigationController* m_clsNavController;

//member methods

- (void) createMainFrame;

//facebook variable
#pragma mark - facebook
@property (strong, nonatomic) FBSession *session;


@end
